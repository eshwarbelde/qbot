var filterOptionsCongig = {};
var total_tickets = 0;
$(document).ready(function () {
  getTicketMetrics();
  // screenResolutionMetrics();
  drawBasicColumnChart2()
  // drawBarChart1()
  getAgentMetrics()
});

function getTicketMetrics(){
  $.ajax({
    type: 'get',
    url: '/getTicketMetrics',
    data: {},      
    success: function (data) {
      console.log(data)
      window.total_tickets = parseInt(data.total_tickets)
      $("#totalTickets").html(data.total_tickets);
      $("#avg_resolution_time").html(parseFloat(data.avg_resolution_time).toFixed(1));
      $("#pwc_count").html(data.pwc_count);
      $("#pwc_time").html(parseFloat(data.pwc_time).toFixed(1));
      getProductAndCustomerMetrics()
      overallTicketRatingMetrics()
    }, error: function (error) {
      console.log(error);
    }
  });
}

function getAgentMetrics(){
  $.ajax({
    type: 'get',
    url: '/agentMetrics',
    data: {},      
    success: function (data) {
      preapareAgentPerformaceTable(data)

    }, error: function (error) {
      console.log(error);
    }
  });
}

function getProductAndCustomerMetrics(){
  console.log(window.total_tickets)
  $.ajax({
    type: 'get',
    url: '/getProductAndCustomerMetrics',
    data: {},      
    success: function (data) {
      console.log(data)
      let top_five_products = data.top_five_products,
      top_five_products_avg = {data: [], categories: []}
      top_five_products.sort((a,b) => b.count - a.count); 
      for( let i in top_five_products){
        result = parseFloat(((parseInt(top_five_products[i].count) / window.total_tickets)* 100).toFixed(2))
        top_five_products_avg.categories.push(top_five_products[i].product.substring(0, 10))
        top_five_products_avg.data.push(result)
      }

      /* let top_five_products = data.top_five_products,
      top_five_products_avg = []
      for( let i in top_five_products){
        result = parseFloat(((parseInt(top_five_products[i].count) / window.total_tickets)* 100).toFixed(2))
        top_five_products_avg.push({name:top_five_products[i].product, data: [result]})
      }
      top_five_products_avg.sort((a,b) => (a.data[0] > b.data[0]) ? 1 : ((b.data[0] > a.data[0]) ? -1 : 0));  */

      // drawBarChart0(top_five_products_avg)
      drawBasicColumnChart1(top_five_products_avg)
      prepareCustomerTable(data)
    }, error: function (error) {
      console.log(error);
    }
  });
}
function overallTicketRatingMetrics(){
  console.log(window.total_tickets)
  $.ajax({
    type: 'get',
    url: '/overallTicketRatingMetrics',
    data: {},      
    success: function (data) {
      console.log(data)
      drawPIEChart(data)
      $("#total_initial").html(data.total_initial)
      let width = parseFloat((data.perfect_initial * 100/ data.total_initial).toFixed(0))
      console.log(width)
      $("#perfect_initial_percentage").html(width+" %");
      $("#perfect_initial_percentage_div").css('width', width+"%");

      $("#total_subsequent").html(data.total_subsequent)
      subsequent_width = parseFloat((data.perfect_subsequent * 100/ data.total_subsequent).toFixed(0))
      $("#perfect_subsequent_percentage").html(subsequent_width+" %");
      $("#perfect_subsequent_percentage_div").css('width', subsequent_width+"%");

      $('#total_rfo').text($('#totalTickets').text())
      $("#perfect_rfo_percentage").html("70 %");
      $("#perfect_rfo_percentage_div").css('width', "70%");
    }, error: function (error) {
      console.log(error);
    }
  });
}
function screenResolutionMetrics() {
  console.log(window.screen)
  let deviceWidth = window.screen.width, deviceHeight = window.screen.height
  if (deviceWidth < 600) {
    $('.sidebar-toggle').show()
    $('.box-tools').css('position', 'unset')
    $('.box-title').css('padding-left', '10px')
  } else if (600 < deviceWidth && deviceWidth < 960) {
    if (900 < deviceHeight) {
      $('.logo').show()
    } else {
      $('.sidebar-toggle').show()
    }
  } else if (960 < deviceWidth && deviceWidth < 1264) {
    $('.logo').show()
  } else if (1264 < deviceWidth) {
    $('.logo').show()
  }
};
function prepareCustomerTable(data){
  customers = data.customers
  perfect_customers = data.perfect_customers
  perfect_customers_avg = []
  for(let i in perfect_customers){
    obj = customers.find(rec => rec.customer_name == perfect_customers[i].customer_name);
    if(obj != undefined){
      
      avg = parseFloat(((perfect_customers[i].count / obj.count)*100).toFixed(0))
      sort_key = obj.count*avg
      perfect_customers_avg.push({customer_name: perfect_customers[i].customer_name, total_tickets: obj.count, perfect_percentage:avg, sort_key: sort_key})
    }
  }
  perfect_customers_avg.sort((a,b) => (a.sort_key < b.sort_key) ? 1 : ((b.sort_key < a.sort_key) ? -1 : 0)); 
  console.log(perfect_customers_avg)
  top_five_customers = perfect_customers_avg.slice(0,5)
  // top_five_customers.sort((a,b) => (a.perfect_percentage < b.perfect_percentage) ? 1 : ((b.perfect_percentage < a.perfect_percentage) ? -1 : 0)); 
  console.log(top_five_customers)
  for(let i in top_five_customers){
    $("#customer_"+i).html(top_five_customers[i].customer_name)
    $("#customer_tickets_"+i).html(top_five_customers[i].total_tickets)
    $("#customer_percentage_"+i).css('width', top_five_customers[i].perfect_percentage+"%")
    $("#customer_percentage_value_"+i).html(top_five_customers[i].perfect_percentage)
  }
}

function preapareAgentPerformaceTable(data){
  console.log(data)
  agent_records = []
  for(let i in data){
    values = data[i].values
    total_responses = values['total_initial'] + values['total_subsequent']
    avg = parseFloat((((values['perfect_initial'] + values['perfect_subsequent']) / total_responses) * 100).toFixed(0))
    agent_records[i] = {
      'agent_name': data[i].agent_name,
      'total_responses': total_responses,
      'perfect_responses_percentage': avg,
      'sort_key': total_responses * avg
    }
  }
  console.log(agent_records)
  let agets_rows = agent_records
  agent_records.sort((a,b) => (a.sort_key < b.sort_key) ? 1 : ((b.sort_key < a.sort_key) ? -1 : 0)); 

  top_five_agents = agent_records.slice(0,5)
  threshold_responses = ((window.total_tickets /100) * 0.25) .toFixed(0) 
  console.log(agent_records)
  let agents_threshold = agent_records.filter(function (e) {
    return e.total_responses > threshold_responses;
  });
  console.log(agents_threshold)
  agents_threshold.sort((a,b) => (a.perfect_responses_percentage < b.perfect_responses_percentage) ? 1 : ((b.perfect_responses_percentage < a.perfect_responses_percentage) ? -1 : 0)); 
  console.log(agents_threshold)
  bottom_five_agents = agents_threshold.slice(Math.max(agents_threshold.length - 5, 0))
  // bottom_five_agents = agents_threshold.slice(0, 5)
  console.log(top_five_agents)
  for(let i in top_five_agents){
    $("#agent_"+i).html(top_five_agents[i].agent_name)
    $("#agent_total_responses_"+i).html(top_five_agents[i].total_responses)
    $("#agent_perfect_perentage_div_"+i).css('width', top_five_agents[i].perfect_responses_percentage+"%")
    $("#agent_perfect_perentage_"+i).html(top_five_agents[i].perfect_responses_percentage)
  }

  for(let i in bottom_five_agents){
    $("#bottom_agent_"+i).html(bottom_five_agents[i].agent_name)
    $("#bottom_agent_total_responses_"+i).html(bottom_five_agents[i].total_responses)
    $("#bottom_agent_perfect_perentage_div_"+i).css('width', bottom_five_agents[i].perfect_responses_percentage+"%")
    $("#bottom_agent_perfect_perentage_"+i).html(bottom_five_agents[i].perfect_responses_percentage)
  }
  console.log(bottom_five_agents)
}

function drawPIEChart(data) {
  console.log(data)
  $('#ticketsOverviewTotalId').html(window.total_tickets)
  let dataArray = [{'name': 'Perfect', 'color': '#1CB78E', 'y':0},
      {'name': 'Good', 'color': '#6F3ED8', 'y':0},
      {'name': 'Poor', 'color': '#C20712', 'y':0}]
  for(let i in dataArray) {
    let name = dataArray[i].name.toLowerCase()
    let find = data.tickets_rating.find(o => o.ticket_overall_score == name)
    dataArray[i].count = find ? find.count : 0
    dataArray[i].y = find ? parseFloat(find.count * 100 / window.total_tickets) : 0

    $("#"+name+"TicketsOverview").html(dataArray[i].count)
    $("#"+name+"TicketsOverviewDiv").css('width', dataArray[i].y+"%")
  }
  console.log(dataArray)
  Highcharts.chart('pieChart', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: 0,
      plotShadow: false,
      margin: 0,
      height: 200
    },
    title: {
      text: '',
    },
    exporting: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '<b>{point.name}: {point.y: .2f} %</b>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          distance: '-30%',
          enabled: false,
          format: '{point.y: .0f} %',
          style: {

            fontSize: '12px',
            textOutline: false
          }
        },
        center: ['50%', '50%'],
        size: '80%',
        showInLegend: false
      }
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
      itemMarginTop: 5,
      itemMarginBottom: 5,
      width: '30%'
    },
    series: [{
      type: 'pie',
      innerSize: '75%',
      data: dataArray
    }]
  })
}

function drawBasicColumnChart1(dataObj) {
  console.log(dataObj)
  Highcharts.chart('columnContainer1', {
    chart: {
      type: 'column',
      height: 250
    },
    title: {
      text: 'Top-5 Products',
      align: 'left',
      margin: 10,
      useHTML: true,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    legend: {
      enabled: false
    },
    credits: false,
    exporting: false,
    xAxis: {
      categories: dataObj.categories
    },
    yAxis: [{
      min: 0,
      max: 100,
      title: {
        text: ''
      }
    }],
    tooltip: {
      formatter: function () {
        console.log(this)
        if (this.point.series.name == 'Product')
          return this.point.series.name + '<br>' + this.key + ': <b>' + Highcharts.numberFormat(this.point.y, 2) + '</b> %';
        else return false;
      }
    },
    plotOptions: {
      column: {
        grouping: false,
        shadow: false,
        borderWidth: 0,
      }
    },
    series: [{
      name: 'Projection',
      color: '#E4F7F2',
      data: [100, 100, 100, 100, 100],
      pointPadding: 0.3,
      pointPlacement: -0.2
    }, {
      color: '#12B78E',
      name: 'Product',
      data: dataObj.data,
      pointPadding: 0.3,
      pointPlacement: -0.2
    }]
  });
}


function drawBasicColumnChart2() {
  Highcharts.chart('columnContainer2', {
    chart: {
      type: 'column',
      height: 250
    },
    title: {
      text: 'Top-5 Outage reasons',
      align: 'left',
      margin: 10,
      useHTML: true,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    legend: {
      enabled: false
    },
    credits: false,
    exporting: false,
    xAxis: {
      categories: [
        'CE Alarm',
        'CE Power',
        'No Logs',
        'Fiber Cut',
        'H/W Issue'
      ],
    },
    yAxis: [{
      min: 0,
      max: 40,
      title: {
        text: ''
      }
    }],
    tooltip: {
      formatter: function () {
        console.log(this)
        if (this.point.series.name == 'Reason')
          return this.point.series.name + '<br>' + this.key + ': <b>' + Highcharts.numberFormat(this.point.y, 2) + '</b> %';
        else return false;
      }
    },
    plotOptions: {
      column: {
        grouping: false,
        shadow: false,
        borderWidth: 0
      }
    },
    series: [{
      name: 'ReasonActual',
      color: '#E6DDF8',
      data: [50, 50, 50, 50, 50],
      pointPadding: 0.3,
      pointPlacement: -0.2
    }, {
      color: '#6F3ED8',
      name: 'Reason',
      data: [19, 16, 12, 10, 8],
      pointPadding: 0.3,
      pointPlacement: -0.2
    }]
  });
}

function drawBarChart0(data) {
  console.log(data)
  Highcharts.chart('barChart0', {
    chart: {
      type: 'bar',
      height: 120,
      margin: 0,
      marginTop: -20
    },
    colors: ['#24CBE5', '#64E572', '#FF9655', '#FFF263', '#FFA500'],
    exporting: false,
    credits: false,
    title: {
      align: 'left',
      text: 'Top-5 Products',
      useHTML: true,
      margin: 0,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    xAxis: {
      visible: false,
      categories: ['Product']
    },
    yAxis: {
      min: 0,
      visible: false,
      title: {
        text: ''
      }
    },
    legend: {
      enabled: true,
      reversed: true,
      labelFormatter: function () {
        let name = this.name.substring(0, 10)
        return name
      },
      padding: 0
    },
    plotOptions: {
      bar: {
        pointWidth: 30,
        // stacking: 'normal',
        stacking: 'percent',
        tooltip: {
          pointFormat: '<b>{series.name}</b>: {point.y: .0f} %',
        },
        dataLabels: {
          format: '{point.y: .0f} %',
          enabled: true,
          style: {
            color: 'black',
            textOutline: false
          }
        }
      }
    },
    series: data
  });
}

function drawBarChart1() {
  Highcharts.chart('barChart1', {
    chart: {
      type: 'bar',
      height: 120,
      margin: 0,
      marginTop: -20
    },
    colors: ['#33cc00', '#ED561B', '#DDDF00', '#6A5ACD', '#00a3cc'],
    exporting: false,
    credits: false,
    title: {
      align: 'left',
      text: 'Top-5 Outage reasons',
      margin: 0,
      useHTML: true,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    xAxis: {
      visible: false,
      categories: ['Product']
    },
    yAxis: {
      min: 0,
      max: 100,
      visible: false,
      title: {
        text: ''
      }
    },
    legend: {
      enabled: true,
      reversed: true,
      labelFormatter: function () {
        let name = this.name.substring(0, 10)
        return name
      },
      padding: 0
    },
    plotOptions: {
      bar: {
        pointWidth: 30,
        // stacking: 'normal',
        stacking: 'percent',
        dataLabels: {
          enabled: true,
          format: '{point.y: .0f} %',
          style: {
            color: 'black',
            textOutline: false
          }
        }
      }
    },
    series: [{
      name: 'H/W Issue',
      data: [12]
    }, {
      name: 'Fiber Cut',
      data: [15]
    }, {
      name: 'No Logs',
      data: [20]
    }, {
      name: 'CE Power',
      data: [22]
    }, {
      name: 'CE Alarm',
      data: [31]
    }]
  });
}

function redirectTo(module) {
  console.log(module)
  if(module == 'Customer') {
    location.href = '/customer_dashboard'
  } else if(module == 'Quality') {
    location.href = '/quality_dashboard'
  } else if(module == 'Action') {
    location.href = '/agent_module'
  }
}
var otp = '', isCorrectOtp = false, usersList = [];
$(document).ready(function() {
	/* $("#forgotPasswordDialog").dialog({
	    autoOpen : false, modal : true, show : "blind", hide : "blind", closeOnEscape: true,
        draggable: false,
		resizable: false
	}); */
	getAllUsersData();
})
function getAllUsersData(){
	$.ajax({
		type: 'GET',
		url: '/getUsersList',
		data: {
		},
		success: function (data) {
			console.log(data);
			window.usersList = data;
		},
		error: function (data) {
			console.log(data);
		}
	});
}

$("#forgot").click(function(){
	var username = $("#username").val();
	console.log(username)
	if(username !== ""){
		$("#loadingImg").show();
		$("#forgot").attr("disabled", true);
		let isUserExist = window.usersList.find(userRec=> userRec.user_obj.username === username)
		console.log(isUserExist)
		if(!isUserExist){
			$("#loadingImg").hide();
			$("#forgot").attr("disabled", false);
			$("#error").html("<font color=red>User doesn't exists. Please contact Admin</font>");
		} else {
			let user_email = isUserExist.user_obj.email
			if(user_email != ''){
				$("#errorDiv").html("");
				$("#error").text("");
				getOTP(user_email)
			} else {
				$("#loadingImg").hide();
				$("#forgot").attr("disabled", false);
				$("#error").html("<font color=red>User doesn't have an email to send otp. Please contact Admin</font>");
			}
	    	// $("#resetPasswardDiv").show();
	    	// $("#loginDiv").hide();
			//
			// $("#loadingImg").show();
			// $("#forgot").attr("disabled", true);
			// $("#errorDiv").html("");
			// $("#error").text("");

		}
	} else {
		$("#error").html("<font color=red>Enter username (abc@example.com)</font>");
	}
});
function getOTP(email) {
	console.log(email)
	window.otp = '';
	$.ajax({
	    url: '/forgotPassword',
	    type: 'GET',
	    data: {
	    	"email": email
	    },
	    success: function (resp) {
	    	console.log(resp)
	    	window.otp = resp.otp;
	    	$("#otpDiv").show();
			$("#passwordsDiv").hide();
	    	$("#loadingImg").hide();
	    	$("#forgot").attr("disabled", false);
	    	$("#emailSent").text(email);
	    	$("#userOtp").val("");
	    	$("#newPassword").val("");
	    	$("#reenterPassword").val("");
	    	$("#resetPasswardDiv").show();
	    	$("#loginDiv").hide();
	    	setTimeout(() => {
	    		window.otp = ' ';
			}, 180000);
	    	// $("#forgotPasswordDialog").dialog("open");
	   },
	    error: function (error) {
	    	$("#loadingImg").hide();
	    	$("#forgot").attr("disabled", false);
	        console.log(error);
	    }
	});
}
function checkOtp() {
	if($("#userOtp").val() == window.otp){
		window.isCorrectOtp = true;
		$("#otpDiv").hide();
		$("#passwordsDiv").show();
		$("#errorDiv").html("")
	} else {
		window.isCorrectOtp = false;
		$("#errorDiv").html("OTP is incorrect or is expired")
	}
}
function recoverPassword(){
	var username = $("#username").val();
	var newPassword = $("#newPassword").val();
	var reenterPassword = $("#reenterPassword").val();
	if(newPassword === '' || reenterPassword === ''){
		$("#errorDiv").html("Passwords should not empty")
	} else if(newPassword == reenterPassword){
		$.ajax({
			// url: $("#contextPath").val()+'/resetPassword',
			url: '/resetPassword',
		    type: 'POST',
		    data: {		
		    	"username": username,
		    	"password": newPassword
		    },		
		    success: function (resp) {
				console.log(resp)
		    	if(resp.status == "200"){
		    		$("#passwordsDiv").hide();
			    	$("#resetPasswardDiv").hide();
			    	$("#loginDiv").show();
//		    		$("#forgotPasswordDialog").dialog("close");
		    		$("#error").html("<font color=green> Password changed successfully</font>");
		    	} else{
		    		$("#errorDiv").html("Password change failed")
		    	}
		    },		
		    error: function (error) {
		        console.log(error);
		    }		
		});
	} else if(newPassword != reenterPassword){
		$("#errorDiv").text("Passwords doesnot match")
	}
}
function cancel() {
	$("#resetPasswardDiv").hide();
	$("#loginDiv").show();
}














/* if (chartIndex === 0) {
        let uniqBu = [...window.ratingColorAndName]
        // for(let bu in uniqBu){
        //   uniqBu[bu]['data'] = []
        let setData = [];
          for(let r in records){
            if(0 < records[r].desks.length){
              setData.push({ name: records[r].name, data: [], deskData: []})
              for(let d in records[r].desks){
                if(0 < records[r].desks[d].owner_groups.length){

                  for(let o in records[r].desks[d].owner_groups){
                    for(let i in window.ratingColorAndName) {

                    }
                  }
                }
              }
            }
          }
        // }

      } */
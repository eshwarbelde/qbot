var ratingColorAndName = [
    {name:'perfect', displayName: 'Perfect', color: 'rgb(51, 204, 0)'},
    {name:'good', displayName: 'Good', color: 'rgb(255, 165, 0)'},
    {name:'poor', displayName: 'Poor', color: 'rgb(237, 86, 27)'}
  ],
  ratingsForSorting = ['Perfect', 'Good', 'Poor'],
  chartConfig = {}, createXLSLFormatObj = {}, filterOption = false,
  filterVals = { 'startDate': '', 'endDate': '', 'timeline': 'All', 'severity': 'All', 'product': 'All', 'customer': 'All' },
  piechartData = [], metricsData = {}, timeLineChartData = []
$(document).ready(function () {
  getMasterDataFromDB();
  getDataForPieChart();
  getDataStaticBoxs()
  getDataForAreaChart()
});

function getMasterDataFromDB() {
  $.ajax({
    type: 'get',
    url: '/getMasterDataFromDB',
    data: {
    },
    success: function (records) {
      console.log(records)
      let customer_names = records.customer_name.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      for (let i in customer_names) {
        let customer = customer_names[i].name, index = (i + 1)
        $('#customerDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="customerButton' + index + '" onclick="fetchDataFilterButton(`customer`, `' + index + '`, `' + customer + '`)">' + customer + '</button>')
      }

      let products = records.product.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      for (let i in products) {
        let product = products[i].name, index = (i + 1)
        $('#productDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="productButton' + index + '" onclick="fetchDataFilterButton(`product`, `' + index + '`, `' + product + '`)">' + product + '</button>')
      }

      let severities = records.severity.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      for (let i in severities) {
        let severity = severities[i].name, index = (i + 1)
        $('#severityDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="severityButton' + index + '" onclick="fetchDataFilterButton(`severity`, `' + index + '`, `' + severity + '`)">' + severity + '</button>')
      }
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function filterOptionsBtn() {
  if (window.filterOption) {
    $('#filterOptions').hide()
  } else {
    $('#filterOptions').show()
  }
  window.filterOption = !window.filterOption
}

function fetchDataFilterButton(key, index, value) {
  $('#' + key + 'Div button').removeClass('active')
  $('#' + key + 'Button' + index).addClass('active')
  window.filterVals[key] = value
}

function getDataForPieChart() {
  $.ajax({
    type: 'get',
    url: '/getAllTicketsGroupByCustomerNameAndRating',
    data: {
      customer_name : window.filterVals.customer,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (records) {
      console.log(records)
      window.piechartData = records
      preparePIEChartData(records)
    },  
    error: function (data) {
      console.log(data);
    }
  })
}

function getDataStaticBoxs() {
  $.ajax({
    type: 'get',
    url: '/getAllTicketsGroupByCustomerName',
    data: {
      customer_name : window.filterVals.customer,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (records) {
      console.log(records)
      window.metricsData['keys'] = Object.keys(records)
      window.metricsData['data'] = records
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getDataForAreaChart() {
  $.ajax({
    type: 'get',
    url: '/getAllTicketsGroupByCustomerNameAndRatingForTimeline',
    data: {
      customer_name : window.filterVals.customer,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (records) {
      console.log(records)
      records = records.sort((a, b) => moment(a.ticket_booking_time, 'DD-MM-YYYY').valueOf() - moment(b.ticket_booking_time, 'DD-MM-YYYY').valueOf());
      window.timeLineChartData = records
      prepareDataForAreaChart(records)
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function filterDataAndPlot(){
  $('#filterOptions').hide()
  let customer_name = window.filterVals.customer,
  product = window.filterVals.product
  severity = window.filterVals.severity,
  records = [], totalRecords = [...window.piechartData],
  metricsObj = {keys: [], data: {}}, totalMetricsData = window.metricsData,
  timeLineData = [], totalTimeLineData = [...window.timeLineChartData]
  
  if(customer_name == 'All' && product == 'All' && severity == 'All') {
    records = totalRecords
    metricsObj = totalMetricsData
    timeLineData = totalTimeLineData

  } else if(customer_name != 'All' && product == 'All' && severity == 'All') {
    records = totalRecords.filter(o => o.customer_name == customer_name)
    for(let i in totalMetricsData.keys){
      let key = totalMetricsData.keys[i]
      metricsObj.keys.push(key)
      metricsObj.data[key] = totalMetricsData.data[key].filter(o => o.customer_name == customer_name)
    }
    timeLineData = totalTimeLineData.filter(o => o.customer_name == customer_name)

  } else if(customer_name != 'All' && product != 'All' && severity == 'All') {
    records = totalRecords.filter(o => o.customer_name == customer_name && o.product == product)
    for(let i in totalMetricsData.keys){
      let key = totalMetricsData.keys[i]
      metricsObj.keys.push(key)
      metricsObj.data[key] = totalMetricsData.data[key].filter(o => o.customer_name == customer_name && o.product == product)
    }
    timeLineData = totalTimeLineData.filter(o => o.customer_name == customer_name && o.product == product)

  } else if(customer_name != 'All' && product != 'All' && severity != 'All') {
    records = totalRecords.filter(o => o.customer_name == customer_name && o.product == product && o.severity == severity)
    for(let i in totalMetricsData.keys){
      let key = totalMetricsData.keys[i]
      metricsObj.keys.push(key)
      metricsObj.data[key] = totalMetricsData.data[key].filter(o => o.customer_name == customer_name && o.product == product && o.severity == severity)
    }
    timeLineData = totalTimeLineData.filter(o => o.customer_name == customer_name && o.product == product && o.severity == severity)

  } else if(customer_name == 'All' && product != 'All' && severity == 'All') {
    records = totalRecords.filter(o => o.product == product)
    for(let i in totalMetricsData.keys){
      let key = totalMetricsData.keys[i]
      metricsObj.keys.push(key)
      metricsObj.data[key] = totalMetricsData.data[key].filter(o => o.product == product)
    }
    timeLineData = totalTimeLineData.filter(o => o.product == product)

  } else if(customer_name == 'All' && product != 'All' && severity != 'All') {
    records = totalRecords.filter(o => o.product == product && o.severity == severity)
    for(let i in totalMetricsData.keys){
      let key = totalMetricsData.keys[i]
      metricsObj.keys.push(key)
      metricsObj.data[key] = totalMetricsData.data[key].filter(o => o.product == product && o.severity == severity)
    }
    timeLineData = totalTimeLineData.filter(o => o.product == product && o.severity == severity)

  } else if(customer_name == 'All' && product == 'All' && severity != 'All') {
    records = totalRecords.filter(o => o.severity == severity)
    for(let i in totalMetricsData.keys){
      let key = totalMetricsData.keys[i]
      metricsObj.keys.push(key)
      metricsObj.data[key] = totalMetricsData.data[key].filter(o => o.severity == severity)
    }
    timeLineData = totalTimeLineData.filter(o => o.severity == severity)

  } else if(customer_name != 'All' && product == 'All' && severity != 'All') {
    records = totalRecords.filter(o => o.customer_name == customer_name && o.severity == severity)
    for(let i in totalMetricsData.keys){
      let key = totalMetricsData.keys[i]
      metricsObj.keys.push(key)
      metricsObj.data[key] = totalMetricsData.data[key].filter(o => o.customer_name == customer_name && o.severity == severity)
    }
    timeLineData = totalTimeLineData.filter(o => o.customer_name == customer_name && o.severity == severity)

  }
  preparePIEChartData(records, customer_name)
  prepareDataForAreaChart(timeLineData)
}

function preparePIEChartData(records){
  console.log(records)
  let dataArray = [], totalTickets = 0, downloadData = [['Rating', 'Tickets']];
  // for (var i = 0; i < records.length; i++)
  for (var i in records) {
    var rating = ratingColorAndName.find(rec => rec.name === records[i].ticket_overall_score)
    if(rating){
      totalTickets += records[i].count
      var findRec = dataArray.find(rec=> rec.name == rating.displayName)
      if(!findRec){
        dataArray.push({
          name: rating.displayName,
          y: records[i].count,
          color: rating.color
        })
        downloadData.push([rating.displayName, records[i].count]);
      } else {
        findRec.y += records[i].count
        downloadData.find(rec=>rec[0] == rating.displayName)[1] += records[i].count
      }
    }
  }
  window.createXLSLFormatObj['ticketsReport'] = [...downloadData]
  console.log(dataArray)
  $('#ticketsReportHead').text('Tickets report: '+totalTickets)
  drawPIEChart('ticketsReport', dataArray)
}

function drawPIEChart(chartId, dataArray){
  dataArray.sort((a, b) => { return ratingsForSorting.indexOf(a.name) - ratingsForSorting.indexOf(b.name) })
  window.chartConfig[chartId] = Highcharts.chart(chartId, {
    chart: {
      height: 300
    },
    title: {
      text: '',
      margin: 10,
      useHTML: true,
      style: {
        fontSize: '14px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    exporting: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '<b>{point.name}: {point.y: .0f}</b>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          distance: '-30%',
          enabled: false,
          format: '{point.y: .0f} %',
          style: {
            fontSize: '12px',
            textOutline: false
          }
        },
        center: ['50%', '50%'],
        size: '80%',
        showInLegend: true
      }
    },
    legend: {
      enabled: true
    },
    series: [{
      type: 'pie',
      innerSize: '75%',
      data: dataArray
    }]
  })
}

function prepareDataForAreaChart(records) {
  console.log(records)
  let dataArray = [], downloadData = [['Date time', 'Perfect', 'Good', 'Poor']]
  for(var i in records) {
    let formatDate = records[i].ticket_booking_time, date = moment(formatDate, 'DD-MM-YYYY').valueOf() + 19800000
    let rating = ratingColorAndName.find(rec => rec.name === records[i].ticket_overall_score)
    if(rating) {
      var findRating = dataArray.find(rec => rec.name === rating.displayName);
      if(!findRating){
        var ratingData = []
        ratingData.push([date , records[i].count])
        dataArray.push({ data: ratingData, name: rating.displayName, color: rating.color })
      } else {
        var findDate = findRating.data.find(rec => rec[0] === date)
        if(findDate) findDate[1] += records[i].count
        else findRating.data.push([ date , records[i].count ])
      }
      let find = downloadData.find(rec => rec[0] === formatDate)
      if(!find){
        downloadData.push([ formatDate, 0, 0, 0 ])
        find = downloadData.find(rec => rec[0] === formatDate)
      }
      if(rating.displayName == 'Perfect') find[1] += records[i].count
      else if(rating.displayName == 'Good') find[2] += records[i].count
      else find[3] += records[i].count
    }
  }
  window.createXLSLFormatObj['ticketsTimelineView'] = [...downloadData]
  console.log(dataArray)
  console.log(downloadData)
  drawAreaChart(dataArray, 'ticketsTimelineView')
}

function drawAreaChart(data, chartId) {
  $('#'+ chartId +'Head').text('Tickets timeline view')
  data.sort((a, b) => { return ratingsForSorting.indexOf(a.name) - ratingsForSorting.indexOf(b.name) })
  window.chartConfig[chartId] = Highcharts.stockChart(chartId, {
    chart: {
      height: 300
    },
    yAxis: {
      allowDecimals: false,
       min: 0
     },
    rangeSelector: {
      selected: 1,
      enabled: false
    },
    plotOptions: {
      series: {
        type: 'spline',
        spacingTop: 10,
        showInNavigator: true
      }
    },
    legend: {
      enabled: true,
      align: 'right',
      verticalAlign: 'top',
      itemStyle: {
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 14
      }
    },
    tooltip: {
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
      split: true
    },
    exporting: {
      enabled: false
    },
    credits: false,
    series: data
  });
}
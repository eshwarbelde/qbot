
var allScenarios = [
  { "scenario": "A large enterprise customer has just called to let you know that they are unable to access the Internet from the office location. <br/> <br/> Once you have created a ticket in SNOW, what is your next step?", "response": "" },
  { "scenario": "You have completed checks, you find that the link is down, and that you need to conduct troubleshooting with the customer, at the location, but you are not sure there is a ongoing power cut at the location. <br/> <br/> Please compose your first response to the customer", "response": "" },
  { "scenario": "Customer has responded that there is no power issue on site, and you have conducted troubleshooting and found that the CPE is not powering up. <br/> <br/> Please compose the response to the customer", "response": "" },
  { "scenario": "You realize that this is an issue with 3rd party access provider, so you've booked a ticket with them, and they have asked for access to the customer location from 1:00 PM to 3:00 PM <br/> <br/> Please compose your response to the customer", "response": "" },
  { "scenario": "Field engineer visited the site, and the service has been restored <br/> <br/> Please compose your response to the customer", "response": "" }
], totalScoreId = 0, currentScenario = 0
$(document).ready(function(){
  newSessionFetching(true);
  var date = new Date();
  var dateString = date.getDate() + '/' + (date.getMonth()+1) + '/'+ date.getFullYear()
  $('#todayDate').text(dateString);
  // reportView()
});

function newSessionFetching(initial) {
  window.currentScenario = 0
  let value = window.allScenarios[window.currentScenario].scenario
  $('#responseId').hide();
  $('#optionsRadios').show();
  $('#scenarioId').html(value);
  $('#totalScoreId').text(0);
  window.totalScoreId = 0
  $("#optionsRadios0").prop("checked", true);
  $('#responseId').val($('#optionsRadios0').val());
  $('#resultDiv').hide();
  $('#nextScenarioBtn').show();
  $('#reportBtn').hide();
  $('#analyzeButton').removeAttr('disabled');
  $('#reportViewDiv').hide();
  if(!initial) {
    $('#scenarioForm').hide('slide', {direction: 'left'}, 1000);
    setTimeout(() => {
      $('#scenarioForm').show('slide', {direction: 'right'}, 1000);
    }, 500);
  }
}
$(document).on("click", ".form-check-input", function () {
  console.log(this.id)
  $('#responseId').val($('#'+this.id).val());
})


function fetchAnalysedResult() {
  let scenario_no = (window.currentScenario + 1),
  response = $('#responseId').val().trim()
  if(response == '') return alert('Please enter response')

  $('#loadingImg').show();
  $('#analyzeIcon').hide();
  $('#analyzeButton').attr('disabled','disabled');
  console.log(scenario_no)
  console.log(response)
  setTimeout(() => {
    $.ajax({
      type: 'get',
      url: '/get_agent_analysis_response',
      data: {
        "scenario_no": scenario_no,
        "response": response
      },
      success: function (data) {
        console.log(data)
        window.allScenarios[window.currentScenario].response = response
        window.allScenarios[window.currentScenario]['You have not provided'] = 'NONE!'
        window.allScenarios[window.currentScenario]['You have provided'] = 'NONE!'
        if(data.flag) {
          setRating(data.result.Individual_Score, 'individualRating')
          $('#missingParametersId').text('NONE!');
          $('#suggestionsId').text('NONE!');
          $('#totalScoreId').text(parseInt($('#totalScoreId').text()) + data.result.Score);
          window.totalScoreId += data.result.Score
          $('#errorDiv').hide();
          $('#ratingDiv').show();

          if(scenario_no == 1) {
            $('#missingParametersDiv').hide();
            $('#suggestionsDiv').hide();
          } else {
            if(data.result['You have not provided'] && data.result['You have not provided'] != null) {
              let htmlText = ''
              for(let i in data.result['You have not provided']) {
                let name = data.result['You have not provided'][i].charAt(0).toUpperCase() + data.result['You have not provided'][i].slice(1)
                htmlText += name + (parseInt(i)+1 == data.result['You have not provided'].length ? '' : '') + '<br/>'
              }
              window.allScenarios[window.currentScenario]['You have not provided'] = htmlText
              $('#missingParametersId').html(htmlText);
            }
            if(data.result['You have provided']  && data.result['You have provided'] != null) {
              let htmlText = ''
              for(let i in data.result['You have provided']) {
                let name = data.result['You have provided'][i].charAt(0).toUpperCase() + data.result['You have provided'][i].slice(1);
                htmlText += name + (parseInt(i)+1 == data.result['You have provided'].length ? '' : '') + '<br/>'
              }
              window.allScenarios[window.currentScenario]['You have provided'] = htmlText
              $('#suggestionsId').html(htmlText);
            }

            $('#missingParametersDiv').show();
            $('#suggestionsDiv').show();
          }
        } else {
          $('#errorMsg').html(data.result.result);
          $('#errorDiv').show();
          $('#ratingDiv').hide();
          $('#missingParametersDiv').hide();
          $('#suggestionsDiv').hide();
        }
        $('#loadingImg').hide();
        $('#analyzeIcon').show();
        $('#resultDiv').slideDown("slow")
      }, error: function (error) {
        console.log(error);
      }
    });
  }, 2000);

  /* setTimeout(() => {
    $('#loadingImg').hide();
    $('#analyzeIcon').show();
    $('#resultDiv').slideDown("slow")
  }, 3000); */
}

$(".stars").click(function(e) {
  var gLeft = $(".stars .stars-ghost").offset().left,
     pX = e.pageX;
  console.log(pX)
  console.log(gLeft)
  $(".stars .stars-ghost").width(pX - gLeft);

});

function setRating(rating, divId) {
  console.log(rating)
  let resultText = ''
  if(4 <= rating) resultText = 'Way to go!'
  else if(3 <= rating) resultText = 'Almost there!'
  else resultText = 'You can do better!'

  $('#'+divId+' span').removeClass('individualRatingSelected');
  $('#'+divId+' span').removeClass('individualRatingSelectedHalf');
  $('#resultTextId').text(resultText)
  for(let i=0; i<rating; i++) {
    if(i < rating && rating < (i+1)) $('#'+divId+' span').eq(i).addClass('individualRatingSelectedHalf');
    else $('#'+divId+' span').eq(i).addClass('individualRatingSelected');
  }
}

function reportView() {
  $('#scenarioForm').hide('slide', {direction: 'left'}, 1000);
  $('#reportBtn').hide()
  $('#resultDiv').hide();
  $('#resultDiv').slideDown("slow")
  
  setTimeout(() => {
    $('#analyzeButton').removeAttr('disabled');
    $('#errorDiv').hide();
    $('#ratingDiv').hide();
    // setRating(parseInt($('#totalScoreId').text()));
    setRating(window.totalScoreId, 'reportRating');
    $('#missingParametersDiv').hide();
    $('#suggestionsDiv').hide();

    $('#reportViewDiv').show();
    $('#reportViewId').html('');
    let html = '<table style="width:100%">'
    for(let i = 1; i <= window.allScenarios.length; i++) {
      let obj = window.allScenarios[i-1]

      html += '<tr><td style="width: 10%; vertical-align: top;"><b>Q ' + i + ':</b></td>'
      html += '<td><b>' + obj.scenario + '</b></td></tr>'

      html += '<tr><td style="vertical-align: top;"><b>A ' + i + ':</b></td>'
      html += '<td>' + obj.response + '</td></tr>'

      html += '<tr><td></td>'
      html += '<td><b>You should have included:</b> ' + obj['You have not provided'] + '</td></tr>'

      html += '<tr><td></td>'
      html += '<td><b>Your message includes:</b> ' + obj['You have provided'] + '</td></tr>'
    }
    html += '</table>'
    $('#reportViewId').html(html);

  }, 500);
}

function setNextScenario() {
  $('#scenarioForm').hide('slide', {direction: 'left'}, 1000);
  setTimeout(() => {
    $('#responseId').val('');
    $('#responseId').show();
    $('#optionsRadios').hide();
    $('#analyzeButton').removeAttr('disabled');
    window.currentScenario++
    if(window.currentScenario == 4) {
      $('#reportBtn').show();
      $('#nextScenarioBtn').hide();
    }
    let value = window.allScenarios[window.currentScenario].scenario
    $('#scenarioId').html(value);
    
    $('#resultDiv').hide();
    $('#scenarioForm').show('slide', {direction: 'right'}, 1000);
  }, 500);
}
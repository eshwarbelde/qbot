
var timelines = ['Last 24 Hours', '48 Hours', '1 Week', '1 Month', 'Current Quarter', 'Current Year'],
  chartConfig = {}, createXLSLFormatObj = {}, filterOption = false, discardedTabMode = '%',
  filterVals = {'startDate': '', 'endDate': '', 'timeline': 'All', 'severity': 'All', 'product': 'All', 'ownerGroup': 'All'}
$(document).ready(function () {
  getMasterDataFromDB()
  buildContentComplianceChart();
  buildMissingParametersChart();
  buildTimeComplianceChart();
  buildTimeDifferenceBarChart();

  buildDiscardedBar();
  buildDiscardedTimeline();
});

function getMasterDataFromDB() {
  $.ajax({
    type: 'get',
    url: '/getMasterDataFromDB',
    data: {
    },
    success: function (records) {
      console.log(records)
      let owner_groups = records.owner_group.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      for(let i in owner_groups){
        let ownerGroup = owner_groups[i].name, index = (i+1)
        $('#ownerGroupDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="ownerGroupButton'+index+'" onclick="fetchDataFilterButton(`ownerGroup`, `'+ index +'`, `'+ ownerGroup +'`)">'+ ownerGroup +'</button>')
      }
      
      let products = records.product.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      for(let i in products){
        let product = products[i].name, index = (i+1)
        $('#productDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="productButton'+index+'" onclick="fetchDataFilterButton(`product`, `'+ index +'`, `'+ product +'`)">'+ product +'</button>')
      }
      
      let severities = records.severity.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
      for(let i in severities){
        let severity = severities[i].name, index = (i+1)
        $('#severityDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="severityButton'+index+'" onclick="fetchDataFilterButton(`severity`, `'+ index +'`, `'+ severity +'`)">'+ severity +'</button>')
      }

      for(let i in timelines){
        let timeline = timelines[i], index = (i+1)
        $('#timelineDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="timelineButton'+index+'" onclick="fetchDataFilterButton(`timeline`, `'+ index +'`, `'+ timeline +'`)">'+ timeline +'</button>')
      }
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function filterOptionsBtn() {
  if(window.filterOption) {
    $('#filterOptions').hide()
  } else {
    $('#filterOptions').show()
  }
  window.filterOption = !window.filterOption
}

function filterDataAndPlot() {
  $('#filterOptions').hide()
  buildContentComplianceChart()
  buildMissingParametersChart()
  buildTimeComplianceChart()
  buildTimeDifferenceBarChart()

  buildDiscardedBar()
  buildDiscardedTimeline()
}

function fetchDataFilterButton(key, index, value) {
  $('#'+ key +'Div button').removeClass('active')
  $('#'+ key +'Button'+ index).addClass('active')
  window.filterVals[key] = value
  if(key == 'timeline') {
    let dates = fetchTimeChange(value);

    window.filterVals.startDate = dates.startDate
    window.filterVals.endDate = dates.endDate
  }

}

function buildContentComplianceChart() {
  $.ajax({
    type: 'get',
    url: '/getContentComplianceResults',
    data: {
      owner_group: window.filterVals.ownerGroup,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (records) {
      console.log(records)
      drawContentComplianceChart(records, 'contentCompliance');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function drawContentComplianceChart(dataArray, chartId) {
  let downloadData = [['Name', '#']]
  for (let i in dataArray) {
    downloadData.push([dataArray[i].name, dataArray[i].count])
  }
  downloadData.push(['Total responses', dataArray[0].total_records])
  console.log(downloadData)
  window.createXLSLFormatObj[chartId] = [...downloadData]
  $('#'+chartId+'Head').text('Content compliance: '+ dataArray[0].total_records)
  window.chartConfig[chartId] = Highcharts.chart(chartId, {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: 0,
      plotShadow: false,
      height: 300,
    },
    title: {
      text: ''
    },
    credits: {
      enabled: false
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '<b>{point.name}: {point.y: .0f} %</b>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          enabled: true,
          distance: 5,
          formatter: function () {
            if (0.01 < this.point.y && this.point.y < 1)
              return Highcharts.numberFormat(this.point.y, 2) + ' %';
            else
              return Highcharts.numberFormat(this.point.y, 0) + ' %';
          },
          style: {
            fontSize: '12px',
            textOutline: false
          }
        },
        point: {
          events: {
            click: function (e) {
              console.log(e);
              getContentComplianceRecords(e.target.point.name)
            }
          }
        },
        startAngle: -90,
        endAngle: 90,
        center: ['50%', '75%'],
        size: '110%',
        showInLegend: true,
        pointInterval: 24 * 3600 * 1000
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      labelFormatter: function () {
        return this.name + ' - ' + this.count;
      }
    },
    series: [{
      type: 'pie',
      innerSize: '70%',
      data: dataArray
    }]
  })
}

function buildMissingParametersChart(){
  $.ajax({
    type: 'get',
    url: '/getMissingParametersResults',
    data: {
      owner_group: window.filterVals.ownerGroup,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (records) {
      console.log(records)
      let dataArray = [], categories = [], final_result = []
      for(let i in records){
        path = records[i].shortest_path
        category = path.substring(path.indexOf(',')+1, path.lastIndexOf(','))
        series = path.substring(path.lastIndexOf(',')+1);
        
        if (!categories.includes(series)) categories.push(series)

        let find_obj = dataArray.find(rec => rec.category == category)
        if(!find_obj) dataArray.push({category: category, series: series, count:parseInt(records[i].count)})
        else find_obj.count +=parseInt(records[i].count)
      }
      categories.sort((a,b) => b - a);
      dataArray.sort(function(a, b) {
        if(Number(a.series) === Number(b.series)) {
          return b.count - a.count;
        }
        return Number(b.series) - Number(a.series);
      });
      for(let j in dataArray){
        let data=[]
        for(let k in categories){
          let val = categories[k] == dataArray[j].series ? dataArray[j].count : 0
          data.push(parseInt(val));
        }
        final_result.push({name: dataArray[j].category, data: data})

      }
      console.log(categories)
      console.log(dataArray)
      console.log(final_result)
      drawMissingParametersChart(categories, final_result, 'missingParameters')
    },
    error: function (data) {
      console.log(data);
    }
  }); 
}

function drawMissingParametersChart(categories, data, chartId) {
  let downloadData = [['MP Name', 'MP-3', 'MP-2', 'MP-1']]
  for (let i in data) {
    downloadData.push([data[i].name, data[i].data[0], data[i].data[1], data[i].data[2]])
  }
  console.log(downloadData)
  window.createXLSLFormatObj[chartId] = [...downloadData]
  $('#'+chartId+'Head').text('Missing parameters')
  window.chartConfig[chartId] = Highcharts.chart(chartId, {
    chart: {
      type: 'bar',
      height: 300
    },
    colors: ['#DDDF00', '#6A5ACD', '#FFA500', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#00a3cc', '#33cc00', '#ED561B', '#DDDF00', '#6A5ACD', '#FFA500', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
    title: {
      text: ''
    },
    xAxis: {
      categories: categories,
      title: {
        text: 'Missing parameters'
      }
    },
    yAxis: {
      min: 0,
      stackLabels: {
        enabled: true,
        align: 'right'
      },
      title: {
        text: '# of responses'
      }
    },
    credits: false,
    legend: {
      reversed: true,
      margin: 0
    },
    exporting: false,
    plotOptions: {
      series: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          formatter: function () {
            if (0 < this.point.y)
              return Highcharts.numberFormat(this.point.y, 0);
            else
              return
          },
          style: {
            textOutline: false
          }
        }
      }
    },
    series: data
  });
}

function buildTimeComplianceChart(){
  $.ajax({
    type: 'get',
    url: '/getTimeComplianceResults',
    data: {
      owner_group: window.filterVals.ownerGroup,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (records) {
      console.log(records)
      drawTimeComplianceChart(records, 'timeCompliance');
    },
    error: function (data) {
      console.log(data);
    }
  }); 
}

function drawTimeComplianceChart(dataArray, chartId) {
  let downloadData = [['Name', '#']]
  for (let i in dataArray) {
    downloadData.push([dataArray[i].name, dataArray[i].count])
  }
  downloadData.push(['Total responses', dataArray[0].total_records])
  console.log(downloadData)
  window.createXLSLFormatObj[chartId] = [...downloadData]
  $('#'+chartId+'Head').text('Time compliance: '+dataArray[0].total_records)
  window.chartConfig[chartId] = Highcharts.chart(chartId, {
    chart: {
      height: 300
    },
    title: {
      text: ''
    },
    credits: {
      enabled: false
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '<b>{point.name}: {point.y: .0f} %</b>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          distance: 5,
          enabled: true,
          format: '{point.y: .0f} %',
          style: {
            fontSize: '12px',
            textOutline: false
          }
        },
        center: ['50%', '50%'],
        size: '80%',
        showInLegend: true
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      labelFormatter: function () {
        return this.name + ' - ' + this.count;
      }
    },
    series: [{
      type: 'pie',
      innerSize: '75%',
      data: dataArray
    }]
  })
}

function buildTimeDifferenceBarChart(){
  $.ajax({
    type: 'get',
    url: '/getTimeComplianceDifferenceResultsWithSLA',
    data: {
      owner_group: window.filterVals.ownerGroup,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (records) {
      console.log(records)
      let categories = [], dataArray = [],
      final_result = [
        {'range': '-60 & below', 'count': 0},
        {'range': '-50', 'count': 0},
        {'range': '-40', 'count': 0},
        {'range': '-30', 'count': 0},
        {'range': '-20', 'count': 0},
        {'range': '-10', 'count': 0},
        {'range': '0', 'count': 0},
        {'range': '10', 'count': 0},
        {'range': '20', 'count': 0},
        {'range': '30', 'count': 0},
        {'range': '40', 'count': 0},
        {'range': '50', 'count': 0},
        {'range': '60 & above', 'count': 0}
      ]
      for(let i in records){ 
        if(records[i].range_start < -50){
          var obj_find = final_result.find(obj => obj.range == "-60 & below")
          if(obj_find != undefined) obj_find.count += records[i].count
        } else if(records[i].range_end > 50){
          var obj_find = final_result.find(obj => obj.range == "60 & above")
          if(obj_find != undefined) obj_find.count += records[i].count
        } else {
          let rangeName = records[i].range_start < 0 ? records[i].range_start : records[i].range_end
          final_result.find(obj => obj.range == rangeName).count += records[i].count
        }
      }
      console.log(final_result)
      for(let j in final_result){
        categories.push(final_result[j].range)
        dataArray.push(final_result[j].count)
      }
      console.log(categories)
      console.log(dataArray)
      drawTimeDifferenceBarChart(categories, dataArray, 'timeDifference');
    },
    error: function (data) {
      console.log(data);
    }
  }); 
}

function drawTimeDifferenceBarChart(categories, data, chartId) {
  let downloadData = [['Range', '#']], seriesData = [], colors = ['#33cc00', '#FFA500']
  for (let i in data) {
    downloadData.push([categories[i], data[i]])
    seriesData.push({ y: data[i], color: categories[i].substring(0, 1) == '-' ? colors[0] : colors[1] })
  }
  console.log(downloadData)
  window.createXLSLFormatObj[chartId] = [...downloadData]
  $('#'+chartId+'Head').text('Time difference')
  window.chartConfig[chartId] = Highcharts.chart(chartId, {
    chart: {
      type: 'column',
      height: 300
    },
    title: '',
    xAxis: {
      categories: categories,
      title: {
        text: 'SLA deviation (In minutes)'
      },
      plotLines: [{
        label: {
          text: 'SLA',
          style: {
            fontWeight: 'bold',
            fontSize: '14px'
          },
          rotation: '0'
        },
        color: 'red', // Color value
        dashStyle: 'dashdot', // Style of the plot line. Default to solid
        value: 6, // Value of where the line will appear
        width: 2 // Width of the line    
      }]
    },
    yAxis: {
      title: {
        text: '# of responses'
      }
    },
    credits: false,
    exporting: false,
    plotOptions: {
      series: {
        dataLabels: {
          enabled: true,
          formatter: function () {
            if (0 < this.point.y)
              return Highcharts.numberFormat(this.point.y, 0);
            else
              return
          },
          style: {
            textOutline: false
          }
        }
      }
    },

    series: [{
      name: "Range(In min)",
      showInLegend: false,
      data: seriesData
    }]
  });
}

function fetchTimeChange(timelineValue) {
  var dates = {}
  var currentDate = new Date();
  if(timelineValue == 'All') {
    dates['startDate'] = '';
    dates['endDate'] = '';
    return dates;
  } else if (timelineValue == 'Last 24 Hours') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setDate(currentDate.getDate() - 1)).toISOString();
    return dates;

  } else if (timelineValue == '48 Hours') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setDate(currentDate.getDate() - 2)).toISOString();
    return dates;

  } else if (timelineValue == '1 Week') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setDate(currentDate.getDate() - 7)).toISOString();
    return dates;

  } else if (timelineValue == '1 Month') {
    dates['endDate'] = currentDate.toISOString();
    dates['startDate'] = new Date(currentDate.setMonth(currentDate.getMonth() - 1)).toISOString();
    return dates;

  } else if (timelineValue == 'Current Quarter') {
    var quarter = Math.floor((currentDate.getMonth() / 3));
    var strDate = new Date(currentDate.getFullYear(), quarter * 3, 1);
    var endDate = new Date(strDate.getFullYear(), strDate.getMonth() + 3, 0);
    dates['endDate'] = endDate.toISOString()
    dates['startDate'] = strDate.toISOString()
    return dates;

  } else if (timelineValue == 'Current Year') {
    var month = Math.floor((currentDate.getMonth()));
    if(month < 3){
      var strDate = new Date((currentDate.getFullYear()-1)+ '/04/01');
      var endDate = new Date(currentDate.getFullYear()+ '/03/31');
      dates['endDate'] = endDate.toISOString()
      dates['startDate'] = strDate.toISOString()
    } else {
      var strDate = new Date(currentDate.getFullYear()+ '/04/01');
      var endDate = new Date((currentDate.getFullYear()+1)+ '/03/31');
      dates['endDate'] = endDate.toISOString()
      dates['startDate'] = strDate.toISOString()
    }
    console.log(dates)
    return dates;
  }
}


function fetchDownloadData(file, chartId) {
  /* File Name */
  var filename = file + moment().format('DD-MM-YYYY hh:mm a') + ".xlsx";
  /* Sheet Name */
  var ws_name = "data";

  var wb = XLSX.utils.book_new(),
    ws = XLSX.utils.aoa_to_sheet(window.createXLSLFormatObj[chartId]);

  /* Add worksheet to workbook */
  XLSX.utils.book_append_sheet(wb, ws, ws_name);

  /* Write workbook and Download */
  XLSX.writeFile(wb, filename);
}

function downloadChartAs(filename, chartId, type) {
  // window.filterVals = {'startDate': '', 'endDate': '', 'severity': 'All', 'product': 'All', 'ownerGroup': 'All'}
  let html = '<span> <b> Product : </b>' + window.filterVals.product + '</span> <br>' +
    '<span> <b> Severity : </b>' + window.filterVals.severity + '</span> <br>' +
    '<span> <b> Owner group : </b>' + window.filterVals.ownerGroup + '</span> <br>' +
    '<span> <b> Timeline : </b>' + window.filterVals.timeline + '</span>'
  title = $('#'+chartId+'Head').text(),
  downloadType = type == 'PDF' ? 'application/pdf' : 'image/png'
  window.chartConfig[chartId].exportChart({
    type: downloadType,
    filename: filename + moment().format('DD-MM-YYYY hh:mm a')
  }, {
    chart: {
      width: '1000',
      marginTop: '100',
      height: '500',
    },
    title: {
      text: title,
      align: 'center'
    },
    subtitle: {
      text: html
    },
  });
}

function buildDiscardedBar(){
  $.ajax({
    type: 'get',
    url: '/discardedEmailsGroupByDiscardNumber/',
    data: {
      owner_group: window.filterVals.ownerGroup,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (data) {
      console.log(data)
      var respData = data, dataArray = [], categories = [], plotData = [], downloadData = [["Email type", 'Value (#)']];
      for(var i in respData){
        var findRec = dataArray.find(rec => rec[0] === respData[i].email_type)
        if(!findRec) {
          categories.push(respData[i].email_type)
          dataArray.push([respData[i].email_type, respData[i].count])
        }
        else findRec[1] += respData[i].count
      }
      categories = categories.sort(function(a, b){return a.substring(1) - b.substring(1)});
      for(var i in categories) {
        let responseName = getResponseName(categories[i])
        plotData.push([ responseName, dataArray.find(rec => rec[0] === categories[i])[1] ])
        var innerRowData = []
        innerRowData.push(categories[i])
        innerRowData.push(dataArray.find(rec => rec[0] === categories[i])[1])
        downloadData.push(innerRowData);
      }
      window.createXLSLFormatObj['discardedEmailsGroupByDiscardNumberChart'] = [...downloadData]
      drawColumnChart('discardedEmailsGroupByDiscardNumberChart', plotData)
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getResponseName(name) {
  if(name == 'd1') return 'First response'
  else if(name == 'd2') return 'Second response'
  else if(name == 'd3') return 'Third response'
  else if(name == 'd4') return 'Fourth response'
  else if(name == 'd5') return 'Fifth response'
  else return name
}

function drawColumnChart(chartId, dataArray) {
  console.log(dataArray)
  $('#'+chartId+'Head').text('Distribution of discarded responses')
  window.chartConfig[chartId] = Highcharts.chart(chartId, {
    chart: {
      type: 'column',
      height: 300
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'category',
      title: {
        text: ''
      }
    },
    yAxis: {
      min: 0,
      allowDecimals: false,
      title: {
        text: 'No. of responses'
      }
    },
    credits: false,
    colors: ['#33cc00'],
    plotOptions: {
      column: {
        maxPointWidth: 20,
        colorByPoint: true
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    tooltip: {
      pointFormat: 'Responses <b>{point.y:.0f}</b>'
    },
    series: [{
      name: 'Responses',
      data: dataArray,
      dataLabels: {
        enabled: true,
        format: '{point.y:.0f}'
      }
    }]
  });
}

function buildDiscardedTimeline() {
  $.ajax({
    type: 'get',
    url: '/getDiscardedEmails/',
    data: {
      mode: window.discardedTabMode,
      owner_group: window.filterVals.ownerGroup,
      product: window.filterVals.product,
      severity: window.filterVals.severity,
      start_date: window.filterVals.startDate,
      end_date: window.filterVals.endDate
    },
    success: function (respData) {
      console.log(respData)
      let dataArray = [], downloadData = [["Date Time"]];
      downloadData[0].push((window.discardedTabMode === '%' ? 'Value (%)' : 'Value (#)'))
      let totalRecords = [], records = respData.discarded_data
      if (window.discardedTabMode === '%') {
        totalRecords = respData.initial_data.sort((a, b) => {
          if (moment(a.i_time_stamp, 'DD-MM-YYYY').valueOf() < moment(b.i_time_stamp, 'DD-MM-YYYY').valueOf()) return -1;
          if (moment(a.i_time_stamp, 'DD-MM-YYYY').valueOf() > moment(b.i_time_stamp, 'DD-MM-YYYY').valueOf()) return 1;
          return 0;
        })
      } else {
        totalRecords = respData.discarded_data.sort((a, b) => {
          if (moment(a.d_time_stamp, 'DD-MM-YYYY').valueOf() < moment(b.d_time_stamp, 'DD-MM-YYYY').valueOf()) return -1;
          if (moment(a.d_time_stamp, 'DD-MM-YYYY').valueOf() > moment(b.d_time_stamp, 'DD-MM-YYYY').valueOf()) return 1;
          return 0;
        })
      }
      for (var i in totalRecords) {
        let formatDate = window.discardedTabMode === '%' ? totalRecords[i].i_time_stamp : totalRecords[i].d_time_stamp, date = moment(formatDate, 'DD-MM-YYYY').valueOf() + 19800000,
          percent = 0, findDiscarded = records.find(rec => rec.d_time_stamp === formatDate)
        if (findDiscarded) percent = parseFloat(((totalRecords[i].count / (findDiscarded.count + totalRecords[i].count)) * 100).toFixed(2))
        dataArray.push([date, (window.discardedTabMode === '%' ? percent : totalRecords[i].count)])
        downloadData.push([formatDate, (window.discardedTabMode === '%' ? percent : totalRecords[i].count)]);
      }
      window.createXLSLFormatObj['discardedEmailsChart'] = [...downloadData]
      drawTimelineBarchart('discardedEmailsChart', dataArray, window.discardedTabMode)
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function drawTimelineBarchart(chartId, data, mode) {
  console.log(data)
  $('#'+chartId+'Head').text('Discarded responses as a function of initial responses (d/(d+i))')
  /* if (mode == '#') {
    window.chartConfig[chartId] = Highcharts.stockChart(chartId, {
      chart: {
        height: 300
      },
      rangeSelector: {
        selected: 1,
        enabled: false
      },
      yAxis: {
        min: 0
      },
      plotOptions: {
        series: {
          type: 'spline',
          spacingTop: 10,
          showInNavigator: true
        }
      },
      exporting: {
        enabled: false
      },
      tooltip: {
        pointFormat: '<span style="color:{series.color}">Responses </span>: <b>{point.y}</b> %<br/>',
        split: true
      },
      credits: false,
      series: [{
        type: 'spline',
        name: 'No.of responses',
        data: data,
        dataGrouping: {
          units: [
            ['hour', [1, 3, 6, 12]],
            ['day', [1]],
            ['week', [1]],
            ['month', [1, 3, 6]],
            ['year', [1]]
          ]
        },
        color: '#FFA500' //getRandColor(false, 3),
        // borderColor: getRandColor(true, 2),
        // borderWidth: 1,
      }]
    });
  } else { */
    window.chartConfig[chartId] = Highcharts.stockChart(chartId, {
      chart: {
        alignTicks: true,
        height: 300
      },
      rangeSelector: {
        selected: 1,
        enabled: false
      },
      title: {
        text: ""
      },
      xAxis: {
        type: 'datetime'
      },
      exporting: {
        enabled: false
      },
      credits: false,
      tooltip: {
        pointFormat: '<span style="color:{series.color}">Responses </span>: <b>{point.y}</b> '+(mode == '%'? mode : '')+'<br/>',
        split: true
      },
      series: [{
        maxPointWidth: 20,
        type: 'column',
        name: 'No.of responses',
        data: data,
        dataGrouping: {
          units: [
            ['hour', [1, 3, 6, 12]],
            ['day', [1]],
            ['week', [1]],
            ['month', [1, 3, 6]],
            ['year', [1]]
          ]
        },
        color: '#FFA500' // getRandColor(false, 3),
        // borderColor: getRandColor(true, 2),
        // borderWidth: 1,
      }]
    });
  // }
}

function getRandColor(same, darkness) {
  //6 levels of brightness from 0 to 5, 0 being the darkest
  var rgb = [];
  if(same && lastColor) {
      rgb = lastColor;
  } else {
      rgb = [Math.random() * 256, Math.random() * 256, Math.random() * 256];
  }
  var mix = [darkness * 51, darkness * 51, darkness * 51]; //51 => 255/5
  var mixedrgb = [rgb[0] + mix[0], rgb[1] + mix[1], rgb[2] + mix[2]].map(function (x) {
      return Math.round(x / 2.0)
  })
  lastColor = rgb;
  return "rgb(" + mixedrgb.join(",") + ")";
}
var allTicketsGroupBy = {},
createXLSLFormatObj = {},
chartConfig = {},
productsOldData = ['All'],
severitiesOldData = ['All'],
filterOption = false,
buttonsShowFlag = false,
ownerGroupResponses = {},
ratingColorAndName = [
  { lowerName: 'perfect', name: 'Perfect', color: 'rgb(51, 204, 0)' },
  { lowerName: 'good', name: 'Good', color: 'rgb(255, 165, 0)' },
  { lowerName: 'poor', name: 'Poor', color: 'rgb(237, 86, 27)' }];
$(document).ready(function () {
  getAllTicketsGroupBy();
  // $('#products').val('All');
  // $('#severities').val('All');
  $('#products').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw active" id="productButton0" onclick="fetchProductSelected(`0`)">All</button>')
  $('#severities').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw active" id="severityButton0" onclick="fetchSeveritySelected(`0`)">All</button>')
});

function filterOptionsBtn() {
  if(window.filterOption) {
    $('#filterOptions').hide()
  } else {
    $('#filterOptions').show()
  }
  window.filterOption = !window.filterOption
}

/* $('#products').change(function () {
  window.productsOldData = dropDownOnChange('products', window.productsOldData)
})
$('#severities').change(function () {
  window.severitiesOldData = dropDownOnChange('severities', window.severitiesOldData)
}) */

function fetchProductSelected(index) {
  if(index == 0) {
    $('#products button').removeClass('active')
    $('#productButton'+index).addClass('active')
    window.productsOldData = ['All']
  } else {
    if(window.productsOldData[0] == 'All') {
      $('#productButton0').removeClass('active')
      window.productsOldData.splice(0, 1)
    }
    let item = window.allTicketsGroupBy.products[index]
    if(window.productsOldData.includes(item)) {
      let itemIndex = window.productsOldData.indexOf(item)
      $('#productButton'+index).removeClass('active')
      window.productsOldData.splice(itemIndex, 1)
    } else {
      $('#productButton'+index).addClass('active')
      window.productsOldData.push(item)
    }
  }
}

function fetchSeveritySelected(index) {
  if(index == 0) {
    $('#severities button').removeClass('active')
    $('#severityButton'+index).addClass('active')
    window.severitiesOldData = ['All']
  } else {
    if(window.severitiesOldData[0] == 'All') {
      $('#severityButton0').removeClass('active')
      window.severitiesOldData.splice(0, 1)
    }
    let item = window.allTicketsGroupBy.severities[index]
    if(window.severitiesOldData.includes(item)) {
      let itemIndex = window.severitiesOldData.indexOf(item)
      $('#severityButton'+index).removeClass('active')
      window.severitiesOldData.splice(itemIndex, 1)
    } else {
      $('#severityButton'+index).addClass('active')
      window.severitiesOldData.push(item)
    }
  }
}

/* function dropDownOnChange(dropDownId, oldData) {
  console.log(oldData)
  var totalOptions = document.getElementById(dropDownId).length,
    data = $('#' + dropDownId).val();
  console.log(data)
  console.log(data[0])
  console.log($('#' + dropDownId).val().length)
  if (data[0] === 'All' && $('#' + dropDownId).val().length > 1) {
    console.log(data)
    if (oldData[0] === 'All') {
      console.log(oldData[0])
      document.getElementById(dropDownId + '_0').checked = false
      data.splice(0, 1);
      console.log(data)
      $("#" + dropDownId + "Div .multi-select-container .multi-select-button").text(' ' + data)
      $('#' + dropDownId).val(data);
      return $('#' + dropDownId).val();
    } else {
      document.getElementById(dropDownId + '_0').checked = true
      $('#' + dropDownId).val(data.splice(0, 1))
      for (var i = 1; i < totalOptions; i++) {
        if ($('#' + dropDownId + '_' + i).is(':checked')) document.getElementById(dropDownId + '_' + i).checked = false
      }
      $("#" + dropDownId + "Div .multi-select-container .multi-select-button").text(' ' + $('#' + dropDownId).val())
      return $('#' + dropDownId).val();
    }
  } else return data;
} */

function getAllTicketsGroupBy() {
  $.ajax({
    type: 'get',
    url: '/getAllTicketsGroupByBU',
    data: {},
    success: function (records) {
      console.log(records)
      window.allTicketsGroupBy['data'] = records
      window.allTicketsGroupBy['BUs'] = []
      let products = [], severities = []
      for(let i in records) {
        if(!window.allTicketsGroupBy.BUs.includes(records[i].bu))
          window.allTicketsGroupBy.BUs.push(records[i].bu)
        if(!products.includes(records[i].product))
          products.push(records[i].product)
        if(!severities.includes(records[i].severity))
          severities.push(records[i].severity)
      }
      window.allTicketsGroupBy.BUs.sort()
      products.sort()
      severities.sort()
      products.splice(0, 0, 'All')
      severities.splice(0, 0, 'All')
      for(let i in window.allTicketsGroupBy.BUs){
        let bu = window.allTicketsGroupBy.BUs[i]
        $('#buButtonsDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="buButton'+i+'" onclick="fetchDataFilterButton(`bu`,`' + i + '`)">'+ bu +'</button>')
      }
      for(let i = 1; i < products.length; i++){
        // $('#products').append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
        $('#products').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="productButton'+i+'" onclick="fetchProductSelected(`' + i + '`)">'+ products[i] +'</button>')
      }
      for(let i = 1; i < severities.length; i++){
        // $('#severities').append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
        $('#severities').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="severityButton'+i+'" onclick="fetchSeveritySelected(`' + i + '`)">'+ severities[i] +'</button>')
      }
      window.allTicketsGroupBy['products'] = products
      window.allTicketsGroupBy['severities'] = severities
      // $('#products').multiSelect();
      // $('#severities').multiSelect();
      getAllTicketsGroupByTime()
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function buttonsShowHide() {
  let keys = ['bu', 'desk', 'ownerGroup']
  if(window.buttonsShowFlag) {
    for(let i in keys) {
      $('#'+ keys[i] + 'ButtonsDiv').hide()
      $('#'+keys[i]+'Selected').show()
    }
  } else {
    for(let i in keys) {
      $('#'+ keys[i] + 'ButtonsDiv').show()
      $('#'+keys[i]+'Selected').hide()
    }
  }
  window.buttonsShowFlag = !window.buttonsShowFlag
}

function fetchDataFilterButton(fromKey, index) {
  $('#'+fromKey+'ButtonsDiv button').removeClass('active')
  $('#'+fromKey+'Button'+index).addClass('active')
  if(fromKey == 'bu') {
    $('#deskButtonsDiv').html('')
    window.allTicketsGroupBy[fromKey] = window.allTicketsGroupBy.BUs[index]
    window.allTicketsGroupBy['desks'] = []
    let buRecords = window.allTicketsGroupBy.filteredData.filter(o => o.bu == window.allTicketsGroupBy.bu)
    prepareDataForColumnChart(buRecords, fromKey)
    for(let i in buRecords){
      if(!window.allTicketsGroupBy.desks.includes(buRecords[i].desk))
        window.allTicketsGroupBy.desks.push(buRecords[i].desk)
    }
    window.allTicketsGroupBy.desks.sort()
    for(let i in window.allTicketsGroupBy.desks){
      let desk = window.allTicketsGroupBy.desks[i]
      $('#deskButtonsDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="deskButton'+i+'" onclick="fetchDataFilterButton(`desk`,`' + i + '`)">'+ desk +'</button>')
    }
    fetchDataFilterButton('desk', 0)
  } else if(fromKey == 'desk') {
    $('#ownerGroupButtonsDiv').html('')
    window.allTicketsGroupBy[fromKey] = window.allTicketsGroupBy.desks[index]
    window.allTicketsGroupBy['ownerGroups'] = []
    let deskRecords = window.allTicketsGroupBy.filteredData.filter(o => o.bu == window.allTicketsGroupBy.bu && o.desk == window.allTicketsGroupBy.desk)
    // prepareDataForColumnChart(deskRecords, fromKey)
    create3dPieChart(deskRecords, fromKey, window.allTicketsGroupBy.desk)
    for(let i in deskRecords){
      let ownerGroup = deskRecords[i]['owner_group']
      if(!window.allTicketsGroupBy.ownerGroups.includes(ownerGroup))
        window.allTicketsGroupBy.ownerGroups.push(ownerGroup)
    }
    window.allTicketsGroupBy.ownerGroups.sort()
    for(let i in window.allTicketsGroupBy.ownerGroups){
      let ownerGroup = window.allTicketsGroupBy.ownerGroups[i]
      $('#ownerGroupButtonsDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="ownerGroupButton'+i+'" onclick="fetchDataFilterButton(`ownerGroup`,`' + i + '`)">'+ ownerGroup +'</button>')
    }
    fetchDataFilterButton('ownerGroup', 0)
  } else {
    window.allTicketsGroupBy[fromKey] = window.allTicketsGroupBy.ownerGroups[index]
    let ownerGroupRecords = window.allTicketsGroupBy.filteredData.filter(o => o.bu == window.allTicketsGroupBy.bu && o.desk == window.allTicketsGroupBy.desk && o['owner_group'] == window.allTicketsGroupBy.ownerGroup)
    prepareDataForColumnChart(ownerGroupRecords, fromKey)
    
    let records = window.allTicketsGroupBy.filteredTimeData.filter(o => o.bu == window.allTicketsGroupBy.bu && o.desk == window.allTicketsGroupBy.desk && o['owner_group'] == window.allTicketsGroupBy.ownerGroup)
    prepareDataFoAreaChart(records, fromKey, window.allTicketsGroupBy.ownerGroup)
  }
  $('#'+fromKey+'Selected').text(window.allTicketsGroupBy[fromKey])
}

function getAllTicketsGroupByTime() {
  $.ajax({
    type: 'get',
    url: '/getAllTickets/?owner_group=All' + '&product=All' + "&severity=All" + '&start_date=""' + '&end_date=""',
    data: {
    },
    success: function (records) {
      console.log(records)
      window.allTicketsGroupBy['timeData'] = records
      filterDataAndPlot()
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function filterDataAndPlot() {
  let products = [...window.productsOldData],
    severities = [...window.severitiesOldData],
    records = [...window.allTicketsGroupBy.data]
    timeRecords = [...window.allTicketsGroupBy.timeData]

  if (products[0] != 'All') {
    records = records.filter(function (item) {
      return products.includes(item.product);
    })
    timeRecords = timeRecords.filter(function (item) {
      return products.includes(item.product);
    })
  }
  if (severities[0] != 'All') {
    records = records.filter(function (item) {
      return severities.includes(item.severity);
    })
    timeRecords = timeRecords.filter(function (item) {
      return severities.includes(item.severity);
    })
  }
  window.allTicketsGroupBy['filteredData'] = [...records]
  window.allTicketsGroupBy['filteredTimeData'] = [...timeRecords]
  fetchDataFilterButton('bu', 0)
  createPIEChart(records, 'ticketsOverview')
  prepareDataFoAreaChart(timeRecords, 'ticketsOverview')
}

function prepareDataFoAreaChart(records, fromKey, name){
  let dataArray = [], downloadData = [['Date', 'Poor', 'Good', 'Perfect']]
  records = records.sort((a, b) => moment(a.ticket_booking_time, 'DD-MM-YYYY').valueOf() - moment(b.ticket_booking_time, 'DD-MM-YYYY').valueOf())
  for (var i in records) {
    var rating = ratingColorAndName.find(rec => rec.lowerName === records[i].ticket_overall_score),
    date = moment(records[i].ticket_booking_time, 'DD-MM-YYYY').valueOf() + 19800000,
    actualDate = records[i].ticket_booking_time
    if (rating) {
      if(!downloadData.find(r => r[0] == actualDate)) downloadData.push([actualDate, 0, 0, 0])
      let index = (ratingColorAndName.indexOf(rating) + 1)
      downloadData.find(r => r[0] == actualDate)[index] += records[i].count
      
      var findRating = dataArray.find(rec => rec.name === rating.name);
      if (!findRating) {
        var ratingData = []
        ratingData.push([date , records[i].count])
        dataArray.push({ data: ratingData, name: rating.name, color: rating.color })
      } else {
        var findDate = findRating.data.find(rec => rec[0] === date )
        if (findDate) findDate[1] += records[i].count
        else findRating.data.push([date , records[i].count])
      }
    }
  }
  window.createXLSLFormatObj[fromKey +'AreaChart'] = [...downloadData]
  dataArray.sort((a, b) => { return ratingColorAndName.indexOf(ratingColorAndName.find(o => o.name == a.name)) - ratingColorAndName.indexOf(ratingColorAndName.find(o => o.name == b.name)) })
  console.log(dataArray)
  createAreaChart(dataArray, fromKey, name)
}

function createAreaChart(data, fromKey, name) {
  console.log(fromKey)
  console.log(data)
  $('#'+ fromKey +'AreaChartHead').text(name ? name : 'Tickets overview, Timeline')
  window.chartConfig[fromKey +'AreaChart'] = Highcharts.chart(fromKey +'AreaChart', {
    chart: {
      type: 'spline',
      height: 300
    },
    title: {
      text: '',
      align: 'left',
      margin: 10,
      useHTML: true,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    exporting: {
      enabled: false
    },
    credits: false,
    xAxis: {
      type: 'datetime',
      /* dateTimeLabelFormats: {
        month: '%e. %b',
        year: '%b'
      },
      title: {
        text: 'Date'
      } */
    },
    yAxis: {
      title: {
        text: ''
      },
      min: 0
    },
    /* tooltip: {
      headerFormat: '<b>{series.name}</b><br>',
      pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
    }, */
    tooltip: {
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
      split: true
    },
    legend: {
      enabled: true,
      // reversed: true,
      align: 'right',
      verticalAlign: 'top',
      itemStyle: {
        color: '#000000',
        fontWeight: 'bold',
        fontSize: 14
      }
    },

    plotOptions: {
      series: {
        marker: {
          enabled: true
        }
      }
    },

    // colors: ['#6CF', '#39F', '#06C', '#036', '#000'],

    // Define the data points. All series have a dummy year
    // of 1970/71 in order to be compared on the same x axis. Note
    // that in JavaScript, months start at 0 for January, 1 for February etc.
    series: data,

    responsive: {
      rules: [{
        condition: {
          maxWidth: 500
        },
        chartOptions: {
          plotOptions: {
            series: {
              marker: {
                radius: 2.5
              }
            }
          }
        }
      }]
    }
  });
}

function create3dPieChart(data, fromKey, name) {
  let  dataArray = [], total = 0;
  for(let i in ratingColorAndName) {
    let rating = ratingColorAndName[i], count = 0,
    findRatingWise = data.filter(o => o.ticket_overall_score == rating.lowerName)
    for(let c in findRatingWise) {
      count += parseInt(findRatingWise[c].count)
    }
    dataArray.push({'y': 0, 'count': count, 'name': rating.name, 'color': rating.color})
    total += count
  }
  let downloadData = [['Rating', '#', '%']]
  for(let i in dataArray) {
    let obj = dataArray[i]
    obj.y = (obj.count / total) * 100
    downloadData.push([obj.name, obj.count, obj.y])
  }
  window.createXLSLFormatObj[fromKey+'PieChart'] = [...downloadData]
  console.log(dataArray)

  $('#'+fromKey+'PieChartHead').text((name ? name : 'Tickets overview') + ': '+ total)
  window.chartConfig[fromKey+'PieChart'] = Highcharts.chart(fromKey+'PieChart', {
    chart: {
      type: 'pie',
      options3d: {
        enabled: true,
        alpha: 30
      },
      height: 300,
    },
    title: {
      text: ''
    },
    exporting: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '<b>{point.name}: </b><br/> {point.count} (<b>{point.y: .2f} </b>%)'
    },
    plotOptions: {
      pie: {
        innerSize: 180,
        depth: 30,
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    series: [{
      name: 'Rating',
      data: dataArray
    }]
  });
}

function createPIEChart(data, fromKey, name) {
  let  dataArray = [], total = 0;
  for(let i in ratingColorAndName) {
    let rating = ratingColorAndName[i], count = 0,
    findRatingWise = data.filter(o => o.ticket_overall_score == rating.lowerName)
    for(let c in findRatingWise) {
      count += parseInt(findRatingWise[c].count)
    }
    dataArray.push({'y': 0, 'count': count, 'name': rating.name, 'color': rating.color})
    total += count
  }
  let downloadData = [['Rating', '#', '%']]
  for(let i in dataArray) {
    let obj = dataArray[i]
    obj.y = (obj.count / total) * 100
    downloadData.push([obj.name, obj.count, obj.y])
  }
  window.createXLSLFormatObj[fromKey+'PieChart'] = [...downloadData]
  console.log(dataArray)

  $('#'+fromKey+'PieChartHead').text((name ? name : 'Tickets overview') + ': '+ total)
  window.chartConfig[fromKey+'PieChart'] = Highcharts.chart(fromKey+'PieChart', {
    chart: {
      height: 300
    },
    title: {
      text: '',
      align: 'left',
      margin: 10,
      useHTML: true,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    exporting: {
      enabled: false
    },
    credits: {
      enabled: false
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '<b>{point.name}: </b><br/> {point.count} (<b>{point.y: .2f} </b>%)'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          distance: '-30%',
          enabled: false,
          format: '{point.y: .0f} %',
          style: {
            fontSize: '12px',
            textOutline: false
          }
        },
        center: ['50%', '50%'],
        size: '80%',
        showInLegend: true
      }
    },
    legend: {
      enabled: true
    },
    series: [{
      type: 'pie',
      innerSize: '75%',
      data: dataArray
    }]
  })
}

function prepareDataForColumnChart(records, fromKey) {
  console.log(records)
  let dataObj = {'total': 0, 'data': [], categories: []}
  for(let i in ratingColorAndName) {
    let rating = ratingColorAndName[i], count = 0,
    findRatingWise = records.filter(o => o.ticket_overall_score == rating.lowerName)
    for(let c in findRatingWise) {
      count += parseInt(findRatingWise[c].count)
    }
    dataObj.categories.push(rating.name)
    dataObj.data.push({'y': 0, 'count': count, 'name': rating.name, 'color': rating.color})
    dataObj.total += count
  }
  createColumnChart(dataObj, fromKey)
}

function createColumnChart(dataObj, fromKey) {
  let downloadData = [['Rating', '#', '%']]
  for(let i in dataObj.data) {
    let obj = dataObj.data[i]
    obj.y = (obj.count / dataObj.total) * 100
    downloadData.push([obj.name, obj.count, obj.y])
  }
  console.log(dataObj)
  window.createXLSLFormatObj[fromKey+'ColumnChart'] = [...downloadData]
  $('#'+ fromKey +'ColumnChartHead').text(window.allTicketsGroupBy[fromKey]+': '+ dataObj.total)
  window.chartConfig[fromKey +'ColumnChart'] = Highcharts.chart(fromKey +'ColumnChart', {
    chart: {
      type: 'column',
      height: 300
    },
    title: {
      text: '',
      align: 'left',
      margin: 10,
      useHTML: true,
      style: {
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'roboto'
      }
    },
    legend: {
      enabled: false
    },
    credits: false,
    exporting: false,
    xAxis: {
      categories: dataObj.categories
    },
    yAxis: [{
      min: 0,
      max: 100,
      title: {
        text: ''
      }
    }],
    tooltip: {
      formatter: function () {
        if (this.point.series.name == 'Product')
          return '<b>'+ this.key + ' :</b><br>' + this.point.count + ' (<b>' + Highcharts.numberFormat(this.point.y, 2) + '</b> %)';
        else return false;
      }
    },
    plotOptions: {
      series: {
        point: {
          events: {
            click: function (e) {
              console.log(e)
              if(fromKey == 'ownerGroup') getEmailDetails(e.target.point.category)
            }
          }
        }
      },
      column: {
        grouping: false,
        shadow: false,
        borderWidth: 0,
      }
    },
    series: [{
      name: 'Projection',
      // color: ['#E4F7F2', '#FCEFE3', '#FCEFE3'],
      data: [{'y': 100, 'color': '#FCE3E3'},
            {'y': 100, 'color': '#FCEFE3'},
            {'y': 100, 'color': '#E4F7F2'}],
      pointPadding: 0.3,
      pointPlacement: -0.2
    }, {
      // color: '#12B78E',
      name: 'Product',
      data: dataObj.data,
      pointPadding: 0.3,
      pointPlacement: -0.2,
      cursor: fromKey == 'ownerGroup' ? 'pointer' : 'default'
    }]
  });
}

function getEmailDetails(rating) {
  $.ajax({
    type: 'get',
    url: '/getOwnerGroupResponsesByRating',
    data: {
      owner_group: window.allTicketsGroupBy.ownerGroup,
      rating: rating
    },
    success: function (data) {
      console.log(data)
      window.ownerGroupResponses = data
      let ele = window.document.getElementsByClassName('testclass');
      ele[0].click();
      $('#responsesModalLabel').html('Owner group: '+ window.allTicketsGroupBy.ownerGroup + '&emsp13; Rating: '+ rating)
      prepareTables(data)
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function prepareTables(data) {
  let keys = Object.keys(data), index = 0
  setTable(keys[index])
  function setTable(key) {
    let html = '', records = []
    if(key == 'initial_emails')
      records = data[key].sort((a, b) => moment(b.i_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').startOf('day').valueOf() - moment(a.i_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').startOf('day').valueOf())
    else if(key == 'subsequent_emails')
      records = data[key].sort((a, b) => moment(b.si_timestamp, 'YYYY-MM-DD[T]HH:mm:ss').startOf('day').valueOf() - moment(a.si_timestamp, 'YYYY-MM-DD[T]HH:mm:ss').startOf('day').valueOf())
    else if(key == 'discarded_emails')
      records = data[key].sort((a, b) => moment(b.d_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').startOf('day').valueOf() - moment(a.d_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').startOf('day').valueOf())

    for(let i in records) {
      let rec = records[i]
      html += '<tr> <td>'+ rec.ticket_number +'</td>'
      if(key == 'initial_emails')
        html += '<td>'+ moment(rec.i_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').format('MMM DD - hh:mm A') +'</td>'
      else if(key == 'subsequent_emails')
        html += '<td>'+ moment(rec.si_timestamp, 'YYYY-MM-DD[T]HH:mm:ss').format('MMM DD - hh:mm A') +'</td>'
      else if(key == 'discarded_emails')
        html += '<td>'+ moment(rec.d_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').format('MMM DD - hh:mm A') +'</td>'
      html += '<td class="emailContentCall" id="'+ rec.ticket_number +'&'+key+'" style="cursor: pointer"> <span style="color: blue;"> See full Response </span> &emsp; <i class="mdi mdi-arrow-right-drop-circle"></i></td> </tr>'
    }
    $('#'+key+'_body').html(html)
    index++
    if(index < keys.length) setTable(keys[index])
  }
}

$(document).on("click", '.emailContentCall', function() {    
  let record = window.ownerGroupResponses[this.id.split('&')[1]].find(o => o.ticket_number == this.id.split('&')[0])
  $.ajax({
    type: 'get',
    url: '/getEmailContent',
    data: {
      ticket_number: record.ticket_number,
      email_number: record.email_number,
      email_type: record.email_type
    },
    success: function (data) {
      console.log(data)
      let ele = window.document.getElementsByClassName('testclass1');
      ele[0].click();
      let html = '<div style="margin: 0" class="form-group row">' +
        '<div class="col"> <label>Timestamp</label> <div> <b>' + moment(record.i_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').format('MMM DD - hh:mm A') + '</b> </div> </div>' +
        '<div class="col"> <label>PoA</label> <div> <b>' + record.i_poa + '</b> </div> </div>' +
        '<div class="col"> <label>Service Restore</label> <div> <b>' + record.i_svc_rstr + '</b> </div> </div>' +
        '<div class="col"> <label>Link stat check</label> <div> <b>' + record.i_link_stat_chk + '</b> </div> </div>' +
        '<div class="col"> <label>ETR</label> <div> <b>' + record.i_etr + '</b> </div> </div>' +
        '<div class="col"> <label>ETR time</label> <div> <b>' + record.i_etr_time + '</b> </div> </div>' +
        '</div> <div style="margin: 0" class="form-group row">' +
        '<div class="col"> <label>Logs</label> <div> <b>' + record.i_logs + '</b> </div> </div>' +
        '<div class="col"> <label>Resp time</label> <div> <b>' + record.i_resp_time + '</b> </div> </div>' +
        '<div class="col"> <label>Time update</label> <div> <b>' + record.i_time_update + '</b> </div> </div>' +
        '<div class="col"> <label>CD</label> <div> <b>' + record.i_cust_dep + '</b> </div> </div>' +
        '<div class="col"> <label>Time compliance</label> <div> <b>' + record.i_time_compliance + '</b> </div> </div>' +
        '<div class="col"> <label>Response number</label> <div> <b>' + record.email_number + '</b> </div> </div>' +
        '<div class="col"> <label>Response type</label> <div> <b>' + record.email_type + '</b> </div> </div> </div>'
      $('#responseViewModalLabel').html('Ticket Number <span style="color: #6358D5"><b>'+ record.ticket_number +'</b></span>')
      $('#ticketDetails').html(html)
      $('#emailResponse').text(data)
    },
    error: function (error) {
      console.log(error)
    }
  });
});

function prepareTable(data) {
  var table = $('#initial_emails_table').DataTable({
    "data": data,
    'columns': [
      {
        'className': 'details-control',
        'orderable': false,
        'data': null,
        'defaultContent': ''
      },
      { 'data': 'ticket_number' },
      {
        "render": function (data, type, full, meta) {
          return moment(full.i_time_stamp, 'YYYY-MM-DD[T]HH:mm:ss').format('MMM DD, hh:mm A');
        }
      }
    ],
    'order': [[1, 'asc']]
  });

  // Add event listener for opening and closing details
  $('#initial_emails_table tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {
      // Open this row
      row.child(format(row.data(), 'initial_emails')).show();
      tr.addClass('shown');
    }
  });
}

/* Formatting function for row details - modify as you need */
function format ( d, key) {
  console.log(d)
  console.log(key)
  // `d` is the original data object for the row
  $.ajax({
    type: 'get',
    url: '/getEmailContent',
    data: {
      ticket_number: d.ticket_number,
      email_number: d.email_number,
      email_type: d.email_type
    },
    success: function (data) {
      let html = '<table cellpadding="20">' +
        '<tr>' +
        '<td>100</td>' +
        '<td>200</td>' +
        '<td>300</td>' +
        '</tr>' +
        '<tr>' +
        '<td>400</td>' +
        '<td>500</td>' +
        '<td>600</td>' +
        '</tr>' +
        '</table>' +
        '<table>' +
        '<tr>' +
        '<th>Response Content</th>' +
        '</tr>' +
        '<tr>' +
        '<td>' + data + '</td>' +
        '</tr>' +
        '</table>'
      return html
    },
    error: function (error) {

    }
  });
  /* return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
      '<tr>'+
          '<td>Full name:</td>'+
          '<td>'+d.i_time_stamp+'</td>'+
      '</tr>'+
      '<tr>'+
          '<td>Extension number:</td>'+
          '<td>'+d.ticket_number+'</td>'+
      '</tr>'+
      '<tr>'+
          '<td>Extra info:</td>'+
          '<td>And any further details here (images etc)...</td>'+
      '</tr>'+
  '</table>'; */
}

function getEmailContent(d) {
  $.ajax({
    type: 'get',
    url: '/getEmailContent',
    data: {
      ticket_number: d.ticket_number,
      email_number: d.email_number,
      email_type: d.email_type
    },
    success: function (data) {
      console.log(data)
      let html = ''
      if(key == 'initial_emails'){
        html = '<table>'+
        '<tr>'+
          '<td>100</td>'+
          '<td>200</td>' +
          '<td>300</td>'+
        '</tr>'+
        '<tr>'+
          '<td>400</td>'+
          '<td>500</td>'+
          '<td>600</td>'+
        '</tr>'+
      '</table>'+
      '<table>'+
        '<tr>'+
          '<th>Response Content</th>'+
        '</tr>'+
        '<tr>'+
          '<td>'+ data +'</td>'+
        '</tr>'+
      '</table>'

      }
      return html
    },
    error: function (error) {

    }
  });
}

function fetchDownloadData(file, container) {
  /* File Name */
  var filename = file + moment().format('DD-MM-YYYY hh:mm a') + ".xlsx";
  /* Sheet Name */
  var ws_name = "data";

  var wb = XLSX.utils.book_new(),
    ws = XLSX.utils.aoa_to_sheet(window.createXLSLFormatObj[container]);

  /* Add worksheet to workbook */
  XLSX.utils.book_append_sheet(wb, ws, ws_name);

  /* Write workbook and Download */
  XLSX.writeFile(wb, filename);
}

function downloadChartAs(filename, container, type) {
  let html = '<span> <b> Product : </b>' + window.productsOldData + '</span> <br>' +
    '<span> <b> Severity : </b>' + window.severitiesOldData + '</span>',
  title = $('#'+container+'Head').text(),
  downloadType = type == 'PDF' ? 'application/pdf' : 'image/png'
  window.chartConfig[container].exportChart({
    type: downloadType,
    filename: filename + moment().format('DD-MM-YYYY hh:mm a')
  }, {
    chart: {
      width: '1000',
      marginTop: '100',
      height: '500',
    },
    title: {
      text: title,
      align: 'center'
    },
    subtitle: {
      text: html
    },
  });
}
var ctx = "${pageContext.request.contextPath}", index = 1, createXLSLFormatObj = {}, chartConfig = {}, filterOptionsCongig = {}
var tabname = 'content'
var globalData = {}
var reasonsList = []
var smartEntColors = ['#00a3cc', '#33cc00', '#ED561B', '#DDDF00', '#6A5ACD', '#FFA500', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#00a3cc', '#33cc00', '#ED561B', '#DDDF00', '#6A5ACD', '#FFA500', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
$(document).ready(function () {
  screenResolutionMetrics();
  // Tab-1 Calls
  getComplianceChartData(0);
  getSpecificationAccuracyChartData(1);
  getFaultSegmentAccuracyChartData(2);
  getTimeComplianceChartData(3);

  // Tab-2 Calls
  getRFOReasonNames()

});

function getComplianceChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOComplianceResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['complianceData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#firstTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#firstTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#firstTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#firstTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#firstTab_product"+index).prepend('<option value="All">All</option>')
      $("#firstTab_severity"+index).prepend('<option value="All">All</option>')
      $("#firstTab_owner_group"+index).val('All')
      $("#firstTab_product"+index).val('All')
      $("#firstTab_severity"+index).val('All')
      
      prepareTab1ChartData(index, 'compliance');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function prepareTab1ChartData(index, dataKey) {
  let owner_group = $("#firstTab_owner_group"+index).val()
  let product = $("#firstTab_product"+index).val()
  let severity = $("#firstTab_severity"+index).val()
  let dataArray = [...window.globalData[dataKey+'Data']]
  let total = 0, plotData = [{ name: 'Compliant', count: 0, color: '#33cc00' }, { name: 'Non-Compliant', count: 0, color: '#FFA500' }]
  if(index == 1 || index == 2) plotData = [{ name: 'Accurate', count: 0, color: '#33cc00' }, { name: 'Inaccurate', count: 0, color: '#FFA500' }]
  
  if(owner_group != 'All'){
    dataArray = dataArray.filter(o => o['owner_group'] == owner_group)
  }
  if(product != 'All') {
    dataArray = dataArray.filter(o => o.product == product)
  }
  if(severity != 'All') {
    dataArray = dataArray.filter(o => o.severity == severity)
  }
  console.log(dataArray)
  let key = ['RFO_comp', 'R1_comp', 'R2_comp', 'RFO_timecomp']
  for(let i in dataArray){
    if(dataArray[i][key[index]] == 1) plotData[0].count += dataArray[i].count
    else plotData[1].count += dataArray[i].count
    total += dataArray[i].count
  }
  console.log(plotData)
  drawPIEChart(dataKey+'Chart', plotData, total)
}

function getSpecificationAccuracyChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOSpecificationAccuracyResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['specificationAccuracyData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#firstTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#firstTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#firstTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#firstTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#firstTab_product"+index).prepend('<option value="All">All</option>')
      $("#firstTab_severity"+index).prepend('<option value="All">All</option>')
      $("#firstTab_owner_group"+index).val('All')
      $("#firstTab_product"+index).val('All')
      $("#firstTab_severity"+index).val('All')
      
      prepareTab1ChartData(index, 'specificationAccuracy');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getFaultSegmentAccuracyChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOFaultSegmentAccuracyResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['faultSegmentAccuracyData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#firstTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#firstTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#firstTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#firstTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#firstTab_product"+index).prepend('<option value="All">All</option>')
      $("#firstTab_severity"+index).prepend('<option value="All">All</option>')
      $("#firstTab_owner_group"+index).val('All')
      $("#firstTab_product"+index).val('All')
      $("#firstTab_severity"+index).val('All')
      
      prepareTab1ChartData(index, 'faultSegmentAccuracy');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getTimeComplianceChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOTimeComplianceResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['timeComplianceData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#firstTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#firstTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#firstTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#firstTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#firstTab_product"+index).prepend('<option value="All">All</option>')
      $("#firstTab_severity"+index).prepend('<option value="All">All</option>')
      $("#firstTab_owner_group"+index).val('All')
      $("#firstTab_product"+index).val('All')
      $("#firstTab_severity"+index).val('All')
      
      prepareTab1ChartData(index, 'timeCompliance');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getRFOReasonNames() {
  $.ajax({
    type: 'get',
    url: '/getRFOReasonNames',
    data: {},
    success: function (records) {
      console.log(records)
      for(let i in records) {
        window.reasonsList.push(records[i].trim())
      }
      window.reasonsList.sort()
      for(let i in window.reasonsList) {
        $("#reasonsList").append('<option vlaue='+window.reasonsList[i]+'>'+window.reasonsList[i]+'</option>')
      }
      getRFOComplianceByReasonChartData(0)
      getRFOTimeComplianceByReasonChartData(1)
      getMisClassificationSpecificationByReasonChartData(2)
      getMisClassificationFaultSegmentByReasonChartData(3)
      getRFOMissingParametersByReasonChartData(4)
      getRFOTimeSentByReasonChartData(5)
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getRFOComplianceByReasonChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOComplianceByReasonResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['complianceByReasonData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#secondTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#secondTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#secondTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#secondTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#secondTab_product"+index).prepend('<option value="All">All</option>')
      $("#secondTab_severity"+index).prepend('<option value="All">All</option>')
      $("#secondTab_owner_group"+index).val('All')
      $("#secondTab_product"+index).val('All')
      $("#secondTab_severity"+index).val('All')

      prepareTab2ChartData(0, 'complianceByReason');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getRFOTimeComplianceByReasonChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOTimeComplianceByReasonResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['timeComplianceByReasonData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#secondTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#secondTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#secondTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#secondTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#secondTab_product"+index).prepend('<option value="All">All</option>')
      $("#secondTab_severity"+index).prepend('<option value="All">All</option>')
      $("#secondTab_owner_group"+index).val('All')
      $("#secondTab_product"+index).val('All')
      $("#secondTab_severity"+index).val('All')
      
      prepareTab2ChartData(1, 'timeComplianceByReason');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function prepareTab2ChartData(index, dataKey) {
  let reason = $('#reasonsList').val()
  let owner_group = $("#secondTab_owner_group"+index).val()
  let product = $("#secondTab_product"+index).val()
  let severity = $("#secondTab_severity"+index).val()
  let dataArray = [...window.globalData[dataKey+'Data']]
  let total = 0, plotData = []
  
  if(owner_group != 'All'){
    dataArray = dataArray.filter(o => o['owner_group'] == owner_group)
  }
  if(product != 'All') {
    dataArray = dataArray.filter(o => o.product == product)
  }
  if(severity != 'All') {
    dataArray = dataArray.filter(o => o.severity == severity)
  }

  if (index == 5) {
    records = dataArray.filter(o => o.Reason1.trim() == reason)

    let categories = [],
      final_result = [
        { 'range': '-60 & below', 'count': 0 },
        { 'range': '-50', 'count': 0 },
        { 'range': '-40', 'count': 0 },
        { 'range': '-30', 'count': 0 },
        { 'range': '-20', 'count': 0 },
        { 'range': '-10', 'count': 0 },
        { 'range': '0', 'count': 0 },
        { 'range': '10', 'count': 0 },
        { 'range': '20', 'count': 0 },
        { 'range': '30', 'count': 0 },
        { 'range': '40', 'count': 0 },
        { 'range': '50', 'count': 0 },
        { 'range': '60 & above', 'count': 0 }
      ]
    for (let i in records) {
      if (records[i].range_start < -50) {
        var obj_find = final_result.find(obj => obj.range == "-60 & below")
        if (obj_find != undefined)
          obj_find.count += records[i].count
      } else if (records[i].range_end > 50) {
        var obj_find = final_result.find(obj => obj.range == "60 & above")
        if (obj_find != undefined)
          obj_find.count += records[i].count
      } else {
        let rangeName = records[i].range_start < 0 ? records[i].range_start : records[i].range_end
        final_result.find(obj => obj.range == rangeName).count += records[i].count
      }
    }
    console.log(final_result)
    for (let j in final_result) {
      categories.push(final_result[j].range)
      plotData.push(final_result[j].count)
    }
    drawTimeDifferenceBarChart(dataKey+'Chart', categories, plotData);
  } else if(index == 4){
    let preparedData = [], categories = []
    records = dataArray.filter(o => o.Reason1.trim() == reason)
    for (let i in records) {
      path = records[i].missing_parameters
      category = path.substring(path.indexOf(',') + 1, path.lastIndexOf(','));
      series = path.substring(path.lastIndexOf(',') + 1);
      if (!categories.includes(series)) {
        categories.push(series)
      }
      let find_obj = preparedData.find(rec => rec.category == category)
      if (!find_obj)
      preparedData.push({ category: category, series: series, count: parseInt(records[i].count) })
      else {
        find_obj.count += parseInt(records[i].count)
      }
    }
    categories.sort((a, b) => b - a);
    preparedData.sort(function (a, b) {
      if (Number(a.series) === Number(b.series)) {
        return b.count - a.count;
      }
      return Number(b.series) - Number(a.series);
    });
    for (let j in preparedData) {
      let data = []
      for (let k in categories) {
        let val = categories[k] == preparedData[j].series ? preparedData[j].count : 0
        data.push(parseInt(val.toFixed(0)));
      }
      plotData.push({ name: preparedData[j].category, data: data })

    }
    console.log(categories)
    console.log(preparedData)
    console.log(plotData)
    drawMissingParametersChart(dataKey+'Chart', plotData, categories)
  } else if(index == 0 || index == 1) {
    plotData = [{ name: 'Compliant', count: 0, color: '#33cc00' }, { name: 'Non-Compliant', count: 0, color: '#FFA500' }]
    dataArray = dataArray.filter(o => o.Reason1.trim() == reason)
    
    console.log(dataArray)
    let key = ['RFO_comp', 'RFO_timecomp']
    for(let i in dataArray){
      if(dataArray[i][key[index]] == 1) plotData[0].count += dataArray[i].count
      else plotData[1].count += dataArray[i].count
      total += dataArray[i].count
    }
    console.log(plotData)
    drawPIEChart(dataKey+'Chart', plotData, total)
  } else {
    let keyName = (index == 2 ? 'rfo_specification' : 'RFO_FS')
    console.log(dataArray)
    dataArray = dataArray.filter(o => o.Reason1.trim() == reason)
    console.log(dataArray)
    let imageIndex = 0
    for(let i in dataArray) {
      total += dataArray[i].count
      let find = plotData.find(o => o.name == dataArray[i][keyName].trim())
      if(!find) {
        plotData.push({ name: dataArray[i][keyName].trim(), count: dataArray[i].count, color: smartEntColors[imageIndex], data: [] })
        imageIndex++
      } else find.count += dataArray[i].count
    }
    console.log(plotData)
    drawBarChart(dataKey+'Chart', plotData, total)
  }
}

function getMisClassificationSpecificationByReasonChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getMisClassificationSpecificationByReasonResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['misClassificationSpecificationData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#secondTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#secondTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#secondTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#secondTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#secondTab_product"+index).prepend('<option value="All">All</option>')
      $("#secondTab_severity"+index).prepend('<option value="All">All</option>')
      $("#secondTab_owner_group"+index).val('All')
      $("#secondTab_product"+index).val('All')
      $("#secondTab_severity"+index).val('All')

      prepareTab2ChartData(2, 'misClassificationSpecification');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getMisClassificationFaultSegmentByReasonChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getMisClassificationFaultSegmentByReasonResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['misClassificationFaultSegmentData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#secondTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#secondTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#secondTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#secondTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#secondTab_product"+index).prepend('<option value="All">All</option>')
      $("#secondTab_severity"+index).prepend('<option value="All">All</option>')
      $("#secondTab_owner_group"+index).val('All')
      $("#secondTab_product"+index).val('All')
      $("#secondTab_severity"+index).val('All')

      prepareTab2ChartData(3, 'misClassificationFaultSegment');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getRFOTimeSentByReasonChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOTimeSentByReasonResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['rfoTimeSentData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#secondTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#secondTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#secondTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#secondTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#secondTab_product"+index).prepend('<option value="All">All</option>')
      $("#secondTab_severity"+index).prepend('<option value="All">All</option>')
      $("#secondTab_owner_group"+index).val('All')
      $("#secondTab_product"+index).val('All')
      $("#secondTab_severity"+index).val('All')

      prepareTab2ChartData(5, 'rfoTimeSent');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function getRFOMissingParametersByReasonChartData(index) {
  $.ajax({
    type: 'get',
    url: '/getRFOMissingParametersByReasonResults',
    data: {},
    success: function (records) {
      console.log(records)
      window.globalData['missingParametersData'] = [...records]
      let ows = [], products = [], severities = []
      for(let i in records){
        if(!ows.includes(records[i]['owner_group'])) ows.push(records[i]['owner_group'])
        if(!products.includes(records[i].product)) products.push(records[i].product)
        if(!severities.includes(records[i].severity)) severities.push(records[i].severity)
      }
      ows.sort()
      products.sort()
      severities.sort()
      for(let i in ows) {
        $("#secondTab_owner_group"+index).append('<option vlaue='+ows[i]+'>'+ows[i]+'</option>')
      }
      for(let i in products) {
        $("#secondTab_product"+index).append('<option vlaue='+products[i]+'>'+products[i]+'</option>')
      }
      for(let i in severities) {
        $("#secondTab_severity"+index).append('<option vlaue='+severities[i]+'>'+severities[i]+'</option>')
      }
      $("#secondTab_owner_group"+index).prepend('<option value="All">All</option>')
      $("#secondTab_product"+index).prepend('<option value="All">All</option>')
      $("#secondTab_severity"+index).prepend('<option value="All">All</option>')
      $("#secondTab_owner_group"+index).val('All')
      $("#secondTab_product"+index).val('All')
      $("#secondTab_severity"+index).val('All')

      prepareTab2ChartData(4, 'missingParameters');
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function screenResolutionMetrics() {
  console.log(window.screen)
  let deviceWidth = window.screen.width, deviceHeight = window.screen.height
  if (deviceWidth < 600) {
    $('.sidebar-toggle').show()
    $('.box-tools').css('position', 'unset')
    $('.box-title').css('padding-left', '10px')
  } else if (600 < deviceWidth && deviceWidth < 960) {
    if (900 < deviceHeight) {
      $('.logo').show()
    } else {
      $('.sidebar-toggle').show()
    }
  } else if (960 < deviceWidth && deviceWidth < 1264) {
    $('.logo').show()
  } else if (1264 < deviceWidth) {
    $('.logo').show()
  }
};

function filterOptions(div, chartId, chartIndex) {
  if (window.filterOptionsCongig[chartId] == true) {
    $('#'+div+'FilterId'+chartIndex).removeClass('filterOptionActiveColor')
    window.filterOptionsCongig[chartId] = false
    $('#' + chartIndex + div + 2).hide()
  } else {
    $('#'+div+'FilterId'+chartIndex).addClass('filterOptionActiveColor')
    window.filterOptionsCongig[chartId] = true
    $('#' + chartIndex + div + 2).show()
  }
}

function tabChange(tabname){
  if(tabname == 'tab1'){
    $('#reasonsListLi').hide()
  } else if(tabname == 'tab2'){
    $('#reasonsListLi').show()
  }
}

$('#reasonsList').change(function () {
  prepareTab2ChartData(0, 'complianceByReason');
  prepareTab2ChartData(1, 'timeComplianceByReason');
  prepareTab2ChartData(2, 'misClassificationSpecification');
  prepareTab2ChartData(3, 'misClassificationFaultSegment');
  prepareTab2ChartData(4, 'missingParameters');
  prepareTab2ChartData(5, 'rfoTimeSent');
})

function drawPIEChart(container, dataArray, total) {
  for(let i in dataArray) {
    dataArray[i]['y'] = (dataArray[i].count * 100) / total
  }
  window.chartConfig[container] = Highcharts.chart(container, {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: 0,
      plotShadow: false,
      spacingTop: 20,
      spacingBottom: 20,
      spacingLeft: 20,
      spacingRight: 40
    },
    title: {
      text: 'Total : <b>'+ total +'</b>'  
    },
    credits: {
      enabled: false
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '<b>{point.name}: {point.y: .0f} %</b>'
    },
    plotOptions: {
      pie: {
        dataLabels: {
          // distance:'0%', 
          enabled: true,
          formatter: function ()  {
            if(0.01 < this.point.y  && this.point.y < 1)
              return  Highcharts.numberFormat(this.point.y, 2) + ' %';
            else 
              return Highcharts.numberFormat(this.point.y, 0)+ ' %';
          },
          style: {
            fontSize: '12px',
            textOutline: false
          }
        },
        startAngle: -90,
        endAngle: 90,
        center: ['50%', '75%'],
        size: '110%',
        showInLegend: true,
        pointInterval: 24 * 3600 * 1000
      }
    },
    exporting: {
      enabled: false
    },
    legend: {
      labelFormatter: function () {   
        return this.name + ' : '+ this.count;
      }
    },
    series: [{
      type: 'pie',
      innerSize: '50%',
      data: dataArray
    }]
  })
}

function drawBarChart(container, dataArray, total) {
  console.log(dataArray)
  dataArray = dataArray.sort((a,b) => (a.count - b.count)) 
  for(let i in dataArray) {
    dataArray[i].data[0] = (dataArray[i].count * 100) / total
  }
  Highcharts.chart(container, {
    chart: {
      type: 'bar',
      // height: 150,
      margin: [-100, 0, 0, 30],
      // marginBottom: 30,
    },
    exporting: false,
    credits: false,
    title: {
      text: 'Total : <b>'+ total +'</b>',
      useHTML: true,
      margin: 0,
      padding: 0
    },
    xAxis: {
      visible: false,
      categories: ['Reason']
    },
    yAxis: {
      min: 0,
      visible: false,
      title: {
        text: ''
      }
    },
    legend: {
      enabled: true,
      reversed: true,
      padding: 0,
      margin: 0,
      marginTop: 10,
      labelFormatter: function () {
        return this.name + ' : '+ this.userOptions.count;
      }
    },
    plotOptions: {
      bar: {
        maxPointWidth: 40,
        stacking: 'percent',
        tooltip: {
          pointFormat: '<b>{series.name}</b>: {point.y: .0f} %',
        },
        dataLabels: {
          format: '{point.y: .0f} %',
          enabled: true,
          style: {
            color: 'black',
            textOutline: false
          }
        }
      }
    },
    series: dataArray
  });
}

function drawTimeDifferenceBarChart(container, categories, data) {
  console.log(categories)
  console.log(data)
  let seriesData = [];
  let colors = ['#33cc00', '#FFA500']
  for (let i in data) {
    seriesData.push({ y: data[i], color: categories[i].substring(0, 1) == '-' ? colors[0] : colors[1] })
  }
  Highcharts.chart(container, {
    chart: {
      type: 'column'
    },
    title: '',
    xAxis: {
      categories: categories,
      title: {
        text: 'SLA deviation (In minutes)'
      },
      plotLines: [{
        label: {
          text: 'SLA',
          style: {
            fontWeight: 'bold',
            fontSize: '14px'
          },
          rotation: '0'
        },
        color: 'red', // Color value
        dashStyle: 'dashdot', // Style of the plot line. Default to solid
        value: 6, // Value of where the line will appear
        width: 2 // Width of the line    
      }]
    },
    yAxis: {
      title: {
        text: '# of responses'
      }
    },
    credits: false,
    exporting: false,
    plotOptions: {
      series: {
        borderColor: 'black',
        dataLabels: {
          enabled: true,
          formatter: function () {
            if (0 < this.point.y)
              return Highcharts.numberFormat(this.point.y, 0);
            else
              return
          },
          style: {
            textOutline: false
          }
        }
      }
    },

    series: [{
      name: "Range(In min)",
      showInLegend: false,
      data: seriesData
    }]
  });
}

function drawMissingParametersChart(container, data, categories) {
  console.log(data)
  Highcharts.chart(container, {
    chart: {
      type: 'bar'
    },
    colors: ['#DDDF00', '#6A5ACD', '#FFA500', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4', '#00a3cc', '#33cc00', '#ED561B', '#DDDF00', '#6A5ACD', '#FFA500', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
    title: {
      text: ''
    },
    xAxis: {
      categories: categories,
      title: {
        text: 'Missing parameters'
      }
    },
    yAxis: {
      min: 0,
      stackLabels: {
        enabled: true,
        align: 'right'
      },
      title: {
        text: '# of responses'
      }
    },
    credits: false,
    legend: {
      reversed: true
    },
    exporting: false,
    plotOptions: {
      bar: {
        maxPointWidth: 40,
      },
      series: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
          formatter: function () {
            if (0 < this.point.y)
              return Highcharts.numberFormat(this.point.y, 0);
            else
              return
          },
          style: {
            textOutline: false
          }
        }
      }
    },
    series: data
  });
}

function downloadChartAs(type, concatString, index, container, title, filename) {
  var html = '<span> <b> Timeline : </b>' + $('#' + concatString + 'timeLine' + index).val() + '</span> <br>' +
    '<span><b> Owner group : </b>' + $('#' + concatString + 'owner_group' + index).val() + '</span> <br>' +
    '<span> <b> Product : </b>' + $('#' + concatString + 'product' + index).val() + '</span> <br>' +
    '<span> <b> Severity : </b>' + $('#' + concatString + 'severity' + index).val() + '</span>'

  var downloadType = 'image/png'
  if (type == 'PDF') downloadType = 'application/pdf'
  window.chartConfig[container].exportChart({
    type: downloadType,
    filename: filename + moment().format('DD-MM-YYYY hh:mm a')
  }, {
    chart: {
      width: '1000',
      marginTop: 150,
      height: 500
    },
    title: {
      text: title
    },
    subtitle: {
      text: html
    },
  });
}

function fetchDownloadData(file, downloadDataArray) {
  /* File Name */
  var filename = file + moment().format('DD-MM-YYYY hh:mm a') + ".xlsx";
  /* Sheet Name */
  var ws_name = "data";

  var wb = XLSX.utils.book_new(),
    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj[downloadDataArray]);

  /* Add worksheet to workbook */
  XLSX.utils.book_append_sheet(wb, ws, ws_name);

  /* Write workbook and Download */
  XLSX.writeFile(wb, filename);
}
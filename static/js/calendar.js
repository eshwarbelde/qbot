(function ($) {
  'use strict';
  $(function () {
    $.fn.andSelf = function () {
      return this.addBack.apply(this, arguments);
    }

    $('.smallCalendar').pignoseCalendar({
      multiple: true
    });

    if ($('#calendar').length) {
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,basicWeek,basicDay'
        },
        locale: 'en',
        dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
        ],
        dayNamesShort: ['SUN', 'MON', 'TUE', 'WED', 'THUS', 'FRI', 'SAT'],
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
        ],
        monthNamesShort: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        defaultDate: moment().format('YYYY-MM-DD'),
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [{
          title: 'Birthday Party',
          start: '2020-08-03T11:00:00',
          color: primaryColor
        },
        {
          title: 'Conference',
          start: '2020-08-07T12:00:00',
          color: warningColor
        },
        {
          title: 'Lunch party',
          start: '2020-08-11T01:00:00',
          color: successColor
        },
        {
          title: 'Family time',
          start: '2020-08-20T03:00:00',
          color: primaryColor
        },
        {
          title: 'Meetup',
          start: '2020-08-23T09:00:00',
          color: dangerColor
        },
        {
          title: 'Family time',
          start: '2020-09-01T03:00:00',
          color: warningColor
        },
        {
          title: 'Meeting with Alex',
          start: '2020-09-06T08:00:00',
          color: primaryColor
        }
        ],
        eventRender: function (event, eventElement) {
          if (event.color == dangerColor) {
            eventElement.addClass("event-invers-danger");
          }
          if (event.color == warningColor) {
            eventElement.addClass("event-invers-warning");
          }
          if (event.color == infoColor) {
            eventElement.addClass("event-invers-info");
          }
          if (event.color == successColor) {
            eventElement.addClass("event-invers-success");
          }
          if (event.color == primaryColor) {
            eventElement.addClass("event-invers-primary");
          }
        },

      })
    }
  });
})(jQuery)
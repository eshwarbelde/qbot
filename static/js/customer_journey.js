var buttonsShowFlag = false, plotData = [], currentCustomerName = '',
  /* columnChartData = [
    { maxVal: 40, yAxis: '% of tickets', color: '#6A5ACD', data: [ { name: '<=30', y: 14}, { name: '30-60', y: 29}, { name: '60-90', y: 35}, { name: '90-120', y: 14}, { name: '>120', y: 8}] },
    { maxVal: 45, yAxis: '% of vendor tickets created', color: '#24CBE5', data: [ { name: '<=5', y: 3}, { name: '5-10', y: 19}, { name: '10-15', y: 37}, { name: '15-20', y: 38}, { name: '>20', y: 3}] },
    { maxVal: 30, yAxis: '% of responses to vendor queries', color: '#FF9655', data: [ { name: '<=5', y: 10}, { name: '5-7', y: 19}, { name: '7-10', y: 25}, { name: '10-15', y: 25}, { name: '>15', y: 21}] }
  ] */
  columnChartData = [
    { title: 'Time from ticket creation to completion of trouble shooting', maxVal: 40, yAxis: '% of tickets', color: '#6A5ACD', data: [{ name: '30-60', y: 5 }, { name: '60- 90', y: 15 }, { name: '90-120', y: 32 }, { name: '120-180', y: 28 }, { name: '180-240', y: 17 }, { name: '>240', y: 3 }] },
    { title: 'Time to create ticket with vendor', maxVal: 45, yAxis: '% of vendor tickets created', color: '#24CBE5', data: [{ name: '<=5', y: 3 }, { name: '5-10', y: 19 }, { name: '10-15', y: 37 }, { name: '15-20', y: 38 }, { name: '> 20', y: 3 }] },
    { title: 'Time to respond to vendor query', maxVal: 40, yAxis: '% of responses to vendor queries', color: '#FF9655', data: [{ name: '<=5', y: 3 }, { name: '5-10', y: 8 }, { name: '10-15', y: 33 }, { name: '15-20', y: 35 }, { name: '>20', y: 20 }] }
  ],
  sankeyLevel = {
      'Punjab National Bank': {'snow': -1, 'customer': -1},
      'Citi Bank NA': {'snow': 0, 'customer': -1},
      'The Federal Bank Limited': {'snow': 0, 'customer': -1}
    }
  customer_names = [
    'Punjab National Bank',
    'Citi Bank NA',
    'The Federal Bank Limited',
    'SBI Funds Management Private Limited',
    'Aditya Birla Fashion and Retail Limited',
    'IndusInd Bank Limited',
    'MORGAN STANLEY ADVANTAGE SERVICES PRIVATE LIMITED',
    'Titan Company Limited',
    'Manappuram Finance Limited',
    'Tata Consultancy Services Ltd - MOIA',
    'VSoft Technologies Private Limited',
    'Ericsson India Private Limited',
    'OUTSOURCE PARTNER INTERNATIONAL INC',
    'Foiwe Info Global Solutions LLP',
    'Agro Tech Foods Limited'],
  customer_sankey_fullforms = {
    'New': 'Ticket created',
    'WIP': 'Work in Progress',
    'ACC': 'Ticket acknowledged by agent',
    'TS': 'Troubleshooting',
    'Hold': 'Ticket on Hold',
    'POA': 'Plan of action shared',
    'FE': 'Field engineer needed',
    'PI_problem': 'Problem identified_problem',
    'Update': 'Update shared with customer',
    'ETR': 'Estimated time of restoration',
    'Resolved': 'Service is restored',
    'Closed':  'Ticket closed'
  }
$(document).ready(function () {
  // getMasterDataFromDB();
  // fetchJson();
  // drawColumnChart(0);
  // drawColumnChart(1);
  // drawColumnChart(2);
  setCustomerNames()
});

function setCustomerNames() {
  for (let i in window.customer_names) {
    let customer = window.customer_names[i]
    $("#customerDiv").append("<option value='" + customer + "'>" + customer + "</option>");
  }
  fetchingRFO(window.customer_names[0])

  let html = ''
  for(let i in Object.keys(customer_sankey_fullforms)) {
    console.log(parseInt(i) % 3)
    if(i != 0 && parseInt(i) % 3 == 0) html += '</ul></div>'
    if(parseInt(i) % 3 == 0) html += '<div class="col-md-3"><ul>'
    
    let key = Object.keys(customer_sankey_fullforms)[i],
    val = customer_sankey_fullforms[key]

    html += '<li><b>' + key + ': </b>' + val + '</li>'
  }
  console.log(html)
  $('#customerBasedSankeyNames').html(html)
  // alert()
}

$("#customerDiv").on('change', function () {
  console.log($("#customerDiv").val())
  fetchingRFO($("#customerDiv").val())
})

function fetchingRFO(customerName) {
  $("#customerDiv").val(customerName)
  $("#customerSelected").text(customerName)
  $.ajax({
    type: 'get',
    url: '/get_rfos',
    data: {
      "customer_name": customerName
    },
    success: function (data) {
      console.log(data)
      $('#rfoDiv').html('');
      $("#rfoDiv").append("<option value='All'>All</option>");
      for(let i in data.rfo_list){
        let rfo = data.rfo_list[i]
        $("#rfoDiv").append("<option value='" + rfo + "'>" + rfo + "</option>");
      }
      fetchingData(customerName, 'All')
    },
    error: function (data) {
      console.log(data);
    }
  });
}

$("#rfoDiv").on('change', function () {
  console.log($("#rfoDiv").val())
  fetchingData($("#customerDiv").val(), $("#rfoDiv").val())
})

function filterOptionsBtn() {
  let keys = ['customer', 'product', 'rfo']
  if (window.buttonsShowFlag) {
    for (let i in keys) {
      $('#' + keys[i] + 'Div').hide()
      $('#' + keys[i] + 'Selected').show()
    }
  } else {
    for (let i in keys) {
      $('#' + keys[i] + 'Div').show()
      $('#' + keys[i] + 'Div').val($('#' + keys[i] + 'Selected').text())
      $('#' + keys[i] + 'Selected').hide()
    }
  }
  window.buttonsShowFlag = !window.buttonsShowFlag
}


function fetchingData(customerName, rfo) {
  $('#rfoDiv').val(rfo);
  $("#rfoSelected").text(rfo);
  $.ajax({
    type: 'get',
    url: '/get_sankeydata_casestate',
    data: {
      "customer_name": customerName,
      "rfo": rfo
    },
    success: function (data) {
      // let json = JSON.parse(data)
      console.log(data)
      console.log(JSON.parse(data.data).data)
      let plotData = JSON.parse(data.data).data,
      levelDecrease = window.sankeyLevel[customerName].snow,
      levelClosed = data.length + levelDecrease, levelResolved = (data.length + levelDecrease) - 1
      nodes = [
        { id: "CLOSED", level: levelClosed }, { id: "Closed", level: levelClosed },
        { id: "Resolved", level: levelResolved }, { id: "RESOLVED", level: levelResolved }
      ]
      if(customerName == 'The Federal Bank Limited' && rfo == 'All')
        nodes = [{ id: "CLOSED", level: data.length - 2 }, { id: "Closed", level: data.length - 2 }]
      console.log(levelClosed+ ' - '+ levelResolved)
      drawSankey('snowBasedSankeyChart', 'SNOW Based', plotData, nodes);
    },
    error: function (data) {
      console.log(data);
    }
  });
  
  $.ajax({
    type: 'get',
    url: '/get_sankeydata_customer',
    data: {
      "customer_name": customerName,
      "rfo": rfo
    },
    success: function (data) {
      console.log(data)
      console.log(JSON.parse(data.data).data)
      let plotData = JSON.parse(data.data).data,
      levelDecrease = window.sankeyLevel[customerName].customer,
      levelClosed = data.length + levelDecrease, 
      levelRfo = (data.length + levelDecrease) - 1,
      levelResolved = (data.length + levelDecrease) - 2,
      nodes = [
        { id: "CLOSED", level: levelClosed }, { id: "Closed", level: levelClosed },
        { id: "RFO", level: levelRfo }, { id: "Rfo", level: levelRfo },
        { id: "Resolved", level: levelResolved }, { id: "RESOLVED", level: levelResolved }
      ]
      if(customerName == 'The Federal Bank Limited' && rfo == 'All')
        nodes = [{ id: "CLOSED", level: data.length - 6 }, { id: "Closed", level: data.length - 6 }]
      console.log(levelClosed+ ' - '+ levelRfo + ' - '+ levelResolved)
      drawSankey('customerBasedSankeyChart', 'Customer Based', plotData, nodes);
    },
    error: function (data) {
      console.log(data);
    }
  });
}

function fetchJson() {
  $.getJSON("/static/data/updated/samplecase_state.json", function (json) {
    console.log(json);
    drawSankey('snowBasedSankeyChart', 'SNOW Based', json.data, [{ id: "Closed", level: 6 }, { id: "Resolved", level: 5 }]);
  })
  $.getJSON("/static/data/updated/samplepredictions.json", function (json) {
    console.log(json);
    drawSankey('customerBasedSankeyChart', 'Customer Based', json.data, [{ id: "CLOSED", level: 9 }, { id: "RFO", level: 8 }, { id: "RESOLVED", level: 7 }]);
    // , [{id: "CLOSED", level: 10}, {id: "RFO", level: 9}, {id: "RESOLVED", level: 8}]);
  })
  $.getJSON("/static/data/updated/sampleinternal.json", function (json) {
    console.log(json);
    drawSankey('fullBasedSankeyChart', 'Full Based', json.data, [{id: "CLOSED", level: 13}, {id: "RFO", level: 12}, {id: "RESOLVED", level: 11}]);
    // , [{id: "CLOSED", level: 13}, {id: "RFO", level: 12}, {id: "RESOLVED", level: 10}]);
  })
  /* $.getJSON("/static/data/case_stage_414_feb9th.json", function(json) {
    // console.log(json);
    console.log(JSON.parse(json));
    let jsonData = JSON.parse(json)
    window.plotData = []
    console.log(Object.keys(jsonData.time))
    for(let i in Object.keys(jsonData.time)) {
      // let source = jsonData['State Of Ticket(OLD)'][i],
      // target = jsonData['State Of Ticket(New)'][i],
      let source = jsonData['predictions'][i],
      target = jsonData['target'][i],
      time = jsonData.time[i]
      window.plotData.push([source, target, time])      
    }
    console.log(window.plotData);
    drawSankey('fullBasedSankeyChart', 'Full Based', window.plotData) // nodes: [{id: "CLOSED", level: 9}, {id: "RFO", level: 8}, {id: "RESOLVED", level: 7}];
  }); */
}

function getMasterDataFromDB() {
  $.ajax({
    type: 'get',
    url: '/getMasterDataFromDB',
    data: {
    },
    success: function (records) {
      console.log(records);
      if (records.product && 0 < records.product.length) {
        let products = records.product.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        for (let i in products) {
          let product = products[i].name, index = (i + 1)
          $("#productDiv").append("<option value='" + product + "'>" + product + "</option>");
          // $('#productDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="productButton' + index + '" onclick="fetchDataFilterButton(`product`, `' + index + '`, `' + product + '`)">' + product + '</button>')
        }
      }

      /* if (records.severity && 0 < records.severity.length) {
        let severities = records.severity.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        for (let i in severities) {
          let severity = severities[i].name, index = (i + 1)
          console.log(severity)
          $('#severityDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="severityButton' + index + '" onclick="fetchDataFilterButton(`severity`, `' + index + '`, `' + severity + '`)">' + severity + '</button>')
        }
      } */

      if (records.customer_name && 0 < records.customer_name.length) {
        let customer_names = records.customer_name.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
        for (let i in customer_names) {
          let customer = customer_names[i].name, index = i
          $("#customerDiv").append("<option value='" + customer + "'>" + customer + "</option>");
          // $('#customerDiv').append('<button type="button" class="btn btn-outline-primary btn-rounded btn-fw" id="customerButton' + index + '" onclick="fetchDataFilterButton(`customer`, `' + index + '`, `' + customer + '`)">' + customer + '</button>')
        }
        fetchingRFO(customer_names[0].name, true)
      }
    },
    error: function (data) {
      console.log(data);
    }
  });
}

/* function fetchDataFilterButton(fromKey, index, name) {
  $('#' + fromKey + 'Div button').removeClass('active')
  $('#' + fromKey + 'Button' + index).addClass('active')

  if (fromKey == 'customer') {
    $('#customerSelected').text(name);
    fetchDataFilterButton('product', 0, 'All')
    fetchDataFilterButton('severity', 0, 'All')
  } else if (fromKey == 'product') {
    $('#productSelected').text(name);
  } else if (fromKey == 'severity') {
    $('#severitySelected').text(name);
  }
} */


function drawSankey(chartId, disName, plotData, nodes) {
  console.log(nodes)
  Highcharts.seriesTypes.sankey.prototype.orderNodes = false;
  Highcharts.chart(chartId, {
    chart: {
      height: 600,
      type: 'sankey',
      margin: 50,
      // width: 2000
    },
    // colors: ['#000', '#FFF000'],
    title: {
      text: ''
    },
    credits: {
      enabled: false
    },
    tooltip: {
      // enabled: false,
      // headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      // pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },
    accessibility: {
      point: {
        valueDescriptionFormat: '{index}. {point.from} to {point.to}, {point.weight}.'
      }
    },
    plotOptions: {
      series: {
          cursor: (disName == 'Customer Based') ? 'pointer' : 'default',
          point: {
              events: {
                  click: function () {
                    if(disName == 'Customer Based'){
                      if(-1 < this.id.indexOf('TS')) drawColumnChart(0);
                      else if(-1 < this.id.indexOf('FE')) drawColumnChart(1)
                      else if(-1 < this.id.indexOf('PI')) drawColumnChart(2)
                    }
                  }
              }
          }
      }
  },
    series: [{
      maxLinkWidth: 5,
      keys: (disName != 'Full Based' ? ['from', 'to', 'weight'] : ['from', 'to', 'weight', 'color']),
      minLinkWidth: 1,
      /* data: [
        {color: "#BB0000", from: "Red", to: "Colour Demo", weight: 10},
        {color: "#00BB00", from: "Green", to: "Colour Demo", weight: 4},
        {color: "#0000BB",  from: "Blue", to: "Colour Demo", weight: 6},
        {color: "#0000BB", from: "Colour Demo", to: "Orange", weight: 10},
        {color: "#0000BB",  from: "Colour Demo", to: "Yellow", weight: 10}
      ], */
      data: plotData,
      nodes: nodes,
      /* [
        { id: 'CLOSED', level: 10}
      ], */
      // data: [
      //   ['Brazil', 'Portugal', 5],
      //   ['Brazil', 'France', 1],
      //   ['Brazil', 'Spain', 1],
      //   ['Brazil', 'England', 1],

      //   ['Canada', 'Portugal', 1],
      //   ['Canada', 'France', 5],
      //   ['Canada', 'England', 1],

      //   ['Mexico', 'Portugal', 1],
      //   ['Mexico', 'France', 1],
      //   ['Mexico', 'Spain', 5],
      //   ['Mexico', 'England', 1],
      //   ['Mexico', 'England ', 5],

      //   ['USA', 'Portugal', 1],
      //   ['USA', 'France', 1],
      //   ['USA', 'Spain', 1],
      //   ['USA', 'England', 5],

      //   ['Portugal', 'Angola', 2],
      //   ['Portugal', 'Senegal', 1],
      //   ['Portugal', 'Morocco', 1],
      //   ['Portugal', 'South Africa', 3],

      //   ['France', 'Angola', 1],
      //   ['France', 'Senegal', 3],
      //   ['France', 'Mali', 3],
      //   ['France', 'Morocco', 3],
      //   ['France', 'South Africa', 1],

      //   ['Spain', 'Senegal', 1],
      //   ['Spain', 'Morocco', 3],
      //   ['Spain', 'South Africa', 1],

      //   ['England', 'Angola', 1],
      //   ['England', 'Senegal', 1],
      //   ['England', 'Morocco', 2],
      //   ['England', 'South Africa', 7],

      //   ['South Africa', 'China', 5],
      //   ['South Africa', 'India', 1],
      //   ['South Africa', 'Japan', 3],

      //   ['Angola', 'China', 5],
      //   ['Angola', 'India', 1],
      //   ['Angola', 'Japan', 3],

      //   ['Senegal', 'China', 5],
      //   ['Senegal', 'India', 1],
      //   ['Senegal', 'Japan', 3],

      //   ['Mali', 'China', 5],
      //   ['Mali', 'India', 1],
      //   ['Mali', 'Japan', 3],

      //   ['Morocco', 'China', 5],
      //   ['Morocco', 'India', 1],
      //   ['Morocco', 'Japan', 3]
      // ],
      type: 'sankey',
      name: disName,
      /* states: {
        hover: {
          color: '#a4edba',
          borderColor: 'gray',
          formatter: function (e) {
            console.log(e)
          }
        }
      } */
    }],


    /* plotOptions: {
      sankey: {
        point: {
          events: {
            mouseOver: function() {
              linksHover(this, '');
            },
            mouseOut: function() {
              linksHover(this, '');
            }
          }
        }
      }
    } */

  });
}

function linksHover(point, state) {
  console.log(point.isNode)
  if (point.isNode) {
    point.linksFrom.forEach(function (l) {
      l.setState(state);
    });
  }
}

function drawColumnChart(index) {
  let ele = window.document.getElementsByClassName('testclass');
  ele[0].click();
  $('#responsesModalLabel').text(columnChartData[index].title)
  Highcharts.chart('columnChart', {
    chart: {
      height: 500,
      type: 'column'
    },
    colors: [columnChartData[index].color],
    title: {
      text: ''
    },
    accessibility: {
      announceNewData: {
        enabled: true
      }
    },
    xAxis: {
      type: 'category',
      title: {
        text: 'Minutes',
        style: {
          fontWeight: 'bold'
        }
      },
    },
    yAxis: {
      title: {
        text: columnChartData[index].yAxis,
        style: {
          fontWeight: 'bold'
        }
      },
      labels: {
        formatter: function () {
          return this.value + '%';
        }
      },
      max: columnChartData[index].maxVal
    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        maxPointWidth: 45,
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y:.0f} %'
        }
      }
    },

    credits: {
      enabled: false
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
      {
        name: "Browsers",
        colorByPoint: true,
        data: columnChartData[index].data
      }
    ]
  })
}
from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import accuracy_score, confusion_matrix ,classification_report,f1_score
from modules.utils import save_pickle_file, merge_dataframe
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
import pandas as pd
pd.set_option('mode.chained_assignment', None)

#from modules import utils
from modules.preprocessing import Preprocessing
import matplotlib.pyplot as plt # Helps in plotting any graph

from modules.utils import merge_dataframe


class TestModel(object):
    def __init__(self, data):
        self.file_path = data
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.models = {}
        self.dtm_models = {}
        self.model_path = '../models/5w_classifier_model_v2.pkl'
        self.dtm_model_path = '../models/document_term_matrix_model_v2.pkl'

    @staticmethod
    def train_model(X, y,predict_variable):
        # Intialize classifier
        if predict_variable=='Why':
            #model = RandomForestClassifier(n_estimators=150,class_weight='balanced', random_state=25)
            model= LogisticRegression(random_state=0,solver='lbfgs')
            #model = SGDClassifier(random_state=0)
        else:
            model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        # Fit training data using the Bernoulli NB to train the model
        model.fit(X, y)
        return model

    def predict_values(self, X_test, predict_variable):
        # Predict the target variable values of the test dataset on trained model.
        return self.models[predict_variable].predict(X_test)

    def are_columns_exist(self, df_columns, validate_columns):
        for v_col in validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def start_training(self, df, predict_variables):
        df['Closure notes_cleaned'] = df['Closure notes'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['Closure notes_cleaned'] = df['Closure notes_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['Closure notes_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.

        # # one hot encoding
        # onehotencoder = OneHotEncoder()
        # products = pd.DataFrame(onehotencoder.fit_transform(df[['u_product']]).toarray(), columns=onehotencoder.get_feature_names())

        # Distribute the data into train and test datasets where 80% is used for training and 20% for testing.
        # X = pd.concat([df_email_word_matrix, products], axis=1)
        X = df_email_word_matrix
        for predict_variable in predict_variables:
            print(predict_variable)
            self.dtm_models[predict_variable] = count_vectorizer
            y = df[predict_variable]#.astype(int)
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, stratify=y, random_state=25)
            X_train_temp=X_train.copy()
            model = self.train_model(X_train, y_train,predict_variable)
            self.models[predict_variable] = model

            y_pred = self.predict_values(X_train, predict_variable)
            y_pred = pd.DataFrame(y_pred, index=y_train.index, columns=[predict_variable+'_predicted'])

            # Drop rows if all values of the predicted variables are zeros.
            keep_indexes = df.loc[~(df[predict_variables] == 0).all(axis=1)].index
            y_train = y_train[y_train.index.isin(list(keep_indexes))]
            y_pred = y_pred[y_pred.index.isin(list(keep_indexes))]
            # Get accuracy score
            accuracy = round(f1_score(y_true=y_train, y_pred=y_pred,average='weighted'), 2)
            print('Accuracy_train: ', accuracy)
            target_name=['0','1']
            if predict_variable=='Reason1':
                target_name=self.models[predict_variable].classes_
            print(classification_report(y_train, y_pred, target_names=target_name))
            #y_pred[predict_variable+'_predicted']=pd.Series(y_pred[predict_variable])
            #print(df.loc[y_pred.index]['Closure notes'])
            temp=df.loc[y_pred.index][['Closure notes',predict_variable]]
            y_pred1=pd.merge(temp,y_pred, left_index=True, right_index=True)
            #y_pred1=pd.concat([y_pred,temp],sort=False)
            y_pred1.to_excel('../output/training_set_'+predict_variable+'.xlsx',index=False)


            y_pred = self.predict_values(X_test, predict_variable)
            y_pred = pd.DataFrame(y_pred, index=y_test.index, columns=[predict_variable+'_predicted'])
            # Drop rows if all values of the predicted variables are zeros.
            keep_indexes = df.loc[~(df[predict_variables] == 0).all(axis=1)].index
            y_test = y_test[y_test.index.isin(list(keep_indexes))]
            y_pred = y_pred[y_pred.index.isin(list(keep_indexes))]

            temp = df.loc[y_pred.index][['Closure notes', predict_variable]]
            y_pred1 = pd.merge(temp, y_pred,  left_index=True, right_index=True)
            # y_pred1=pd.concat([y_pred,temp],sort=False)
            y_pred1.to_excel('../output/test_set_' + predict_variable + '.xlsx',index=False)

            # Get accuracy score
            accuracy = round(f1_score(y_true=y_test, y_pred=y_pred,average='weighted'), 2)
            print('Accuracy_test: ', accuracy)
            target_name = ['0', '1']
            if predict_variable != 'Reason1':
                #target_name = y_test.unique()
                print(classification_report(y_test, y_pred, target_names=target_name))
            # Get confusion matrix
            binary = confusion_matrix(y_true=y_test, y_pred=y_pred)
            # plot confusion matrix
            plot_confusion_matrix(conf_mat=binary)
            plt.savefig('confusion_matrix_{}.png'.format(predict_variable))



    def execute(self):
        # Training on all columns except i_link stat_sim
        validate_columns = ['Closure notes', 'What', 'Where', 'Why', 'Who fixed it', 'How fixed','Reason1']
        predict_variables = ['What', 'Where', 'Why', 'Who fixed it', 'How fixed','Reason1']
        ignore_cols = [ 'Reason2']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        df = self.file_path[select_cols]
        self.are_columns_exist(df.columns, validate_columns)
        df.dropna(inplace=True)
        df.replace('High bandwidth utilization', 'High Bandwidth Utilization', inplace=True)
        df.replace('Customer end Power issue','Customer end Power Issue', inplace=True)
        df.replace('Customer not reachable/ Available', ' Customer not Reachable/Available', inplace=True)
        df.replace('Fiber cut', 'Fiber Cut', inplace=True)
        df.replace('fiber cut', 'Fiber Cut', inplace=True)
        df = df[df['Reason1'] != '??']
        self.start_training(df, predict_variables)
        save_pickle_file(self.dtm_model_path, self.dtm_models)
        save_pickle_file(self.model_path, self.models)

if __name__=='__main__':
    data_file = '../training_data/RFO Sample classification_6 May.xlsx'
    sheet_names = ['CEP', 'FC', 'LNP', 'ACE', 'HB', 'II', 'UD', 'SUI', 'CI', 'CNA']
    data = merge_dataframe(data_file, sheet_names)
    data = data.sample(frac=1).reset_index(drop=True)
    TestModel(data).execute()
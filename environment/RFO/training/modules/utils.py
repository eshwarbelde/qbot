import pickle
import re
import pandas as pd


def merge_dataframe(path, sheet_names):
    lst_df = []
    for sheet_name in sheet_names:
        df = load_excel(path, sheet_name)
        print(df.shape)
        lst_df.append(df)
    merge_df = pd.concat(lst_df, ignore_index=True)
    print(merge_df.shape)
    print(merge_df.columns)
    return merge_df

def save_pickle_file(path, data):
    print('Saving the file: {}'.format(path))
    with open(path, 'wb') as fp:
        pickle.dump(data, fp)
        print('Saved in location: {}'.format(path))

def load_pickle_file(path):
    print('Loading the file: {}'.format(path))
    with open(path, 'rb') as fp:
        return pickle.load(fp)

def save_csv(path, df):
    print('Saving the CSV: {}'.format(path))
    df.to_csv(path, index=False)
    print('Output File: {}'.format(path))

def load_excel(path, sheet_name):
    print('Loading the Excel: {}'.format(path))
    return pd.read_excel(path, sheet_name=sheet_name)

def save_excel(path, df):
    print('Saving the Excel: {}'.format(path))
    df.to_excel(path, index=False)
    print('Output File: {}'.format(path))

def extract_time1(text):
    regex = r"(\d+:\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec)"
    matches = re.findall(regex, text, re.IGNORECASE)
    if len(matches) == 0:
        regex = r"(\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec)"
        matches = re.findall(regex, text, re.IGNORECASE)
    for match in matches:
        return ' '.join(match)
    
def extract_time(text):
    regex=r'etr'
    matches=re.findall(regex, text, re.IGNORECASE)
    if len(matches)>=1:
        string = string=r'(today|tomorrow|eod)'
        matches= re.findall(string, text, re.IGNORECASE)
        if len(matches)==0:
            regex = r"(\d+:\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec|pm|ist)"
            matches = re.findall(regex, text, re.IGNORECASE)
            if len(matches) == 0:
                regex = r"(\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec)"
                matches = re.findall(regex, text, re.IGNORECASE)
        for match in matches:
            return ' '.join(match)

    elif len(matches)==0:
        regex = r"(\d+:\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec|pm|ist)"
        matches = re.findall(regex, text, re.IGNORECASE)
        if len(matches) == 0:
            regex = r"(\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec)"
            matches = re.findall(regex, text, re.IGNORECASE)
    for match in matches:
        return ' '.join(match)
        


import pandas as pd

# from environment.classification.utils import save_pickle_file, merge_dataframe
# from environment.classification.preprocessing import Preprocessing
from sklearn.ensemble import RandomForestClassifier
from modules.utils import save_pickle_file, merge_dataframe
from modules.preprocessing import Preprocessing
from sklearn.linear_model import LogisticRegression

class Training(object):
    def __init__(self, file_path_data):
        """
        Initialize the variables.
        """
        self.file_path = file_path_data
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.models = {}
        self.dtm_models = {}
        self.model_path = '../models/5w_classifier_model_v1.pkl'
        self.dtm_model_path = '../models/document_term_matrix_model_v1.pkl'

    @staticmethod
    def train_model(X, y,predict_variable):
        """
        Train the classification model.
        :param X: Independent variables/features
        :param y: dependent variable/feature
        :return: Returns the model object after fitting on the training data.
        """
        if predict_variable == 'Why':
            # model = RandomForestClassifier(n_estimators=150,class_weight='balanced', random_state=25)
            model = LogisticRegression(random_state=0, solver='lbfgs')
            # model = SGDClassifier(random_state=0)
        else:
            model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        model.fit(X, y)
        return model

    def are_columns_exist(self, df_columns, validate_columns):
        """
        Check all columns are exist or not
        :param df_columns: List of columns in the data
        :return:
        """
        for v_col in validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def start_training(self, df, predict_variables):
        df['Closure notes_cleaned'] = df['Closure notes'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['Closure notes_cleaned'] = df['Closure notes_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['Closure notes_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.

        for predict_variable in predict_variables:
            print(predict_variable)
            self.dtm_models[predict_variable] = count_vectorizer
            model = self.train_model(df_email_word_matrix, df[predict_variable],predict_variable) #.astype(int)
            self.models[predict_variable] = model

    def execute(self):
        """
        List of operations over the training data and build the model and store in the models directory.
        :return:
        """
        # Training on all columns except i_link stat_sim
        validate_columns = ['Closure notes', 'What', 'Where', 'Why','Who fixed it', 'How fixed','Reason1']
        predict_variables = ['What', 'Where', 'Why','Who fixed it', 'How fixed','Reason1']
        ignore_cols = ['Reason2']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        df = self.file_path[select_cols]
        df.replace('fiber cut','Fiber Cut',inplace=True)
        df.replace('Fiber cut', 'Fiber Cut', inplace=True)
        df.dropna(inplace=True)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

        # Save all the models
        save_pickle_file(self.dtm_model_path, self.dtm_models)
        save_pickle_file(self.model_path, self.models)

if __name__=='__main__':
    data_file = '../data/RFO Sample classification_25 Apr.xlsx'
    sheet_names = ['CEP', 'FC', 'LNP', 'ACE', 'HB', 'II', 'UD','SUI','CI','CNA']
    data = merge_dataframe(data_file, sheet_names)
    Training(data).execute()
from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import accuracy_score, confusion_matrix,f1_score,classification_report
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn import preprocessing
import pandas as pd
pd.set_option('mode.chained_assignment', None)
#from modules import utils
from modules.preprocessing import Preprocessing
import matplotlib.pyplot as plt # Helps in plotting any graph
from modules.utils import save_pickle_file, merge_dataframe

class Training(object):
    def __init__(self, file_path_data):
        """
        Initialize the variables.
        """
        self.file_path = file_path_data
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        #self.le = preprocessing.LabelEncoder()
        self.model_path = '../models/reason2_classifier_model_fc_v1.pkl'
        self.dtm_model_path = '../models/document_term_matrix_model_reason2_fc_v1.pkl'
        #self.le_path = '../models/le_reason1_v1.pkl'
    @staticmethod
    def train_model(X, y):
        """
        Train the classification model.
        :param X: Independent variables/features
        :param y: dependent variable/feature
        :return: Returns the model object after fitting on the training data.
        """
        model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        #model = LogisticRegression(random_state=0, solver='lbfgs')
        #model = SGDClassifier(penalty='l1',random_state=0)
        # Fit training data using the Bernoulli NB to train the model
        model.fit(X, y)
        return model

    def start_training(self, df, predict_variables):
        df['Closure notes_cleaned'] = df['Closure notes'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['Closure notes_cleaned'] = df['Closure notes_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['Closure notes_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.
        #self.le.fit(predict_variables)
        model = self.train_model(df_email_word_matrix, predict_variables)
        return count_vectorizer,model,dtm

    def predict_data(self, df, model_1,cv_1):
        df['Closure notes_cleaned'] = df['Closure notes'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['Closure notes_cleaned'] = df['Closure notes_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['Closure notes_cleaned'])
        # Generate document term vector space.
        dtm = self.preprocessing_obj.transform_document_term_vector(cv_1,emails)
        predictions = model_1.predict(dtm)
        return predictions

    def execute(self):
        """
        List of operations over the training data and build the model and store in the models directory.
        :return:
        """
        validate_columns = ['Closure notes','Reason2']
        predict_variables = ['Reason2']
        ignore_cols = ['What', 'Where', 'Why','Who fixed it', 'How fixed','Reason1']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        df = self.file_path[select_cols]
        df.dropna(inplace=True)
        df.replace('dual fiber cut','Dual fiber cut',inplace=True)
        df.replace('multiple fiber cuts', 'Multiple Fiber cut', inplace=True)
        #df=df[df['Reason1']!='??']
        X=df[['Closure notes']]
        y=df[['Reason2']]
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, stratify=y, random_state=25)


        #dtm,model,le=self.start_training(df, predict_variables)
        cv, model,dtm= self.start_training(X_train, y_train['Reason2'])
        train_predictions=model.predict(dtm)

        train_set=pd.DataFrame()
        train_set['Closure notes']=pd.Series(X_train['Closure notes'])
        train_set['Reason2'] = pd.Series(y_train['Reason2'])
        train_set.reset_index(drop=True,inplace=True)
        train_set['Predicted'] = pd.Series(train_predictions)
        train_set.to_excel('../output/trainset_reason2_fc.xlsx',index=False)

        accuracy = round(f1_score(y_true=y_train, y_pred=train_predictions,average='weighted'), 2)
        print('Train_F1_score: ', accuracy)
        target_name = model.classes_
        print(classification_report(y_train, train_predictions, target_names=target_name))
        # Get confusion matrix
        binary = confusion_matrix(y_true=y_train, y_pred=train_predictions)
        # plot confusion matrix
        plot_confusion_matrix(conf_mat=binary)
        plt.savefig('confusion_matrix_{}.png'.format('Reason2_fc'))

        test_predictions= self.predict_data(X_test,model,cv)

        test_set = pd.DataFrame()
        test_set['Closure notes'] = pd.Series(X_test['Closure notes'])
        test_set['Reason2'] = pd.Series(y_test['Reason2'])
        test_set.reset_index(drop=True, inplace=True)
        test_set['Predicted'] = pd.Series(test_predictions)
        test_set.to_excel('../output/test_set_reason2_fc.xlsx', index=False)

        accuracy = round(f1_score(y_true=y_test, y_pred=test_predictions, average='weighted'), 2)
        print('Test_F1_score: ', accuracy)
        # Get confusion matrix
        binary = confusion_matrix(y_true=y_test, y_pred=test_predictions)
        # plot confusion matrix
        #print(binary)
        plot_confusion_matrix(conf_mat=binary)


        # Save all the models
        save_pickle_file(self.dtm_model_path, cv)
        save_pickle_file(self.model_path, model)
        #save_pickle_file(self.model_path, le)

if __name__=='__main__':
    data_file = '../training_data/RFO Sample classification_6 May.xlsx'
    sheet_names = ['FC']
    data = merge_dataframe(data_file, sheet_names)
    Training(data).execute()
import pandas as pd
pd.set_option('mode.chained_assignment', None)
from environment.RFO.training.modules.utils import load_pickle_file
from environment.RFO.training.modules.preprocessing import Preprocessing
import sys
from datetime import datetime, timedelta

#import math


class RFO(object):
    def __init__(self, file_path_data=None):
        """
        Initialize the variables.
        """
        #self.file_path = file_path_data
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.model_path_Ws_reason1 = 'environment/RFO/models/5w_classifier_model_v2.pkl'
        self.dtm_model_path_Ws_reason1 = 'environment/RFO/models/document_term_matrix_model_v2.pkl'
        #self.model_path_Ws = 'environment/RFO/models/email_classifier_model_rfo.pkl'
        #self.dtm_model_path_Ws = 'environment/RFO/models/document_term_matrix_model_rfo.pkl'
        self.model_path_FC = 'environment/RFO/models/reason2_classifier_model_fc_v1.pkl'
        self.dtm_model_path_FC = 'environment/RFO/models/document_term_matrix_model_reason2_fc_v1.pkl'
        self.model_path_SUI = 'environment/RFO/models/reason2_classifier_model_sui_v1.pkl'
        self.dtm_model_path_SUI = 'environment/RFO/models/document_term_matrix_model_reason2_sui_v1.pkl'
        self.predict_variables = ['What', 'Where', 'Why', 'Who fixed it', 'How fixed','Reason1']


    def predict_data(self, df, model_1,cv_1,predict_variable):
        df['Closure notes_cleaned'] = df['Closure notes'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['Closure notes_cleaned'] = df['Closure notes_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['Closure notes_cleaned'])
        # Generate document term vector space.
        dtm = self.preprocessing_obj.transform_document_term_vector(cv_1[predict_variable],emails)
        predictions = model_1[predict_variable].predict(dtm)
        return predictions

    def predict_reason2(self, df, model_1,cv_1):
        df['Closure notes_cleaned'] = df['Closure notes'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['Closure notes_cleaned'] = df['Closure notes_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['Closure notes_cleaned'])
        # Generate document term vector space.
        dtm = self.preprocessing_obj.transform_document_term_vector(cv_1,emails)
        predictions = model_1.predict(dtm)
        return predictions

    @staticmethod
    def rfo_time(closing_time, rfo_closure_time):
        t1 = datetime.strptime(str(rfo_closure_time), "%Y-%m-%d %X")
        t2 = datetime.strptime(str(closing_time), "%Y-%m-%d %X")
        updated_booking_time = t2+timedelta(hours=12)
        diff = t1.timestamp() - updated_booking_time.timestamp()
        if t1.timestamp() <= updated_booking_time.timestamp():
            return 1,diff
        else:
            return 0,diff

    @staticmethod
    def rfo_scoring(rows):
        score = 0
        scenario = 0
        if rows['Reason1'] == 'Alarm from customer end':
            if rows['What'] and rows['Where']  and rows['Why'] == 0:
                score = 1
                scenario = 1

        elif rows['Reason1'] == 'Customer end Power Issue':
            if rows['What'] and rows['Where']:
                score = 1
                scenario = 2

        elif rows['Reason1'] == 'Subscriber Unit Issue':
            if rows['What'] and rows['Where']  and rows['Why'] and rows['Who fixed it'] and rows['How fixed']:
                score = 1
                scenario = 3

        elif rows['Reason1'] == 'Fiber Cut':
            if rows['Reason2'] == 'Fiber Cut' or rows['Reason2'] == 'Degradation':
                if rows['What'] and rows['Where']:
                    score = 1
                    scenario = 9
                else:
                    score = 0
                    scenario = 0

            elif rows['Reason2'] == 'Dual fiber cut' or rows['Reason2'] == 'Last Mile fiber cut':
                if rows['What'] and rows['Where'] and rows['Why'] and rows['Who fixed it'] and rows['How fixed']:
                    score = 1
                    scenario = 3
                else:
                    score = 0
                    scenario = 0
            elif rows['What']  and rows['Why']:
                score = 1
                scenario = 4
        elif rows['Reason1'] == 'High Bandwidth Utilization':
            if rows['What']  and rows['Why']:
                score = 1
                scenario = 4
        elif rows['Reason1'] == 'Insufficient information' or rows['Reason1'] == 'Undetermined'\
                or rows['Reason1'] == 'Relevant logs not provided' or rows['Reason1'] == ' Customer not Reachable/Available':
            if rows['What'] and rows['Why'] == 0:
                score = 1
                scenario = 5
        elif rows['Reason1'] == 'Planned Activity' or rows['Reason1'] == 'Migration':
            if rows['What'] and rows['Where'] == 0  and rows['Why'] and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                score = 1
                scenario = 6
        elif rows['Reason1'] == 'Configuration Issue':
            if rows['What'] and rows['Where']  and rows['Who fixed it'] and rows['How fixed']:
                score = 1
                scenario = 8
        else:
            if rows['What'] == 0 and rows['Where'] == 0  and rows['Why'] == 0 and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                score = 1
                scenario = 7
            else:
                score = 0
                scenario = 0
        return score,scenario

    @staticmethod
    def shortest_path(rows):
        missing = 'None'
        if rows['Reason1'] == 'Alarm from customer end':
            if rows['What'] == 0 and rows['Where'] == 0:
                missing = str('1,What,Where,2')
            elif rows['What'] == 0:
                missing = str('1,What,1')
            elif rows['Where'] == 0:
                missing = str('1,Where,1')
            else:
                missing = str('undetermined')
        elif rows['Reason1'] == 'Customer end Power Issue':
            if rows['What'] == 0 and rows['Where'] == 0:
                missing = str('2,What,Where,2')
            elif rows['What'] == 0:
                missing = str('2,What,1')
            elif rows['Where'] == 0:
                missing = str('2,Where,1')
            else:
                missing = str('undetermined')

        elif rows['Reason1'] == 'Subscriber Unit Issue':
            if rows['What'] == 0 and rows['Where'] == 0  and rows['Why'] == 0 and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                missing = str('3,What,Where,Why,Who,How,5')
            elif rows['Why'] == 0 and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                missing = str('3,Why,Who,How,3')
            elif rows['What'] == 0 and rows['Where'] == 0  and rows['Why'] == 0:
                missing = str('3,What,Where,Why,3')
            elif rows['What'] == 0 and rows['Where'] == 0:
                missing = str('3,What,Where,2')
            else:
                missing = str('undetermined')

        elif rows['Reason1'] == 'Fiber Cut':
            if rows['Reason2'] == 'Fiber Cut' or rows['Reason2'] == 'Degradation':
                if rows['What'] == 0 and rows['Where'] == 0:
                    missing = str('9,What,Where,2')
                elif rows['What'] == 0:
                    missing = str('9,What,1')
                elif rows['Where'] == 0:
                    missing = str('9,Where,1')
                else:
                    missing = str('undetermined')

            elif rows['Reason2'] == 'Dual fiber cut' or rows['Reason2'] == 'Last Mile fiber cut':
                if rows['What'] == 0 and rows['Where'] == 0 and rows['Why'] == 0 and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                    missing = str('3,What,Where,Why,Who,How,5')
                elif rows['Why'] == 0 and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                    missing = str('3,Why,Who,How,3')
                elif rows['What'] == 0 and rows['Where'] == 0 and rows['Why'] == 0:
                    missing = str('3,What,Where,Why,3')
                elif rows['What'] == 0 and rows['Where'] == 0:
                    missing = str('3,What,Where,2')
                else:
                    missing = str('undetermined')
            elif rows['What'] == 0 and rows['Why'] == 0:
                missing = str('4,What,Why,2')
            elif rows['Why'] == 0:
                missing = str('4,Why,1')
            elif rows['What'] == 0:
                missing = str('4,What,1')
            else:
                missing = str('undetermined')


        elif rows['Reason1'] == 'High Bandwidth Utilization':
            if rows['What']  and rows['Why']:
                missing = str('4,What,Why,2')
            elif rows['Why'] == 0:
                missing = str('4,Why,1')
            elif rows['What'] == 0:
                missing = str('4,What,1')
            else:
                missing = str('undetermined')

        elif rows['Reason1'] == 'Insufficient information' or rows['Reason1'] == 'Undetermined' \
                or rows['Reason1'] == 'Relevant logs not provided' or rows['Reason1'] == ' Customer not Reachable/Available':
            if rows['What'] == 0:
                missing = str('5,What,1')
            else:
                missing = str('undetermined')

        elif rows['Reason1'] == 'Planned Activity' or rows['Reason1'] == 'Migration':
            if rows['What'] == 0 and rows['Why'] == 0:
                missing = str('6,What,Why,2')
            elif rows['Why'] == 0:
                missing = str('6,Why,1')
            elif rows['What'] == 0:
                missing = str('6,What,1')
            else:
                missing = str('undetermined')

        elif rows['Reason1'] == 'Configuration Issue':
            if rows['What'] == 0 and rows['Where'] == 0 and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                missing = str('8,What,Where,Who,How,4')
            elif rows['What'] == 0 and rows['Where'] == 0 and rows['Who fixed it'] == 0:
                missing = str('8,What,Where,Who,3')
            elif rows['Where'] == 0 and rows['Who fixed it'] == 0 and rows['How fixed'] == 0:
                missing = str('8,What,Who,How,3')
            elif rows['What'] == 0 and rows['Where'] == 0:
                missing = str('8,What,Where,2')
            else:
                missing = str('undetermined')
        return missing


    def run(self,dataframe):
        try:
            df = dataframe
            df = df[['Closure notes','u_number','sys_created_on','sys_created_by','RFO Responsbile','RFO Cause','RFO Specification','Fault Segment','rfo_closure']]
            predict_variables = ['What', 'Where', 'Why','Who fixed it', 'How fixed','Reason1']
            #print(load_pickle_file(self.dtm_model_path_Ws))
            for predict_variable in predict_variables:
                df[predict_variable] = self.predict_data(df, load_pickle_file(self.model_path_Ws_reason1),
                                                         load_pickle_file(self.dtm_model_path_Ws_reason1),
                                                         predict_variable)
                '''if predict_variable != 'aaa': #'Reason1':
                    df[predict_variable] = self.predict_data(df, load_pickle_file(self.model_path_Ws_reason1),
                                                             load_pickle_file(self.dtm_model_path_Ws_reason1), predict_variable)
                else:
                    df[predict_variable] = self.predict_data(df,load_pickle_file(self.model_path_Ws),
                                            load_pickle_file(self.dtm_model_path_Ws),predict_variable)'''
            try:
                print('Reason1 FiberCut')
                df.loc[df['Reason1'].isin(['Fiber Cut']), 'Reason2'] = self.predict_reason2(
                      df.loc[df['Reason1'].isin(['Fiber Cut'])],load_pickle_file(self.model_path_FC),load_pickle_file(self.dtm_model_path_FC))
            except:
                df['Reason2'] = ''
            try:
                print('Reason1 Subscriber unit issue')
                df.loc[df['Reason1'].isin(['Subscriber Unit Issue']), 'Reason2'] = self.predict_reason2(
                    df.loc[df['Reason1'].isin(['Subscriber Unit Issue'])],load_pickle_file(self.model_path_FC),load_pickle_file(self.dtm_model_path_FC))
            except:
                df['Reason2'] = ''

            for idx,rows in df.iterrows():
                df.loc[idx,'RFO_comp'],df.loc[idx,'RFO_Scenario'] = self.rfo_scoring(rows)

            df.loc[df['RFO Specification'] == df['Reason1'], 'R1_comp'] = int(1)
            df.loc[df['RFO Specification'] != df['Reason1'], 'R1_comp'] = int(0)
            df['Reason2'].fillna('no reason',inplace=True)
            #df['Fault Segment'].fillna('no reason', inplace=True)
            df['Fault Segment'].replace('','no reason',inplace=True)
            print('reason2 calculation')

            df.loc[df['Fault Segment'] == df['Reason2'], 'R2_comp'] = int(1)
            df.loc[df['Fault Segment'] != df['Reason2'], 'R2_comp'] = int(0)

            df['missing_parameters'] = ''
            #Shortest_path
            for index,rows in df.iterrows():
                if rows['RFO_comp'] == 0:
                    df.loc[index,'missing_parameters'] = self.shortest_path(rows)

            for index,rows in df.iterrows():
                df.loc[index,'rfo_time_compilance'],df.loc[index,'rfo_time_diff'] = self.rfo_time(rows['sys_created_on'],rows['rfo_closure'])

            print('RFO Done')

            time_for_file_path = str(datetime.now().strftime("%d-%m-%Y %Hh%Mm%Ss"))
            try:
                df.to_excel('environment/RFO/output/results_'+time_for_file_path+'.xlsx',index=False)
            except:
                pass

            return df
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)

if __name__=='__main__':
    #data_file = './training_data/Rfo_reason1.xlsx'
    #data_file = './training_data/Mass testing.xlsx'
    RFO().run()
import psutil
import os
from datetime import datetime

from environment.dashboard.admin.admin_cache import AdminCache
from environment.dashboard.admin.admin_dao import AdminModuleDataFetch
from environment.dashboard.agent.agent_cache import AgentCache
from environment.dashboard.agent.agent_dao import AgentModuleDataFetch
from environment.dashboard.customer.customer_cache import CustomerCache
from environment.dashboard.customer.customer_dao import CustomerModuleDataFetch
from django.core.cache import cache


class CacheRefresher:
    agent_cache = None
    customer_cache = None
    admin_cache = None

    def refresh_cache(self):
        print("Memory used:" ,psutil.Process(os.getpid()).memory_info())
        #Loading the cache mobule wise
        self.load_agent_cache()
        self.load_admin_cache()
        self.load_customer_cache()

        d1 = datetime.now()
        cache.set("all_agent_names", AgentCache.all_agent_names, None)
        cache.set("all_agents_performance", AgentCache.all_agents_performance, None)
        cache.set("content_compliance_results_by_agent", AgentCache.content_compliance_results_by_agent, None)
        cache.set("time_compliance_results_by_agent", AgentCache.time_compliance_results_by_agent, None)
        cache.set("missing_parameters_results_by_agent", AgentCache.missing_parameters_results_by_agent, None)
        cache.set("distinct_agent_scores", AgentCache.distinct_agent_scores, None)
        cache.set("subsequent_content_compliance_results_by_agent", AgentCache.subsequent_content_compliance_results_by_agent, None)
        cache.set("subsequent_time_compliance_results_by_agent", AgentCache.subsequent_time_compliance_results_by_agent, None)
        cache.set("subsequent_missing_parameters_results_by_agent", AgentCache.subsequent_missing_parameters_results_by_agent, None)
        cache.set("rfo_content_compliance_results_by_agent", AgentCache.rfo_content_compliance_results_by_agent, None)
        cache.set("rfo_time_compliance_results_by_agent", AgentCache.rfo_time_compliance_results_by_agent, None)
        cache.set("rfo_missing_parameters_results_by_agent", AgentCache.rfo_missing_parameters_results_by_agent, None)
        cache.set("rfo_mis_classification_specification_by_agent", AgentCache.rfo_mis_classification_specification_by_agent, None)
        cache.set("rfo_mis_classification_fault_segment_by_agent", AgentCache.rfo_mis_classification_fault_segment_by_agent, None)
        cache.set("subsequent_responses_combinations_by_agent", AgentCache.subsequent_responses_combinations_by_agent, None)
        cache.set("discarded_responses_combinations_by_agent", AgentCache.discarded_responses_combinations_by_agent, None)
        d2 = datetime.now()
        print("time taken for agent cache swapping - ", d2-d1)

        d1 = datetime.now()
        cache.set("master_data", AdminCache.master_data, None)
        cache.set("bus_list", AdminCache.bus_list, None)
        cache.set("desks_list", AdminCache.desks_list, None)
        cache.set("owner_groups_list", AdminCache.owner_groups_list, None)
        d2 = datetime.now()
        print("time taken for admin cache swapping - ", d2-d1)

        d1 = datetime.now()
        cache.set("all_tickets_group_by_customer_name_and_rating", CustomerCache.all_tickets_group_by_customer_name_and_rating, None)
        cache.set("all_tickets_group_by_customer_name_and_rating_for_timeline", CustomerCache.all_tickets_group_by_customer_name_and_rating_for_timeline, None)
        cache.set("all_tickets_group_by_customer_name", CustomerCache.all_tickets_group_by_customer_name, None)
        cache.set("all_tickets_by_customer", CustomerCache.all_tickets_by_customer, None)
        d2 = datetime.now()
        print("time taken for customer cache swapping - ", d2-d1)
        print("Memory used:", psutil.Process(os.getpid()).memory_info())

    @staticmethod
    def load_agent_cache():
        print("Loading agent module cache on startup...")
        connection = AgentModuleDataFetch()
        connection.get_agent_names()  # all_agent_names cache
        connection.get_all_agents_performance()  # all_agents_performance cache
        connection.get_content_compliance_results_by_agent()  # content_compliance_results_by_agent cache
        connection.get_time_compliance_results_by_agent()  # time_compliance_results_by_agent cache
        connection.get_missing_parameters_results_by_agent()  # missing_parameters_results_by_agent cache
        connection.get_all_distinct_agents_performance()  # distinct_agent_names cache

        connection.get_subsequent_content_compliance_results_by_agent() #subsequent_content_compliance_results_by_agent cache
        connection.get_subsequent_time_compliance_results_by_agent()  # subsequent_time_compliance_results_by_agent cache
        connection.get_subsequent_missing_parameters_results_by_agent()  # subsequent_missing_parameters_results_by_agent cache

        connection.get_rfo_content_compliance_results_by_agent() #rfo_content_compliance_results_by_agent cache
        connection.get_rfo_time_compliance_results_by_agent()  # rfo_time_compliance_results_by_agent cache
        connection.get_rfo_missing_parameters_results_by_agent()  # rfo_missing_parameters_results_by_agent cache
        connection.get_rfo_mis_classification_specification_by_agent()  # rfo_mis_classification_specification_by_agent cache
        connection.get_rfo_mis_classification_fault_segment_by_agent()  # rfo_mis_classification_fault_segment_by_agent cache

        connection.get_subsequent_responses_combinations_by_agent()  # subsequent_responses_combinations_by_agent cache
        connection.get_discarded_responses_combinations_by_agent()  # discarded_responses_combinations_by_agent cache

    @staticmethod
    def load_admin_cache():
        print("Loading admin module cache on startup...")
        connection = AdminModuleDataFetch()
        connection.get_master_data()  # master_data cache
        connection.get_bus_list()  # bus_list cache
        connection.get_desks_list()  # desks_list cache

    @staticmethod
    def load_customer_cache():
        print("Loading customer module cache on startup...")
        connection = CustomerModuleDataFetch()
        connection.get_all_tickets_group_by_customer_name_and_rating()  # all_tickets_group_by_customer_name_and_rating
        connection.get_all_tickets_group_by_customer_name_and_rating_for_timeline()  # all_tickets_group_by_customer_name_and_rating_for_timeline
        connection.get_all_tickets_group_by_customer_name()  # all_tickets_group_by_customer_name
        connection.get_all_tickets_by_customer()  # all_tickets_by_customer

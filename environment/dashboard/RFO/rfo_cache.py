class RFOCache:
    # Tab-1
    rfo_compliance_results = None
    rfo_specification_accuracy_results = None
    rfo_fault_segment_accuracy_results = None
    rfo_time_compliance_results = None
    # Tab-2
    rfo_reason_names = None
    rfo_compliance_by_reason_results = None
    rfo_time_compliance_by_reason_results = None
    mis_classification_specification_by_reason_results = None
    mis_classification_fault_segment_by_reason_results = None
    rfo_time_sent_by_reason_results = None
    rfo_missing_parameters_by_reason_results = None
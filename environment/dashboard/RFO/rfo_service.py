from django.http import JsonResponse

from environment.dashboard.RFO.rfo_cache import RFOCache
from environment.dashboard.RFO.rfo_dbo import RFOModuleDataFetch

connection = RFOModuleDataFetch()

def get_rfo_compliance_results():
    result = RFOCache.rfo_compliance_results
    if result is None:
        result = connection.get_rfo_compliance_results()
    return result

def get_rfo_specification_accuracy_results():
    result = RFOCache.rfo_specification_accuracy_results
    if result is None:
        result = connection.get_rfo_specification_accuracy_results()
    return result

def get_rfo_fault_segment_accuracy_results():
    result = RFOCache.rfo_fault_segment_accuracy_results
    if result is None:
        result = connection.get_rfo_fault_segment_accuracy_results()
    return result

def get_rfo_time_compliance_results():
    result = RFOCache.rfo_time_compliance_results
    if result is None:
        result = connection.get_rfo_time_compliance_results()
    return result

def get_rfo_reason_names():
    result = RFOCache.rfo_reason_names
    if result is None:
        result = connection.get_rfo_reason_names()
    return result

def get_rfo_compliance_by_reason_results():
    result = RFOCache.rfo_compliance_by_reason_results
    if result is None:
        result = connection.get_rfo_compliance_by_reason_results()
    return result

def get_rfo_time_compliance_by_reason_results():
    result = RFOCache.rfo_time_compliance_by_reason_results
    if result is None:
        result = connection.get_rfo_time_compliance_by_reason_results()
    return result

def get_mis_classification_specification_by_reason_results():
    result = RFOCache.mis_classification_specification_by_reason_results
    if result is None:
        result = connection.get_mis_classification_specification_by_reason_results()
    return result

def get_mis_classification_fault_segment_by_reason_results():
    result = RFOCache.mis_classification_fault_segment_by_reason_results
    if result is None:
        result = connection.get_mis_classification_fault_segment_by_reason_results()
    return result

def get_rfo_time_sent_by_reason_results():
    result = RFOCache.rfo_time_sent_by_reason_results
    if result is None:
        result = connection.get_rfo_time_sent_by_reason_results()
    return result

def get_rfo_missing_parameters_by_reason_results():
    result = RFOCache.rfo_missing_parameters_by_reason_results
    if result is None:
        result = connection.get_rfo_missing_parameters_by_reason_results()
    return result
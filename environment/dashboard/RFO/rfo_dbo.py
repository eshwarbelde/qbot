import MySQLdb
from django.http import JsonResponse
from environment.dashboard.RFO.rfo_cache import RFOCache


class RFOModuleDataFetch:
    def __init__(self):
        self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
    
    def get_rfo_compliance_results(self):
        print("Getting RFO Compliance Results")
        query = "SELECT owner_group, product, severity, RFO_comp, count(*) FROM tickets GROUP BY " \
                "owner_group, product, severity, RFO_comp;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], RFO_comp=row[3],
                       count=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.rfo_compliance_results = result
        return result

    def get_rfo_specification_accuracy_results(self):
        print("Getting RFO Specification Accuracy Results")
        query = "SELECT owner_group, product, severity, R1_comp, count(*) FROM tickets GROUP BY " \
                "owner_group, product, severity, R1_comp;"
        cursor = self.con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], R1_comp=row[3],
                       count=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        RFOCache.rfo_specification_accuracy_results = result
        return result

    def get_rfo_fault_segment_accuracy_results(self):
        print("Getting RFO Fault segment Accuracy Results")
        query = "SELECT owner_group, product, severity, R2_comp, count(*) FROM tickets WHERE Reason2 <> '' GROUP BY " \
                "owner_group, product, severity, R2_comp;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], R2_comp=row[3],
                       count=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.rfo_fault_segment_accuracy_results = result
        return result
    
    def get_rfo_time_compliance_results(self):
        print("Getting RFO Time Compliance Results")
        query = "SELECT owner_group, product, severity, RFO_timecomp, count(*) FROM tickets GROUP BY " \
                "owner_group, product, severity, RFO_timecomp;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], RFO_timecomp=row[3],
                       count=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.rfo_time_compliance_results = result
        return result

    def get_rfo_reason_names(self):
        print("Getting RFO Reason names")
        query = "SELECT DISTINCT Reason1 FROM tickets"
        # "SELECT DISTINCT Reason1 FROM tickets UNION SELECT DISTINCT Reason2 FROM tickets"
        # "SELECT DISTINCT(CONCAT(Reason1, Reason2)) AS Reason FROM tickets GROUP BY (Reason);"
        # "SELECT DISTINCT Reason1, Reason2 FROM tickets;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [row[0] for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.rfo_reason_names = result
        return result

    def get_rfo_compliance_by_reason_results(self):
        print("Getting RFO Compliance Results By Reason")
        query = "SELECT owner_group, product, severity, Reason1, RFO_comp, count(*) FROM tickets GROUP BY " \
                "owner_group, product, severity, Reason1, RFO_comp;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], Reason1=row[3], RFO_comp=row[4],
                       count=row[5])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.rfo_compliance_by_reason_results = result
        return result

    def get_rfo_time_compliance_by_reason_results(self):
        print("Getting RFO Time Compliance Results By Reason")
        query = "SELECT owner_group, product, severity, Reason1, RFO_timecomp, count(*) FROM tickets GROUP BY " \
                "owner_group, product, severity, Reason1, RFO_timecomp;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], Reason1=row[3], RFO_timecomp=row[4],
                       count=row[5])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.rfo_time_compliance_by_reason_results = result
        return result

    def get_mis_classification_specification_by_reason_results(self):
        print("Getting RFO Mis Classification specification Results By Reason")
        query = "SELECT owner_group, product, severity, Reason1, rfo_specification, count(*) FROM tickets WHERE R1_comp = 0 GROUP BY " \
                "owner_group, product, severity, Reason1, rfo_specification;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], Reason1=row[3], rfo_specification=row[4],
                       count=row[5])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.mis_classification_specification_by_reason_results = result
        return result

    def get_mis_classification_fault_segment_by_reason_results(self):
        print("Getting RFO Mis Classification Fault Segment Results By Reason")
        query = "SELECT owner_group, product, severity, Reason1, RFO_FS, count(*) FROM tickets WHERE R2_comp = 0 GROUP BY " \
                "owner_group, product, severity, Reason1, RFO_FS;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], Reason1=row[3], RFO_FS=row[4],
                       count=row[5])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        RFOCache.mis_classification_fault_segment_by_reason_results = result
        return result

    def get_rfo_time_sent_by_reason_results(self):
        print("Getting RFO time sent Results By Reason")
        query = "select owner_group, product, severity, Reason1, 10*floor(RFO_diff/600) range_start, 10*floor(RFO_diff/600) + 10 range_end, concat(10*floor(RFO_diff/600), '  -  ', 10*floor(RFO_diff/600) + 10) as `range`, " \
            +"count(*) as count  FROM tickets group by owner_group, product, severity, Reason1, `range` order by range_start;"
        
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], Reason1=row[3],
            range_start=row[4], range_end=row[5], range=row[6], count=row[7]) for row in cursor.fetchall()]

        cursor.close()
        con.close()
        RFOCache.rfo_time_sent_by_reason_results = result
        return result

    def get_rfo_missing_parameters_by_reason_results(self):
        query = "SELECT owner_group, product, severity, Reason1, missing_parameters, count(*) FROM tickets WHERE missing_parameters <> 'undetermined' and " \
                "missing_parameters <> 'undertermined' and missing_parameters <> '' and missing_parameters <> 'None' and " \
                "missing_parameters <> 'nan' GROUP BY " \
                "owner_group, product, severity, Reason1, missing_parameters;"
        print(query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(owner_group=row[0], product=row[1], severity=row[2], Reason1=row[3], missing_parameters=row[4], count=row[5]) for row in cursor.fetchall()]
        RFOCache.rfo_missing_parameters_by_reason_results = result
        return result
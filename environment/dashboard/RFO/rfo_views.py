from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.template import loader
from environment.dashboard.RFO import rfo_service

def get_action_module_rfo_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('action_module_rfo.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')

def get_rfo_compliance_results(request):    
    result = rfo_service.get_rfo_compliance_results()
    return JsonResponse(result, safe=False)

def get_rfo_specification_accuracy_results(request):
    result = rfo_service.get_rfo_specification_accuracy_results()
    return JsonResponse(result, safe=False)

def get_rfo_fault_segment_accuracy_results(request):
    result = rfo_service.get_rfo_fault_segment_accuracy_results()
    return JsonResponse(result, safe=False)

def get_rfo_time_compliance_results(request):    
    result = rfo_service.get_rfo_time_compliance_results()
    return JsonResponse(result, safe=False)

def get_rfo_reason_names(request):
    result = rfo_service.get_rfo_reason_names()
    return JsonResponse(result, safe=False)

def get_rfo_compliance_by_reason_results(request):
    result = rfo_service.get_rfo_compliance_by_reason_results()
    return JsonResponse(result, safe=False)

def get_rfo_time_compliance_by_reason_results(request):
    result = rfo_service.get_rfo_time_compliance_by_reason_results()
    return JsonResponse(result, safe=False)

def get_mis_classification_specification_by_reason_results(request):
    result = rfo_service.get_mis_classification_specification_by_reason_results()
    return JsonResponse(result, safe=False)

def get_mis_classification_fault_segment_by_reason_results(request):
    result = rfo_service.get_mis_classification_fault_segment_by_reason_results()
    return JsonResponse(result, safe=False)

def get_rfo_time_sent_by_reason_results(request):
    result = rfo_service.get_rfo_time_sent_by_reason_results()
    return JsonResponse(result, safe=False)

def get_rfo_missing_parameters_by_reason_results(request):
    result = rfo_service.get_rfo_missing_parameters_by_reason_results()
    return JsonResponse(result, safe=False)
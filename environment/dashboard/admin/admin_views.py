from django.http import JsonResponse

from environment.dashboard.admin import admin_service


def get_master_data(request):
    result = admin_service.get_master_data()
    return JsonResponse(result, safe=False)


def add_bu(request):
    name = request.POST.get('buName', None)
    result = admin_service.add_bu(name)
    return JsonResponse(result, safe=False)


def add_desk(request):
    name = request.POST.get('deskName', None)
    bu_id = request.POST.get('bu_id', None)
    result = admin_service.add_desk(name, bu_id)
    return JsonResponse(result, safe=False)


def add_owner_group(request):
    name = request.POST.get('ownerGroupName', None)
    initial_sla = request.POST.get('initialSLA', None)
    subsequent_sla = request.POST.get('subsequentSLA', None)
    desk_id = request.POST.get('desk_id', None)
    result = admin_service.add_owner_group(name, initial_sla, subsequent_sla, desk_id)
    return JsonResponse(result, safe=False)


def get_bus_list(request):
    result = admin_service.get_bus_list()
    return JsonResponse(result, safe=False)


def get_desks_list(request):
    result = admin_service.get_desks_list()
    return JsonResponse(result, safe=False)


def get_owner_groups_list(request):
    result = admin_service.get_owner_groups_list()
    return JsonResponse(result, safe=False)


def get_data_for_bu_chart(request):
    result = admin_service.get_data_for_bu_chart()
    return JsonResponse(result, safe=False)


def remove_item(request):
    row_id = request.POST.get('id', None)
    table_name = request.POST.get('entity', None)
    result = admin_service.remove_item(table_name, row_id)
    return JsonResponse(result, safe=False)


def get_user_bus(request):
    bu_list = request.session['bu_list']
    print("User BU LIst")
    print(bu_list)
    data = {
        'buList': bu_list
    }
    return JsonResponse(data, safe=False)

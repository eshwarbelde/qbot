import MySQLdb

from environment.dashboard.admin.admin_cache import AdminCache


class AdminModuleDataFetch:

    def __init__(self):
        self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")

    """ def get_master_data(self):
        tables = ['product', 'severity', 'customer_name', 'owner_group']
        master_data = {}
        for table_name in tables:
            query = "SELECT name FROM " + table_name
            cursor = self.con.cursor()
            cursor.execute(query)
            master_data[table_name] = [dict(name=row[0]) for row in cursor.fetchall()]
            cursor.close()
        AdminCache.master_data = master_data
        return master_data """
    
    def get_master_data(self):
        keys = ['product', 'severity', 'customer_name', 'owner_group']
        master_data = {}
        for key in keys:
            if key != 'owner_group':
                query = "SELECT distinct "+ key +" FROM tickets"
            else:
                query = "SELECT distinct name FROM " + key
            cursor = self.con.cursor()
            cursor.execute(query)
            master_data[key] = [dict(name=row[0]) for row in cursor.fetchall()]
            cursor.close()
        AdminCache.master_data = master_data
        return master_data

    def add_bu(self, name):
        query = "INSERT INTO bu(name) values('" + name + "');"
        row_id = self.insert(query)
        self.get_bus_list()
        return row_id

    def add_desk(self, name, bu_id):
        query = "INSERT INTO desk(name, bu_id) values('" + name + "', " + bu_id + ");"
        row_id = self.insert(query)
        self.get_desks_list()
        return row_id

    def add_owner_group(self, name, initial_sla, subsequent_sla, desk_id):
        query = "INSERT INTO owner_group(name, initial_sla, subsequent_sla, desk_id) values('" + name + \
                "', '" + initial_sla + "', '" + subsequent_sla + "', " + desk_id + ");"
        row_id = self.insert(query)
        self.get_owner_groups_list()
        return row_id

    def get_bus_list(self):
        cursor = self.con.cursor()
        query = "SELECT id, name from bu;"
        cursor.execute(query)
        result = [dict(id=row[0], name=row[1]) for row in cursor.fetchall()]
        cursor.close()
        AdminCache.bus_list = result
        return result

    def get_desks_list(self):
        cursor = self.con.cursor()
        query = "SELECT desk.id, desk.name desk, bu.name bu from desk JOIN bu ON desk.bu_id=bu.id;"
        cursor.execute(query)
        result = [dict(id=row[0], name=row[1], bu=row[2]) for row in cursor.fetchall()]
        cursor.close()
        AdminCache.desks_list = result
        return result

    def get_owner_groups_list(self):
        query = "SELECT owner_group.id, owner_group.name owner_group, owner_group.initial_sla, owner_group.subsequent_sla, " \
                "desk.name from owner_group JOIN desk ON owner_group.desk_id=desk.id;"
        cursor = self.con.cursor()
        print(query)
        cursor.execute(query)
        result = [dict(id=row[0], name=row[1], initial_sla=row[2], subsequent_sla=row[3], desk=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        AdminCache.owner_groups_list = result
        return result

    def remove_item(self, table_name, row_id):
        query = "DELETE FROM " + table_name + " WHERE id="+row_id+";"
        count = self.delete(query)
        if table_name == "bu":
            self.get_bus_list()
        elif table_name == "desk":
            self.get_desks_list()
        elif table_name == "owner_group":
            self.get_owner_groups_list()

        if count > 0:
            record = {'message': 'Success'}
        else:
            record = {'message': 'Error'}
        return record

    def insert(self, query):
        try:
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()  # commit the transaction
            last_id = cursor.lastrowid
            cursor.close()
            return last_id
        except:
            self.con.rollback()

    def delete(self, query):
        try:
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()
            count = cursor.rowcount
            cursor.close()
            return count
        except Exception as e:
            print(e)
            self.con.rollback()

from environment.dashboard.admin.admin_cache import AdminCache
from environment.dashboard.admin.admin_dao import AdminModuleDataFetch

connection = AdminModuleDataFetch()


def get_master_data():
    result = AdminCache.master_data
    if result is None:
        result = connection.get_master_data()
    return result


def add_bu(name):
    return connection.add_bu(name)


def add_desk(name, bu_id):
    return connection.add_desk(name, bu_id)


def add_owner_group(name, initial_sla, subsequent_sla, desk_id):
    return connection.add_owner_group(name, initial_sla, subsequent_sla, desk_id)

def get_bus_list():
    result = AdminCache.bus_list
    if result is None:
        result = connection.get_bus_list()
    return result


def get_desks_list():
    result = AdminCache.desks_list
    if result is None:
        result = connection.get_desks_list()
    return result


def get_owner_groups_list():
    result = AdminCache.owner_groups_list
    if result is None:
        result = connection.get_owner_groups_list()
    return result


def get_data_for_bu_chart():
    print("Getting data for BUChart")
    bu_list = get_bus_list()
    desks_list = get_desks_list()
    owner_group_list = get_owner_groups_list()
    result = []
    for row in bu_list:
        bu_obj = {'name': row['name']}
        desks_array = []
        for desk in desks_list:
            if desk['bu'] == row['name']:
                desk_obj = {'name': desk['name']}
                desks_array.append(desk_obj)
                owner_groups_array = []
                for og in owner_group_list:
                    if og['desk'] == desk['name']:
                        owner_group_obj = {'name': og['name']}
                        owner_groups_array.append(owner_group_obj)
                desk_obj['owner_groups'] = owner_groups_array
        bu_obj['desks'] = desks_array
        result.append(bu_obj)
    AdminCache.data_for_bu_chart = result
    return result


def remove_item(table_name, row_id):
    return connection.remove_item(table_name, row_id)
from django.db import models
import pymongo

# Create your models here.
class Ticket(models.Model):
	ticket_number = models.CharField(max_length=20)
	ticket_booking_agent_name = models.CharField(max_length=50)
	ticket_booking_time = models.DateTimeField()
	owner_group = models.CharField(max_length=20)
	service_id = models.CharField(max_length=20)
	severity = models.CharField(max_length=30)
	circuit_type = models.CharField(max_length=20)
	ticket_type = models.CharField(max_length=20)
	channel = models.CharField(max_length=20)
	rating = models.CharField(max_length=20)
	class Meta:
		verbose_name = "ticket"
		verbose_name_plural = "tickets"

	def __unicode__(self):
		return self.ticket_number

class initial_mails(models.Model):
	ticket_number = models.CharField(max_length=20)
	i_sla_time = models.DateTimeField()
	i_link_stat_chk = models.BooleanField()
	i_link_stat_sim = models.BooleanField()
	i_logs = models.BooleanField()
	i_cust_dep =  models.BooleanField()
	i_poa =  models.BooleanField()
	i_time_1actual = models.DateTimeField()
	i_time_2actual = models.DateTimeField()
	i_time_1u = models.BooleanField()
	i_time_2u = models.BooleanField()
	i_resp_time = models.BooleanField()
	i_time_update = models.DateTimeField()
	i_time_benchmak = models.DateTimeField()
	i_time = models.BooleanField()
	i_gram = models.BooleanField()
	i_spell = models.BooleanField()
	i_comp_rating = models.CharField(max_length=5)

	class Meta:
		verbose_name = "Initial_mail"
		verbose_name_plural = "Initial_mails"


	def __unicode__(self):
		return self.ticket_number

	def getAllInitialMails(self):
		return initial_mails.objects.all()
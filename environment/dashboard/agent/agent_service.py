from django.core.cache import cache
from environment.dashboard.agent.agent_dao import AgentModuleDataFetch
from environment.classification.agent_training import scoring
import pandas as pd

connection = AgentModuleDataFetch()
scoring_models = scoring()
sankey_data = pd.read_excel('environment/dashboard/agent/filtered.xlsx')
casestate = pd.read_excel('environment/dashboard/agent/casestate.xlsx')


def get_rfos(customername):
    data = sankey_data[sankey_data['u_customer_name'] == customername]
    rfos = list(data['RFO Specification'].unique())
    dic ={'rfo_list':rfos}
    return dic


def get_sankeydata_customer(customername,rfo):
    print(rfo)
    if rfo=='All':
        data = sankey_data[sankey_data['u_customer_name']==customername]
    else:
        data = sankey_data[sankey_data['u_customer_name']==customername]
        data = data[data['RFO Specification']==rfo]
    return data

def get_sankeydata_casestate(customername,rfo):
    if rfo=='All':
        data = casestate[casestate['u_customer_name']==customername]
    else:
        data = casestate[casestate['u_customer_name']==customername]
        data = data[data['RFO Specification']==rfo]
    return data


def get_agent_analysis_response(scenario_no, response):
    flag,result = scoring_models.predictions(scenario_no, response)
    return flag,result

def get_agent_names():
    result = cache.get("all_agent_names")
    return result

#def get_sankeydata_customer():


def get_all_responses_by_agent_name(agent_name, owner_group, start_date, end_date):
    return connection.get_all_responses_by_agent_name(agent_name, owner_group, start_date, end_date)


def get_all_responses_by_agent_name_and_rating(agent_name, resp_rating):
    return connection.get_all_responses_by_agent_name_and_rating(agent_name, resp_rating)


def get_all_responses_by_agent_name_and_time_compliance(agent_name, time_compliance):
    return connection.get_all_responses_by_agent_name_and_time_compliance(agent_name, time_compliance)


def get_all_responses_by_agent_name_for_timeline(agent_name, owner_group, start_date, end_date):
    return connection.get_all_responses_by_agent_name_for_timeline(agent_name, owner_group, start_date, end_date)


def get_all_agents_performance():
    result = cache.get("all_agents_performance")
    # if result is None:
    #     result = connection.get_all_agents_performance()
    return result


def get_content_compliance_results_by_agent():
    result = cache.get("content_compliance_results_by_agent")
    # if result is None:
    #     result = connection.get_content_compliance_results_by_agent()
    return result


def get_time_compliance_results_by_agent():
    result = cache.get("time_compliance_results_by_agent")
    # if result is None:
    #     result = connection.get_time_compliance_results_by_agent()
    return result


def get_missing_parameters_results_by_agent():
    result = cache.get("missing_parameters_results_by_agent")
    # if result is None:
    #     result = connection.get_missing_parameters_results_by_agent()
    return result


def get_time_compliance_difference_results_with_sla_for_agent(agent_name):
    return connection.get_time_compliance_difference_results_with_SLA_for_agent(agent_name)


def get_all_distinct_agents_performance(agent_name, week_no):
    data = cache.get("distinct_agent_scores")
    # if data is None:
    #     data = connection.get_all_distinct_agents_performance()

    result = []
    for record in data:
        if agent_name != "" and agent_name == record['agent_name']:
            result.append(record)
    return result

def get_subsequent_content_compliance_results_by_agent():
    result = cache.get("subsequent_content_compliance_results_by_agent")
    # if result is None:
    #     result = connection.get_subsequent_content_compliance_results_by_agent()
    return result

def get_subsequent_time_compliance_results_by_agent():
    result = cache.get("subsequent_time_compliance_results_by_agent")
    # if result is None:
    #     result = connection.get_subsequent_time_compliance_results_by_agent()
    return result

def get_subsequent_missing_parameters_results_by_agent():
    result = cache.get("subsequent_missing_parameters_results_by_agent")
    # if result is None:
    #     result = connection.get_subsequent_missing_parameters_results_by_agent()
    return result

def get_subsequent_time_compliance_difference_results_with_sla_for_agent(agent_name):
    return connection.get_subsequent_time_compliance_difference_results_with_sla_for_agent(agent_name)

def get_rfo_content_compliance_results_by_agent():
    result = cache.get("rfo_content_compliance_results_by_agent")
    # if result is None:
    #     result = connection.get_rfo_content_compliance_results_by_agent()
    return result

def get_rfo_time_compliance_results_by_agent():
    result = cache.get("rfo_time_compliance_results_by_agent")
    # if result is None:
    #     result = connection.get_rfo_time_compliance_results_by_agent()
    return result

def get_rfo_missing_parameters_results_by_agent():
    result = cache.get("rfo_missing_parameters_results_by_agent")
    # if result is None:
    #     result = connection.get_rfo_missing_parameters_results_by_agent()
    return result

def get_rfo_time_compliance_difference_results_with_sla_for_agent(agent_name):
    return connection.get_rfo_time_compliance_difference_results_with_sla_for_agent(agent_name)

def get_rfo_mis_classification_specification_by_agent():
    result = cache.get("rfo_mis_classification_specification_by_agent")
    # if result is None:
    #     result = connection.get_rfo_mis_classification_specification_by_agent()
    return result

def get_rfo_mis_classification_fault_segment_by_agent():
    result = cache.get("rfo_mis_classification_fault_segment_by_agent")
    # if result is None:
    #     result = connection.get_rfo_mis_classification_fault_segment_by_agent()
    return result

def get_subsequent_responses_combinations_by_agent():
    result = cache.get("subsequent_responses_combinations_by_agent")
    # if result is None:
    #     result = connection.get_subsequent_responses_combinations_by_agent()
    return result

def get_discarded_responses_combinations_by_agent():
    result = cache.get("discarded_responses_combinations_by_agent")
    # if result is None:
    #     result = connection.get_discarded_responses_combinations_by_agent()
    return result
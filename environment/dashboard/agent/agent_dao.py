import MySQLdb
from datetime import datetime
from environment.dashboard.agent.agent_cache import AgentCache
class AgentModuleDataFetch:
    def __init__(self):
        self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")

    def get_agent_names(self):
        print("Getting all the unique agent names...")
        query = "SELECT DISTINCT agent_name FROM tickets;"
        cursor = self.con.cursor()
        cursor.execute(query)
        result = [row[0] for row in cursor.fetchall()]
        cursor.close()
        AgentCache.all_agent_names = result
        return result

    def get_all_responses_by_agent_name(self, agent_name, owner_group, start_date, end_date):
        print("calling this method because cache is not present:", agent_name)
        print("Getting all responses by agent:", agent_name)
        d1 = datetime.now()
        if agent_name != 'All' and agent_name != '':
            if owner_group != 'All' and owner_group != '':
                if start_date != "" and end_date != "":
                    initial_query = "SELECT resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY resp_rating;"
                    subsequent_query = "SELECT resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY resp_rating;"
                    discarded_query = "SELECT resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY resp_rating;"

                    initial_time_query = "SELECT resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY i_time_compliance;"
                    subsequent_time_query = "SELECT resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY si_time_compliance;"
                    discarded_time_query = "SELECT resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY d_time_compliance;"
                else:
                    initial_query = "SELECT resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY resp_rating;"
                    subsequent_query = "SELECT resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY resp_rating;"
                    discarded_query = "SELECT resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY resp_rating;"

                    initial_time_query = "SELECT resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY i_time_compliance;"
                    subsequent_time_query = "SELECT resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY si_time_compliance;"
                    discarded_time_query = "SELECT resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY d_time_compliance;"
            else:
                initial_query = "SELECT resp_rating, count(*) FROM initial_responses WHERE agent_name='" + agent_name + "' GROUP BY resp_rating;"
                subsequent_query = "SELECT resp_rating, count(*) FROM subsequent_responses WHERE agent_name='" + agent_name + "' GROUP BY resp_rating;"
                discarded_query = "SELECT resp_rating, count(*) FROM discarded_responses WHERE agent_name='" + agent_name + "' GROUP BY resp_rating;"

                initial_time_query = "SELECT resp_rating, count(*) FROM initial_responses WHERE agent_name='" + agent_name + "' GROUP BY i_time_compliance;"
                subsequent_time_query = "SELECT resp_rating, count(*) FROM subsequent_responses WHERE agent_name='" + agent_name + "' GROUP BY si_time_compliance;"
                discarded_time_query = "SELECT resp_rating, count(*) FROM discarded_responses WHERE agent_name='" + agent_name + "' GROUP BY d_time_compliance;"
        else:
            initial_query = "SELECT resp_rating, count(*) FROM initial_responses GROUP BY resp_rating;"
            subsequent_query = "SELECT resp_rating, count(*) FROM subsequent_responses GROUP BY resp_rating;"
            discarded_query = "SELECT resp_rating, count(*) FROM discarded_responses GROUP BY resp_rating;"

            initial_time_query = "SELECT resp_rating, count(*) FROM initial_responses GROUP BY i_time_compliance;"
            subsequent_time_query = "SELECT resp_rating, count(*) FROM subsequent_responses GROUP BY " \
                                    "si_time_compliance;"
            discarded_time_query = "SELECT resp_rating, count(*) FROM discarded_responses GROUP BY d_time_compliance;"

        print(initial_query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(initial_query)
        initial_result = cursor.fetchall()
        initial_result = [dict(resp_rating=row[0], count=row[1]) for row in initial_result]
        cursor.close()

        cursor = con.cursor()
        cursor.execute(subsequent_query)
        subsequent_result = cursor.fetchall()
        subsequent_result = [dict(resp_rating=row[0], count=row[1]) for row in subsequent_result]
        cursor.close()
        con.close()

        print(discarded_query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(discarded_query)
        discarded_result = cursor.fetchall()
        discarded_result = [dict(resp_rating=row[0], count=row[1]) for row in discarded_result]
        cursor.close()
        con.close()

        print(initial_time_query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(initial_time_query)
        initial_time_result = cursor.fetchall()
        initial_time_result = [dict(i_time_compliance=row[0], count=row[1]) for row in initial_time_result]
        cursor.close()
        con.close()

        print(subsequent_time_query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(subsequent_time_query)
        subsequent_time_result = cursor.fetchall()
        subsequent_time_result = [dict(si_time_compliance=row[0], count=row[1]) for row in subsequent_time_result]
        cursor.close()
        con.close()

        print(discarded_time_query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(discarded_time_query)
        discarded_time_result = cursor.fetchall()
        discarded_time_result = [dict(d_time_compliance=row[0], count=row[1]) for row in discarded_time_result]
        cursor.close()
        con.close()

        quality_results = {
            "initial_emails": initial_result,
            "subsequent_emails": subsequent_result,
            "discarded_emails": discarded_result
        }
        time_results = {
            "initial_emails": initial_time_result,
            "subsequent_emails": subsequent_time_result,
            "discarded_emails": discarded_time_result
        }
        final_results = {
            "quality": quality_results,
            "time": time_results
        }
        d2 = datetime.now()
        print(d2 - d1)
        return final_results

    def get_all_responses_by_agent_name_and_rating(self, agent_name, resp_rating):
        if resp_rating == 'Discarded':
            query = "SELECT * FROM discarded_responses WHERE agent_name='"+agent_name+"';"
        elif resp_rating == 'Compliant':
            query = "SELECT * FROM initial_responses WHERE agent_name='"+agent_name+"' and resp_rating='Perfect';"
        else:
            query = "SELECT * FROM initial_responses WHERE agent_name='" + agent_name + \
                    "' and resp_rating='Needs Improvement';"
        print(query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(line) for line in [zip([column[0] for column in cursor.description], row)
                                          for row in cursor.fetchall()]]
        cursor.close()
        con.close()
        return result

    def get_all_responses_by_agent_name_and_time_compliance(self, agent_name, time_compliance):
        if time_compliance == 'Compliant':
            query = "SELECT * FROM initial_responses WHERE agent_name='" + agent_name + "' and i_time_compliance = 1;"
        else:
            query = "SELECT * FROM initial_responses WHERE agent_name='" + agent_name + \
                    "' and i_time_compliance = 0;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(line) for line in [zip([column[0] for column in cursor.description], row)
                                          for row in cursor.fetchall()]]
        cursor.close()
        con.close()
        return result

    def get_all_responses_by_agent_name_for_timeline(self, agent_name, owner_group, start_date, end_date):
        print("Getting all responses by agent:", agent_name, " for timeline chart")
        d1 = datetime.now()
        if agent_name != 'All' and agent_name != '':
            if owner_group != 'All' and owner_group != '':
                if start_date != "" and end_date != "":
                    initial_query = "SELECT date_format(i_time_stamp, '%d-%m-%Y') as i_time_stamp,resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY date_format(i_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY i_time_stamp;"
                    subsequent_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp,resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY date_format(si_timestamp, '%d-%m-%Y'),resp_rating ORDER BY si_timestamp;"
                    discarded_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp,resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' and tickets.ticket_booking_time>='" + start_date + "' and tickets.ticket_booking_time<='" + end_date + "' GROUP BY date_format(d_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY d_time_stamp;"
                else:
                    initial_query = "SELECT date_format(i_time_stamp, '%d-%m-%Y') as i_time_stamp,resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY date_format(i_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY i_time_stamp;"
                    subsequent_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp,resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "' GROUP BY date_format(si_timestamp, '%d-%m-%Y'),resp_rating ORDER BY si_timestamp;"
                    discarded_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp,resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE tickets.owner_group ='" + owner_group + "' and initial_responses.agent_name='" + agent_name + "'  GROUP BY date_format(d_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY d_time_stamp;"
            else:
                initial_query = "SELECT date_format(i_time_stamp, '%d-%m-%Y') as i_time_stamp,resp_rating, count(*) FROM initial_responses WHERE initial_responses.agent_name='" + agent_name + "' GROUP BY date_format(i_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY i_time_stamp;"
                subsequent_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp,resp_rating, count(*) FROM subsequent_responses WHERE initial_responses.agent_name='" + agent_name + "' GROUP BY date_format(si_timestamp, '%d-%m-%Y'),resp_rating ORDER BY si_timestamp;"
                discarded_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp,resp_rating, count(*) FROM discarded_responses WHERE initial_responses.agent_name='" + agent_name + "'  GROUP BY date_format(d_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY d_time_stamp;"
        else:
            initial_query = "SELECT date_format(i_time_stamp, '%d-%m-%Y') as i_time_stamp,resp_rating, count(*) FROM initial_responses GROUP BY date_format(i_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY i_time_stamp;"
            subsequent_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp,resp_rating, count(*) FROM subsequent_responses GROUP BY date_format(si_timestamp, '%d-%m-%Y'),resp_rating ORDER BY si_timestamp;"
            discarded_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp,resp_rating, count(*) FROM discarded_responses  GROUP BY date_format(d_time_stamp, '%d-%m-%Y'),resp_rating ORDER BY d_time_stamp;"

        print(initial_query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(initial_query)
        initial_result = cursor.fetchall()
        initial_result = [[row[0], row[1], row[2]] for row in initial_result]
        print(initial_result)

        print(initial_query)
        cursor.execute(subsequent_query)
        subsequent_result = cursor.fetchall()
        subsequent_result = [[row[0], row[1], row[2]] for row in subsequent_result]
        print(subsequent_result)

        print(discarded_query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(discarded_query)
        discarded_result = cursor.fetchall()
        discarded_result = [[row[0], row[1], row[2]] for row in discarded_result]
        print(discarded_result)
        final_results = {
            "initial_emails": initial_result,
            "subsequent_emails": subsequent_result,
            "discarded_emails": discarded_result
        }
        return final_results

    def get_all_agents_performance(self):
        print("Getting all agent performances by mean")
        query = "select owner_group, initial_responses.agent_name, count(*) from initial_responses JOIN tickets ON " \
                "tickets.ticket_number=initial_responses.ticket_number GROUP BY owner_group, " \
                "initial_responses.agent_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_total_initial = [dict(owner_group=row[0], agent_name=row[1], count=row[2]) for row in cursor.fetchall()]
        cursor.close()
        
        query = "select owner_group, initial_responses.agent_name, count(*) from initial_responses JOIN tickets ON " \
                "tickets.ticket_number=initial_responses.ticket_number WHERE resp_rating='Perfect' GROUP BY " \
                "owner_group, initial_responses.agent_name;"
        
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_perfect_initial = [dict(owner_group=row[0], agent_name=row[1], count=row[2]) for row in cursor.fetchall()]
        cursor.close()

        query = "select owner_group, subsequent_responses.agent_name, count(*) from subsequent_responses JOIN tickets" \
                " ON tickets.ticket_number=subsequent_responses.ticket_number GROUP BY owner_group, " \
                "subsequent_responses.agent_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_total_subsequent = [dict(owner_group=row[0], agent_name=row[1], count=row[2])
                                  for row in cursor.fetchall()]
        cursor.close()

        query = "select owner_group, subsequent_responses.agent_name, count(*) from subsequent_responses JOIN tickets" \
                " ON tickets.ticket_number=subsequent_responses.ticket_number WHERE resp_rating='Perfect' GROUP BY " \
                "owner_group, subsequent_responses.agent_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_perfect_subsequent = [dict(owner_group=row[0], agent_name=row[1], count=row[2])
                                    for row in cursor.fetchall()]
        cursor.close()
        result = []        
        for row in agent_total_initial:
            result.append(dict(owner_group=row['owner_group'], agent_name=row['agent_name'], values=dict(
                total_initial=row['count'], perfect_initial=0,  total_subsequent=0, perfect_subsequent=0)))
        for row in agent_perfect_initial:
            match = next((d for d in result if d['agent_name'] == row['agent_name'] and
                          d['owner_group'] == row['owner_group']), None)
            if not match:
                result.append(dict(owner_group=row['owner_group'], agent_name=row['agent_name'], values=dict(
                    total_initial=0, perfect_initial=row['count'],  total_subsequent=0, perfect_subsequent=0)))
            else:
                match['values']['perfect_initial'] = row['count']
            
        for row in agent_total_subsequent:
            match = next((d for d in result if d['agent_name'] == row['agent_name'] and
                          d['owner_group'] == row['owner_group']), None)
            if not match:
                result.append(dict(owner_group=row['owner_group'], agent_name=row['agent_name'], values=dict(
                    total_initial=0, perfect_initial=0, total_subsequent=row['count'], perfect_subsequent=0)))
            else:
                match['values']['total_subsequent'] = row['count']

        for row in agent_perfect_subsequent:
            match = next((d for d in result if d['agent_name'] == row['agent_name'] and
                          d['owner_group'] == row['owner_group']), None)
            if not match:
                result.append(dict(owner_group=row['owner_group'], agent_name=row['agent_name'], values=dict(
                    total_initial=0, perfect_initial=0, total_subsequent=0, perfect_subsequent=row['count'])))
            else:
                match['values']['perfect_subsequent'] = row['count']
        agent_records = []
        for row in result:
            values = row['values']
            perfect_responses = values['perfect_initial'] + values['perfect_subsequent']
            total_responses = values['total_initial'] + values['total_subsequent']  
            avg = round((perfect_responses / total_responses) * 100, 0)
            agent_records.append({
              'owner_group': row['owner_group'],  
              'agent_name': row['agent_name'],
              'perfect_responses': perfect_responses,
              'total_responses': total_responses,
              'perfect_responses_percentage': avg
            })
        final_result = []
        cursor = self.con.cursor()
        query = "SELECT id, name from bu;"
        cursor.execute(query)
        bu_list = [dict(id=row[0], name=row[1]) for row in cursor.fetchall()]
        cursor.close()

        cursor = self.con.cursor()
        query = "SELECT desk.id, desk.name, bu.name from desk JOIN bu ON desk.bu_id=bu.id;"
        cursor.execute(query)
        desks_list = [dict(id=row[0], name=row[1], bu=row[2]) for row in cursor.fetchall()]
        cursor.close()

        cursor = self.con.cursor()
        query = "SELECT owner_group.id, owner_group.name, desk.name from owner_group JOIN desk ON " \
                "owner_group.desk_id=desk.id;"
        print(query)
        cursor.execute(query)
        owner_group_list = [dict(id=row[0], name=row[1], desk=row[2])
                            for row in cursor.fetchall()]
        cursor.close()

        for bu in bu_list:
            bu_obj = {'name': bu['name']}
            desks_array = []
            for desk in desks_list:
                if desk['bu'] == bu['name']:
                    desk_obj = {'name': desk['name']}
                    desks_array.append(desk_obj)
                    owner_groups_array = []
                    for og in owner_group_list:
                        if og['desk'] == desk['name']:
                            owner_group_obj = {'name': og['name']}
                            owner_groups_array.append(owner_group_obj)
                            agents_array = []
                            for agent_record in agent_records:
                                if agent_record['owner_group'] == og['name']:
                                    agents_array.append({
                                        'agent_name': agent_record['agent_name'],
                                        'perfect_responses': agent_record['perfect_responses'],
                                        'total_responses': agent_record['total_responses'],
                                        'perfect_responses_percentage': agent_record['perfect_responses_percentage']
                                    })
                            owner_group_obj['agents'] = agents_array
                    desk_obj['owner_groups'] = owner_groups_array
            bu_obj['desks'] = desks_array
            final_result.append(bu_obj)
        AgentCache.all_agents_performance = final_result
        return final_result

    def get_content_compliance_results_by_agent(self):
        discrded_total = "SELECT agent_name, count(*) FROM discarded_responses GROUP BY agent_name;"
        initial_perfect = "SELECT agent_name, count(*) FROM initial_responses WHERE resp_rating = 'Perfect'  " \
                          "GROUP BY agent_name;"
        initial_needs_improvement = "SELECT agent_name, count(*) FROM initial_responses WHERE resp_rating = " \
                                    "'Needs Improvement' GROUP BY agent_name;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(discrded_total)
        discarded = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        con.close()
        cursor = self.con.cursor()
        cursor.execute(initial_perfect)
        perfect_initial = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        cursor = self.con.cursor()
        cursor.execute(initial_needs_improvement)
        needs_improvement_initial = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        result = []
        for row in discarded:
            result.append(dict(agent_name=row['agent_name'], values=dict(discarded=row['count'], perfect_initial=0,
                                                                         needs_improvement_initial=0)))

        for row in perfect_initial:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(discarded=0, perfect_initial=row['count'],
                                                                             needs_improvement_initial=0)))
            else:
                match['values']['perfect_initial'] = row['count']
        for row in needs_improvement_initial:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(discarded=0, perfect_initial=0,
                                                                             needs_improvement_initial=row['count'])))
            else:
                match['values']['needs_improvement_initial'] = row['count']
        agent_records = []
        for row in result:
            values = row['values']
            total_records = values['discarded'] + values['perfect_initial'] + values['needs_improvement_initial']
            records = [dict(name='Discarded', count=values['discarded'], total_records=total_records,
                            percentage=round((values['discarded'] * 100) / total_records)),
                       dict(name='Compliant', count=values['perfect_initial'], total_records=total_records,
                            percentage=round((values['perfect_initial'] * 100) / total_records)),
                       dict(name='Non-compliant', count=values['needs_improvement_initial'],
                            total_records=total_records,
                            percentage=round((values['needs_improvement_initial'] * 100) / total_records))]

            agent_records.append({
              'agent_name': row['agent_name'],
              'values': records
            })
        AgentCache.content_compliance_results_by_agent = agent_records
        return agent_records

    def get_time_compliance_results_by_agent(self):
        compliant_query = "SELECT agent_name, count(*) FROM initial_responses WHERE i_time_compliance=1 " \
                          "GROUP BY agent_name;"
        non_compliant_query = "SELECT agent_name, count(*) FROM initial_responses WHERE i_time_compliance=0 " \
                              "GROUP BY agent_name;"
        cursor = self.con.cursor()
        cursor.execute(compliant_query)
        time_compliant = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        print(non_compliant_query)
        cursor = self.con.cursor()
        cursor.execute(non_compliant_query)
        time_non_compliant = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        result = []
        for row in time_compliant:
            result.append(dict(agent_name=row['agent_name'], values=dict(compliant=row['count'], non_compliant=0)))

        for row in time_non_compliant:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(compliant=0, non_compliant=row['count'])))
            else:
                match['values']['non_compliant'] = row['count']
        agent_records = []
        for row in result:
            values = row['values']
            total_records = values['compliant'] + values['non_compliant']
            records = [dict(name='Compliant', count=values['compliant'], total_records=total_records,
                            percentage=round((values['compliant'] * 100) / total_records)),
                       dict(name='Non-compliant', count=values['non_compliant'], total_records=total_records,
                            percentage=round((values['non_compliant'] * 100) / total_records))]

            agent_records.append({
              'agent_name': row['agent_name'],
              'values': records
            })
        AgentCache.time_compliance_results_by_agent = agent_records
        return result

    def get_missing_parameters_results_by_agent(self):
        query = "select agent_name, shortest_path, sum(count) counts FROM ((select agent_name, shortest_path, " \
                "count(*) count from initial_responses WHERE shortest_path <> 'undetermined' and shortest_path <> 'nan' and " \
                "shortest_path <> 'None' group by agent_name, shortest_path) UNION (select agent_name, shortest_path," \
                " count(*) count from discarded_responses WHERE shortest_path <> 'undetermined' and shortest_path <> 'nan' and " \
                "shortest_path <> 'None' group by agent_name, shortest_path)) as tmp" \
                " group by agent_name, shortest_path;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(agent_name=row[0], shortest_path=row[1], count=row[2]) for row in cursor.fetchall()]
        AgentCache.missing_parameters_results_by_agent = result
        return result

    def get_time_compliance_difference_results_with_SLA_for_agent(self, agent_name):
        query = "select 10*floor(difference/10) range_start, 10*floor(difference/10) + 10 range_end, concat(10*floor(difference/10), '  -  ', 10*floor(difference/10) + 10) as `range`, " \
            +"count(*) as count  from (SELECT initial_responses.ticket_number, i_time_stamp,  ticket_booking_time, TIMESTAMPDIFF(MINUTE,ticket_booking_time, i_time_stamp), " \
            +"initial_sla, (TIMESTAMPDIFF(MINUTE,ticket_booking_time, i_time_stamp)) - owner_group.initial_sla difference from initial_responses JOIN tickets ON initial_responses.ticket_number=tickets.ticket_number " \
            +"JOIN owner_group ON tickets.owner_group = owner_group.name where initial_sla <> 'As per customer segment' and initial_responses.agent_name='"+agent_name+"') as t group by `range` order by range_start;"

        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(range_start=row[0], range_end=row[1], range=row[2], count=row[3]) for row in cursor.fetchall()]
        cursor.close()
        con.close()
        return result

    def get_all_distinct_agents_performance(self):
        distinct_agents = self.get_distinct_agents()
        agent_scores = []
        for record in distinct_agents:
            agent_score = self.agent_overall_performance(record['agent_name'], record['week'])
            dict = {
                "agent_name": record['agent_name'],
                "week_no": record['week'],
                "agent_score": agent_score
            }
            agent_scores.append(dict)
        print(agent_scores)
        AgentCache.distinct_agent_scores = agent_scores
        return agent_scores

    def get_distinct_agents(self):
        query = "select distinct agent_name, week_no FROM (SELECT agent_name, CONCAT(YEAR(ticket_booking_time), '/', " \
                "WEEK(ticket_booking_time)) week_no FROM tickets GROUP BY agent_name, week_no UNION ALL " \
                "SELECT agent_name,CONCAT(YEAR(d_time_stamp), '/', WEEK(d_time_stamp)) week_no FROM " \
                "discarded_responses GROUP BY agent_name, week_no UNION ALL SELECT agent_name," \
                "CONCAT(YEAR(i_time_stamp), '/', WEEK(i_time_stamp)) week_no FROM initial_responses GROUP BY " \
                "agent_name, week_no UNION ALL SELECT agent_name,CONCAT(YEAR(si_timestamp), '/', WEEK(si_timestamp)) " \
                "week_no FROM subsequent_responses GROUP BY agent_name, week_no) t;"
        cursor = self.con.cursor()
        cursor.execute(query)
        data = [dict(agent_name=row[0], week=row[1]) for row in cursor.fetchall()]
        return data

    def agent_overall_performance(self, agent_name, week_no):
        rfo_score = 0
        query = "select count(*) FROM tickets where agent_name='"+agent_name+"' AND " \
                "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time))='"+week_no +"' GROUP BY agent_name, " \
                "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time));"
        cursor = self.con.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        no_of_rfo = 0
        if cursor.rowcount > 0:
            no_of_rfo = row[0]
        if no_of_rfo > 0:
            query = "select count(*) FROM tickets where agent_name='" + agent_name + "' AND R1_comp=1 AND " \
                    "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time))='" + week_no + "' GROUP BY " \
                    "agent_name, CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            reason1 = 0
            if cursor.rowcount > 0:
                reason1 = row[0]
            query = "select count(*) FROM tickets where agent_name='" + agent_name + "' AND R2_comp=1 AND " \
                    "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time))='" + week_no + "' GROUP BY " \
                    "agent_name, CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            reason2 = 0
            if cursor.rowcount > 0:
                reason2 = row[0]
            query = "select count(*) FROM tickets where agent_name='" + agent_name + "' AND RFO_timecomp=1 AND " \
                    "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time))='" + week_no + "' GROUP BY agent_name," \
                    "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            time = 0
            if cursor.rowcount > 0:
                time = row[0]
            query = "select count(*) FROM tickets where agent_name='" + agent_name + "' AND RFO_comp=1 AND " \
                    "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time))='" + week_no + "' GROUP BY agent_name, " \
                    "CONCAT(YEAR(ticket_booking_time), '/', WEEK(ticket_booking_time));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            quality = 0
            if cursor.rowcount > 0:
                quality = row[0]
            rfo_score = (reason1 + reason2 + time + quality) / (no_of_rfo * 4)
        query = "select count(*) FROM discarded_responses where agent_name='"+agent_name+"' AND " \
                "CONCAT(YEAR(d_time_stamp), '/', WEEK(d_time_stamp))='"+week_no +"' GROUP BY agent_name, " \
                "CONCAT(YEAR(d_time_stamp), '/', WEEK(d_time_stamp));"
        cursor = self.con.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        discarded_count = 0
        if cursor.rowcount > 0:
            discarded_count = row[0]
            discarded_time = discarded_count
        else:
            discarded_time = 0
        query = "select count(*) FROM initial_responses where agent_name='"+agent_name+"' AND " \
                "CONCAT(YEAR(i_time_stamp), '/', WEEK(i_time_stamp))='"+week_no +"' GROUP BY agent_name, " \
                "CONCAT(YEAR(i_time_stamp), '/', WEEK(i_time_stamp));"
        cursor = self.con.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        initial_count = 0
        if cursor.rowcount > 0:
            initial_count = row[0]

        if initial_count > 0:
            query = "select count(*) FROM initial_responses where agent_name='" + agent_name + "' AND " \
                    "i_time_compliance=1 AND CONCAT(YEAR(i_time_stamp), '/', WEEK(i_time_stamp))='" + week_no + "' GROUP BY " \
                    "agent_name, CONCAT(YEAR(i_time_stamp), '/', WEEK(i_time_stamp));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            initial_time = 0
            if cursor.rowcount > 0:
                initial_time = row[0]
            query = "select count(*) FROM initial_responses where agent_name='" + agent_name + "' AND " \
                    "resp_score=1 AND CONCAT(YEAR(i_time_stamp), '/', WEEK(i_time_stamp))='" + week_no + "' GROUP BY " \
                    "agent_name, CONCAT(YEAR(i_time_stamp), '/', WEEK(i_time_stamp));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            initial_quality = 0
            if cursor.rowcount > 0:
                initial_quality = row[0]

            No_of_compliant_initial_discarded = int(discarded_time + initial_time + initial_quality)
            Total_initial_discarded = (discarded_count * 2) + (initial_count * 2)
            initial_score = No_of_compliant_initial_discarded / Total_initial_discarded
        else :
            initial_time = 0
            initial_quality = 0
            initial_score = 0

        query = "select count(*) FROM subsequent_responses where agent_name='"+agent_name+"' AND " \
                "CONCAT(YEAR(si_timestamp), '/', WEEK(si_timestamp))='"+week_no +"' GROUP BY agent_name, " \
                "CONCAT(YEAR(si_timestamp), '/', WEEK(si_timestamp));"
        cursor = self.con.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        subsequent_count = 0
        if cursor.rowcount > 0:
            subsequent_count = row[0]

        if subsequent_count > 0 :
            query = "select count(*) FROM subsequent_responses where agent_name='" + agent_name + "' AND " \
                    "si_time_compliance=1 AND CONCAT(YEAR(si_timestamp), '/', WEEK(si_timestamp))='" + week_no + "' GROUP BY " \
                    "agent_name, CONCAT(YEAR(si_timestamp), '/', WEEK(si_timestamp));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            subsequent_time = 0
            if cursor.rowcount > 0:
                subsequent_time = row[0]
            query = "select count(*) FROM subsequent_responses where agent_name='" + agent_name + "' AND " \
                "resp_score=1 AND CONCAT(YEAR(si_timestamp), '/', WEEK(si_timestamp))='" + week_no + "' GROUP BY " \
                "agent_name, CONCAT(YEAR(si_timestamp), '/', WEEK(si_timestamp));"
            cursor = self.con.cursor()
            cursor.execute(query)
            row = cursor.fetchone()
            subsequent_quality = 0
            if cursor.rowcount > 0:
                subsequent_quality = row[0]

            subsequent_score = (subsequent_time + subsequent_quality) / (subsequent_count * 2)
        else:
            subsequent_score = 0
        agent_score = (rfo_score + initial_score + subsequent_score)
        return agent_score

    def get_subsequent_content_compliance_results_by_agent(self):
        subsequent_perfect = "SELECT agent_name, count(*) FROM subsequent_responses WHERE resp_rating = 'Perfect'  " \
                          "GROUP BY agent_name;"
        subsequent_needs_improvement = "SELECT agent_name, count(*) FROM subsequent_responses WHERE resp_rating = " \
                                    "'Needs Improvement' GROUP BY agent_name;"
        cursor = self.con.cursor()
        cursor.execute(subsequent_perfect)
        perfect_subsequent = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        cursor = self.con.cursor()
        cursor.execute(subsequent_needs_improvement)
        needs_improvement_subsequent = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        result = []
        for row in perfect_subsequent:
            result.append(dict(agent_name=row['agent_name'], values=dict(perfect_subsequent=row['count'],
                                                                         needs_improvement_subsequent=0)))
        for row in needs_improvement_subsequent:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(perfect_subsequent=0,
                                                                             needs_improvement_subsequent=row['count'])))
            else:
                match['values']['needs_improvement_subsequent'] = row['count']
        agent_records = []
        for row in result:
            values = row['values']
            total_records = values['perfect_subsequent'] + values['needs_improvement_subsequent']
            records = [ dict(name='Compliant', count=values['perfect_subsequent'], total_records=total_records,
                            percentage=round((values['perfect_subsequent'] * 100) / total_records)),
                       dict(name='Non-compliant', count=values['needs_improvement_subsequent'], total_records=total_records,
                            percentage=round((values['needs_improvement_subsequent'] * 100) / total_records))]

            agent_records.append({
              'agent_name': row['agent_name'],
              'values': records
            })
        AgentCache.subsequent_content_compliance_results_by_agent = agent_records
        return agent_records

    def get_subsequent_time_compliance_results_by_agent(self):
        compliant_query = "SELECT agent_name, count(*) FROM subsequent_responses WHERE si_time_compliance=1 " \
                          "GROUP BY agent_name;"
        non_compliant_query = "SELECT agent_name, count(*) FROM subsequent_responses WHERE si_time_compliance=0 " \
                              "GROUP BY agent_name;"
        cursor = self.con.cursor()
        cursor.execute(compliant_query)
        time_compliant = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        
        cursor = self.con.cursor()
        cursor.execute(non_compliant_query)
        time_non_compliant = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        result = []
        for row in time_compliant:
            result.append(dict(agent_name=row['agent_name'], values=dict(compliant=row['count'], non_compliant=0)))

        for row in time_non_compliant:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(compliant=0, non_compliant=row['count'])))
            else:
                match['values']['non_compliant'] = row['count']
        agent_records = []
        for row in result:
            values = row['values']
            total_records = values['compliant'] + values['non_compliant']
            records = [dict(name='Compliant', count=values['compliant'], total_records=total_records,
                            percentage=round((values['compliant'] * 100) / total_records)),
                       dict(name='Non-compliant', count=values['non_compliant'], total_records=total_records,
                            percentage=round((values['non_compliant'] * 100) / total_records))]

            agent_records.append({
              'agent_name': row['agent_name'],
              'values': records
            })
        AgentCache.subsequent_time_compliance_results_by_agent = agent_records
        return result

    def get_subsequent_missing_parameters_results_by_agent(self):
        query = "select agent_name, shortest_path, count(*) count from subsequent_responses WHERE shortest_path <> 'undetermined' and shortest_path <> 'nan' and shortest_path <> 'None' group by agent_name, shortest_path;"
        
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(agent_name=row[0], shortest_path=row[1], count=row[2]) for row in cursor.fetchall()]
        AgentCache.subsequent_missing_parameters_results_by_agent = result
        return result

    def get_subsequent_time_compliance_difference_results_with_sla_for_agent(self, agent_name):
        query = "select 10*floor(difference/10) range_start, 10*floor(difference/10) + 10 range_end, concat(10*floor(difference/10), '  -  ', 10*floor(difference/10) + 10) as `range`, " \
            +"count(*) as count from (SELECT subsequent_responses.ticket_number, si_timestamp,  ticket_booking_time, TIMESTAMPDIFF(MINUTE,ticket_booking_time, si_timestamp), " \
            +"subsequent_sla, (TIMESTAMPDIFF(MINUTE,ticket_booking_time, si_timestamp)) - owner_group.subsequent_sla difference from subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number " \
            +"JOIN owner_group ON tickets.owner_group = owner_group.name where subsequent_sla <> 'As per customer segment' and subsequent_responses.agent_name='"+agent_name+"') as t group by `range` order by range_start;"

        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(range_start=row[0], range_end=row[1], range=row[2], count=row[3]) for row in cursor.fetchall()]
        cursor.close()
        con.close()
        return result

    def get_rfo_content_compliance_results_by_agent(self):
        rfo_perfect = "SELECT agent_name, count(*) FROM tickets WHERE RFO_comp = 0 " \
                          "GROUP BY agent_name;"
        rfo_needs_improvement = "SELECT agent_name, count(*) FROM tickets WHERE RFO_comp = 1 " \
                          "GROUP BY agent_name;"
        cursor = self.con.cursor()
        cursor.execute(rfo_perfect)
        perfect_rfo = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        cursor = self.con.cursor()
        cursor.execute(rfo_needs_improvement)
        needs_improvement_rfo = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        result = []
        for row in perfect_rfo:
            result.append(dict(agent_name=row['agent_name'], values=dict(perfect_rfo=row['count'],
                                                                         needs_improvement_rfo=0)))
        for row in needs_improvement_rfo:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(perfect_rfo=0,
                                                                             needs_improvement_rfo=row['count'])))
            else:
                match['values']['needs_improvement_rfo'] = row['count']
        agent_records = []
        for row in result:
            values = row['values']
            total_records = values['perfect_rfo'] + values['needs_improvement_rfo']
            records = [ dict(name='Compliant', count=values['perfect_rfo'], total_records=total_records,
                            percentage=round((values['perfect_rfo'] * 100) / total_records)),
                       dict(name='Non-compliant', count=values['needs_improvement_rfo'], total_records=total_records,
                            percentage=round((values['needs_improvement_rfo'] * 100) / total_records))]

            agent_records.append({
              'agent_name': row['agent_name'],
              'values': records
            })
        AgentCache.rfo_content_compliance_results_by_agent = agent_records
        return agent_records

    def get_rfo_time_compliance_results_by_agent(self):
        compliant_query = "SELECT agent_name, count(*) FROM tickets WHERE RFO_timecomp = 1 " \
                          "GROUP BY agent_name;"
        non_compliant_query = "SELECT agent_name, count(*) FROM tickets WHERE RFO_timecomp = 0 " \
                              "GROUP BY agent_name;"
        cursor = self.con.cursor()
        cursor.execute(compliant_query)
        time_compliant = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        
        cursor = self.con.cursor()
        cursor.execute(non_compliant_query)
        time_non_compliant = [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        result = []
        for row in time_compliant:
            result.append(dict(agent_name=row['agent_name'], values=dict(compliant=row['count'], non_compliant=0)))

        for row in time_non_compliant:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(compliant=0, non_compliant=row['count'])))
            else:
                match['values']['non_compliant'] = row['count']
        agent_records = []
        for row in result:
            values = row['values']
            total_records = values['compliant'] + values['non_compliant']
            records = [dict(name='Compliant', count=values['compliant'], total_records=total_records,
                            percentage=round((values['compliant'] * 100) / total_records)),
                       dict(name='Non-compliant', count=values['non_compliant'], total_records=total_records,
                            percentage=round((values['non_compliant'] * 100) / total_records))]

            agent_records.append({
              'agent_name': row['agent_name'],
              'values': records
            })
        AgentCache.rfo_time_compliance_results_by_agent = agent_records
        return result

    def get_rfo_missing_parameters_results_by_agent(self):
        query = "SELECT agent_name, missing_parameters as shortest_path, count(*) FROM tickets WHERE missing_parameters <> 'undetermined' and " \
                "missing_parameters <> 'undertermined' and missing_parameters <> 'None' and missing_parameters <> '' and " \
                "missing_parameters <> 'nan' GROUP BY " \
                "agent_name, shortest_path;"
        
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(agent_name=row[0], shortest_path=row[1], count=row[2]) for row in cursor.fetchall()]
        AgentCache.rfo_missing_parameters_results_by_agent = result
        return result

    def get_rfo_time_compliance_difference_results_with_sla_for_agent(self, agent_name):
        query = "select 10*floor(RFO_diff/600) range_start, 10*floor(RFO_diff/600) + 10 range_end, concat(10*floor(RFO_diff/600), '  -  ', 10*floor(RFO_diff/600) + 10) as `range`, " \
            +"count(*) as count  FROM tickets WHERE agent_name='"+agent_name+"' group by `range` order by range_start;"
        
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(range_start=row[0], range_end=row[1], range=row[2], count=row[3]) for row in cursor.fetchall()]
        cursor.close()
        con.close()
        return result

    def get_rfo_mis_classification_specification_by_agent(self):
        query = "SELECT agent_name, Reason1, count(*) FROM tickets WHERE R1_comp <> 0 GROUP BY " \
                "agent_name, Reason1;"

        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(agent_name=row[0], Reason1=row[1], count=row[2])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        AgentCache.rfo_mis_classification_specification_by_agent = result
        return result

    def get_rfo_mis_classification_fault_segment_by_agent(self):
        query = "SELECT agent_name, Reason1, count(*) FROM tickets WHERE R2_comp <> 0 GROUP BY " \
                "agent_name, Reason1;"

        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(agent_name=row[0], Reason1=row[1], count=row[2])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        AgentCache.rfo_mis_classification_fault_segment_by_agent = result
        return result

    def get_subsequent_responses_combinations_by_agent(self):
        query = "SELECT agent_name, email_type, count(*) FROM subsequent_responses GROUP BY " \
                "agent_name, email_type;"

        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(agent_name=row[0], email_type=row[1], count=row[2])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        AgentCache.subsequent_responses_combinations_by_agent = result
        return result

    def get_discarded_responses_combinations_by_agent(self):
        query = "SELECT agent_name, email_type, count(*) FROM discarded_responses GROUP BY " \
                "agent_name, email_type;"

        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(agent_name=row[0], email_type=row[1], count=row[2])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        AgentCache.discarded_responses_combinations_by_agent = result
        return result
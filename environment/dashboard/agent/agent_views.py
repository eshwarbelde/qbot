from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.template import loader
from environment.dashboard.agent import agent_service

def get_rfos(request):
    customername = request.GET.get('customer_name', None)
    dic = agent_service.get_rfos(customername)
    return JsonResponse(dic, safe=False)

def get_sankeydata_customer(request):
    customername = request.GET.get('customer_name', None)
    rfo = request.GET.get('rfo', None)
    customerdata = agent_service.get_sankeydata_customer(customername,rfo)
    lst = []
    for tkt, df in customerdata.groupby('u_number'):
        lst.append(df.shape[0])
    length = max(lst)
    dict = customerdata[['predictions', 'target', 'time']].to_json( orient="split")#.to_dict()
    dic = {'length': length, 'data': dict}
    #json = customerdata[['predictions', 'target', 'time']].to_json( orient="split")
    return JsonResponse(dic, safe=False)

def get_sankeydata_casestate(request):
    customername = request.GET.get('customer_name', None)
    rfo = request.GET.get('rfo', None)
    casestate = agent_service.get_sankeydata_casestate(customername,rfo)
    lst = []
    for tkt, df in casestate.groupby('u_number'):
        lst.append(df.shape[0])
    length = max(lst)
    dict = casestate[['State Of Ticket(OLD)', 'State Of Ticket(New)', 'time']].to_json( orient="split")#.to_dict()
    dic = {'length': length, 'data': dict}
    #json = casestate[['State Of Ticket(OLD)', 'State Of Ticket(New)', 'time']].to_json( orient="split")
    return JsonResponse(dic, safe=False)



def get_agent_analysis_response(request):
    scenario_no = request.GET.get('scenario_no', None)
    response = request.GET.get('response', None)
    flag, result = agent_service.get_agent_analysis_response(scenario_no, response)
    dic = {'flag': flag, 'result': result}
    return JsonResponse(dic, safe=False)


def agent_module(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('agent_module.html')  # getting our template
        return HttpResponse(template.render())  # rendering the template in HttpResponse
    else:
        template = loader.get_template('login.html')  # getting our template
        response = redirect('/login/')
        return response


def get_agent_names(request):
    result = agent_service.get_agent_names()
    return JsonResponse(result, safe=False)


def get_all_responses_by_agent_name_and_rating(request):
    agent_name = request.GET['agent_name']
    resp_rating = request.GET['resp_rating']
    result = agent_service.get_all_responses_by_agent_name_and_rating(agent_name, resp_rating)
    return JsonResponse(result, safe=False)


def get_all_responses_by_agent_name_and_time_compliance(request):
    agent_name = request.GET['agent_name']
    time_compliance = request.GET['time_compliance']
    result = agent_service.get_all_responses_by_agent_name_and_time_compliance(agent_name, time_compliance)
    return JsonResponse(result, safe=False)


def get_all_responses_by_agent_name(request):
    agent_name = request.GET['agent_name']
    owner_group = request.GET['owner_group']
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    result = agent_service.get_all_responses_by_agent_name(agent_name, owner_group, start_date, end_date)
    return JsonResponse(result, safe=False)


def get_all_responses_by_agent_name_for_timeline(request):
    agent_name = request.GET['agent_name']
    owner_group = request.GET['owner_group']
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    result = agent_service.get_all_responses_by_agent_name_for_timeline(agent_name, owner_group, start_date, end_date)
    return JsonResponse(result, safe=False)


def get_all_agents_performance(request):
    result = agent_service.get_all_agents_performance()
    return JsonResponse(result, safe=False)


def get_content_compliance_results_by_agent(request):
    result = agent_service.get_content_compliance_results_by_agent()
    return JsonResponse(result, safe=False)


def get_time_compliance_results_by_agent(request):
    result = agent_service.get_time_compliance_results_by_agent()
    return JsonResponse(result, safe=False)


def get_missing_parameters_results_by_agent(request):
    result = agent_service.get_missing_parameters_results_by_agent()
    return JsonResponse(result, safe=False)


def get_time_compliance_difference_results_with_sla_for_agent(request):
    agent_name = request.GET.get('agent_name', None)
    result = agent_service.get_time_compliance_difference_results_with_sla_for_agent(agent_name)
    return JsonResponse(result, safe=False)


def get_all_distinct_agents_performance(request):
    agent_name = request.GET.get('agent_name', None)
    week_no = request.GET.get('week_no', None)
    result = agent_service.get_all_distinct_agents_performance(agent_name, week_no)
    return JsonResponse(result, safe=False)

def get_subsequent_content_compliance_results_by_agent(request):
    result = agent_service.get_subsequent_content_compliance_results_by_agent()
    return JsonResponse(result, safe=False)

def get_subsequent_time_compliance_results_by_agent(request):
    result = agent_service.get_subsequent_time_compliance_results_by_agent()
    return JsonResponse(result, safe=False)

def get_subsequent_missing_parameters_results_by_agent(request):
    result = agent_service.get_subsequent_missing_parameters_results_by_agent()
    return JsonResponse(result, safe=False)

def get_subsequent_time_compliance_difference_results_with_sla_for_agent(request):
    agent_name = request.GET.get('agent_name', None)
    result = agent_service.get_subsequent_time_compliance_difference_results_with_sla_for_agent(agent_name)
    return JsonResponse(result, safe=False)

def get_rfo_content_compliance_results_by_agent(request):
    result = agent_service.get_rfo_content_compliance_results_by_agent()
    return JsonResponse(result, safe=False)

def get_rfo_time_compliance_results_by_agent(request):
    result = agent_service.get_rfo_time_compliance_results_by_agent()
    return JsonResponse(result, safe=False)

def get_rfo_missing_parameters_results_by_agent(request):
    result = agent_service.get_rfo_missing_parameters_results_by_agent()
    return JsonResponse(result, safe=False)

def get_rfo_time_compliance_difference_results_with_sla_for_agent(request):
    agent_name = request.GET.get('agent_name', None)
    result = agent_service.get_rfo_time_compliance_difference_results_with_sla_for_agent(agent_name)
    return JsonResponse(result, safe=False)

def get_rfo_mis_classification_specification_by_agent(request):
    result = agent_service.get_rfo_mis_classification_specification_by_agent()
    return JsonResponse(result, safe=False)

def get_rfo_mis_classification_fault_segment_by_agent(request):
    result = agent_service.get_rfo_mis_classification_fault_segment_by_agent()
    return JsonResponse(result, safe=False)

def get_subsequent_responses_combinations_by_agent(request):
    result = agent_service.get_subsequent_responses_combinations_by_agent()
    return JsonResponse(result, safe=False)

def get_discarded_responses_combinations_by_agent(request):
    result = agent_service.get_discarded_responses_combinations_by_agent()
    return JsonResponse(result, safe=False)
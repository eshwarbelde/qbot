class AgentCache:
    all_agent_names = None
    all_agents_performance = None
    content_compliance_results_by_agent = None
    time_compliance_results_by_agent = None
    missing_parameters_results_by_agent = None

    subsequent_content_compliance_results_by_agent = None
    subsequent_time_compliance_results_by_agent = None
    subsequent_missing_parameters_results_by_agent = None

    rfo_content_compliance_results_by_agent = None
    rfo_time_compliance_results_by_agent = None
    rfo_missing_parameters_results_by_agent = None
    rfo_mis_classification_specification_by_agent = None
    rfo_mis_classification_fault_segment_by_agent = None

    subsequent_responses_combinations_by_agent = None
    discarded_responses_combinations_by_agent = None

    distinct_agent_scores = None

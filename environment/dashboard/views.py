from django.shortcuts import redirect
from django.template import loader
from django.http import JsonResponse, HttpResponse, Http404
from bson.json_util import dumps

from environment.cache.subsequent_module_cache import SubSequentModuleCache
from environment.dashboard.dbo.connection import MySQLConnection
from environment.dashboard.dbo.initial_data_fetch import InitialDataFetch
from environment.dashboard.dbo.subsequent_data_fetch import SubsequentDataFetch
from environment.dashboard.dbo.discard_data_fetch import DiscardedDataFetch
from environment.dashboard.dbo.metrics_screen_data_fetch import Metrics
from environment.cache.quality_dashboard_cache import QualityDashboardCache
import json
import os

db_connection = MySQLConnection()


def get_landing_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('landing.html')  # getting our template
        return HttpResponse(template.render())  # rendering the template in HttpResponse
    else:
        return redirect('/login/')


def get_quality_dashboard_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('quality_dashboard.html')  # getting our template
        return HttpResponse(template.render())  # rendering the template in HttpResponse
    else:
        return redirect('/login/')


def get_action_module_initial_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('action_module_initial.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')


def get_action_module_subsequent_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('action_module_subsequent.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')


def get_data_management_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('data_management.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')


def get_calendar_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '' and request.session['user_role'] == 'ADMIN':
        template = loader.get_template('calendar.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')


def get_process_history_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '' and request.session['user_role'] == 'ADMIN':
        template = loader.get_template('process_history.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')

def get_customer_journey_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '' and request.session['user_role'] == 'ADMIN':
        template = loader.get_template('customer_journey.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')

def get_agent_training_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '' and request.session['user_role'] == 'ADMIN':
        template = loader.get_template('agent_training.html')
        return HttpResponse(template.render())
    else:
        return redirect('/login/')


def get_process_status(request):
    row_id = request.GET.get('row_id', None)
    return db_connection.get_status(row_id)

def get_latest_process_status(request):
    return db_connection.get_latest_process_status()


def load_owner_groups(request):
    owner_groups = json.load(open('environment/classification/config/owner_group.json'))
    return JsonResponse(dumps(owner_groups), safe=False)


def get_desks_list(request):
    return db_connection.get_desks_list()


def get_owner_groups_list(request):
    return db_connection.get_owner_groups_list()


def addMasterData(request):
    table_name = request.POST.get('collection_name', None)
    name = request.POST.get('record', None)
    return JsonResponse(db_connection.add_master_data(table_name, name), safe=False)


def downloadExcel(request):
    filename_to_download = request.POST.get('filename_to_download', None)
    print(filename_to_download)
    file_path_to_download = 'environment/classification/output/' + filename_to_download
    if os.path.exists(file_path_to_download):
        with open(file_path_to_download, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path_to_download)
            return response
    raise Http404


def get_tickets_group_by_bu(request):
    result = QualityDashboardCache.tickets_group_by_bu
    if result != None:
        return JsonResponse(result, safe=False)
    else:
        return db_connection.get_tickets_group_by_bu()

def get_tickets_group_by_owner_group(request):
    product = request.GET['product']
    severity = request.GET['severity']
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    return db_connection.get_tickets_group_by_owner_group('tickets', product, severity, start_date, end_date)

def get_tickets_by_owner_group(request):
    owner_group = request.GET['owner_group']
    product = request.GET['product']
    severity = request.GET['severity']
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    
    result = QualityDashboardCache.tickets_by_owner_group
    if result != None:
        return JsonResponse(result, safe=False)
    else:
        return db_connection.get_all_tickets_by_owner_group('tickets', owner_group, product, severity, start_date, end_date)

def get_owner_group_tickets_by_category_and_rating(request):
    owner_group = request.GET.get('owner_group', None)
    category = request.GET.get('category', None)
    rating = request.GET.get('rating', None)
    return db_connection.get_owner_group_tickets_by_category_and_rating(owner_group, category, rating)

def get_owner_group_responses_by_category_and_rating(request):
    owner_group = request.GET.get('owner_group', None)
    category = request.GET.get('category', None)
    rating = request.GET.get('rating', None)
    return db_connection.get_owner_group_responses_by_category_and_rating(owner_group, category, rating)

def get_owner_group_responses_by_rating(request):
    owner_group = request.GET.get('owner_group', None)
    rating = request.GET.get('rating', None)
    return db_connection.get_owner_group_responses_by_rating(owner_group, rating)
    
def get_all_tickets(request):
    owner_group = request.GET['owner_group']
    product = request.GET['product']
    severity = request.GET['severity']
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    result = QualityDashboardCache.all_tickets_for_timeline
    if result != None:
        return JsonResponse(result, safe=False)
    else:
        return db_connection.get_all_tickets('tickets', owner_group, product, severity, start_date, end_date)


def get_agent_owner_groups(request):
    agent_name = request.GET['agent_name']
    print("Getting owner groups of agent: ", agent_name)
    return db_connection.get_owner_groups('owner_group', agent_name)


def get_all_intial_responses_group_by_rating(request):
    connection = InitialDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET.get('start_date', None)
    enddate = request.GET.get('end_date', None)
    return connection.getAllIntialResponsesGroupByRating('initial_emails', owner_group, product, severity,
                                                         startdate, enddate)


def getAllInitialEmailsWithOnlyLogs(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllInitialEmailsWithOnlyLogs('initial_emails', mode, owner_group, product, severity, startdate,
                                                      enddate)

def get_content_compliance_results(request):
    connection = InitialDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET.get('start_date', None)
    enddate = request.GET.get('end_date', None)
    return connection.get_content_compliance_results(owner_group, product, severity, startdate, enddate)

def get_time_compliance_results(request):
    connection = InitialDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)
    return connection.get_time_compliance_results(owner_group, product, severity, start_date, end_date)

def getTimeComplianceDifferenceResultsWithSLA(request):
    connection = InitialDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)
    return connection.getTimeComplianceDifferenceResultsWithSLA(owner_group, product, severity, start_date, end_date)

def get_initial_responses_by_rating(request):
    connection = InitialDataFetch()
    resp_rating = request.GET['resp_rating']
    return connection.get_initial_responses_by_rating(resp_rating)

def get_initial_responses_by_time_compliance(request):
    connection = InitialDataFetch()
    time_compliance = request.GET['time_compliance']
    return connection.get_initial_responses_by_time_compliance(time_compliance)

def get_missing_parameters_results(request):
    connection = InitialDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    start_date = request.GET.get('start_date', None)
    end_date = request.GET.get('end_date', None)
    return connection.get_missing_parameters_results(owner_group, product, severity, start_date, end_date)

def getAllInitialEmailsThatMissedLinkStatus(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET.get('start_date', None)
    enddate = request.GET.get('end_date', None)
    return connection.getAllInitialEmailsThatMissedLinkStatus('initial_emails', mode, owner_group, product, severity,
                                                              startdate, enddate)


def getAllInitialEmailsThatMissedETR(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET.get('start_date', None)
    enddate = request.GET.get('end_date', None)
    return connection.getAllInitialEmailsThatMissedETR('initial_emails', mode, owner_group, product, severity,
                                                       startdate, enddate)


def getAllInitialEmailsThatMissedResponseTime(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET.get('start_date', None)
    enddate = request.GET.get('end_date', None)
    return connection.getAllInitialEmailsThatMissedResponseTime('initial_emails', mode, owner_group, product, severity,
                                                                startdate, enddate)


def getAllInitialEmailsThatMissedPOA(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllInitialEmailsThatMissedPOA('initial_emails', mode, owner_group, product, severity,
                                                       startdate, enddate)


def getAllServiceRestoreEmailsAndNoLogs(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllServiceRestoreEmailsAndNoLogs('initial_emails', mode, owner_group, product, severity,
                                                          startdate, enddate)


def getAllEmailsWithNoServiceRestoreAndNoPOA(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllEmailsWithNoServiceRestoreAndNoPOA('initial_emails', mode, owner_group, product, severity,
                                                               startdate, enddate)


def getAllInitialEmailsThatMissedSLATime(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllInitialEmailsThatMissedSLATime('initial_emails', mode, owner_group, product, severity,
                                                           startdate, enddate)


def getAllInitialEmailsWithSLAVoilationGroupByQualityComplaint(request):
    connection = InitialDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllInitialEmailsWithSLAVoilationGroupByQualityComplaint('initial_emails', mode, owner_group,
                                                                                 product, severity, startdate, enddate)

def get_email_content(request):
    ticket_number = request.GET.get('ticket_number', None)
    email_number = request.GET.get('email_number', None)
    email_type = request.GET.get('email_type', None)
    return db_connection.get_email_content('email_content', ticket_number, email_number, email_type)


def getDiscardedEmails(request):
    discarded_data_fetch = DiscardedDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return discarded_data_fetch.getDiscardedEmails(mode, owner_group, product, severity, startdate, enddate)


def discardedEmailsGroupByDiscardNumber(request):
    discarded_data_fetch = DiscardedDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return discarded_data_fetch.discardedEmailsGroupByDiscardNumber(owner_group, product, severity, startdate, enddate)


# Subsequent data fetching views
def getAllSubsequentResponsesGroupByRating(request):
    connection = SubsequentDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllSubsequentResponsesGroupByRating('subsequent_emails', owner_group, product, severity,
                                                             startdate, enddate)


def getAllSubsequentEmailsWithNoProgressGroupByEmailType(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllSubsequentEmailsWithNoProgressGroupByEmailType('subsequent_emails', mode, owner_group,
                                                                           product, severity, startdate, enddate)


def getAllSubsequentEmailsWithNoProgress(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllSubsequentEmailsWithNoProgress('subsequent_emails', mode, owner_group, product, severity,
                                                           startdate, enddate)


def getAllSubsequentEmailsWithNoServiceRestoreAndNoETR(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllSubsequentEmailsWithNoServiceRestoreAndNoETR('subsequent_emails', mode, owner_group,
                                                                         product, severity, startdate, enddate)


def getAllTicketsWithNoProblemFound(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    return connection.getAllTicketsWithNoProblemFound('subsequent_emails', mode, owner_group, product, severity,
                                                      start_date, end_date)


def getAllSubsequentEmailsWithServiceRestoreAndNoLogs(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    return connection.getAllSubsequentEmailsWithServiceRestoreAndNoLogs('subsequent_emails', mode, owner_group, product,
                                                                        severity, start_date, end_date)


def getAllSubsequentEmailsWithOnlyLogs(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllSubsequentEmailsWithOnlyLogs('subsequent_emails', mode, owner_group, product, severity,
                                                         startdate, enddate)


def getAllSubsequentEmailsWithSLAVoilationGroupByEmailType(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    startdate = request.GET['start_date']
    enddate = request.GET['end_date']
    return connection.getAllSubsequentEmailsWithSLAVoilationGroupByEmailType('subsequent_emails', mode, owner_group,
                                                                             product, severity, startdate, enddate)


def get_all_subsequent_emails_with_sla_violation_group_by_quality_complaint(request):
    connection = SubsequentDataFetch()
    mode = request.GET.get('mode', None)
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    return connection.get_all_subsequent_emails_with_sla_violation_group_by_quality_complaint('subsequent_emails', mode,
                                                                                              owner_group, product,
                                                                                              severity,
                                                                                              start_date, end_date)


# Process History data fetch views
def get_process_history_list(request):
    return db_connection.get_process_history_list('process_status')

def get_ticket_metrics(request):
    metrics = Metrics()
    return metrics.get_ticket_metrics()

def get_product_and_customer_metrics(request):
    metrics = Metrics()
    return metrics.get_product_and_customer_metrics()

def overall_ticket_rating_metrics(request):
    metrics = Metrics()
    return metrics.overall_ticket_rating_metrics()

def agent_metrics(request):
    metrics = Metrics()
    return metrics.agent_metrics()

def get_subsequent_content_compliance_results(request):
    result = SubSequentModuleCache.content_compliance_results
    if result is not None:
        return JsonResponse(result, safe=False)
    else:
        owner_group = request.GET.get('owner_group', None)
        product = request.GET.get('product', None)
        severity = request.GET.get('severity', None)
        start_date = request.GET['start_date']
        end_date = request.GET['end_date']
        connection = SubsequentDataFetch()
        return connection.get_content_compliance_results(owner_group, product, severity, start_date, end_date)


def get_subsequent_time_compliance_results(request):
    result = SubSequentModuleCache.time_compliance_results
    if result is not None:
        return JsonResponse(result, safe=False)
    else:
        owner_group = request.GET.get('owner_group', None)
        product = request.GET.get('product', None)
        severity = request.GET.get('severity', None)
        start_date = request.GET['start_date']
        end_date = request.GET['end_date']
        connection = SubsequentDataFetch()
        return connection.get_time_compliance_results(owner_group, product, severity, start_date, end_date)


def get_subsequent_missing_parameters_results(request):
    result = SubSequentModuleCache.missing_parameters_results
    if result != None:
        return JsonResponse(result, safe=False)
    else:
        owner_group = request.GET.get('owner_group', None)
        product = request.GET.get('product', None)
        severity = request.GET.get('severity', None)
        start_date = request.GET['start_date']
        end_date = request.GET['end_date']
        connection = SubsequentDataFetch()
        return connection.get_missing_parameters_results(owner_group, product, severity, start_date, end_date)


def get_subsequent_time_compliance_difference_results_with_SLA(request):
    connection = SubsequentDataFetch()
    owner_group = request.GET.get('owner_group', None)
    product = request.GET.get('product', None)
    severity = request.GET.get('severity', None)
    start_date = request.GET['start_date']
    end_date = request.GET['end_date']
    return connection.get_time_compliance_difference_results_with_SLA(owner_group, product, severity, start_date, end_date)


def get_subsequent_responses_by_rating(request):
    connection = SubsequentDataFetch()
    resp_rating = request.GET['resp_rating']
    return connection.get_subsequent_responses_by_rating(resp_rating)


def get_subsequent_responses_by_time_compliance(request):
    connection = SubsequentDataFetch()
    time_compliance = request.GET['time_compliance']
    return connection.get_subsequent_responses_by_time_compliance(time_compliance)

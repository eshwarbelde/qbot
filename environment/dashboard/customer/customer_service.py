from environment.cache.qbot_cache import CacheRefresher
from environment.dashboard.customer.customer_dao import CustomerModuleDataFetch
from django.core.cache import cache
connection = CustomerModuleDataFetch()
customer_cache = CacheRefresher.customer_cache


def get_all_tickets_group_by_customer_name_and_rating():
    result = cache.get('all_tickets_group_by_customer_name_and_rating')
    print('in customer service--"all_tickets_group_by_customer_name_and_rating"')
    print(result)
    # if result is None:
    #     result = connection.get_all_tickets_group_by_customer_name_and_rating()
    return result


def get_all_tickets_group_by_customer_name_and_rating_for_timeline():
    result = cache.get("all_tickets_group_by_customer_name_and_rating_for_timeline")
    # if result is None:
    #     result = connection.get_all_tickets_group_by_customer_name_and_rating_for_timeline()
    return result


def get_all_tickets_group_by_customer_name():
    result = cache.get("all_tickets_group_by_customer_name")
    # if result is None:
    #     result = connection.get_all_tickets_group_by_customer_name()
    return result


def get_all_tickets_by_customer():
    result = cache.get("all_tickets_by_customer")
    # if result is None:
    #     result = connection.get_all_tickets_by_customer()
    return result


def get_customer_tickets_by_category_and_rating(customer_name, category, rating):
    return connection.get_customer_tickets_by_category_and_rating(customer_name, category, rating)


def get_customer_responses_by_category_and_rating(customer_name, category, rating):
    return connection.get_customer_responses_by_category_and_rating(customer_name, category, rating)

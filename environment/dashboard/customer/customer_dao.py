import MySQLdb
from django.core.cache import cache
from environment.dashboard.customer.customer_cache import CustomerCache


class CustomerModuleDataFetch:
    def __init__(self):
        self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")

    def get_all_tickets_group_by_customer_name_and_rating(self):
        print("Getting all tickets group by customer_name and rating because cache is not present")
        query = "SELECT customer_name, product, severity,ticket_overall_score, count(*) FROM tickets GROUP BY " \
                "customer_name, product, severity,ticket_overall_score;"
        print(query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(customer_name=row[0], product=row[1], severity=row[2], ticket_overall_score=row[3],
                       count=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        CustomerCache.all_tickets_group_by_customer_name_and_rating = result
        return result

    def get_all_tickets_group_by_customer_name_and_rating_for_timeline(self):
        print("Getting all tickets group by customer name, rating because cache is not present:")
        query = "SELECT customer_name, product, severity, ticket_overall_score, " \
                "date_format(ticket_booking_time, '%d-%m-%Y') as ticket_booking_time, count(*) FROM tickets " \
                "GROUP BY customer_name, product, severity,ticket_overall_score, " \
                "date_format(ticket_booking_time, '%d-%m-%Y') ORDER BY ticket_booking_time;"
        print(query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(customer_name=row[0], product=row[1], severity=row[2], ticket_overall_score=row[3],
                       ticket_booking_time=row[4], count=row[5]) for row in cursor.fetchall()]
        cursor.close()
        con.close()
        CustomerCache.all_tickets_group_by_customer_name_and_rating_for_timeline = result
        return result

    def get_all_tickets_group_by_customer_name(self):
        print("Getting all tickets group by customer name:")
        total_tickets_query = "SELECT customer_name, product, severity, count(*) FROM tickets GROUP BY customer_name," \
                              " product, severity;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        print(total_tickets_query)
        cursor.execute(total_tickets_query)
        total_tickets_results = [dict(customer_name=row[0], product=row[1], severity=row[2], count=row[3])
                                 for row in cursor.fetchall()]
        cursor.close()
        con.close()

        pwc_count_query = "SELECT customer_name, product, severity, count(*) FROM tickets  WHERE pwc_count <> 0 " \
                          "GROUP BY customer_name, product, severity;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        print(pwc_count_query)
        cursor.execute(pwc_count_query)
        pwc_count_results = [dict(customer_name=row[0], product=row[1], severity=row[2], count=row[3])
                             for row in cursor.fetchall()]
        cursor.close()
        con.close()

        pwc_time_query = "SELECT customer_name, product, severity, SUM(pwc_time/3600) FROM tickets" \
                         " group by customer_name, product, severity;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        print(pwc_time_query)
        cursor.execute(pwc_time_query)
        pwc_time_results = [dict(customer_name=row[0], product=row[1], severity=row[2], pwc_time=row[3])
                            for row in cursor.fetchall()]
        cursor.close()
        con.close()

        ticket_resolution_time_query = "SELECT customer_name, product, severity, SUM((TIMESTAMPDIFF(MINUTE," \
                                       "ticket_booking_time, ticket_close_time ))) resolution_time from tickets " \
                                       "group by customer_name, product, severity;"
        
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        print(ticket_resolution_time_query)
        cursor.execute(ticket_resolution_time_query)
        ticket_resolution_time_results = [dict(customer_name=row[0], product=row[1], severity=row[2],
                                               resolution_time=row[3])
                                          for row in cursor.fetchall()]
        cursor.close()
        con.close()
        no_problem_found_query = "SELECT customer_name, product, severity, count(*) FROM tickets WHERE " \
                                 "no_problem_found=1 group by customer_name, product, severity"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        print(no_problem_found_query)
        cursor.execute(no_problem_found_query)
        no_problem_found_results = [dict(customer_name=row[0], product=row[1], severity=row[2], count=row[3])
                                    for row in cursor.fetchall()]
        cursor.close()
        con.close()
        final_result = {
            'total_tickets':  total_tickets_results,
            'pwc_count': pwc_count_results,
            'pwc_time': pwc_time_results,
            'ticket_resolution_time': ticket_resolution_time_results,
            'no_problem_found': no_problem_found_results
        }
        CustomerCache.all_tickets_group_by_customer_name = final_result
        return final_result

    def get_all_tickets_by_customer(self):
        initial_query = "SELECT customer_name, product, severity, resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number GROUP BY customer_name, product, severity, resp_rating;"
        cursor = self.con.cursor()
        cursor.execute(initial_query)
        initial_result = [dict(customer_name=row[0], product=row[1], severity=row[2], resp_rating=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
        subsequent_query = "SELECT customer_name, product, severity, resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number GROUP BY customer_name, product, severity, resp_rating;"
        cursor = self.con.cursor()
        cursor.execute(subsequent_query)
        subsequent_result = [dict(customer_name=row[0], product=row[1], severity=row[2], resp_rating=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
        discarded_query = "SELECT customer_name, product, severity, resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number GROUP BY customer_name, product, severity, resp_rating;"
        cursor = self.con.cursor()
        cursor.execute(discarded_query)
        discarded_result = [dict(customer_name=row[0], product=row[1], severity=row[2], resp_rating=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
                    
        initial_time_query = "SELECT customer_name, product, severity, i_time_compliance, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number GROUP BY customer_name, product, severity, i_time_compliance;"
        cursor = self.con.cursor()
        cursor.execute(initial_time_query)
        initial_time_result = [dict(customer_name=row[0], product=row[1], severity=row[2], i_time_compliance=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
                
        subsequent_time_query = "SELECT customer_name, product, severity, si_time_compliance, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number GROUP BY customer_name, product, severity, si_time_compliance;"
        cursor = self.con.cursor()
        cursor.execute(subsequent_time_query)
        subsequent_time_result = [dict(customer_name=row[0], product=row[1], severity=row[2], si_time_compliance=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()

        discarded_time_query = "SELECT customer_name, product, severity, d_time_compliance, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number GROUP BY customer_name, product, severity, d_time_compliance;"
        cursor = self.con.cursor()
        cursor.execute(discarded_time_query)
        discarded_time_result = [dict(customer_name=row[0], product=row[1], severity=row[2], d_time_compliance=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
        quality_results = {
            "initial_emails": initial_result,
            "subsequent_emails": subsequent_result,
            "discarded_emails": discarded_result
        }
        time_results = {
            "initial_emails": initial_time_result,
            "subsequent_emails": subsequent_time_result,
            "discarded_emails": discarded_time_result
        }
        final_results = {
            "quality": quality_results,
            "time": time_results
        }
        CustomerCache.all_tickets_by_customer = final_results
        return final_results
    
    def get_customer_tickets_by_category_and_rating(self, customer_name, category, rating):
        print("Getting customer tickets by category and rating")
        if category == "Initial Responses":
            if customer_name != "All":
                query  = "SELECT i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr," \
                         "i_time_compliance, count(*) from initial_responses JOIN tickets ON " \
                         "tickets.ticket_number=initial_responses.ticket_number WHERE " \
                         "customer_name='"+customer_name+"' and resp_rating='" + rating + \
                         "' GROUP BY i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr," \
                         "i_time_compliance;"
            else:
                query  = "SELECT i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr," \
                         "i_time_compliance, count(*) from initial_responses JOIN tickets ON " \
                         "tickets.ticket_number=initial_responses.ticket_number WHERE resp_rating='" + rating + \
                         "' GROUP BY i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr," \
                         "i_time_compliance;"
            cursor = self.con.cursor()
            cursor.execute(query)
            result = [dict(i_resp_time=row[0], i_link_stat_chk=row[1], i_logs=row[2], i_poa=row[3], i_cust_dep=row[4],
                           i_etr=row[5], i_svc_rstr=row[6],i_time_compliance=row[7], count=row[8])
                      for row in cursor.fetchall()]
            cursor.close()
            i_resp_time_0, i_link_stat_chk_0, i_logs_0, i_poa_0, i_cust_dep_0, i_etr_0, i_svc_rstr_0, i_time_compliance_0 = 0,0,0,0,0,0,0,0
            i_resp_time_1, i_link_stat_chk_1, i_logs_1, i_poa_1, i_cust_dep_1, i_etr_1, i_svc_rstr_1,i_time_compliance_1 = 0,0,0,0,0,0,0,0                          
            for data in result:
                if data['i_resp_time'] == 0:
                    i_resp_time_0 += data['count']
                else:
                    i_resp_time_1+=data['count']
                if data['i_link_stat_chk'] == 0:
                    i_link_stat_chk_0 += data['count']  
                else:
                    i_link_stat_chk_1 += data['count']
                if data['i_logs'] == 0:
                    i_logs_0 += data['count']
                else:
                    i_logs_1 += data['count']
                if data['i_poa'] == 0:
                    i_poa_0 += data['count']  
                else:
                    i_poa_1 += data['count']
                if data['i_cust_dep'] == 0:
                    i_cust_dep_0 += data['count']  
                else:
                    i_cust_dep_1 += data['count']
                if data['i_etr'] == 0:
                    i_etr_0 += data['count']  
                else: 
                    i_etr_1 += data['count']
                if data['i_svc_rstr'] == 0:
                    i_svc_rstr_0 += data['count']  
                else: 
                    i_svc_rstr_1 += data['count'] 
                if data['i_time_compliance'] == 0:
                    i_time_compliance_0 += data['count']  
                else: 
                    i_time_compliance_1 += data['count']
            final_result=[
                {
                    'name': 0,
                    'data': [
                                dict(name='i_resp_time', y=i_resp_time_0),dict(name='i_link_stat_chk', y=i_link_stat_chk_0),
                                dict(name='i_logs', y=i_logs_0),dict(name='i_poa', y=i_poa_0),
                                dict(name='i_cust_dep', y=i_cust_dep_0),dict(name='i_etr', y=i_etr_0),
                                dict(name='i_svc_rstr', y=i_svc_rstr_0),dict(name='i_time_compliance', y=i_time_compliance_0)                                
                            ]
                },
                {
                    'name': 1,
                    'data': [
                                dict(name='i_resp_time', y=i_resp_time_1),dict(name='i_link_stat_chk', y=i_link_stat_chk_1),
                                dict(name='i_logs', y=i_logs_1),dict(name='i_poa', y=i_poa_1),
                                dict(name='i_cust_dep', y=i_cust_dep_1),dict(name='i_etr', y=i_etr_1),
                                dict(name='i_svc_rstr', y=i_svc_rstr_1),dict(name='i_time_compliance', y=i_time_compliance_1)                                
                            ]
                }
            ]       
        elif category == "Subsequent Responses":
            if customer_name != "All":
                query  = "SELECT si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance, count(*) from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE customer_name='"+customer_name+"' and resp_rating='"+rating+"' GROUP BY si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance;"
            else:
                query  = "SELECT si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance, count(*) from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE resp_rating='"+rating+"' GROUP BY si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance;"
            cursor = self.con.cursor()
            cursor.execute(query)
            result = [dict(si_resp_time=row[0], si_no_prob=row[1], si_logs=row[2], si_prog=row[3], si_cd=row[4], si_etr=row[5], si_svc_rstr=row[6],si_time_compliance=row[7], count=row[8]) for row in cursor.fetchall()]
            cursor.close()
            si_resp_time_0, si_no_prob_0, si_logs_0, si_prog_0, si_cd_0, si_etr_0, si_svc_rstr_0, si_time_compliance_0 = 0,0,0,0,0,0,0,0
            si_resp_time_1, si_no_prob_1, si_logs_1, si_prog_1, si_cd_1, si_etr_1, si_svc_rstr_1, si_time_compliance_1 = 0,0,0,0,0,0,0,0
            for data in result:
                if data['si_resp_time'] == 0: 
                    si_resp_time_0+=data['count']  
                else: 
                    si_resp_time_1+=data['count']
                if data['si_no_prob'] == 0: 
                    si_no_prob_0 += data['count'] 
                else: 
                    si_no_prob_1 += data['count']
                if data['si_logs'] == 0: 
                    si_logs_0 += data['count']  
                else: 
                    si_logs_1 += data['count']
                if data['si_prog'] == 0: 
                    si_prog_0 += data['count'] 
                else: 
                    si_prog_1 += data['count']
                if data['si_cd'] == 0: 
                    si_cd_0 += data['count'] 
                else: 
                    si_cd_1 += data['count']
                if data['si_etr'] == 0: 
                    si_etr_0 += data['count'] 
                else: 
                    si_etr_1 += data['count']
                if data['si_svc_rstr'] == 0: 
                    si_svc_rstr_0 += data['count'] 
                else: 
                    si_svc_rstr_1 += data['count'] 
                if data['si_time_compliance'] == 0:
                    si_time_compliance_0 += data['count'] 
                else: 
                    si_time_compliance_1 += data['count']
            final_result=[
                {
                    'name': 0,
                    'data': [
                                dict(name='si_resp_time', y=si_resp_time_0),dict(name='si_no_prob', y=si_no_prob_0),
                                dict(name='si_logs', y=si_logs_0),dict(name='si_prog', y=si_prog_0),
                                dict(name='si_cd', y=si_cd_0),dict(name='si_etr', y=si_etr_0),
                                dict(name='si_svc_rstr', y=si_svc_rstr_0),dict(name='si_time_compliance', y=si_time_compliance_0)                                
                            ]
                },
                {
                    'name': 1,
                    'data': [
                                dict(name='si_resp_time', y=si_resp_time_1),dict(name='si_no_prob', y=si_no_prob_1),
                                dict(name='si_logs', y=si_logs_1),dict(name='si_prog', y=si_prog_1),
                                dict(name='si_cd', y=si_cd_1),dict(name='si_etr', y=si_etr_1),
                                dict(name='si_svc_rstr', y=si_svc_rstr_1),dict(name='si_time_compliance', y=si_time_compliance_1)
                            ]
                }
            ]
        elif category == "Discarded Responses":
            if customer_name != "All":
                query = "SELECT d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance, count(*) from discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number WHERE customer_name='"+customer_name+"' and resp_rating='"+rating+"' GROUP BY d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance;"
            else:
                query = "SELECT d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance, count(*) from discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number WHERE resp_rating='"+rating+"' GROUP BY d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance;"
            cursor = self.con.cursor()
            cursor.execute(query)
            result = [dict(d_resp_time=row[0], d_link_stat_chk=row[1], d_logs=row[2], d_poa=row[3], d_cust_dep=row[4], d_etr=row[5], d_svc_rstr=row[6],d_time_compliance=row[7], count=row[8]) for row in cursor.fetchall()]
            cursor.close()
            d_resp_time_0, d_link_stat_chk_0, d_logs_0, d_poa_0, d_cust_dep_0, d_etr_0, d_svc_rstr_0, d_time_compliance_0 = 0,0,0,0,0,0,0,0
            d_resp_time_1, d_link_stat_chk_1, d_logs_1, d_poa_1, d_cust_dep_1, d_etr_1, d_svc_rstr_1, d_time_compliance_1 = 0,0,0,0,0,0,0,0
            for data in result:
                if data['d_resp_time'] == 0: 
                    d_resp_time_0+=data['count']  
                else: 
                    d_resp_time_1+=data['count']
                if data['d_link_stat_chk'] == 0: 
                    d_link_stat_chk_0 += data['count'] 
                else: 
                    d_link_stat_chk_1 += data['count']
                if data['d_logs'] == 0: 
                    d_logs_0 += data['count']  
                else: 
                    d_logs_1 += data['count']
                if data['d_poa'] == 0: 
                    d_poa_0 += data['count'] 
                else: 
                    d_poa_1 += data['count']
                if data['d_cust_dep'] == 0: 
                    d_cust_dep_0 += data['count'] 
                else: 
                    d_cust_dep_1 += data['count']
                if data['d_etr'] == 0: 
                    d_etr_0 += data['count'] 
                else: 
                    d_etr_1 += data['count']
                if data['d_svc_rstr'] == 0: 
                    d_svc_rstr_0 += data['count'] 
                else: 
                    d_svc_rstr_1 += data['count'] 
                if data['d_time_compliance'] == 0:
                    d_time_compliance_0 += data['count'] 
                else: 
                    d_time_compliance_1 += data['count']
            final_result = [
                {
                    'name': 0,
                    'data': [
                                dict(name='d_resp_time', y=d_resp_time_0),dict(name='d_link_stat_chk', y=d_link_stat_chk_0),
                                dict(name='d_logs', y=d_logs_0),dict(name='d_poa', y=d_poa_0),
                                dict(name='d_cust_dep', y=d_cust_dep_0),dict(name='d_etr', y=d_etr_0),
                                dict(name='d_svc_rstr', y=d_svc_rstr_0),dict(name='d_time_compliance', y=d_time_compliance_0)                                
                            ]
                },
                {
                    'name': 1,
                    'data': [
                                dict(name='d_resp_time', y=d_resp_time_1),dict(name='d_link_stat_chk', y=d_link_stat_chk_1),
                                dict(name='d_logs', y=d_logs_1),dict(name='d_poa', y=d_poa_1),
                                dict(name='d_cust_dep', y=d_cust_dep_1),dict(name='d_etr', y=d_etr_1),
                                dict(name='d_svc_rstr', y=d_svc_rstr_1),dict(name='d_time_compliance', y=d_time_compliance_1)
                            ]
                }
            ]
        return final_result

    def get_customer_responses_by_category_and_rating(self, customer_name, category, rating):
        print("Getting customer responses by category and rating")
        if category == "Initial Responses":
            if customer_name != "All":
                query = "SELECT i_time_stamp, initial_responses.ticket_number, i_poa, i_svc_rstr, i_link_stat_chk, " \
                        "i_etr, i_etr_time, i_logs, i_resp_time, i_time_update, i_cust_dep, i_time_compliance, " \
                        "resp_rating, email_number, email_type from initial_responses JOIN tickets ON " \
                        "tickets.ticket_number=initial_responses.ticket_number WHERE customer_name='" + customer_name \
                        + "' and resp_rating='"+rating+"' ORDER BY i_time_stamp DESC LIMIT 50;"
            else:
                query = "SELECT i_time_stamp, initial_responses.ticket_number, i_poa, i_svc_rstr, i_link_stat_chk, i_etr, i_etr_time, i_logs, i_resp_time, i_time_update, i_cust_dep, i_time_compliance, resp_rating, email_number, email_type from initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE resp_rating='"+rating+"' ORDER BY i_time_stamp DESC LIMIT 50"
            cursor = self.con.cursor()
            cursor.execute(query)
            final_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
            cursor.close()
         
        elif category == "Subsequent Responses":
            if customer_name != "All":
                query = "SELECT si_timestamp, subsequent_responses.ticket_number, si_prog, si_svc_rstr, si_no_prob, si_etr, si_etr_time, si_logs, si_resp_time, si_time_update, si_cd, si_time_compliance, resp_rating, email_number, email_type from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE customer_name='"+customer_name+"' and resp_rating='"+rating+"' ORDER BY si_timestamp DESC LIMIT 50"
            else:
                query = "SELECT si_timestamp, subsequent_responses.ticket_number, si_prog, si_svc_rstr, si_no_prob, si_etr, si_etr_time, si_logs, si_resp_time, si_time_update, si_cd, si_time_compliance, resp_rating, email_number, email_type from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE resp_rating='"+rating+"' ORDER BY si_timestamp DESC LIMIT 50"
            cursor = self.con.cursor()
            cursor.execute(query)
            final_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
            cursor.close()

        elif category == "Discarded Responses":
            if customer_name != "All":
                query = "SELECT d_time_stamp, discarded_responses.ticket_number, d_poa, d_svc_rstr, d_link_stat_chk, d_etr, d_etr_time, d_logs, d_resp_time, d_time_update, d_cust_dep, d_time_compliance, resp_rating, email_number, email_type from discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number WHERE customer_name='"+customer_name+"' and resp_rating='"+rating+"' ORDER BY d_time_stamp DESC LIMIT 50"
            else:
                query = "SELECT d_time_stamp, discarded_responses.ticket_number, d_poa, d_svc_rstr, d_link_stat_chk, d_etr, d_etr_time, d_logs, d_resp_time, d_time_update, d_cust_dep, d_time_compliance, resp_rating, email_number, email_type from discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number WHERE resp_rating='"+rating+"' ORDER BY d_time_stamp DESC LIMIT 50"
            
            cursor = self.con.cursor()
            cursor.execute(query)
            final_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row)
                                                     for row in cursor.fetchall()]]
            cursor.close()
        return final_results

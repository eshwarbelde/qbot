from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.template import loader
from environment.dashboard.customer import customer_service


def customer_dashboard(request):
    if 'user_name' in request.session and request.session['user_name'] != '':
        template = loader.get_template('customer_dashboard.html')
        return HttpResponse(template.render())
    else:
        template = loader.get_template('login.html')  # getting our template
        response = redirect('/login/')
        return response

# Customer page views
def get_all_tickets_group_by_customer_name_and_rating(request):
    result = customer_service.get_all_tickets_group_by_customer_name_and_rating()
    return JsonResponse(result, safe=False)


def get_all_tickets_group_by_customer_name_and_rating_for_timeline(request):
    result = customer_service.get_all_tickets_group_by_customer_name_and_rating_for_timeline()
    return JsonResponse(result, safe=False)


def get_all_tickets_group_by_customer_name(request):
    result = customer_service.get_all_tickets_group_by_customer_name()
    return JsonResponse(result, safe=False)


def get_all_tickets_by_customer(request):
    result = customer_service.get_all_tickets_by_customer()
    return JsonResponse(result, safe=False)


def get_customer_tickets_by_category_and_rating(request):
    customer_name = request.GET.get('customer_name', None)
    category = request.GET.get('category', None)
    rating = request.GET.get('rating', None)

    result = customer_service.get_customer_tickets_by_category_and_rating(customer_name, category, rating)
    return JsonResponse(result, safe=False)

def get_customer_responses_by_category_and_rating(request):
    customer_name = request.GET.get('customer_name', None)
    category = request.GET.get('category', None)
    rating = request.GET.get('rating', None)
    result = customer_service.get_customer_responses_by_category_and_rating(customer_name, category, rating)
    return JsonResponse(result, safe=False)
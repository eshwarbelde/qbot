import MySQLdb
from django.http import JsonResponse


class Metrics:
    
    def __init__(self):
        self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")

    def get_ticket_metrics(self):
        print("Getting the ticket metrics")
        query = "select count(*) from tickets;"
        cursor = self.con.cursor()
        cursor.execute(query)
        data = cursor.fetchone()
        cursor.close()
        total_tickets = data[0]
        
        # query = "select 24*avg(datediff(ticket_close_time,ticket_booking_time)) from tickets;"
        query = "SELECT count(*), SUM((TIMESTAMPDIFF(MINUTE,ticket_booking_time, ticket_close_time )))/ (count(*) * 60) resolution_time from tickets;"
        cursor = self.con.cursor()
        cursor.execute(query)
        data = cursor.fetchone()
        cursor.close()
        avg_resolution_time = data[1]
        
        query = "select count(*) from tickets where pwc_count > 0;"
        cursor = self.con.cursor()
        cursor.execute(query)
        data = cursor.fetchone()
        cursor.close()
        pwc_count = data[0]   
        
        query = "select avg(pwc_time/3600) from tickets;"
        cursor = self.con.cursor()
        cursor.execute(query)
        data = cursor.fetchone()
        cursor.close()
        pwc_time = data[0]

        final_result = {
            'total_tickets': total_tickets,
            'avg_resolution_time': avg_resolution_time,
            'pwc_count': pwc_count,
            'pwc_time': pwc_time
        }
        return JsonResponse(final_result, safe=False) 

    def get_product_and_customer_metrics(self):
        print("Calculating the product and customer metrics")
        query = "select product, count(*) from tickets group by product order by count(*) DESC LIMIT 5;"
        cursor = self.con.cursor()
        cursor.execute(query)
        top_five_products = [dict(product=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        query = "select customer_name, count(*) from tickets group by customer_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        customers = [dict(customer_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        query = "select customer_name, count(*) from tickets WHERE ticket_overall_score='Perfect' group by customer_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        perfect_customers = [dict(customer_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        final_result = {
            'top_five_products': top_five_products,
            'customers': customers,
            'perfect_customers': perfect_customers
        }
        return JsonResponse(final_result, safe=False) 

    def overall_ticket_rating_metrics(self):
        query = "select tickets.ticket_overall_score, count(*) from tickets group by ticket_overall_score;"
        cursor = self.con.cursor()
        cursor.execute(query)
        tickets_rating = [dict(ticket_overall_score=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        query = "select count(*) from initial_responses;"
        cursor = self.con.cursor()
        cursor.execute(query)
        total_initial= cursor.fetchone()[0]
        cursor.close()

        query = "select count(*) from initial_responses where resp_rating='Perfect';"
        cursor = self.con.cursor()
        cursor.execute(query)
        perfect_initial= cursor.fetchone()[0]
        cursor.close()

        query = "select count(*) from subsequent_responses;"
        cursor = self.con.cursor()
        cursor.execute(query)
        total_subsequent= cursor.fetchone()[0]
        cursor.close()

        query = "select count(*) from subsequent_responses where resp_rating='Perfect';"
        cursor = self.con.cursor()
        cursor.execute(query)
        perfect_subsequent= cursor.fetchone()[0]
        cursor.close()

        final_result = {
            'tickets_rating': tickets_rating,
            'total_initial': total_initial,
            'perfect_initial': perfect_initial,
            'total_subsequent': total_subsequent,
            'perfect_subsequent': perfect_subsequent
        }
        return JsonResponse(final_result, safe=False) 

    def agent_metrics(self):
        query = "select agent_name, count(*) as total from initial_responses group by agent_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_total_initial= [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        
        query = "SELECT agent_name, count(*) FROM initial_responses WHERE resp_rating='Perfect' GROUP BY agent_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_perfect_initial= [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        query = "select agent_name, count(*) from subsequent_responses group by agent_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_total_subsequent= [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        query = "select agent_name, count(*) from subsequent_responses where resp_rating='Perfect' group by agent_name;"
        cursor = self.con.cursor()
        cursor.execute(query)
        agent_perfect_subsequent= [dict(agent_name=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        result = []        
        for row in agent_total_initial:
            result.append(dict(agent_name=row['agent_name'], values=dict(total_initial=row['count'], perfect_initial=0,  total_subsequent=0, perfect_subsequent=0)))
        for row in agent_perfect_initial:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(total_initial=0, perfect_initial=row['count'],  total_subsequent=0, perfect_subsequent=0)))
            else:
                match['values']['perfect_initial'] = row['count']
            
        for row in agent_total_subsequent:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(total_initial=0, perfect_initial=0, total_subsequent=row['count'], perfect_subsequent=0)))
            else:
                match['values']['total_subsequent'] = row['count']

        for row in agent_perfect_subsequent:
            match = next((d for d in result if d['agent_name'] == row['agent_name']), None)
            if not match:
                result.append(dict(agent_name=row['agent_name'], values=dict(total_initial=0, perfect_initial=0, total_subsequent=0, perfect_subsequent=row['count'])))
            else:
                match['values']['perfect_subsequent'] = row['count']

        return JsonResponse(result, safe=False) 

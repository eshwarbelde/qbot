import MySQLdb
from django.http import JsonResponse
from environment.cache.quality_dashboard_cache import QualityDashboardCache


class MySQLConnection:
    def __init__(self):
        self.con = MySQLdb.connect(host="localhost",user= "root", passwd="mysql",db= "qualitybot",charset='utf8',use_unicode=True)
        self.cursor = self.con.cursor()

    def close_connection(self):
        try:
            if self.con:
                self.con.close()
        except Exception as e:
            print(e)

    def insert(self, query):
        try:
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()  # commit the transaction
            last_id = cursor.lastrowid
            cursor.close()
            return last_id
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            print(e)
            return None

    def update(self, query):
        print('update query: ', query)
        try:
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()
            count = cursor.rowcount
            cursor.close()
            return count
        except Exception as e:
            print(e)

    def delete(self, query):
        try:
            cursor = self.con.cursor()
            cursor.execute(query)
            self.con.commit()
            count = cursor.rowcount
            cursor.close()
            return count
        except Exception as e:
            print(e)
            self.con.rollback()

    def is_ticket_exist(self, ticket_number):
        try:
            query = "SELECT ticket_number from tickets WHERE ticket_number='"+ticket_number+"'"
            cursor = self.con.cursor()
            cursor.execute(query)
            cursor.fetchall()
            no_of_rows = cursor.rowcount
            cursor.close()
            if no_of_rows > 0:
                return True
            else:
                return False
        except:
            self.con.rollback()

    def add_master_data(self, table_name, name):
        query = "INSERT INTO "+table_name+"(name) values('"+name+"')"
        return self.insert(query)

    def get_status(self, row_id):
        query = "SELECT * FROM process_status WHERE id="+str(row_id)
        print(query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(line) for line in [zip([column[0] for column in cursor.description], row)
                                          for row in cursor.fetchall()]]
        cursor.close()
        con.close()
        return JsonResponse(result, safe=False)

    def get_latest_process_status(self):
        query = "SELECT * FROM process_status WHERE mode='Run' ORDER BY id DESC LIMIT 1"
        cursor = self.con.cursor()
        cursor.execute(query)
        result = [dict(line) for line in [zip([column[0] for column in cursor.description], row)
                                          for row in cursor.fetchall()]]
        cursor.close()
        return JsonResponse(result, safe=False)

    def get_all_owner_groups(self):
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        query = "SELECT owner_group.id, owner_group.name, owner_group.initial_sla, owner_group.subsequent_sla," \
                "desk.name FROM owner_group JOIN desk ON owner_group.desk_id=desk.id"
        print(query)
        cursor.execute(query)
        result = [dict(id=row[0], name=row[1], initial_sla=row[2], subsequent_sla=row[3], desk=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        con.close()
        return result

    def get_all_owner_groups_on_segmanets(self):
        query = "SELECT service_desk_identifier, customer_type, initial_sla, subsequent_sla from owner_groups_on_segment"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(service_desk_identifier=rec[0], customer_type = rec[1], initial_sla=rec[2], subsequent_sla=rec[3])
                  for rec in cursor.fetchall()]
        return result

    def get_owner_groups(self, collection_name, agent_name):
        cursor = self.con.cursor()
        query = "SELECT owner_group FROM agent_owner_groups WHERE agent_name='"+agent_name+"'"
        cursor.execute(query)
        result = [dict(name=row[0]) for row in cursor.fetchall()]
        cursor.close()
        return JsonResponse(result, safe=False)

    def get_tickets_group_by_bu(self):
        print("Getting all tickets group by bu and product severity rating")
        query = "SELECT bu.name as bu, desk.name as desk, owner_group.name as owner_group from owner_group JOIN desk ON desk.id=owner_group.desk_id JOIN bu ON bu.id=desk.bu_id"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        data = [dict(bu=row[0], desk=row[1], owner_group=row[2]) for row in cursor.fetchall()]
        cursor.close()
        con.close()    
                
        query = "SELECT product, severity, tickets.owner_group, ticket_overall_score, count(*) FROM tickets GROUP BY product, severity, owner_group, ticket_overall_score;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(product=row[0], severity=row[1], owner_group=row[2], ticket_overall_score=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
        con.close()
        final_result = []
        for row in result:
            for item in data:
                if item["owner_group"] == row['owner_group']:
                    final_result.append(dict(product=row['product'], severity=row['severity'], bu=item['bu'], desk=item['desk'], owner_group=item['owner_group'], ticket_overall_score=row['ticket_overall_score'], count=row['count']))
                    break
        print('Eshwar')
        print(final_result)
        QualityDashboardCache.tickets_group_by_bu = final_result
        return JsonResponse(result, safe=False)

    def get_tickets_group_by_owner_group(self, collection_name, product, severity, start_date, end_date):
        print("Getting all tickets group by owner_group and rating")
        result = []
        query = "SELECT id, name FROM bu"
        cursor = self.con.cursor()
        cursor.execute(query)
        bu_list = [dict(id=row[0], name=row[1]) for row in cursor.fetchall()]
        cursor.close()
        query = "SELECT desk.id, desk.name, bu.name FROM desk JOIN bu ON desk.bu_id=bu.id"
        cursor = self.con.cursor()
        cursor.execute(query)
        desks_list = [dict(id=row[0], name=row[1], bu=row[2]) for row in cursor.fetchall()]
        cursor.close()
        query = "SELECT owner_group.id, owner_group.name, owner_group.initial_sla, owner_group.subsequent_sla, " \
                "desk.name FROM owner_group JOIN desk ON owner_group.desk_id=desk.id"
        print(query)
        cursor = self.con.cursor()
        cursor.execute(query)
        owner_group_list = [dict(id=row[0], name=row[1], initial_sla=row[2], subsequent_sla=row[3], desk=row[4])
                  for row in cursor.fetchall()]
        cursor.close()
        for bu in bu_list:
            bu_obj = {'name': bu['name']}
            desks_array = []
            for desk in desks_list:
                if desk['bu'] == bu['name']:
                    desk_obj = {'name': desk['name']}
                    desks_array.append(desk_obj)
                    owner_groups_array = []
                    for og in owner_group_list:
                        if og['desk'] == desk['name']:
                            cursor = self.con.cursor()
                            query = "SELECT ticket_overall_score, COUNT(*) FROM tickets WHERE owner_group='" \
                                    + og['name']+"' GROUP BY ticket_overall_score"
                            cursor.execute(query)
                            scores = cursor.fetchall()
                            ratings = []
                            for score in scores:
                                ratings.append(dict(ticket_overall_score=score[0], count=score[1]))
                            owner_groups_array.append(dict(name=og['name'], ratings=ratings))
                            cursor.close()
                    desk_obj['owner_groups'] = owner_groups_array
            bu_obj['desks'] = desks_array
            result.append(bu_obj)
        return JsonResponse(result, safe=False)

    def get_all_tickets_by_owner_group(self, collection_name, owner_group, product, severity, start_date, end_date):
        print("Getting all initial emails group by rating and equal to selected owner_group")
        initial_query = "SELECT owner_group, product, severity, resp_rating, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number GROUP BY owner_group, product, severity, resp_rating;"
        cursor = self.con.cursor()
        cursor.execute(initial_query)
        initial_result = [dict(owner_group=row[0], product=row[1], severity=row[2], resp_rating=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
        subsequent_query = "SELECT owner_group, product, severity, resp_rating, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number GROUP BY owner_group, product, severity, resp_rating;"
        cursor = self.con.cursor()
        cursor.execute(subsequent_query)
        subsequent_result = [dict(owner_group=row[0], product=row[1], severity=row[2], resp_rating=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
        discarded_query = "SELECT owner_group, product, severity, resp_rating, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number GROUP BY owner_group, product, severity, resp_rating;"
        cursor = self.con.cursor()
        cursor.execute(discarded_query)
        discarded_result = [dict(owner_group=row[0], product=row[1], severity=row[2], resp_rating=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
                    
        initial_time_query = "SELECT owner_group, product, severity, i_time_compliance, count(*) FROM initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number GROUP BY owner_group, product, severity, i_time_compliance;"
        cursor = self.con.cursor()
        cursor.execute(initial_time_query)
        initial_time_result = [dict(owner_group=row[0], product=row[1], severity=row[2], i_time_compliance=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
                
        subsequent_time_query = "SELECT owner_group, product, severity, si_time_compliance, count(*) FROM subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number GROUP BY owner_group, product, severity, si_time_compliance;"
        cursor = self.con.cursor()
        cursor.execute(subsequent_time_query)
        subsequent_time_result = [dict(owner_group=row[0], product=row[1], severity=row[2], si_time_compliance=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()

        discarded_time_query = "SELECT owner_group, product, severity, d_time_compliance, count(*) FROM discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number GROUP BY owner_group, product, severity, d_time_compliance;"
        cursor = self.con.cursor()
        cursor.execute(discarded_time_query)
        discarded_time_result = [dict(owner_group=row[0], product=row[1], severity=row[2], d_time_compliance=row[3], count=row[4]) for row in cursor.fetchall()]
        cursor.close()
        quality_results = {
            "initial_emails": initial_result,
            "subsequent_emails": subsequent_result,
            "discarded_emails": discarded_result
        }
        time_results = {
            "initial_emails": initial_time_result,
            "subsequent_emails": subsequent_time_result,
            "discarded_emails": discarded_time_result
        }
        final_results = {
            "quality": quality_results,
            "time": time_results
        }
        QualityDashboardCache.tickets_by_owner_group = final_results
        return JsonResponse(final_results, safe=False)
    
    def get_owner_group_tickets_by_category_and_rating(self, owner_group, category, rating):
        print("Getting customer tickets by category and rating")
        if category == "Initial Responses":
            if owner_group != "All":
                query  = "SELECT i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr,i_time_compliance, count(*) from initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE owner_group='"+owner_group+"' and resp_rating='"+rating+"' GROUP BY i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr,i_time_compliance"
            else:
                query  = "SELECT i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr,i_time_compliance, count(*) from initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE resp_rating='"+rating+"' GROUP BY i_resp_time, i_link_stat_chk, i_logs, i_poa, i_cust_dep, i_etr, i_svc_rstr,i_time_compliance"
            cursor = self.con.cursor()
            cursor.execute(query)
            result = [dict(i_resp_time=row[0], i_link_stat_chk=row[1], i_logs=row[2], i_poa=row[3], i_cust_dep=row[4], i_etr=row[5], i_svc_rstr=row[6],i_time_compliance=row[7], count=row[8]) for row in cursor.fetchall()]
            cursor.close()
            i_resp_time_0, i_link_stat_chk_0, i_logs_0, i_poa_0, i_cust_dep_0, i_etr_0, i_svc_rstr_0,i_time_compliance_0 = 0,0,0,0,0,0,0,0
            i_resp_time_1, i_link_stat_chk_1, i_logs_1, i_poa_1, i_cust_dep_1, i_etr_1, i_svc_rstr_1,i_time_compliance_1 = 0,0,0,0,0,0,0,0                          
            for data in result:
                if data['i_resp_time'] == 0:
                    i_resp_time_0 += data['count']
                else:
                    i_resp_time_1+=data['count']
                if data['i_link_stat_chk'] == 0:
                    i_link_stat_chk_0 += data['count']  
                else:
                    i_link_stat_chk_1 += data['count']
                if data['i_logs'] == 0:
                    i_logs_0 += data['count']
                else:
                    i_logs_1 += data['count']
                if data['i_poa'] == 0:
                    i_poa_0 += data['count']  
                else:
                    i_poa_1 += data['count']
                if data['i_cust_dep'] == 0:
                    i_cust_dep_0 += data['count']  
                else:
                    i_cust_dep_1 += data['count']
                if data['i_etr'] == 0:
                    i_etr_0 += data['count']  
                else: 
                    i_etr_1 += data['count']
                if data['i_svc_rstr'] == 0:
                    i_svc_rstr_0 += data['count']  
                else: 
                    i_svc_rstr_1 += data['count'] 
                if data['i_time_compliance'] == 0:
                    i_time_compliance_0 += data['count']  
                else: 
                    i_time_compliance_1 += data['count']
            final_result=[
                {
                    'name': 0,
                    'data': [
                                dict(name='i_resp_time', y=i_resp_time_0),dict(name='i_link_stat_chk', y=i_link_stat_chk_0),
                                dict(name='i_logs', y=i_logs_0),dict(name='i_poa', y=i_poa_0),
                                dict(name='i_cust_dep', y=i_cust_dep_0),dict(name='i_etr', y=i_etr_0),
                                dict(name='i_svc_rstr', y=i_svc_rstr_0),dict(name='i_time_compliance', y=i_time_compliance_0)                                
                            ]
                },
                {
                    'name': 1,
                    'data': [
                                dict(name='i_resp_time', y=i_resp_time_1),dict(name='i_link_stat_chk', y=i_link_stat_chk_1),
                                dict(name='i_logs', y=i_logs_1),dict(name='i_poa', y=i_poa_1),
                                dict(name='i_cust_dep', y=i_cust_dep_1),dict(name='i_etr', y=i_etr_1),
                                dict(name='i_svc_rstr', y=i_svc_rstr_1),dict(name='i_time_compliance', y=i_time_compliance_1)                                
                            ]
                }
            ]       
        elif category == "Subsequent Responses":
            if owner_group != "All":
                query = "SELECT si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance, count(*) from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE owner_group='"+owner_group+"' and resp_rating='"+rating+"' GROUP BY si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance;"
            else:
                query = "SELECT si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance, count(*) from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE resp_rating='"+rating+"' GROUP BY si_resp_time, si_no_prob, si_logs, si_prog, si_cd, si_etr, si_svc_rstr, si_time_compliance;"
            cursor = self.con.cursor()
            cursor.execute(query)
            result = [dict(si_resp_time=row[0], si_no_prob=row[1], si_logs=row[2], si_prog=row[3], si_cd=row[4], si_etr=row[5], si_svc_rstr=row[6],si_time_compliance=row[7], count=row[8]) for row in cursor.fetchall()]
            cursor.close()
            si_resp_time_0, si_no_prob_0, si_logs_0, si_prog_0, si_cd_0, si_etr_0, si_svc_rstr_0, si_time_compliance_0 = 0,0,0,0,0,0,0,0
            si_resp_time_1, si_no_prob_1, si_logs_1, si_prog_1, si_cd_1, si_etr_1, si_svc_rstr_1, si_time_compliance_1 = 0,0,0,0,0,0,0,0
            for data in result:
                if data['si_resp_time'] == 0: 
                    si_resp_time_0+=data['count']  
                else: 
                    si_resp_time_1+=data['count']
                if data['si_no_prob'] == 0: 
                    si_no_prob_0 += data['count'] 
                else: 
                    si_no_prob_1 += data['count']
                if data['si_logs'] == 0: 
                    si_logs_0 += data['count']  
                else: 
                    si_logs_1 += data['count']
                if data['si_prog'] == 0: 
                    si_prog_0 += data['count'] 
                else: 
                    si_prog_1 += data['count']
                if data['si_cd'] == 0: 
                    si_cd_0 += data['count'] 
                else: 
                    si_cd_1 += data['count']
                if data['si_etr'] == 0: 
                    si_etr_0 += data['count'] 
                else: 
                    si_etr_1 += data['count']
                if data['si_svc_rstr'] == 0: 
                    si_svc_rstr_0 += data['count'] 
                else: 
                    si_svc_rstr_1 += data['count'] 
                if data['si_time_compliance'] == 0:
                    si_time_compliance_0 += data['count'] 
                else: 
                    si_time_compliance_1 += data['count']
            final_result=[
                {
                    'name': 0,
                    'data': [
                                dict(name='si_resp_time', y=si_resp_time_0),dict(name='si_no_prob', y=si_no_prob_0),
                                dict(name='si_logs', y=si_logs_0),dict(name='si_prog', y=si_prog_0),
                                dict(name='si_cd', y=si_cd_0),dict(name='si_etr', y=si_etr_0),
                                dict(name='si_svc_rstr', y=si_svc_rstr_0),dict(name='si_time_compliance', y=si_time_compliance_0)                                
                            ]
                },
                {
                    'name': 1,
                    'data': [
                                dict(name='si_resp_time', y=si_resp_time_1),dict(name='si_no_prob', y=si_no_prob_1),
                                dict(name='si_logs', y=si_logs_1),dict(name='si_prog', y=si_prog_1),
                                dict(name='si_cd', y=si_cd_1),dict(name='si_etr', y=si_etr_1),
                                dict(name='si_svc_rstr', y=si_svc_rstr_1),dict(name='si_time_compliance', y=si_time_compliance_1)
                            ]
                }
            ]
        elif category == "Discarded Responses":
            if owner_group != "All":
                query = "SELECT d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance, count(*) from discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number WHERE owner_group='"+owner_group+"' and resp_rating='"+rating+"' GROUP BY d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance;"
            else:
                query = "SELECT d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance, count(*) from discarded_responses JOIN tickets ON tickets.ticket_number=discarded_responses.ticket_number WHERE resp_rating='"+rating+"' GROUP BY d_resp_time, d_link_stat_chk, d_logs, d_poa, d_cust_dep, d_etr, d_svc_rstr, d_time_compliance;"
            cursor = self.con.cursor()
            cursor.execute(query)
            result = [dict(d_resp_time=row[0], d_link_stat_chk=row[1], d_logs=row[2], d_poa=row[3], d_cust_dep=row[4], d_etr=row[5], d_svc_rstr=row[6],d_time_compliance=row[7], count=row[8]) for row in cursor.fetchall()]
            cursor.close()
            d_resp_time_0, d_link_stat_chk_0, d_logs_0, d_poa_0, d_cust_dep_0, d_etr_0, d_svc_rstr_0, d_time_compliance_0 = 0,0,0,0,0,0,0,0
            d_resp_time_1, d_link_stat_chk_1, d_logs_1, d_poa_1, d_cust_dep_1, d_etr_1, d_svc_rstr_1, d_time_compliance_1 = 0,0,0,0,0,0,0,0
            for data in result:
                if data['d_resp_time'] == 0: 
                    d_resp_time_0+=data['count']  
                else: 
                    d_resp_time_1+=data['count']
                if data['d_link_stat_chk'] == 0: 
                    d_link_stat_chk_0 += data['count'] 
                else: 
                    d_link_stat_chk_1 += data['count']
                if data['d_logs'] == 0: 
                    d_logs_0 += data['count']  
                else: 
                    d_logs_1 += data['count']
                if data['d_poa'] == 0: 
                    d_poa_0 += data['count'] 
                else: 
                    d_poa_1 += data['count']
                if data['d_cust_dep'] == 0: 
                    d_cust_dep_0 += data['count'] 
                else: 
                    d_cust_dep_1 += data['count']
                if data['d_etr'] == 0: 
                    d_etr_0 += data['count'] 
                else: 
                    d_etr_1 += data['count']
                if data['d_svc_rstr'] == 0: 
                    d_svc_rstr_0 += data['count'] 
                else: 
                    d_svc_rstr_1 += data['count'] 
                if data['d_time_compliance'] == 0:
                    d_time_compliance_0 += data['count'] 
                else: 
                    d_time_compliance_1 += data['count']
            final_result=[
                {
                    'name': 0,
                    'data': [
                                dict(name='d_resp_time', y=d_resp_time_0),dict(name='d_link_stat_chk', y=d_link_stat_chk_0),
                                dict(name='d_logs', y=d_logs_0),dict(name='d_poa', y=d_poa_0),
                                dict(name='d_cust_dep', y=d_cust_dep_0),dict(name='d_etr', y=d_etr_0),
                                dict(name='d_svc_rstr', y=d_svc_rstr_0),dict(name='d_time_compliance', y=d_time_compliance_0)                                
                            ]
                },
                {
                    'name': 1,
                    'data': [
                                dict(name='d_resp_time', y=d_resp_time_1),dict(name='d_link_stat_chk', y=d_link_stat_chk_1),
                                dict(name='d_logs', y=d_logs_1),dict(name='d_poa', y=d_poa_1),
                                dict(name='d_cust_dep', y=d_cust_dep_1),dict(name='d_etr', y=d_etr_1),
                                dict(name='d_svc_rstr', y=d_svc_rstr_1),dict(name='d_time_compliance', y=d_time_compliance_1)
                            ]
                }
            ]
        return JsonResponse(final_result, safe=False)

    def get_owner_group_responses_by_category_and_rating(self, owner_group, category, rating):
        print("Getting customer responses by category and rating")
        if category == "Initial Responses":
            if owner_group != "All":
                query  = "SELECT i_time_stamp, initial_responses.ticket_number, i_poa, i_svc_rstr, i_link_stat_chk, i_etr, i_etr_time, i_logs, i_resp_time, i_time_update, i_cust_dep, i_time_compliance, resp_rating, email_number, email_type from initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE owner_group='"+owner_group+"' and resp_rating='"+rating+"' ORDER BY i_time_stamp DESC LIMIT 50"
            else:
                query  = "SELECT i_time_stamp, initial_responses.ticket_number, i_poa, i_svc_rstr, i_link_stat_chk, i_etr, i_etr_time, i_logs, i_resp_time, i_time_update, i_cust_dep, i_time_compliance, resp_rating, email_number, email_type from initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE resp_rating='"+rating+"' ORDER BY i_time_stamp DESC LIMIT 50"
            cursor = self.con.cursor()
            cursor.execute(query)
            final_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
            cursor.close()
         
        elif category == "Subsequent Responses":
            if owner_group != "All":
                query  = "SELECT si_timestamp, subsequent_responses.ticket_number, si_prog, si_svc_rstr, si_no_prob, si_etr, si_etr_time, si_logs, si_resp_time, si_time_update, si_cd, si_time_compliance, resp_rating, email_number, email_type from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE owner_group='"+owner_group+"' and resp_rating='"+rating+"' ORDER BY si_timestamp DESC LIMIT 50"
            else:
                query  = "SELECT si_timestamp, subsequent_responses.ticket_number, si_prog, si_svc_rstr, si_no_prob, si_etr, si_etr_time, si_logs, si_resp_time, si_time_update, si_cd, si_time_compliance, resp_rating, email_number, email_type from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE resp_rating='"+rating+"' ORDER BY si_timestamp DESC LIMIT 50"
            cursor = self.con.cursor()
            cursor.execute(query)
            final_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
            cursor.close()

        elif category == "Discarded Responses":
            if owner_group != "All":
                query = "SELECT d_time_stamp, discarded_responses.ticket_number, d_poa, d_svc_rstr, d_link_stat_chk, " \
                        "d_etr, d_etr_time, d_logs, d_resp_time, d_time_update, d_cust_dep, d_time_compliance, " \
                        "resp_rating, email_number, email_type from discarded_responses JOIN tickets ON " \
                        "tickets.ticket_number=discarded_responses.ticket_number WHERE owner_group='"+owner_group\
                        + "' and resp_rating='"+rating+"' ORDER BY d_time_stamp DESC LIMIT 50"
            else:
                query = "SELECT d_time_stamp, discarded_responses.ticket_number, d_poa, d_svc_rstr, d_link_stat_chk, " \
                        "d_etr, d_etr_time, d_logs, d_resp_time, d_time_update, d_cust_dep, d_time_compliance, " \
                        "resp_rating, email_number, email_type from discarded_responses JOIN tickets ON " \
                        "tickets.ticket_number=discarded_responses.ticket_number WHERE resp_rating='"+rating \
                        + "' ORDER BY d_time_stamp DESC LIMIT 50"
            
            cursor = self.con.cursor()
            cursor.execute(query)
            final_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
            cursor.close()
        return JsonResponse(final_results, safe=False)

    def get_owner_group_responses_by_rating(self, owner_group, rating):
        print("Getting customer responses by rating")
        if owner_group != "All":
            initial_query  = "SELECT i_time_stamp, initial_responses.ticket_number, i_poa, i_svc_rstr, i_link_stat_chk, i_etr, i_etr_time, i_logs, i_resp_time, i_time_update, i_cust_dep, i_time_compliance, resp_rating, email_number, email_type from initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE owner_group='"+owner_group+"' and resp_rating='"+rating+"' ORDER BY i_time_stamp DESC LIMIT 50"
        else:
            initial_query  = "SELECT i_time_stamp, initial_responses.ticket_number, i_poa, i_svc_rstr, i_link_stat_chk, i_etr, i_etr_time, i_logs, i_resp_time, i_time_update, i_cust_dep, i_time_compliance, resp_rating, email_number, email_type from initial_responses JOIN tickets ON tickets.ticket_number=initial_responses.ticket_number WHERE resp_rating='"+rating+"' ORDER BY i_time_stamp DESC LIMIT 50"
        cursor = self.con.cursor()
        cursor.execute(initial_query)
        initial_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
        cursor.close()
         
        if owner_group != "All":
            subsequent_query  = "SELECT si_timestamp, subsequent_responses.ticket_number, si_prog, si_svc_rstr, si_no_prob, si_etr, si_etr_time, si_logs, si_resp_time, si_time_update, si_cd, si_time_compliance, resp_rating, email_number, email_type from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE owner_group='"+owner_group+"' and resp_rating='"+rating+"' ORDER BY si_timestamp DESC LIMIT 50"
        else:
            subsequent_query  = "SELECT si_timestamp, subsequent_responses.ticket_number, si_prog, si_svc_rstr, si_no_prob, si_etr, si_etr_time, si_logs, si_resp_time, si_time_update, si_cd, si_time_compliance, resp_rating, email_number, email_type from subsequent_responses JOIN tickets ON tickets.ticket_number=subsequent_responses.ticket_number WHERE resp_rating='"+rating+"' ORDER BY si_timestamp DESC LIMIT 50"
        cursor = self.con.cursor()
        cursor.execute(subsequent_query)
        subsequent_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
        cursor.close()

        if owner_group != "All":
            discarded_query = "SELECT d_time_stamp, discarded_responses.ticket_number, d_poa, d_svc_rstr, d_link_stat_chk, " \
                    "d_etr, d_etr_time, d_logs, d_resp_time, d_time_update, d_cust_dep, d_time_compliance, " \
                    "resp_rating, email_number, email_type from discarded_responses JOIN tickets ON " \
                    "tickets.ticket_number=discarded_responses.ticket_number WHERE owner_group='"+owner_group\
                    + "' and resp_rating='"+rating+"' ORDER BY d_time_stamp DESC LIMIT 50"
        else:
            discarded_query = "SELECT d_time_stamp, discarded_responses.ticket_number, d_poa, d_svc_rstr, d_link_stat_chk, " \
                    "d_etr, d_etr_time, d_logs, d_resp_time, d_time_update, d_cust_dep, d_time_compliance, " \
                    "resp_rating, email_number, email_type from discarded_responses JOIN tickets ON " \
                    "tickets.ticket_number=discarded_responses.ticket_number WHERE resp_rating='"+rating \
                    + "' ORDER BY d_time_stamp DESC LIMIT 50"
        
        cursor = self.con.cursor()
        cursor.execute(discarded_query)
        discarded_results = [dict(line) for line in [zip([column[0] for column in cursor.description], row) for row in cursor.fetchall()]]
        cursor.close()

        final_results = {
            "initial_emails": initial_results,
            "subsequent_emails": subsequent_results,
            "discarded_emails": discarded_results
        }
        return JsonResponse(final_results, safe=False)

    def get_all_tickets(self, collection_name, owner_group, product, severity, start_date, end_date):
        print("Getting all tickets group by bu and product severity rating")
        query = "SELECT bu.name as bu, desk.name as desk, owner_group.name as owner_group from owner_group " \
                "JOIN desk ON desk.id=owner_group.desk_id JOIN bu ON bu.id=desk.bu_id"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        data = [dict(bu=row[0], desk=row[1], owner_group=row[2]) for row in cursor.fetchall()]
        cursor.close()
        con.close()    
                
        query = "SELECT product, severity, owner_group, date_format(ticket_booking_time, '%d-%m-%Y') as " \
                "ticket_booking_time, ticket_overall_score, count(*) FROM tickets GROUP BY product, severity, " \
                "owner_group,date_format(ticket_booking_time, '%d-%m-%Y'), ticket_overall_score;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(product=row[0], severity=row[1], owner_group=row[2], ticket_booking_time=row[3],
                       ticket_overall_score=row[4], count=row[5]) for row in cursor.fetchall()]
        cursor.close()
        con.close()
        final_result = []
        for row in result:
            for item in data:
                if item["owner_group"] == row['owner_group']:
                    final_result.append(dict(product=row['product'], severity=row['severity'], bu=item['bu'], desk=item['desk'], owner_group=item['owner_group'], ticket_booking_time=row['ticket_booking_time'], ticket_overall_score=row['ticket_overall_score'], count=row['count']))
                    break
        QualityDashboardCache.all_tickets_for_timeline = final_result
        return JsonResponse(result, safe=False)

    def get_email_content(self, collection_name, ticket_number, email_number, email_type):
        content = ''
        query = "SELECT response_text FROM email_content WHERE ticket_number='"+ticket_number+"' and email_number=" \
                + str(email_number)+" and email_type='"+email_type+"'"
        cursor = self.con.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        cursor.close()
        if len(result) > 0:
            content = result[0][0]
        return JsonResponse(content, safe=False)

    def get_process_history_list(self, collection_name):
        query = "SELECT * FROM process_status"
        cursor = self.con.cursor()
        cursor.execute(query)
        result = [dict(line) for line in
                            [zip([column[0] for column in cursor.description], row) for row in
                             cursor.fetchall()]]
        cursor.close()
        return JsonResponse(result, safe=False)

    @staticmethod
    def query_filters(owner_group, product, severity, start_date, end_date):
        index = 0
        where = ""
        if owner_group != "All":
            where += "tickets.owner_group = '"+owner_group+"'"
            index += 1
        if product != "All":
            if index == 0:
                where += "tickets.product = '"+product+"'"
            else:
                where += " and tickets.product = '"+product+"'"
            index += 1
        if severity != "All":
            if index == 0:
                where += "tickets.severity = '"+severity+"'"
            else:
                where += " and tickets.severity = '"+severity+"'"
            index += 1
        if start_date != "" and end_date != "":
            if index == 0:
                where += "tickets.ticket_booking_time > '"+start_date+"' and tickets.ticket_booking_time < '"+end_date+"'"
            else:
                where += " and tickets.ticket_booking_time > '"+start_date+"' and tickets.ticket_booking_time < '"+end_date+"'"
            index += 1        

        return where
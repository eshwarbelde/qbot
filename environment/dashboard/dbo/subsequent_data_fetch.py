import MySQLdb
from django.http import JsonResponse


class SubsequentDataFetch:
    def __init__(self):
        self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
    
    def get_content_compliance_results(self, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            subsequent_perfect = "SELECT count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+"  and resp_rating = 'Perfect';"
            subsequent_needs_improvement = "SELECT count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+"  and resp_rating = 'Needs Improvement';"
        else:
            subsequent_perfect = "SELECT count(*) FROM subsequent_responses WHERE resp_rating = 'Perfect';"
            subsequent_needs_improvement = "SELECT count(*) FROM subsequent_responses WHERE resp_rating = 'Needs Improvement';"
        
        cursor = self.con.cursor()
        cursor.execute(subsequent_perfect)
        perfect_subsequent = cursor.fetchone()[0]
        cursor.close()

        cursor = self.con.cursor()
        cursor.execute(subsequent_needs_improvement)
        needs_improvement_subsequent = cursor.fetchone()[0]
        cursor.close()

        total_records = perfect_subsequent + needs_improvement_subsequent
        result = []
        result.append({'name': 'Compliant', 'count':perfect_subsequent, 'total_records':total_records, 'color':'rgb(51, 204, 0)', 'y': round((perfect_subsequent * 100)/total_records)})
        result.append({'name': 'Non-compliant', 'count': needs_improvement_subsequent, 'total_records':total_records, 'color':'rgb(255, 190, 25)', 'y': round((needs_improvement_subsequent * 100)/total_records)})
        return JsonResponse(result, safe=False)

    def get_time_compliance_results(self, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            compliant_query = "SELECT count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_time_compliance=1 and "+where_string+";"
            non_compliant_query = "SELECT count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_time_compliance=0 and "+where_string+";"
        else:
            compliant_query = "SELECT count(*) FROM subsequent_responses WHERE si_time_compliance=1;"
            non_compliant_query = "SELECT count(*) FROM subsequent_responses WHERE si_time_compliance=0;"
        print(compliant_query)
        cursor = self.con.cursor()
        cursor.execute(compliant_query)
        time_compliant =  cursor.fetchone()[0]
        cursor.close()
        print(non_compliant_query)
        cursor = self.con.cursor()
        cursor.execute(non_compliant_query)
        time_non_compliant =  cursor.fetchone()[0]
        cursor.close()
        total_initial = time_compliant + time_non_compliant
        result = []
        result.append({'name':"Compliant", 'count':time_compliant, 'color':'rgb(51, 204, 0)', 'total_records':total_initial,'y':round((time_compliant*100 / total_initial))})
        result.append({'name':"Non compliant", 'count':time_non_compliant, 'color':'rgb(255, 190, 25)', 'total_records':total_initial, 'y':round((time_non_compliant*100 / total_initial))})
        return JsonResponse(result, safe=False)

    def get_missing_parameters_results(self, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            query = "select shortest_path, count(*) count from subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" and shortest_path <> 'undetermined' and shortest_path <> 'None' and shortest_path <> 'nan' group by shortest_path;"
        else:
            query = "select shortest_path, count(*) count from subsequent_responses WHERE shortest_path <> 'undetermined' and shortest_path <> 'None' and shortest_path <> 'nan' group by shortest_path;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(shortest_path=row[0], count=row[1]) for row in cursor.fetchall()]
        return JsonResponse(result, safe=False)

    def get_time_compliance_difference_results_with_SLA(self, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            query = "select 10*floor(difference/10) range_start, 10*floor(difference/10) + 10 range_end, concat(10*floor(difference/10), '  -  ', 10*floor(difference/10) + 10) as `range`, " \
                +"count(*) as count  from (SELECT subsequent_responses.ticket_number, si_timestamp,  ticket_booking_time, TIMESTAMPDIFF(MINUTE,ticket_booking_time, si_timestamp), " \
                +"subsequent_sla, (TIMESTAMPDIFF(MINUTE,ticket_booking_time, si_timestamp)) - owner_group.subsequent_sla difference from subsequent_responses JOIN tickets ON subsequent_" \
                +"responses.ticket_number=tickets.ticket_number JOIN owner_group ON tickets.owner_group = owner_group.name where subsequent_sla <> 'As per customer segment' and "+where_string+") as t group by `range` order by range_start;"
        else:
            query = "select 10*floor(difference/10) range_start, 10*floor(difference/10) + 10 range_end, concat(10*floor(difference/10), '  -  ', 10*floor(difference/10) + 10) as `range`, " \
                +"count(*) as count  from (SELECT subsequent_responses.ticket_number, si_timestamp,  ticket_booking_time, TIMESTAMPDIFF(MINUTE,ticket_booking_time, si_timestamp), " \
                +"subsequent_sla, (TIMESTAMPDIFF(MINUTE,ticket_booking_time, si_timestamp)) - owner_group.subsequent_sla difference from subsequent_responses JOIN tickets ON subsequent_" \
                +"responses.ticket_number=tickets.ticket_number JOIN owner_group ON tickets.owner_group = owner_group.name where subsequent_sla <> 'As per customer segment') as t group by `range` order by range_start;"

        con = self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(range_start=row[0], range_end=row[1], range=row[2], count=row[3]) for row in cursor.fetchall()]

        cursor.close()
        con.close()
        return JsonResponse(result, safe=False)

    def get_subsequent_responses_by_rating(self, resp_rating):
        if resp_rating == 'Compliant':
            query = "SELECT * FROM subsequent_responses WHERE resp_rating='Perfect';"
        else:
            query = "SELECT * FROM subsequent_responses WHERE resp_rating='Needs Improvement';"
        print(query)
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(line) for line in [zip([column[0] for column in cursor.description], row)
                                          for row in cursor.fetchall()]]
        cursor.close()
        con.close()
        return JsonResponse(result, safe=False)

    def get_subsequent_responses_by_time_compliance(self, time_compliance):
        if time_compliance == 'Compliant':
            query = "SELECT * FROM subsequent_responses WHERE si_time_compliance = 1;"
        else:
            query = "SELECT * FROM subsequent_responses WHERE si_time_compliance = 0;"
        con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = con.cursor()
        cursor.execute(query)
        result = [dict(line) for line in [zip([column[0] for column in cursor.description], row)
                                          for row in cursor.fetchall()]]
        cursor.close()
        con.close()
        return JsonResponse(result, safe=False)
    
    def getAllSubsequentResponsesGroupByRating(self, collection_name, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            query = "SELECT resp_score, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY resp_score;"
        else:
            query = "SELECT resp_score, count(*) FROM subsequent_responses GROUP BY resp_score;"
        cursor = self.con.cursor()
        cursor.execute(query)
        total_records = [dict(resp_rating=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        return JsonResponse(total_records, safe=False)

    def getAllSubsequentEmailsWithNoProgress(self, collection_name, mode, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_prog=0 and "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        else:
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses WHERE si_prog=0 GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)
        

    def getAllSubsequentEmailsWithNoProgressGroupByEmailType(self, collection_name, mode, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT email_type, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_prog=0 and "+where_string+" GROUP BY email_type;"
            total_records_query = "SELECT email_type, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY email_type;"
        else:
            records_query = "SELECT email_type, count(*) FROM subsequent_responses WHERE si_prog=0 GROUP BY email_type;"
            total_records_query = "SELECT email_type, count(*) FROM subsequent_responses GROUP BY email_type;"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(email_type=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(email_type=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)
    
    def getAllSubsequentEmailsWithNoServiceRestoreAndNoETR(self, collection_name, mode, owner_group, product,
                                                           severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_svc_rstr=0 and si_etr=0 and "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        else:
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses WHERE si_svc_rstr=0 and si_etr=0 GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)

    def getAllTicketsWithNoProblemFound(self, collection_name, mode, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_no_prob=1 and "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        else:
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses WHERE si_no_prob=1 GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)

    def getAllSubsequentEmailsWithServiceRestoreAndNoLogs(self, collection_name, mode, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_svc_rstr=1 and si_logs=0 and "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        else:
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses WHERE si_svc_rstr=1 and si_logs=0 GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)

    def getAllSubsequentEmailsWithOnlyLogs(self, collection_name, mode, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_svc_rstr=0 and si_logs=1 and si_prog=0 and si_no_prob=0 and si_etr=0 and si_resp_time=0 and si_cd=0 and "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        else:
            records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses WHERE si_svc_rstr=0 and si_logs=1 and si_prog=0 and si_no_prob=0 and si_etr=0 and si_resp_time=0 and si_cd=0 GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
            total_records_query = "SELECT date_format(si_timestamp, '%d-%m-%Y') as si_timestamp, count(*) FROM subsequent_responses GROUP BY date_format(si_timestamp, '%d-%m-%Y');"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(si_timestamp=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)
    
    def getAllSubsequentEmailsWithSLAVoilationGroupByEmailType(self, collection_name, mode, owner_group, product,
                                                               severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT email_type, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_time_compliance=0 and "+where_string+" GROUP BY email_type;"
            total_records_query = "SELECT email_type, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY email_type;"
        else:
            records_query = "SELECT email_type, count(*) FROM subsequent_responses WHERE si_time_compliance=0 GROUP BY email_type;"
            total_records_query = "SELECT email_type, count(*) FROM subsequent_responses GROUP BY email_type;"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(email_type=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(email_type=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)
    
    def get_all_subsequent_emails_with_sla_violation_group_by_quality_complaint(self, collection_name, mode, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            records_query = "SELECT resp_score, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE si_time_compliance=0 and "+where_string+" GROUP BY resp_score;"
            total_records_query = "SELECT resp_score, count(*) FROM subsequent_responses JOIN tickets ON subsequent_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY resp_score;"
        else:
            records_query = "SELECT resp_score, count(*) FROM subsequent_responses WHERE si_time_compliance=0 GROUP BY resp_score;"
            total_records_query = "SELECT resp_score, count(*) FROM subsequent_responses GROUP BY resp_score;"
        print(total_records_query)
        cursor = self.con.cursor()
        cursor.execute(records_query)
        resultsList_with_condition = [dict(resp_rating=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()
        if mode == '#':
            final_result = {
                  'records' : resultsList_with_condition
                }
        else:
            cursor = self.con.cursor()
            cursor.execute(total_records_query)
            total_records = [dict(resp_rating=row[0], count=row[1]) for row in cursor.fetchall()]
            cursor.close()
            final_result = {
              'records': resultsList_with_condition,
              'total_records': total_records
            }
        return JsonResponse(final_result, safe=False)

    @staticmethod
    def query_filters(owner_group, product, severity, start_date, end_date):
        index = 0
        where = ""
        if owner_group != "All":
            where += "tickets.owner_group = '"+owner_group+"'"
            index += 1
        if product != "All":
            if index == 0:
                where += "tickets.product = '"+product+"'"
            else:
                where += " and tickets.product = '"+product+"'"
            index += 1
        if severity != "All":
            if index == 0:
                where += "tickets.severity = '"+severity+"'"
            else:
                where += " and tickets.severity = '"+severity+"'"
            index += 1
        if start_date != "" and end_date != "":
            if index == 0:
                where += "tickets.ticket_booking_time > '"+start_date+"' and tickets.ticket_booking_time < '"+end_date+"'"
            else:
                where += " and tickets.ticket_booking_time > '"+start_date+"' and tickets.ticket_booking_time < '"+end_date+"'"
            index += 1        

        return where

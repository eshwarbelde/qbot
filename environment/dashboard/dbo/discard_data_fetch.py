from django.http import JsonResponse
import MySQLdb


class DiscardedDataFetch:
    def __init__(self):
        self.con = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")

    def getDiscardedEmails(self, mode, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        if where_string != "":
            initial_responses_query = "SELECT date_format(i_time_stamp, '%d-%m-%Y') as i_time_stamp, count(*) FROM initial_responses JOIN tickets ON initial_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(i_time_stamp, '%d-%m-%Y');"
        else:
            initial_responses_query = "SELECT date_format(i_time_stamp, '%d-%m-%Y') as i_time_stamp, count(*) FROM initial_responses GROUP BY date_format(i_time_stamp, '%d-%m-%Y');"
        cursor = self.con.cursor()
        cursor.execute(initial_responses_query)
        initial_result = [dict(i_time_stamp=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()

        if where_string != "":
            discarded_responses_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp, count(*) FROM discarded_responses JOIN tickets ON discarded_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(d_time_stamp, '%d-%m-%Y');"
        else:
            discarded_responses_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp, count(*) FROM discarded_responses GROUP BY date_format(d_time_stamp, '%d-%m-%Y');"
        cursor = self.con.cursor()
        cursor.execute(discarded_responses_query)
        discarded_result = [dict(d_time_stamp=row[0], count=row[1]) for row in cursor.fetchall()]
        cursor.close()    

        final_result = {
            "initial_data": initial_result,
            "discarded_data": discarded_result
        }
        return JsonResponse(final_result, safe=False)

    def discardedEmailsGroupByDiscardNumber(self, owner_group, product, severity, start_date, end_date):
        where_string = self.query_filters(owner_group, product, severity, start_date, end_date)
        
        if where_string != "":
            discarded_responses_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp, email_type, count(*) FROM discarded_responses JOIN tickets ON discarded_responses.ticket_number=tickets.ticket_number WHERE "+where_string+" GROUP BY date_format(d_time_stamp, '%d-%m-%Y'), email_type;"
        else:
            discarded_responses_query = "SELECT date_format(d_time_stamp, '%d-%m-%Y') as d_time_stamp, email_type, count(*) FROM discarded_responses GROUP BY date_format(d_time_stamp, '%d-%m-%Y'), email_type;"
        cursor = self.con.cursor()
        cursor.execute(discarded_responses_query)
        discarded_result = [dict(i_time_stamp=row[0], email_type=row[1], count=row[2]) for row in cursor.fetchall()]
        cursor.close()    

        return JsonResponse(discarded_result, safe=False)

    @staticmethod
    def query_filters(owner_group, product, severity, start_date, end_date):
        index = 0
        where = ""
        if owner_group != "All":
            where += "tickets.owner_group = '"+owner_group+"'"
            index += 1
        if product != "All":
            if index == 0:
                where += "tickets.product = '"+product+"'"
            else:
                where += " and tickets.product = '"+product+"'"
            index += 1
        if severity != "All":
            if index == 0:
                where += "tickets.severity = '"+severity+"'"
            else:
                where += " and tickets.severity = '"+severity+"'"
            index += 1
        if start_date != "" and end_date != "":
            if index == 0:
                where += "tickets.ticket_booking_time > '"+start_date+"' and tickets.ticket_booking_time < '"+end_date+"'"
            else:
                where += " and tickets.ticket_booking_time > '"+start_date+"' and tickets.ticket_booking_time < '"+end_date+"'"
            index += 1        

        return where

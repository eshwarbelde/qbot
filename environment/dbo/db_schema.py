class DbSchema:
    tickets_schema = {
        'ticket_number': {'type': 'string', 'nullable': True},
        'ticket_booking_time': {'type': 'datetime', 'nullable': True},
        'agent_name': {'type': 'string', 'nullable': True},
        'owner_group': {'type': 'string', 'nullable': True},
        'product': {'type': 'string', 'nullable': True},
        'ticket_final_status': {'type': 'string', 'nullable': True},
        'severity': {'type': 'string', 'nullable': True},
        'customer_name': {'type': 'string', 'nullable': True},
        'service_id': {'type': 'string', 'nullable': True},
        'service_segment': {'type': 'string', 'nullable': True},
        'ticket_close_time': {'type': 'datetime', 'nullable': True},
        'ticket_overall_score': {'type': 'string', 'nullable': True},
        'ticket_absolute_score':{'type': 'integer', 'nullable': True},
        'no_problem_found':{'type': 'integer', 'nullable': True},
        'pwc_count':{'type': 'integer', 'nullable': True},
        'pwc_time':{'type': 'integer', 'nullable': True}
    }

    ticket_history_schema = {
        'ticket_number': {'type': 'string', 'nullable': True},
        'ticket_state': {'type': 'string', 'nullable': True},
        'state_start_time': {'type': 'datetime', 'nullable': True},
        'owner_group': {'type': 'string', 'nullable': True}
    }

    email_content_schema = {
        'ticket_number': {'type': 'string', 'nullable': True},
        'email_number': {'type': 'integer', 'nullable': True},
        'response_text': {'type': 'string', 'nullable': True},
        'email_type': {'type': 'string', 'nullable': True}
    }

    initial_emails_schema = {
        'ticket_number': {'type': 'string', 'nullable': True},
        'agent_name': {'type': 'string', 'nullable': True},
        'i_time_stamp': {'type': 'datetime', 'nullable': True},
        'i_slatime': {'type': 'datetime', 'nullable': True},
        'i_link_stat_chk': {'type': 'integer', 'nullable': True},
        'i_link_stat_sim': {'type': 'integer', 'nullable': True},
        'i_logs': {'type': 'integer', 'nullable': True},
        'i_cust_dep': {'type': 'integer', 'nullable': True},
        'i_poa': {'type': 'integer', 'nullable': True},
        'i_svc_rstr': {'type': 'integer', 'nullable': True},
        'i_etr': {'type': 'integer', 'nullable': True},
        'i_etr_time': {'type': 'string', 'nullable': True},
        'i_resp_time': {'type': 'integer', 'nullable': True},
        'i_time_update': {'type': 'string', 'nullable': True},
        'i_time_compliance': {'type': 'integer', 'nullable': True},
        'i_sla_time': {'type': 'string', 'nullable': True},
        'resp_rating': {'type': 'integer', 'nullable': True},
        'email_number': {'type': 'integer', 'nullable': True},
        'email_type': {'type': 'string', 'nullable': True}
    }

    discarded_emails_schema = {
        'ticket_number': {'type': 'string', 'nullable': True},
        'agent_name': {'type': 'string', 'nullable': True},
        'email_number': {'type': 'integer', 'nullable': True},
        'd_time_stamp': {'type': 'datetime', 'nullable': True},
        'd_link_stat_chk': {'type': 'integer', 'nullable': True},
        'd_link_stat_sim': {'type': 'integer', 'nullable': True},
        'd_logs': {'type': 'integer', 'nullable': True},
        'd_cust_dep': {'type': 'integer', 'nullable': True},
        'd_poa': {'type': 'integer', 'nullable': True},
        'd_svc_rstr': {'type': 'integer', 'nullable': True},
        'd_etr': {'type': 'integer', 'nullable': True},
        'd_etr_time': {'type': 'string', 'nullable': True},
        'd_resp_time': {'type': 'integer', 'nullable': True},
        'd_time_update': {'type': 'string', 'nullable': True},
        'd_time_compliance': {'type': 'integer', 'nullable': True},
        'd_sla_time': {'type': 'string', 'nullable': True},
        'resp_rating': {'type': 'integer', 'nullable': True},
        'email_type': {'type': 'string', 'nullable': True}
    }

    subsequent_emails_schema = {
        'ticket_number': {'type': 'string'},
        'subsequent_email_number': {'type': 'integer'},
        'agent_name': {'type': 'string', 'nullable': True},
        'si_timestamp': {'type': 'datetime'},
        'si_prog': {'type': 'integer'},
        'si_svc_rstr': {'type': 'integer'},
        'si_no_prob': {'type': 'integer'},
        'si_etr': {'type': 'integer'},
        'si_etr_time': {'type': 'string'},
        'si_logs': {'type': 'integer'},
        'si_resp_time': {'type': 'integer'},
        'si_time_update': {'type': 'string'},
        'si_cd': {'type': 'integer'},
        'si_time_compliance': {'type': 'integer','nullable': True},
        'si_case_state': {'type': 'integer'},
        'si_case_state_time': {'type': 'string'},
        'si_case_state_group': {'type': 'string'},
        'email_type': {'type': 'string'},
        'email_number': {'type': 'integer'},
        'si_sla_time': {'type': 'string','nullable': True},
        'ticket_id': {'type': 'string'},
        'resp_rating': {'type': 'integer'}
    }
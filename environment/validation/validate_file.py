import pandas as pd
import sys

class ValidateFile:
    def __init__(self, data_file_path=None,timing_file_path=None):
        self.data_file_path = data_file_path
        self.timing_file_path = timing_file_path
        self.data_file_columns=['u_number', 'u_ticket_created_time', 'sys_created_on', 'sys_created_by',
                               'u_current_assignment_group', 'u_product', 'u_current_state',
                               'u_impact', 'u_customer_name', 'u_service_identifier', 'Update Type',
                               'A End Provider', 'Ticket status', 'Customer Update', 'Update Type.1',
                               'RFO Responsbile', 'RFO Cause', 'RFO Specification', 'Fault Segment','Closure notes','rfo_closure']
        self.timing_file_columns=['Number','Current Assignment Group', 'State Of Ticket(New)',
                               'State Of Ticket(OLD)','State Start Time','State End  time',]
        
    def run(self):
        try:
            data=pd.read_excel(self.data_file_path)
            timing=pd.read_excel(self.timing_file_path)
            flag_data=True
            flag_timing=True
            unmatched_tickets=[]
            unmatched_cols=[]
            print(len(data.u_number.unique()),'data file')
            for col in self.data_file_columns:
                if col not in data.columns:
                    #raise KeyError('{} column is not exist in the data'.format(col))
                    flag_data = False
                    unmatched_cols.append(col)
                    print(col,'column not in data file')
                    
            for col in self.timing_file_columns: 
                if col not in timing.columns:
                    flag_timing = False
                    unmatched_cols.append(col)
                    print(col,'column not in timing file')
            
            if len(unmatched_cols)==0:
                if len(data.u_number.unique())==len(timing.Number.unique()):
                    for i in data.u_number.unique():
                        if i not in timing.Number.unique():
                            unmatched_tickets.append(i)
                            print(unmatched_tickets,'not in timing file')
                            flag_data = False
                    for i in timing.Number.unique():
                        if i not in data.u_number.unique():
                            unmatched_tickets.append(i)
                            print(unmatched_tickets,'not in data file')
                            flag_timing = False
                
                else:
                    flag_data = False
                    flag_timing = False
                    print('checking for unmatched tickets')
                    for ticket_number in data['u_number'].unique():
                        if ticket_number not in timing['Number'].unique():
                            print(ticket_number,'data')
                    for ticket_number in timing['Number'].unique():
                        if ticket_number not in data['u_number'].unique():
                            print(ticket_number,'timing')
            else:
                flag_data = False
                flag_timing = False
            return flag_data,flag_timing
        except Exception as e:
            print('Error')
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
        
        

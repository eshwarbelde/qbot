"""environment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from environment.cache.qbot_cache import CacheRefresher
from environment.classification.classification_views import ClassificationView
from environment.classification import upload_files
from environment.dashboard import views
from environment.dashboard.admin import admin_views
from environment.dashboard.agent import agent_views
from environment.dashboard.customer import customer_views
from environment.user_management import manage_user
from environment.dashboard.RFO import rfo_views

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', manage_user.login),

    # loading the templates
    path('landing/', views.get_landing_template),
    path('quality_dashboard/', views.get_quality_dashboard_template),
    path('action_module_initial/', views.get_action_module_initial_template),
    path('action_module_subsequent/', views.get_action_module_subsequent_template),
    path('action_module_rfo/', rfo_views.get_action_module_rfo_template),
    path('data_management/', views.get_data_management_template),
    path('calendar/', views.get_calendar_template),
    path('process_history/', views.get_process_history_template),
    path('customer_journey/', views.get_customer_journey_template),
    path('agent_training/', views.get_agent_training_template),

    # User module urls
    path('login/', manage_user.login),
    path('logout/', csrf_exempt(manage_user.logout)),
    path('users/', manage_user.get_users_template),
    path('addUser', csrf_exempt(manage_user.add_user)),
    path('getUserRecord/', manage_user.get_user_record),
    path('editUser', csrf_exempt(manage_user.edit_user)),
    path('removeUser', csrf_exempt(manage_user.remove_user)),
    path('getUsersList', manage_user.get_users_list),
    path('forgotPassword', manage_user.forgot_password),
    path('resetPassword', csrf_exempt(manage_user.reset_password)),
    #agent_training
    path('get_agent_analysis_response/',agent_views.get_agent_analysis_response),
    path('get_sankeydata_customer/',agent_views.get_sankeydata_customer),
    path('get_rfos/', agent_views.get_rfos),
    path('get_sankeydata_casestate/',agent_views.get_sankeydata_casestate),

    # agent_module_urls
    path('agent_module/', agent_views.agent_module),
    path('getAgentNames/', agent_views.get_agent_names),
    path('getAllResponsesByAgentName/', agent_views.get_all_responses_by_agent_name),
    path('get_all_responses_by_agent_name_and_rating/', agent_views.get_all_responses_by_agent_name_and_rating),
    path('get_all_responses_by_agent_name_and_time_compliance/',
         agent_views.get_all_responses_by_agent_name_and_time_compliance),
    path('getAllTicketsByAgentNameForTimeLine/', agent_views.get_all_responses_by_agent_name_for_timeline),
    path('getAllAgentsPerformanceByMean/', agent_views.get_all_agents_performance),
    path('getContentComplianceResultByAgent/', agent_views.get_content_compliance_results_by_agent),
    path('getTimeComplianceResultByAgent/', agent_views.get_time_compliance_results_by_agent),
    path('getMissingParametersResultsByAgent/', agent_views.get_missing_parameters_results_by_agent),
    path('getTimeComplianceDifferenceResultsWithSLAForAgent/',
         agent_views.get_time_compliance_difference_results_with_sla_for_agent),
    path('getAllAgentsPerformance', agent_views.get_all_distinct_agents_performance),
    # Sub-Sequent
    path('getSubsequentContentComplianceResultByAgent/', agent_views.get_subsequent_content_compliance_results_by_agent),
    path('getSubsequentTimeComplianceResultByAgent/', agent_views.get_subsequent_time_compliance_results_by_agent),
    path('getSubsequentMissingParametersResultsByAgent/', agent_views.get_subsequent_missing_parameters_results_by_agent),
    path('getSubsequentTimeComplianceDifferenceResultsWithSLAForAgent/',
         agent_views.get_subsequent_time_compliance_difference_results_with_sla_for_agent),
     # RFO
    path('getRFOContentComplianceResultByAgent/', agent_views.get_rfo_content_compliance_results_by_agent),
    path('getRFOTimeComplianceResultByAgent/', agent_views.get_rfo_time_compliance_results_by_agent),
    path('getRFOMissingParametersResultsByAgent/', agent_views.get_rfo_missing_parameters_results_by_agent),
    path('getRFOTimeComplianceDifferenceResultsWithSLAForAgent/',
         agent_views.get_rfo_time_compliance_difference_results_with_sla_for_agent),
    path('getRFOMisClassificationSpecificationByAgent/', agent_views.get_rfo_mis_classification_specification_by_agent),
    path('getRFOMisClassificationFaultSegmentByAgent/', agent_views.get_rfo_mis_classification_fault_segment_by_agent),

    path('getSubsequentResponsesCombinationsByAgent/', agent_views.get_subsequent_responses_combinations_by_agent),
    path('getDiscardedResponsesCombinationsByAgent/', agent_views.get_discarded_responses_combinations_by_agent),

    # customer module urls
    path('customer_dashboard/', customer_views.customer_dashboard),
    path('getAllTicketsGroupByCustomerNameAndRating/',
         customer_views.get_all_tickets_group_by_customer_name_and_rating),
    path('getAllTicketsGroupByCustomerNameAndRatingForTimeline/',
         customer_views.get_all_tickets_group_by_customer_name_and_rating_for_timeline),
    path('getAllTicketsGroupByCustomerName/', customer_views.get_all_tickets_group_by_customer_name),
    path('getAllTicketsByCustomer/', customer_views.get_all_tickets_by_customer),
    path('getCustomerTicketsByCategoryAndRating/', customer_views.get_customer_tickets_by_category_and_rating),
    path('getCustomerResponsesByCategoryAndRating/', customer_views.get_customer_responses_by_category_and_rating),

    # admin module urls
    path('getMasterData/', admin_views.get_master_data),
    path('getMasterDataFromDB/', admin_views.get_master_data),
    path('addBU', csrf_exempt(admin_views.add_bu)),
    path('addDesk', csrf_exempt(admin_views.add_desk)),
    path('addOwnerGroup', csrf_exempt(admin_views.add_owner_group)),
    path('getBUsList/', admin_views.get_bus_list),
    path('getDesksList/', admin_views.get_desks_list),
    path('getOwnerGroupsList/', admin_views.get_owner_groups_list),
    path('getDataForBUChart/', admin_views.get_data_for_bu_chart),
    path('removeItem', csrf_exempt(admin_views.remove_item)),
    path('getUserBUs/', csrf_exempt(admin_views.get_user_bus)),

    # upload files urls
    path('file_upload/', ClassificationView.file_upload),
    path('upload/', csrf_exempt(upload_files.upload)),
    path('downloadExcel/', csrf_exempt(views.downloadExcel)),

    path('getProcessStatus/', views.get_process_status),
    path('getLatestProcessStatus/', views.get_latest_process_status),
    path('loadOwnerGroups/', views.load_owner_groups),
    path('addMasterData/', csrf_exempt(views.addMasterData)),

    path('getAgentOwnerGroups/', views.get_agent_owner_groups),
    path('getAllTickets/', views.get_all_tickets),
    path('getAllTicketsGroupByBU/', views.get_tickets_group_by_bu),
    path('getAllTicketsByOwnerGroup/', views.get_tickets_by_owner_group),
    path('getOwnerGroupTicketsByCategoryAndRating/', views.get_owner_group_tickets_by_category_and_rating),
    path('getOwnerGroupResponsesByCategoryAndRating/', views.get_owner_group_responses_by_category_and_rating),
    
    path('getTicketsGroupByOwnerGroup/', views.get_tickets_group_by_owner_group),
    path('getContentComplianceResults/', views.get_content_compliance_results),
    path('getTimeComplianceResults/', views.get_time_compliance_results),
    path('getMissingParametersResults/', views.get_missing_parameters_results),
    path('getTimeComplianceDifferenceResultsWithSLA/', views.getTimeComplianceDifferenceResultsWithSLA),
    path('getInitialResponsesByRating/', views.get_initial_responses_by_rating),
    path('getInitialResponsesByTimeCompliance/', views.get_initial_responses_by_time_compliance),
    path('getOwnerGroupResponsesByRating/', views.get_owner_group_responses_by_rating),

    path('getAllInitialEmailsThatMissedLinkStatus/', views.getAllInitialEmailsThatMissedLinkStatus),
    path('getAllInitialEmailsThatMissedETR/', views.getAllInitialEmailsThatMissedETR),
    path('getAllInitialEmailsThatMissedResponseTime/', views.getAllInitialEmailsThatMissedResponseTime),
    path('getAllInitialEmailsThatMissedPOA/', views.getAllInitialEmailsThatMissedPOA),
    path('getAllServiceRestoreEmailsAndNoLogs/', views.getAllServiceRestoreEmailsAndNoLogs),
    path('getAllEmailsWithNoServiceRestoreAndNoPOA/', views.getAllEmailsWithNoServiceRestoreAndNoPOA),
    path('getAllInitialEmailsWithOnlyLogs/', views.getAllInitialEmailsWithOnlyLogs),
    path('getAllInitialEmailsThatMissedSLATime/', views.getAllInitialEmailsThatMissedSLATime),

    path('getAllInitialEmailsWithSLAVoilationGroupByQualityComplaint/',
         views.getAllInitialEmailsWithSLAVoilationGroupByQualityComplaint),
    # EmailContent data fetching 
    path('getEmailContent/', views.get_email_content),

    # Initial responses Data fetching urls
    path('getAllIntialResponsesGroupByRating/', views.get_all_intial_responses_group_by_rating),

    # Discarded Email Data fetching urls
    path('getDiscardedEmails/', views.getDiscardedEmails),
    path('discardedEmailsGroupByDiscardNumber/', views.discardedEmailsGroupByDiscardNumber),

    # subsequent Email Data fetching urls
    path('getSubsequentContentComplianceResults/', views.get_subsequent_content_compliance_results),
    path('getSubsequentTimeComplianceResults/', views.get_subsequent_time_compliance_results),
    path('getSubsequentMissingParametersResults', views.get_subsequent_missing_parameters_results),
    path('getSubsequentTimeComplianceDifferenceResultsWithSLA',
         views.get_subsequent_time_compliance_difference_results_with_SLA),
    path('getSubsequentResponsesByRating/', views.get_subsequent_responses_by_rating),
    path('getSubsequentResponsesByTimeCompliance/', views.get_subsequent_responses_by_time_compliance),

    path('getAllSubsequentResponsesGroupByRating/', views.getAllSubsequentResponsesGroupByRating),
    path('getAllSubsequentEmailsWithNoProgress/', views.getAllSubsequentEmailsWithNoProgress),
    path('getAllSubsequentEmailsWithNoProgressGroupByEmailType/',
         views.getAllSubsequentEmailsWithNoProgressGroupByEmailType),
    path('getAllSubsequentEmailsWithNoServiceRestoreAndNoETR/',
         views.getAllSubsequentEmailsWithNoServiceRestoreAndNoETR),
    path('getAllTicketsWithNoProblemFound/', views.getAllTicketsWithNoProblemFound),
    path('getAllSubsequentEmailsWithServiceRestoreAndNoLogs/',
         views.getAllSubsequentEmailsWithServiceRestoreAndNoLogs),
    path('getAllSubsequentEmailsWithOnlyLogs/', views.getAllSubsequentEmailsWithOnlyLogs),

    path('getAllSubsequentEmailsWithSLAVoilationGroupByEmailType/',
         views.getAllSubsequentEmailsWithSLAVoilationGroupByEmailType),
    path('getAllSubsequentEmailsWithSLAVoilationGroupByQualityComplaint/',
         views.get_all_subsequent_emails_with_sla_violation_group_by_quality_complaint),

    # Process History data fetching urls
    path('getProcessHistoryList/', views.get_process_history_list),

    # Landing page metrics urls
    path('getTicketMetrics/', views.get_ticket_metrics),
    path('getProductAndCustomerMetrics/', views.get_product_and_customer_metrics),
    path('overallTicketRatingMetrics/', views.overall_ticket_rating_metrics),
    path('agentMetrics/', views.agent_metrics),

    # RFO  Data fetching urls
    path('getRFOComplianceResults/', rfo_views.get_rfo_compliance_results),
    path('getRFOSpecificationAccuracyResults/', rfo_views.get_rfo_specification_accuracy_results),
    path('getRFOFaultSegmentAccuracyResults/', rfo_views.get_rfo_fault_segment_accuracy_results),
    path('getRFOTimeComplianceResults/', rfo_views.get_rfo_time_compliance_results),
    path('getRFOReasonNames/', rfo_views.get_rfo_reason_names),
    path('getRFOComplianceByReasonResults/', rfo_views.get_rfo_compliance_by_reason_results),
    path('getRFOTimeComplianceByReasonResults/', rfo_views.get_rfo_time_compliance_by_reason_results),
    path('getMisClassificationSpecificationByReasonResults/', rfo_views.get_mis_classification_specification_by_reason_results),
    path('getMisClassificationFaultSegmentByReasonResults/', rfo_views.get_mis_classification_fault_segment_by_reason_results),
    path('getRFOTimeSentByReasonResults/', rfo_views.get_rfo_time_sent_by_reason_results),
    path('getRFOMissingParametersByReasonResults/', rfo_views.get_rfo_missing_parameters_by_reason_results)
]


print("calling cache on startup")
cache = CacheRefresher()
cache.refresh_cache()
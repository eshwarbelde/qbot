from smtplib import SMTPException

from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import render_to_string
from rest_framework import serializers
import math
import random


def sending_email_for_new_user(to_email, username, password, role):
    print("sending_email_to", to_email)
    c = Context({"username": username, "password": password, "role": role}).flatten()
    html_content = render_to_string('NEW_USER_HTML_TEMPLATE.html', c)
    subject, from_email = 'Welcome', 'K7_support@kynea.io'
    try:
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to_email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
        print("Email sent successfully")
        return True
    except SMTPException as e:
        print('There was an error sending an email: ', e)
        error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
        raise serializers.ValidationError(error)


def sending_email_for_forgot_password(to_email):
    print("Sending email to: ", to_email)
    otp = generate_otp()
    c = Context({"password": otp}).flatten()
    html_content = render_to_string('RESET_PASSWORD_HTML_TEMPLATE.html', c)
    #
    subject, from_email = 'Welcome', 'k7_support@kynea.io'

    try:
        msg = EmailMultiAlternatives(subject, html_content, from_email, [to_email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
        print("Email sent successfully")
        return True, otp
    except SMTPException as e:
        print('There was an error sending an email: ', e)
        # error = {'message': ",".join(e.args) if len(e.args) > 0 else 'Unknown Error'}
        # raise serializers.ValidationError(error)
        return False, ''


# function to generate OTP
def generate_otp():

    # Declare a digits variable
    # which stores all digits
    digits = "0123456789"
    otp = ""

    # length of password can be chaged
    # by changing value in range
    for i in range(6):
        otp += digits[math.floor(random.random() * 10)]
    return otp

import MySQLdb
from bson.json_util import dumps
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template import loader
from environment.user_management import email_sender


conn = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")


def insert(query):
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()  # commit the transaction
        last_id = cursor.lastrowid
        cursor.close()
        return last_id
    except:
        conn.rollback()


def update(query):
    print('update query: ', query)
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        count = cursor.rowcount
        cursor.close()
        return count
    except Exception as e:
        print(e)
        conn.rollback()

def delete(query):
    try:
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        count = cursor.rowcount
        cursor.close()
        return count
    except Exception as e:
        print(e)
        conn.rollback()

def get_users_template(request):
    if 'user_name' in request.session and request.session['user_name'] != '' and \
            request.session['user_role'] == 'ADMIN':
        template = loader.get_template('users.html')
        return HttpResponse(template.render())
    else:
        response = redirect('/login/')
        return response


def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        query = "SELECT id, username, email,role from users WHERE username='"+username+"' and password='"+password+"';"
        print(query)
        connection = MySQLdb.connect("localhost", "root", "mysql", "qualitybot")
        cursor = connection.cursor()
        cursor.execute(query)
        users = cursor.fetchall()
        cursor.close()
        connection.close()
        if len(users) > 0:
            user_record = {}
            for user in users:
                user_record['id'] = user[0]
                user_record['username'] = user[1]
                user_record['email'] = user[2]
                user_record['role'] = user[3]

            query = "SELECT bu.name FROM bu JOIN user_bu on user_bu.bu_id=bu.id WHERE user_bu.user_id="\
                    + str(user_record['id'])
            cursor = conn.cursor()
            cursor.execute(query)
            user_record['bu_list'] = [row[0] for row in cursor.fetchall()]
            cursor.close()
            request.session['user_name'] = username
            request.session['user_role'] = user_record['role']
            request.session['bu_list'] = user_record['bu_list']
            return HttpResponseRedirect('/landing/')
        else:
            return render(request, 'login.html')

    elif request.method == 'GET':
        return render(request, 'login.html')


def logout(request):
    if request.method == 'POST':
        username = request.session['user_name']
        print(username)
        if username != "":
            request.session['user_name'] = ''
            request.session['user_role'] = ''
            request.session['bu_list'] = ''
            print(username, ' logged out successfully')
        return render(request, 'login.html')

    elif request.method == 'GET':
        return render(request, 'login.html')


def add_user(request):
    username = request.POST.get('username', None)
    password = username + '@123'
    email = request.POST.get('email', None)
    role = request.POST.get('role', None)
    bu_list = request.POST.get('buList', None)
    bu_array = bu_list.split(';')
    print("Requesting for adding the user:", username)
    query = "INSERT INTO users(username, password, email, role) values('"+username+"','"+password+"','"+email+"','"+role+"') "
    user_id = insert(query)
    print(user_id)

    if len(bu_array) == 1:
        query = "SELECT id FROM bu WHERE name='" + bu_array[0] + "'"
    if len(bu_array) > 1:
        bus = "','".join(bu_array)
        query = "SELECT id FROM bu WHERE name IN ('"+bus+"')"
    cursor = conn.cursor()
    cursor.execute(query)
    bu_ids = cursor.fetchall()
    cursor.close()
    for row in bu_ids:
        query = "INSERT INTO user_bu(user_id, bu_id) values("+str(user_id)+","+str(row[0])+")"
        insert(query)
    result = email_sender.sending_email_for_new_user(
        to_email=email,
        username=username,
        password=password,
        role=role
    )
    if result:
        data = {
            'status': 200,
            'message': 'User added and email sent to user successfully'
        }
    else:
        data = {
            'status': 200,
            'message': 'User added successfully, but email sending failed'
        }
    return JsonResponse(data, safe=False)


def get_user_record(request):
    username = request.session['user_name']
    query = "SELECT id, username, email, role FROM users WHERE username='"+username+"'"
    print(query)
    cursor = conn.cursor()
    cursor.execute(query)
    records = cursor.fetchall()
    cursor.close()
    record = {}
    for row in records:
        record = dict(id=row[0], username=row[1], email=row[2], role=row[3])
    return JsonResponse(dumps(record), safe=False)


def get_users_list(request):
    query = "SELECT id, username,email,role from users"
    cursor = conn.cursor()
    cursor.execute(query)
    records = cursor.fetchall()
    cursor.close()
    data = []
    for user in records:
        query = "SELECT bu.name from bu join user_bu ON user_bu.bu_id = bu.id WHERE user_bu.user_id="+str(user[0])
        print(query)
        cursor = conn.cursor()
        cursor.execute(query)
        bu_list = [row[0] for row in cursor.fetchall()]
        cursor.close()
        user = dict(id=user[0], username=user[1], email=user[2], role=user[3])
        data.append({
            "user_obj": user,
            "buList": bu_list
        })
    return JsonResponse(data, safe=False)


def edit_user(request):
    username = request.POST.get('username', None)
    print("Requesting for editing the user:", username)
    row_id = request.POST.get('rowId', None)
    role = request.POST.get('role', None)
    bu_list = request.POST.get('buList', None)
    bu_array = bu_list.split(";")

    query = "UPDATE users SET role='"+role+"' WHERE id="+str(row_id)
    updated_rows = update(query)
    query = "DELETE FROM user_bu WHERE user_id="+str(row_id)
    deleted_count = delete(query)

    bus = "','".join(bu_array);
    query = "SELECT id FROM bu WHERE name IN ('"+bus+"')"
    cursor = conn.cursor()
    cursor.execute(query)
    bu_ids = cursor.fetchall()
    cursor.close()
    for row in bu_ids:
        query = "INSERT INTO user_bu(user_id,bu_id) values("+str(row_id)+","+str(row[0])+")"
        insert(query)
    data = {
        'status': 200,
        'message': 'User updated successfully'
    }
    return JsonResponse(dumps(data), safe=False)


def remove_user(request):
    row_id = request.POST.get('rowId', None)
    query = "DELETE FROM user_bu WHERE user_id="+str(row_id)
    delete(query)
    query = "DELETE FROM users WHERE id="+str(row_id)
    count = delete(query)
    if count > 0:
        data = {
            'status': 200,
            'message': 'User removed successfully'
        }
    else:
        data = {
            'status': 500,
            'message': 'User delete failed'
        }
    return JsonResponse(dumps(data), safe=False)


def forgot_password(request):
    email = request.GET.get('email', None)
    print(email)
    result, otp = email_sender.sending_email_for_forgot_password(to_email=email)
    if result:
        record = {
            'message': 'Email with OTP sent to '+ email,
            'otp': otp
        }
    else:
        record = {
            'message': 'Email with OTP sending failed',
            'otp': ''
        }
    return JsonResponse(record, safe=False)

def reset_password(request):
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    query = "UPDATE users SET password='"+password+"' WHERE username='"+username+"';"
    row_count = update(query)
    print(row_count)
    if row_count > 0:
        record = {
            'status': 200, 
            'message': 'Password changed successfully'
        }
    else:
        record = {
            'status': 500, 
            'message': 'Password change failed, Please retry after sometime.'
        }
    return JsonResponse(record, safe=False)

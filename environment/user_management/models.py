from django.db import models


class User(models.Model):
    username = models.CharField(max_length=50, default='')
    password = models.CharField(max_length=50, default='')
    email = models.CharField(max_length=50, default='')
    role = models.CharField(max_length=20, default='')

    def __str__(self):
        return self.username

    class Meta:
        db_table = "users"


class BU(models.Model):
    name = models.CharField(max_length=50, default='')

    def __str__(self):
        return self.name

    class Meta:
        db_table = "bu"


class Desk(models.Model):
    name = models.CharField(max_length=50, default='')
    bu = models.ForeignKey(BU, on_delete=models.CASCADE, default='')

    def __str__(self):
        return self.name

    class Meta:
        db_table = "desk"


class UserBuList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    bu = models.ForeignKey(BU, on_delete=models.CASCADE)

    class Meta:
        db_table = "user_bu"


class Ticket(models.Model):
    ticket_number = models.CharField(max_length=20, default='')
    ticket_booking_time = models.DateTimeField(blank=True, null=True)
    agent_name = models.CharField(max_length=50, default='')
    owner_group = models.CharField(max_length=50, default='')
    product = models.CharField(max_length=50, default='')
    ticket_final_status = models.CharField(max_length=50, default='')
    severity = models.CharField(max_length=50, default='')
    customer_name = models.CharField(max_length=50, default='')
    service_id = models.CharField(max_length=50, default='')
    service_segment = models.CharField(max_length=50, default='')
    ticket_close_time = models.DateTimeField(blank=True, null=True)
    ticket_overall_score = models.CharField(max_length=50, default='')
    ticket_absolute_score = models.IntegerField(blank=True, null=True)
    no_problem_found = models.IntegerField(blank=True, null=True)
    pwc_count = models.IntegerField(blank=True, null=True)
    pwc_time = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "tickets"


class InitialResponse(models.Model):
    ticket_number = models.CharField(max_length=20, default='')
    agent_name = models.CharField(max_length=50, default='')
    i_time_stamp = models.DateTimeField(blank=True, null=True)
    i_link_stat_chk = models.IntegerField(blank=True, null=True)
    i_link_stat_sim = models.IntegerField(blank=True, null=True)
    i_logs = models.IntegerField(blank=True, null=True)
    i_cust_dep = models.IntegerField(blank=True, null=True)
    i_poa = models.IntegerField(blank=True, null=True)
    i_svc_rstr = models.IntegerField(blank=True, null=True)
    i_etr = models.IntegerField(blank=True, null=True)
    i_etr_time = models.IntegerField(blank=True, null=True)
    i_resp_time = models.IntegerField(blank=True, null=True)
    i_time_update = models.CharField(max_length=20, default='')
    i_time_compliance = models.IntegerField(blank=True, null=True)
    resp_rating = models.IntegerField(blank=True, null=True)
    email_number = models.IntegerField(blank=True, null=True)
    email_type = models.CharField(max_length=5, default='')

    class Meta:
        db_table = "initial_responses"


class SubsequentResponse(models.Model):
    ticket_number = models.CharField(max_length=20, default='')
    agent_name = models.CharField(max_length=50, default='')
    si_timestamp = models.DateTimeField(blank=True, null=True)
    si_prog = models.IntegerField(blank=True, null=True)
    si_svc_rstr = models.IntegerField(blank=True, null=True)
    si_no_prob = models.IntegerField(blank=True, null=True)
    si_etr = models.IntegerField(blank=True, null=True)
    si_etr_time = models.IntegerField(blank=True, null=True)
    si_logs = models.IntegerField(blank=True, null=True)
    si_resp_time = models.IntegerField(blank=True, null=True)
    si_time_update = models.CharField(max_length=20, default='')
    si_cd = models.IntegerField(blank=True, null=True)
    si_time_compliance = models.IntegerField(blank=True, null=True)
    si_sla_time = models.CharField(max_length=50, default='')
    resp_rating = models.IntegerField(blank=True, null=True)
    email_type = models.CharField(max_length=5, default='')
    email_number = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "subsequent_responses"


class DiscardedResponses(models.Model):
    ticket_number = models.CharField(max_length=20, default='')
    agent_name = models.CharField(max_length=50, default='')
    email_number = models.IntegerField(blank=True, null=True)
    d_time_stamp = models.DateTimeField(blank=True, null=True)
    d_link_stat_chk = models.IntegerField(blank=True, null=True)
    d_link_stat_sim = models.IntegerField(blank=True, null=True)
    d_logs = models.IntegerField(blank=True, null=True)
    d_cust_dep = models.IntegerField(blank=True, null=True)
    d_poa = models.IntegerField(blank=True, null=True)
    d_svc_rstr = models.IntegerField(blank=True, null=True)
    d_etr = models.IntegerField(blank=True, null=True)
    d_etr_time = models.CharField(max_length=50, default='')
    d_resp_time = models.IntegerField(blank=True, null=True)
    d_time_update = models.CharField(max_length=20, default='')
    d_time_compliance = models.IntegerField(blank=True, null=True)
    resp_rating = models.IntegerField(blank=True, null=True)
    email_type = models.CharField(max_length=5, default='')

    class Meta:
        db_table = "discarded_responses"


class EmailContent(models.Model):
    ticket_number = models.CharField(max_length=20, default='')
    response_text = models.TextField()
    email_type = models.CharField(max_length=20, default='')
    email_number = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "email_content"


class TicketHistory(models.Model):
    ticket_number = models.CharField(max_length=20, default='')
    ticket_state = models.CharField(max_length=20, default='')
    state_start_time = models.DateTimeField(blank=True, null=True)
    owner_group = models.CharField(max_length=50, default='')

    class Meta:
        db_table = "ticket_history"


class ProcessStatus(models.Model):
    username = models.CharField(max_length=30, default='')
    mode = models.CharField(max_length=10, default='')
    process_start_time = models.DateTimeField(blank=True, null=True)
    data_file_name = models.CharField(max_length=100, default='')
    timing_file_name = models.CharField(max_length=100, default='')
    file_upload = models.CharField(max_length=30, default='')
    filtering = models.CharField(max_length=30, default='')
    prediction = models.CharField(max_length=30, default='')
    scoring = models.CharField(max_length=30, default='')
    persistance = models.CharField(max_length=30, default='')
    total_tickets = models.IntegerField(blank=True, null=True)
    total_responses = models.IntegerField(blank=True, null=True)
    total_null = models.IntegerField(blank=True, null=True)
    no_of_tickets_duplicate_removed = models.IntegerField(blank=True, null=True)
    no_of_responses_duplicate_removed = models.IntegerField(blank=True, null=True)
    no_of_tickets_products_removed = models.IntegerField(blank=True, null=True)
    no_of_responses_products_removed = models.IntegerField(blank=True, null=True)
    no_of_tickets_resolvergrp_removed = models.IntegerField(blank=True, null=True)
    no_of_responses_resolvergrp_removed = models.IntegerField(blank=True, null=True)
    no_of_tickets_1resolvergrpNA_removed = models.IntegerField(blank=True, null=True)
    no_of_responses_1resolvergrpNA_removed = models.IntegerField(blank=True, null=True)
    no_of_tickets_pwc_susp_removed = models.IntegerField(blank=True, null=True)
    no_of_responses_pwc_susp_removed = models.IntegerField(blank=True, null=True)
    no_of_tickets_impact_removed = models.IntegerField(blank=True, null=True)
    no_of_responses_impact_removed = models.IntegerField(blank=True, null=True)
    no_of_tickets_internal_removed = models.IntegerField(blank=True, null=True)
    no_of_responses_internal_removed = models.IntegerField(blank=True, null=True)
    no_tickets_into_machine = models.IntegerField(blank=True, null=True)
    no_emails_into_machine = models.IntegerField(blank=True, null=True)
    no_emails_discarded = models.IntegerField(blank=True, null=True)
    no_emails_initial = models.IntegerField(blank=True, null=True)
    no_emails_subsequent = models.IntegerField(blank=True, null=True)
    process_end_time = models.DateTimeField(blank=True, null=True)
    final_status = models.CharField(max_length=50, default='')
    current_state = models.CharField(max_length=50, default='')
    filename_to_download = models.CharField(max_length=50, default='')
    unique_dates = models.TextField(default='')
    end_process = models.CharField(max_length=50, default='')
    first_customer_no_prblm = models.IntegerField(blank=True, null=True)
    second_customer_no_prblm = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "process_status"

import json
import pickle
from environment.classification.modules.preprocessing import Preprocessing
import warnings
warnings.filterwarnings("ignore")
import numpy as np

class scoring():
    def __init__(self):
        self.preprocessing_obj = Preprocessing()
        print('Loading models for predictions Agent Analysis')
        # self.dtm1 = pickle.load(open('./models/document_term_matrix_model_v7.pkl', 'rb'))
        # self.ecm1 = pickle.load(open('./models/email_classifier_model_v7.pkl', 'rb'))
        # self.dtm2 = pickle.load(open('./models/sub_document_term_matrix_model_v7.pkl', 'rb'))
        # self.ecm2 = pickle.load(open('./models/sub_email_classifier_model_v7.pkl', 'rb'))
        self.dtm1 = pickle.load(open('environment/classification/models/document_term_matrix_model_v7.pkl', 'rb'))
        self.ecm1 = pickle.load(open('environment/classification/models/email_classifier_model_v7.pkl', 'rb'))
        self.dtm2 = pickle.load(open('environment/classification/models/sub_document_term_matrix_model_v7.pkl', 'rb'))
        self.ecm2 = pickle.load(open('environment/classification/models/sub_email_classifier_model_v7.pkl', 'rb'))


    def model_pred(self, txt):

        pred_dict = {}

        l1_labels = list(self.dtm1.keys())
        l2_labels = list(self.dtm2.keys())

        txt = self.preprocessing_obj.clean_text(txt)
        txt = self.preprocessing_obj.remove_stop_words_and_apply_stemmer(txt)
        #print('cleaned_text',txt)
        for label in l1_labels:
            dt = self.dtm1[label].transform([txt])
            ec = int(self.ecm1[label].predict(dt)[0])
            pred_dict[label] = ec
        for label in l2_labels:
            dt = self.dtm2[label].transform([txt])
            ec = int(self.ecm2[label].predict(dt)[0])
            pred_dict[label] = ec

        return pred_dict

    def result(self, score, ind_score, provided, Not_provided):

        labels = ['i_link_stat_chk', 'i_poa', 'i_cust_dep', 'si_prog', 'i_resp_time', 'i_svc_rstr', 'i_logs']
        full_names = ['link status check', 'plan of action', 'customer dependency', 'progressive update',
                      'response time', 'service restored', 'logs']
        if Not_provided != None:
            Not_provided = [full_names[labels.index(i)] for i in Not_provided]
        if provided != None:
            provided = [full_names[labels.index(i)] for i in provided]
        result = {'Score': score, 'Individual_Score': float(ind_score),
                  'You have provided': provided,
                  'You have not provided': Not_provided}
        return result

    def predictions(self,scenario_id,response):
        scenarios = {1: 'Initiate checks to identify issues with the link',
                     2: ['i_link_stat_chk', 'i_poa', 'i_cust_dep'],
                     3: ['si_prog', 'i_resp_time'],
                     4: ['si_prog', 'i_cust_dep'],
                     5: ['si_prog', 'i_svc_rstr', 'i_logs']}
        scenario_ids = list(scenarios.keys())
        try:
            scenario_id = int(scenario_id)
            response = str(response)
            if scenario_id not in scenario_ids:
                raise ValueError
            elif type(response) != str:
                raise TypeError
            pred = self.model_pred(response)
            # print(pred)
            flag = True
            if scenario_id == scenario_ids[0]:
                if (response == scenarios[1]):
                    score = 1
                    ind_score = 5
                    provided = None
                    Not_provided = None
                else:
                    score = 0
                    ind_score = 0
                    provided = None
                    Not_provided = None
                result = {'Score': score, 'Individual_Score': ind_score, 'You have provided': provided,
                          'You have not provided': Not_provided}
                print(flag, result)
                return flag, result
            elif scenario_id == scenario_ids[1]:
                if (pred['i_link_stat_chk'] == 1) and (pred['i_poa'] == 1) and (pred['i_cust_dep'] == 1):
                    score = 1
                    ind_score = 5
                    provided = scenarios[2]
                    Not_provided = None
                else:
                    score = 0
                    provided = [i for i, j in pred.items() if (j == 1) and (i in scenarios[2])]
                    ind_score = np.ceil(len(provided) * 1.6)
                    Not_provided = [i for i, j in pred.items() if j == 0 and (i in scenarios[2])]
                result = self.result(score, ind_score, provided, Not_provided)
                print(flag, result)
                return flag, result

            elif scenario_id == scenario_ids[2]:
                if (pred['si_prog'] == 1) and (pred['i_resp_time'] == 1):
                    score = 1
                    ind_score = 5
                    provided = scenarios[3]
                    Not_provided = None
                else:
                    score = 0
                    provided = [i for i, j in pred.items() if j == 1 and (i in scenarios[3])]
                    ind_score = len(provided) * 2.5
                    Not_provided = [i for i, j in pred.items() if j == 0 and (i in scenarios[3])]
                result = self.result(score, ind_score, provided, Not_provided)
                print(flag, result)
                return flag, result

            elif scenario_id == scenario_ids[3]:
                if (pred['si_prog'] == 1) and (pred['i_cust_dep'] == 1):
                    score = 1
                    ind_score = 5
                    provided = scenarios[4]
                    Not_provided = None

                else:
                    score = 0
                    provided = [i for i, j in pred.items() if j == 1 and (i in scenarios[4])]
                    ind_score = len(provided) * 2.5
                    Not_provided = [i for i, j in pred.items() if j == 0 and (i in scenarios[4])]
                result = self.result(score, ind_score, provided, Not_provided)
                print(flag, result)
                return flag, result

            elif scenario_id == scenario_ids[4]:
                if (pred['si_prog'] == 1) and (pred['i_svc_rstr'] == 1) and (pred['i_logs'] == 1):
                    score = 1
                    ind_score = 5
                    provided = scenarios[5]
                    Not_provided = None
                else:
                    score = 0
                    provided = [i for i, j in pred.items() if j == 1 and (i in scenarios[5])]
                    ind_score = np.ceil(len(provided) * 1.6)
                    Not_provided = [i for i, j in pred.items() if j == 0 and (i in scenarios[5])]
                result = self.result(score, ind_score, provided, Not_provided)
                print(flag, result)
                return flag, result

        except ValueError:
            error = "Scenario ID must be an integer in the range of 1 to 5"
            flag = False
            #print(flag, error)
            return flag, {'result': error}
        except TypeError:
            error = "response should be string"
            flag = False
            #print(flag, error)
            return flag, {'result': error}
        except Exception as e:
            print(e)
            flag = False
            error = 'An error occured in the input'
            #print(flag, error)
            return flag, {'result':error}

if __name__ == '__main__':
    class_obj = scoring()
    while True:
        scenario  = input('please enter scenario')
        text = input('please enter text')
        flag,result = class_obj.predictions(int(scenario),str(text))
        print(flag)
        print(result)

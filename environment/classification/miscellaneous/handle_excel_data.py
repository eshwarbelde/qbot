def gpvn(df, value_starts=None):
    df = df[df['i_cust_dep'].isin([1,0])]
    if value_starts:
        df = df[df['u_number'].str.startswith(value_starts)]
    if value_starts == 'CSP':
        drop_columns = ['i_link stat_sim']
        df.drop(columns=drop_columns, axis=1, inplace=True)
    return df
import pandas as pd

# PATH = '200 Tickets Scored data_Cleansed_Initial.xlsx'
PATH = 'Transactional Dump -Base file for 06th & 07th June_Final.xlsx'

raw_file = pd.read_excel(PATH)
print('before: ', raw_file.shape)
raw_file.dropna(subset=['Notes_flown_to_Customer'], inplace=True)
print('after: ', raw_file.shape)
raw_file.sort_values(by='Number', inplace=True)
raw_file.sort_values(by='Date_Of_Interact', inplace=True)
intial_df = raw_file.groupby('Number').apply(lambda group: group.iloc[0:2, 1:]).reset_index().drop(columns=['level_1'])
subs_df = raw_file.groupby('Number').apply(lambda group: group.iloc[2:, 1:]).reset_index().drop(columns=['level_1'])
print(intial_df.shape, subs_df.shape)

intial_df.to_csv('intial_tickets_test.csv', index=False, encoding = 'utf-8')
subs_df.to_csv('subsequent_tickets_test.csv', index=False, encoding = 'utf-8')
import pandas as pd
pd.set_option('mode.chained_assignment', None)
import json
from environment.dashboard.dbo.connection import MySQLConnection
from datetime import datetime
import sys
from .log import get_logger


class DataFilter:
    def __init__(self, file_name, username, row_id,timing_file = None,mode=None):
        self.logging = get_logger('filtering')
        self.filename = file_name
        self.username = username
        self.timing_file_name = timing_file
        self.case_state_transition_data = pd.read_excel('environment/classification/input/'+self.timing_file_name,\
                usecols=['Number','Current Assignment Group','State Of Ticket(New)','State Of Ticket(OLD)','State Start Time','State End  time'])
        self.row_id = row_id
        self.connection = MySQLConnection()
        self.impact = ['Total Loss of Service', 'Partial Loss']
        self.remove_products = ['CAPNPLC', 'VNO', 'VNO Access', 'VNO Internet', 'VNO MPLS', 'VNO IPSec']
        self.current_state_remove = ['Pending with customer', 'Suspended']
        self.data_file_name = 'environment/classification/output/'
        self.remove_resolver_groups = json.load(open('environment/classification/config/remove_resolver_groups.json'))
        self.unique_records_droped = []
        self.mode = mode
        self.unique_records = []
        self.already_processed_tickets = []

    def is_ticket_exist(self,ticket):
        doc = self.connection.is_ticket_exist(ticket)
        if doc:
            self.already_processed_tickets.append(ticket)
        else:
            self.unique_records.append(ticket)
    
    def filtering(self,sheet_name=0):  # data_file,
        self.logging.debug("filtering initiated")
        try:
            query = "UPDATE process_status SET current_state='Filtering', filtering='Inprogress' WHERE id=" + str(self.row_id)


            self.connection.update(query)

            data = pd.read_excel('environment/classification/input/'+self.filename, sheet_name=sheet_name)

            self.logging.debug("file loaded")
            total_tickets = len(data['u_number'].unique())
            total_responses = data.shape[0]
            total_null = int(data['Customer Update'].isnull().sum())
            data['Customer Update'].dropna(inplace=True) #remove nulls in customer update 

            current_time = datetime.now().strftime("%H:%M:%S")    
            print('filtering initiated at',current_time)

            #remove products (u_product)
            total_responses_temp = data.shape[0]
            tickets_count_temp = len(data['u_number'].unique())
            data = data[~data['u_product'].isin(self.remove_products)]
            no_of_tickets_products_removed = tickets_count_temp - len(data['u_number'].unique())
            no_of_responses_products_removed = total_responses_temp-data.shape[0]

            #remove internal
            total_responses_temp = data.shape[0]
            tickets_count_temp = len(data['u_number'].unique())
            if 'Update Type.1' in data:
                data = data[data['Update Type.1'] == 'Customer']
            no_of_tickets_internal_removed = tickets_count_temp - len(data['u_number'].unique())
            no_of_responses_internal_removed = total_responses_temp - data.shape[0]

            #remove resolver_group (Resolver Group)   # before this was considered(u_current_assignment_group)
            total_responses_temp = data.shape[0]
            tickets_count_temp = len(data['u_number'].unique())
            data = data[~data['Resolver Group'].isin(self.remove_resolver_groups['resolver_group'])]
            no_of_tickets_resolvergrp_removed = tickets_count_temp-len(data['u_number'].unique())
            no_of_responses_resolvergrp_removed = total_responses_temp - data.shape[0]

            total_responses_temp = data.shape[0]
            tickets_count_temp = len(data['u_number'].unique())
            unique_number = data['u_number'].unique()
            for num in range(len(unique_number)):
                if data.groupby('u_number').get_group(unique_number[num]).shape[0] == 1:
                    df = pd.DataFrame(columns=data.columns)
                    df = pd.concat(
                        [df, data.groupby('u_number').get_group(unique_number[num])])
                    df['Resolver Group'].fillna('Empty Value',inplace=True)
                    if df['Resolver Group'].iloc[0] == 'Empty Value':
                        self.unique_records_droped.append(unique_number[num])
                        data.drop(df.index, inplace=True)
            no_of_tickets_1resolvergrpNA_removed = tickets_count_temp - len(data['u_number'].unique())
            no_of_responses_1resolvergrpNA_removed = total_responses_temp-data.shape[0]

            #remove impact
            total_responses_temp = data.shape[0]
            tickets_count_temp = len(data['u_number'].unique())
            data = data[data['u_impact'].isin(self.impact)]
            no_of_tickets_impact_removed = tickets_count_temp - len(data['u_number'].unique())
            no_of_responses_impact_removed = total_responses_temp - data.shape[0]
            
            #pwc key
            unumber1 = list(data.u_number.unique())
            for i in range(len(unumber1)):
                df_temp = data.groupby('u_number').get_group(unumber1[i])
                df_pwc = df_temp[df_temp['u_current_state'] == 'Pending with customer']
                if df_pwc.shape[0] >= 1:
                    for index_temp,rows_temp in df_temp.iterrows():
                        data.loc[index_temp,'pwc_count'] = int(df_pwc.shape[0])
                else:
                    for index_temp4,rows_temp4 in df_temp.iterrows():
                        data.loc[index_temp4,'pwc_count'] = 0

            self.case_state_transition_data['State End  time'].fillna('None',inplace=True)
            for i in range(len(unumber1)):
                time_stamp = 0
                df_temp1 = data.groupby('u_number').get_group(unumber1[i])
                timing_file_temp = self.case_state_transition_data.groupby('Number').get_group(unumber1[i])
                try:
                    pwc_temp = timing_file_temp[timing_file_temp['State Of Ticket(New)']=='Pending with customer']
                except:
                    pwc_temp = pd.DataFrame()
                if pwc_temp.shape[0]>=1:
                    for ind,rows_temp3 in pwc_temp.iterrows():
                        if rows_temp3['State End  time'] == 'None':
                            start_stamp = datetime.strptime(str(rows_temp3['State Start Time']),"%Y-%m-%d %X")
                            end_stamp = start_stamp
                            time = end_stamp.timestamp()-start_stamp.timestamp()
                            time_stamp = time_stamp+time
                        else:
                            start_stamp = datetime.strptime(str(rows_temp3['State Start Time']),"%Y-%m-%d %X")
                            end_stamp = datetime.strptime(str(rows_temp3['State End  time']),"%Y-%m-%d %X")
                            time = end_stamp.timestamp()-start_stamp.timestamp()
                            time_stamp = time_stamp+time
                    #time_stamp=pd.Timedelta(seconds=time_stamp)
                    for index_temp1,rows_temp1 in df_temp1.iterrows():
                        if data.loc[index_temp1,'pwc_count'] >= 1:
                            data.loc[index_temp1,'pwc_time'] = int(time_stamp)
                else:
                    for index_temp2,rows_temp2 in df_temp1.iterrows():
                        data.loc[index_temp2,'pwc_time'] = int(0)

            data['pwc_time'].fillna(0,inplace=True)
            #data['pwc_time'].fillna(pd.Timedelta(0, unit='s'),inplace=True)
            #data['pwc_time'] = pd.to_datetime(data['pwc_time'])
            #ticket state
            total_responses_temp = data.shape[0]
            tickets_count_temp = len(data['u_number'].unique())
            data = data[~data['u_current_state'].isin(self.current_state_remove)]
            no_of_tickets_pwc_susp_removed = tickets_count_temp - len(data['u_number'].unique())
            no_of_responses_pwc_susp_removed = total_responses_temp - data.shape[0]

            #remove duplicates
            # file11 = data[data.duplicated()]
            # print(file11)
            total_responses_temp = data.shape[0]
            tickets_count_temp = len(data['u_number'].unique())
            data.drop_duplicates(subset=['u_number','sys_created_on'],keep='first', inplace=True)#subset=['u_number','sys_created_on'],            
            no_of_tickets_duplicate_removed = tickets_count_temp - len(data['u_number'].unique())
            no_of_responses_duplicate_removed = total_responses_temp - data.shape[0]

            unique_dates = list(data["u_ticket_created_time"].map(lambda t: str(t.date())).unique())
            unique_dates = ','.join(unique_dates)
            #email numbers and ticket closing time 
            unique_number = data['u_number'].unique()
            len_tkts = (data.shape[0])
            i=0
            for unique in range(len(unique_number)):
                email_num = 0
                i += 1
                print(i/len_tkts * 100, end='\r')
                temp = data.groupby('u_number').get_group(unique_number[unique])
                timing_file = self.case_state_transition_data.groupby('Number').get_group(unique_number[unique])
                timing_file_temp = timing_file['State Start Time'].iloc[-1]
                for index,rows in temp.iterrows():
                    data.loc[index, 'ticket_closing_time'] = timing_file_temp
                    email_num += 1
                    data.loc[index, 'E_number'] = int(email_num)

            print('email numbers added')
            query = "UPDATE process_status SET total_tickets="+str(total_tickets)+", total_responses="+str(total_responses) \
                    +", total_null=" + str(total_null) \
                    +", no_of_tickets_duplicate_removed=" + str(no_of_tickets_duplicate_removed) \
                    +", no_of_responses_duplicate_removed=" + str(no_of_responses_duplicate_removed) \
                    +", no_of_tickets_products_removed=" + str(no_of_tickets_products_removed) \
                    +", no_of_responses_products_removed=" + str(no_of_responses_products_removed) \
                    +", no_of_tickets_resolvergrp_removed=" + str(no_of_tickets_resolvergrp_removed) \
                    +", no_of_responses_resolvergrp_removed=" + str(no_of_responses_resolvergrp_removed) \
                    +", no_of_tickets_1resolvergrpNA_removed=" + str(no_of_tickets_1resolvergrpNA_removed) \
                    +", no_of_responses_1resolvergrpNA_removed=" + str(no_of_responses_1resolvergrpNA_removed) \
                    +", no_of_tickets_pwc_susp_removed=" + str(no_of_tickets_pwc_susp_removed) \
                    +", no_of_responses_pwc_susp_removed=" + str(no_of_responses_pwc_susp_removed) \
                    +", no_of_tickets_impact_removed=" + str(no_of_tickets_impact_removed) \
                    +", no_of_responses_impact_removed=" + str(no_of_responses_impact_removed) \
                    +", no_of_tickets_internal_removed=" + str(no_of_tickets_internal_removed) \
                    +", no_of_responses_internal_removed=" + str(no_of_responses_internal_removed) \
                    +", no_tickets_into_machine=" + str(len(data['u_number'].unique())) \
                    +", no_emails_into_machine=" + str(len(data['E_number'])) \
                    +", unique_dates='" + unique_dates \
                    +"', filtering='Success' WHERE id=" + str(self.row_id)
            self.connection.update(query)
            self.logging.debug("filtering done")
            if self.mode:
                unumber_tickets=data['u_number'].unique()
                print("Checking whether ticket already exist or not")
                self.logging.debug("Checking whether ticket already exist or not")
                for i in unumber_tickets:
                        self.is_ticket_exist(i)
                unumber = self.unique_records
                data = data[data['u_number'].isin(unumber)]
                
            return data, self.case_state_transition_data
        except Exception as e:
            print(e)
            self.logging.debug(e)
            self.logging.debug('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            query = "UPDATE process_status SET filtering='Error' WHERE id=" + str(self.row_id)
            self.connection.update(query)

if __name__ == '__main__':
    DataFilter(file_path="data/ManOnNet_2019-09-30.xlsx").filtering()

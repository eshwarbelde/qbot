from datetime import datetime, timedelta
import re
from environment.dashboard.dbo.connection import MySQLConnection
from environment.classification.modules.preprocessing import Preprocessing
import sys
from .log import get_logger

class Timing:
    def __init__(self, data_file=None, timing_file=None, time_for_file_path=None, row_id=None):
        self.logging = get_logger('timing file')
        self.data_file = data_file
        self.timing_file_name = timing_file
        # self.owner_groups = json.load(open('environment/classification/config/owner_group.json'))
        # self.owner_groups_number = json.load(open('environment/classification/config/owner_groups_on_segment.json'))

        self.output_csv_path = 'environment/classification/output/ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.row_id = row_id
        self.connection = MySQLConnection()
        self.owner_groups = self.connection.get_all_owner_groups()
        self.owner_groups_number = self.connection.get_all_owner_groups_on_segmanets()
        self.preprocessing_obj = Preprocessing()

    #a method to extract time and to convert it into seconds
    @staticmethod
    def extract_time(text):
        regex = r"(\d+)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec)"
        matches = re.findall(regex, text, re.IGNORECASE)
        try:
            if len(matches) >= 1:
                time = str(matches[0][1].lower())
                if time == 'minutes' or time == 'mins' or time == 'minute' or time == 'min':
                    return int(matches[0][0])*60
                elif time == 'hour' or time == 'hrs' or time == 'hr' or time == 'hours':
                    return int(matches[0][0])*60*60
                elif time == 'seconds' or time == 'second' or time == 'sec':
                    return int(matches[0][0])
                else:
                    return ''
            else:
                regex = r"(an)\s*(hour|hrs|hr|hours|minutes|minute|mins|min|seconds|second|sec)"
                matches = re.findall(regex, text, re.IGNORECASE)
                if len(matches) > 0:
                    return int(1*60*60)
                else:
                    return ''
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print(e,'extract_time')
            return ''

    #extract time in days like today,tomorrow, an hour a hour
    @staticmethod
    def extract_time_days(text):
        regex = r'(eta|etr|tetr)'
        matches = re.findall(regex, text, re.IGNORECASE)
        try:
            if len(matches) >= 1:
                string = r'(today|tomorrow|eod)'
                time_ext = re.findall(string, text, re.IGNORECASE)
                if len(time_ext) >= 1:
                    return time_ext
                else:
                    string = r"(\d+:\d+)\s*"
                    time_ext = re.findall(string, text, re.IGNORECASE)
                    if len(time_ext):
                        try:
                            hours = int(time_ext[0][0:2])
                            minutes = int(time_ext[0][3:5])
                            return [hours, minutes]
                        except:
                            hours = int(time_ext[0][0:1])
                            minutes = int(time_ext[0][2:4])
                            return [hours, minutes]

            else:
                string = r"(\d+:\d+)\s*"
                time_ext = re.findall(string, text, re.IGNORECASE)
                if len(time_ext):
                    try:
                        hours = int(time_ext[0][0:2])
                        minutes = int(time_ext[0][3:5])
                        return [hours, minutes]
                    except:
                        hours = int(time_ext[0][0:1])
                        minutes = int(time_ext[0][2:4])
                        return [hours, minutes]
        except Exception as e:
            print(e,'extract_time_days')
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            return ''
    
    @staticmethod   
    def update_stamp(lst, creation_time):
        try:
            if type(lst[0]) == str:
                if lst[0].lower() == 'today' or lst[0].lower() == 'eod':
                    t1 = datetime.strptime(str(creation_time), "%Y-%m-%d %X")
                    t1 = t1.replace(hour=23, minute=59, second=59)
                    return t1
                elif lst[0].lower() == 'tomorrow':
                    t1 = datetime.strptime(str(creation_time), "%Y-%m-%d %X")
                    t1 = t1.replace(day=t1.day+1)
                    return t1
            else:
                try:
                    t1 = datetime.strptime(str(creation_time), "%Y-%m-%d %X")
                    t1 = t1.replace(hour=int(t1.strftime("%H"))+lst[0], minute=int(t1.strftime("%M"))+lst[1])
                    return t1
                except:
                    t1 = datetime.strptime(str(creation_time), "%Y-%m-%d %X")
                    t1 = t1.replace(hour=lst[0], minute=lst[1])
                    #print(lst, creation_time, t1)
                    return t1
        except Exception as e:
            print(e,'update_stamp')
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            t1 = datetime.strptime(str(creation_time), "%Y-%m-%d %X")
            print('exception in update_stamp')
            return t1

    #get sla time from database
    def sla_time(self, assignment_group, owner_groups, ticket_number,index):
        product = assignment_group.lower()
        initial = 0
        subsequent = 0
        try:
            for row in owner_groups:
                if row['name'].lower() == product:
                    initial = row['initial_sla']
                    subsequent = row['subsequent_sla']
                    if initial == 'As per customer segment':
                        for record in self.owner_groups_number:
                            if ticket_number.startswith('CSP'+record['service_desk_identifier']) \
                                    or \
                                    ticket_number.startswith('CSR'+record['service_desk_identifier']
                                                             ):
                                initial = record['initial_sla']
                                subsequent = record['subsequent_sla']
                        if initial == 'As per customer segment':
                            initial = 30
                            subsequent = 45
                            print(ticket_number, product, "product doesn't exist in config")
        except Exception as e:
            print('exception in sla_time picking default sla 30 mins and 45 mins')
            initial = 30
            subsequent = 45
        return int(initial), int(subsequent)

    # time compliance for initial mails
    def initial(self, current_time, booking_time, sla):
        try:
            t1 = datetime.strptime(str(current_time), "%Y-%m-%d %X")
            t2 = datetime.strptime(str(booking_time), "%Y-%m-%d %X")
            updated_booking_time = t2+timedelta(minutes=sla)
            diff=t1.timestamp() - updated_booking_time.timestamp()
            if t1.timestamp() <= updated_booking_time.timestamp():
                return 1,diff
            else:
                return 0,diff
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print('Exception in initial function ')
            print(e)
            return 0, 0

    # time extraction from mails
    def seconds_ext(self, current_time, booking_time, sla):
        try:
            t1 = datetime.strptime(str(current_time), "%Y-%m-%d %X")
            t2 = datetime.strptime(str(booking_time), "%Y-%m-%d %X")
            updated_booking_time = t2+timedelta(seconds=sla)
            diff=t1.timestamp() - updated_booking_time.timestamp()
            if t1.timestamp() <= updated_booking_time.timestamp():
                return 1,diff
            else:
                return 0,diff
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print(e)
            return 0,0

    # time extraction from mails such as EOD Today Tomorrow
    def seconds_ext_list(self, current_time, booking_time, time):
        try:
            t1 = datetime.strptime(str(current_time), "%Y-%m-%d %X")
            updated_booking_time = self.update_stamp(time, booking_time)
            diff= t1.timestamp() - updated_booking_time.timestamp()
            if t1.timestamp() <= updated_booking_time.timestamp():
                return 1,diff
            else:
                return 0,diff
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print(e)
            return 0, 0

    # time compliace for next subsequent mails
    def multiple_subsequent(self, current_time, booking_time, email_count, email_count_temp, initial,
                            subsequent, reset_stamp):
        try:
            if reset_stamp:
                t1 = datetime.strptime(str(current_time), "%Y-%m-%d %X")
                t2 = datetime.strptime(str(booking_time), "%Y-%m-%d %X")
                email_count_temp = int(email_count_temp[1:])
                updated_booking_time = t2+timedelta(minutes=email_count_temp*subsequent)
                diff= t1.timestamp() - updated_booking_time.timestamp()
                if t1.timestamp() <= updated_booking_time.timestamp():
                    return 1,diff
                else:
                    return 0,diff
            else:
                t1 = datetime.strptime(str(current_time), "%Y-%m-%d %X")
                t2 = datetime.strptime(str(booking_time), "%Y-%m-%d %X")
                email_count = int(email_count[1:])
                updated_booking_time = t2+timedelta(minutes=initial)+timedelta(minutes=email_count*subsequent)
                diff = t1.timestamp() - updated_booking_time.timestamp()
                if t1.timestamp() <= updated_booking_time.timestamp():
                    return 1,diff
                else:
                    return 0,diff
        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            return 0, 0
            
    # for subsequent mails
    def s1_subsequent(self, current_time, booking_time, initial, subsequent, reset_stamp):
        try:
            if reset_stamp:
                t1 = datetime.strptime(str(current_time), "%Y-%m-%d %X")
                t2 = datetime.strptime(str(booking_time), "%Y-%m-%d %X")
                updated_booking_time = t2 + timedelta(minutes = subsequent)
                diff = t1.timestamp() - updated_booking_time.timestamp()
                if t1.timestamp() <= updated_booking_time.timestamp():
                    return 1,diff
                else:
                    return 0,diff
            else:
                t1 = datetime.strptime(str(current_time), "%Y-%m-%d %X")
                t2 = datetime.strptime(str(booking_time), "%Y-%m-%d %X")
                updated_booking_time = t2 + timedelta(minutes = initial) + timedelta(minutes = subsequent)
                diff = t1.timestamp() - updated_booking_time.timestamp()
                if t1.timestamp() <= updated_booking_time.timestamp():
                    return 1,diff
                else:
                    return 0,diff
        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            return 0, 0

    # checking for pwc state
    def pwd_check(self, current_time, booking_time, df_sla):
        start_time_dict = {}
        end_time_dict = {}
        count = 0
        try:
            for i, row in df_sla.iterrows():
                count += 1
                start_time_dict['start_time'+str(count)] = row['State Start Time']
                end_time_dict['end_time'+str(count)] = row['State End  time']
            return start_time_dict, end_time_dict
        except Exception as e:
            print(e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            return start_time_dict, end_time_dict
    
    # resting time
    def time_reset(self, creationtime, timestamp, time_df):
        try:
            t1 = datetime.strptime(str(timestamp), "%Y-%m-%d %X")
            flag = False
            for time_i, time_rows in time_df.iterrows():
                if time_rows['State Of Ticket(New)'] == 'Pending with customer':
                    pwc_stamp = datetime.strptime(str(time_rows['State Start Time']),"%Y-%m-%d %X")
                    if pwc_stamp.timestamp() <= t1.timestamp():
                        for wip_i,wip_rows in time_df.iterrows():
                            if wip_rows['State Of Ticket(New)'] == 'Work in Progress':
                                wip_stamp = datetime.strptime(str(wip_rows['State Start Time']),"%Y-%m-%d %X")
                                if t1.timestamp() >= wip_stamp.timestamp():
                                    flag = True
                                    reset_stamp = wip_stamp

            if flag:
                return reset_stamp, 1
            else:
                return creationtime, 0
        except Exception as e:
            print(e)
            print('exception in time_reset')
            return creationtime, 0

    
    def run(self):
        try:
            self.logging.debug('Time compliance')
            print('Time compliance')
            data = self.data_file
            data.reset_index(drop=True,inplace=True)
            sla=self.timing_file_name
            data['time_compliance'] = data.apply(lambda x: 1 if x['email_type'].startswith('d') else None,axis=1)
            data['time_sec'] = data.apply(lambda x: self.extract_time(self.preprocessing_obj.clean_text_timing(str(x['Customer Update'])))
                                    if x['i_etr'] == 1 or x['i_resp_time'] == 1 or x['si_etr'] == 1 or x['si_resp_time'] == 1 else None,axis=1) #cleaning text before extracting time
            data['time_sec'].fillna('none', inplace=True) 
            data['time_format'] = data.apply(lambda x: self.extract_time_days(self.preprocessing_obj.clean_text_timing(str(x['Customer Update']))) \
                                        if x['i_etr'] == 1 or x['i_resp_time'] == 1 or x['si_etr'] == 1 or x['si_resp_time'] == 1  else None,axis=1)    #extracting time
            unumber = list(data.u_number.unique()) # list of all Ticket numbers for the data

            # timing reset
            len_tkts = (data.shape[0])
            x = 0
            print('time reset')
            self.logging.debug('Time reset')
            for i in range(len(unumber)):
                x += 1
                print(x/len_tkts * 100, end='\r')
                df_temp = data.groupby('u_number').get_group(unumber[i])
                df_sla = sla.groupby('Number').get_group(unumber[i])
                for reset_index, reset_rows in df_temp.iterrows():
                    data.loc[reset_index, 'u_ticket_created_time_temp'], data.loc[reset_index, 'reset'] = \
                        self.time_reset(reset_rows['u_ticket_created_time'], reset_rows['sys_created_on'], df_sla)

            self.logging.debug('temp email count')
            for i in range(len(unumber)):
                x += 1
                print(x/len_tkts * 100, end='\r')
                df_temp = data.groupby('u_number').get_group(unumber[i])
                df_sla = sla.groupby('Number').get_group(unumber[i])
                email_count = 1
                time_temp = 0
                for email_index, email_rows in df_temp.iterrows():
                    if data.loc[email_index, 'reset']:
                        data.loc[email_index, 'email_type_temp'] = 's'+str(email_count)
                        time_temp = data.loc[email_index, 'u_ticket_created_time_temp']
                        try:
                            if time_temp == data.loc[email_index+1, 'u_ticket_created_time_temp']:
                                email_count += 1
                            else:
                                email_count = 1
                        except KeyError:
                            continue
                        except Exception as e:
                            print('temp email count Error on line -- {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
                            continue
                    else:
                        data.loc[email_index, 'email_type_temp'] = ''

            print(len(unumber), 'unique tickets count')
            for i in range(len(unumber)):
                df_temp = data.groupby('u_number').get_group(unumber[i])  # grouping by ticket numbers
                df_sla = sla.groupby('Number').get_group(unumber[i])  # case state of ticket number
                df_sla = df_sla[df_sla['State Of Ticket(New)'] == 'Pending with customer']
                index_count = -1
                for index, rows in df_temp.iterrows():
                    index_count += 1
                    initial, subsequent = self.sla_time(
                        rows['u_current_assignment_group'], self.owner_groups,
                        rows['u_number'], index)  # initial & subsequent sla time from config based on Assignment group
                    if rows['email_type'] == 'i':  # for initial mails
                        current_time = rows['sys_created_on']
                        booking_time = rows['u_ticket_created_time_temp']
                        data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.initial(current_time, booking_time, initial)
                    elif rows['email_type'].startswith('s'):  # All subsequent mails
                        if rows['email_type'] == 's1':
                            if df_temp['i_resp_time'].iloc[index_count-1] == 1 or \
                                    df_temp['i_etr'].iloc[index_count-1] == 1:
                                if type(df_temp['time_format'].iloc[index_count-1]) == list:
                                    current_time = rows['sys_created_on']
                                    booking_time = df_temp['sys_created_on'].iloc[index_count-1]
                                    data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.seconds_ext_list(
                                        current_time, booking_time, df_temp['time_format'].iloc[index_count-1])
                                elif type(df_temp['time_sec'].iloc[index_count-1]) == float or type(df_temp['time_sec'].iloc[index_count-1]) == int:
                                    current_time = rows['sys_created_on']
                                    booking_time = df_temp['sys_created_on'].iloc[index_count-1]
                                    data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.seconds_ext(
                                        current_time, booking_time, df_temp['time_sec'].iloc[index_count-1])
                                    #print(rows['u_number'],df_temp['time_sec'].iloc[index_count-1])
                                else:
                                    current_time = rows['sys_created_on']
                                    booking_time = rows['u_ticket_created_time_temp']
                                    data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.s1_subsequent(
                                        current_time, booking_time, initial, subsequent, rows['reset'])

                            else:
                                current_time = rows['sys_created_on']
                                booking_time = rows['u_ticket_created_time_temp']
                                data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.s1_subsequent(
                                    current_time, booking_time, initial, subsequent, rows['reset'])

                        else:
                            if df_temp['si_resp_time'].iloc[index_count-1] == 1 or\
                                    df_temp['si_etr'].iloc[index_count-1] == 1:
                                if type(df_temp['time_format'].iloc[index_count-1]) == list:
                                    current_time = rows['sys_created_on']
                                    booking_time = df_temp['sys_created_on'].iloc[index_count-1]
                                    data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.seconds_ext_list(
                                        current_time,
                                        booking_time,
                                        df_temp['time_format'].iloc[index_count-1])
                                elif type(df_temp['time_sec'].iloc[index_count-1]) == float or type(df_temp['time_sec'].iloc[index_count-1]) == int:
                                    current_time = rows['sys_created_on']
                                    booking_time = df_temp['sys_created_on'].iloc[index_count-1]
                                    data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.seconds_ext(
                                        current_time,
                                        booking_time,
                                        df_temp['time_sec'].iloc[index_count-1])
                                else:
                                    current_time = rows['sys_created_on']
                                    booking_time = rows['u_ticket_created_time_temp']
                                    data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.multiple_subsequent(
                                        current_time,
                                        booking_time,
                                        rows['email_type'],
                                        rows['email_type_temp'],
                                        initial, subsequent, rows['reset'])
                            else:
                                current_time = rows['sys_created_on']
                                booking_time = rows['u_ticket_created_time_temp']
                                data.loc[index, 'time_compliance'],data.loc[index, 'sla_diff'] = self.multiple_subsequent(
                                    current_time,
                                    booking_time,
                                    rows['email_type'],
                                    rows['email_type_temp'],
                                    initial, subsequent, rows['reset'])

            query = "UPDATE process_status SET prediction='Success' WHERE id="+str(self.row_id)
            self.connection.update(query)
            return data
        except Exception as e:
            self.logging.debug(e)
            self.logging.debug('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            query = "UPDATE process_status SET prediction='Error' WHERE id="+str(self.row_id)
            self.connection.update(query)


if __name__ == '__main__':
    Timing().run()

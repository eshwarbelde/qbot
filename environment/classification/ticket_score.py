import pandas as pd
import numpy as np
from environment.dashboard.dbo.connection import MySQLConnection
from environment.classification.modules.preprocessing import Preprocessing
import spacy
import sys
from environment.RFO.rfo_predictions import RFO
from .log import get_logger


class TicketScoring:
    def __init__(self, data_frame=None, row_id=None):
        self.logging = get_logger('ticket scoring')
        self.data_frame = data_frame
        self.connection = MySQLConnection()
        self.rfo = RFO()
        self.row_id = row_id
        self.nlp = spacy.load("en_core_web_sm")
        self.preprocessing_obj = Preprocessing()

    def similarity_embeddings(self,sent1,sent2):
        try:
            doc1 = self.nlp(sent1)
            doc2 = self.nlp(sent2)
            similarity = doc1.similarity(doc2)
        except Exception as e:
            print(e)
            similarity = 0
        return similarity

    #ticket scoring for subsequent emails
    def scoring_concatinated(self,rows):
        try:
            resp_rating = str('Perfect')
            if rows['si_prog'] and rows['si_no_prob'] and rows['si_logs']:
                comscore_concatinated = 1
            elif rows['si_prog'] and rows['si_svc_rstr'] and rows['si_no_prob']:
                comscore_concatinated = 2
            elif rows['si_prog'] and rows['si_svc_rstr'] == 0 and rows['si_resp_time']:
                comscore_concatinated = 3
            elif rows['si_prog'] and rows['si_svc_rstr'] == 0 and rows['si_cd']:
                comscore_concatinated = 4
            elif rows['si_prog'] and rows['si_svc_rstr'] and rows['si_no_prob'] == 0 and rows['si_logs'] and rows[
                'si_etr'] == 0:
                comscore_concatinated = 6
            elif rows['si_prog'] and rows['si_svc_rstr'] == 0 and rows['si_etr']:
                comscore_concatinated = 7
            elif rows['si_prog'] and rows['si_no_prob']:
                comscore_concatinated = 8
            elif rows['si_prog'] and rows['si_svc_rstr'] == 0 and rows['si_etr'] and rows['si_cd']:
                comscore_concatinated = 9
            elif rows['si_svc_rstr']:
                comscore_concatinated = 5
            else:
                comscore_concatinated = 0
                resp_rating = str('Needs Improvement')
            return int(comscore_concatinated), resp_rating
        except Exception as e:
            print(e)
            self.logging.debug(e)
            self.logging.debug("error in scoring_concatinated")
            return 0,"error"

    #caluclating shortest path for an email to be a compliant
    def shortestpath_subsequent(self,rows):
        try:
            if rows['si_prog'] == 0 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 0 and rows['si_logs'] == 0 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 0 and rows['si_etr'] == 0:
                scenario = str('3,si_prog,si_resp_time,2')
            # doubles
            elif rows['si_prog'] == 1 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 0 and rows['si_logs'] == 1 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 0 and rows['si_etr'] == 0:
                scenario = str('6,si_svc_rstr,1')
            elif rows['si_prog'] == 0 and rows['si_svc_rstr'] == 1 and rows['si_no_prob'] == 1 and rows['si_logs'] == 0 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 0 and rows['si_etr'] == 0:
                scenario = str('2,si_prog,1')
            # elif rows['si_prog']==1 and rows['si_svc_rstr']==0 and rows['si_no_prob']==0 and rows['si_logs']==0 and rows['si_resp_time']==0 and rows['si_cd']==1 and rows['si_etr']==0:
            #     scenario=str('9,si_etr,1')
            elif rows['si_prog'] == 0 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 0 and rows['si_logs'] == 0 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 1 and rows['si_etr'] == 1:
                scenario = str('9,si_prog,1')
            elif rows['si_prog'] == 0 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 1 and rows['si_logs'] == 1 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 0 and rows['si_etr'] == 0:
                scenario = str('1,si_prog,1')
            # singles
            elif rows['si_prog'] == 1 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 0 and rows['si_logs'] == 0 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 0 and rows['si_etr'] == 0:
                scenario = str('3,si_resp_time,1')
            elif rows['si_prog'] == 0 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 1 and rows['si_logs'] == 0 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 0 and rows['si_etr'] == 0:
                scenario = str('2,si_prog,si_svc_rstr,2')
            elif rows['si_prog'] == 0 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 0 and rows['si_logs'] == 0 and \
                    rows['si_resp_time'] == 1 and rows['si_cd'] == 0 and rows['si_etr'] == 0:
                scenario = str('3,si_prog,1')
            elif rows['si_prog'] == 0 and rows['si_svc_rstr'] == 0 and rows['si_no_prob'] == 0 and rows['si_logs'] == 0 and \
                    rows['si_resp_time'] == 0 and rows['si_cd'] == 1 and rows['si_etr'] == 0:
                scenario = str('4,si_prog,1')
            else:
                scenario = str('undetermined')
            return scenario
        except Exception as e:
            self.logging.debug("error in shortestpath_subsequent")
            self.logging.debug(e)
            return "error"

    #caluclating shortest path for an initial email to be compliant
    def shortestpath(self,rows):
        try:
            if rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 0 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('1/6,i_link_stat_chk,i_poa,i_resp_time,3')
            # four
            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 1 and rows['i_cust_dep'] == 1 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 1 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('9,i_link_stat_chk,1')
            # three
            elif rows['i_link_stat_chk'] == 1 and rows['i_logs'] == 1 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('1/6,i_resp_time,1')

            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 1 and rows['i_resp_time'] == 1:
                scenario = str('1/6,i_link_stat_chk,1')

            elif rows['i_link_stat_chk'] == 1 and rows['i_logs'] == 1 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 0 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 1:
                scenario = str('1/6,i_poa,1')
            # doubles
            elif rows['i_link_stat_chk'] == 1 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('1/6,i_resp_time,1')

            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 0 and (rows['i_etr'] == 1 or rows['i_resp_time'] == 1):
                scenario = str('1/6,i_link_stat_chk,1')

            elif rows['i_link_stat_chk'] == 1 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 0 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 1:
                scenario = str('1/6,i_poa,1')

            elif rows['i_link_stat_chk'] == 1 and rows['i_logs'] == 1 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 0 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('1/6,i_resp_time,i_poa,2')

            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 1 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('3/8,i_link_stat_chk,1')

            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 1 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('3/8,i_link_stat_chk,i_resp_time or i_etr,2')
            # singles
            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 0 and \
                    rows['i_svc_rstr'] == 0 and (rows['i_etr'] == 1 or rows['i_resp_time'] == 1):
                scenario = str('1/6,i_link_stat_chk,i_poa,2')  # check

            elif rows['i_link_stat_chk'] == 1 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 0 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('1/6,i_poa,i_resp_time,2')

            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 0 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 1 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('1/6,i_link_stat_chk,i_resp_time,2')

            elif rows['i_link_stat_chk'] == 0 and rows['i_logs'] == 1 and rows['i_cust_dep'] == 0 and rows['i_poa'] == 0 and \
                    rows['i_svc_rstr'] == 0 and rows['i_etr'] == 0 and rows['i_resp_time'] == 0:
                scenario = str('1/6,i_link_stat_chk,i_poa,i_resp_time,3')

            else:
                scenario = str('undetermined')

            return scenario
        except Exception as e:
            self.logging.debug("error in shortestpath")
            self.logging.debug(e)
            return "error"

    def data_dic_method(self,si_prog,si_svc_rstr,si_no_prob,si_etr,si_logs,si_resp_time,si_cd):
        try:
            dic = {
                'si_prog': si_prog,
                'si_svc_rstr': si_svc_rstr,
                'si_no_prob': si_no_prob,
                'si_etr': si_etr,
                'si_logs': si_logs,
                'si_resp_time': si_resp_time,
                'si_cd': si_cd
                }
            return dic
        except Exception as e:
            print("error in data_dic_method")
            print(e)
            self.logging.debug(e)


    def run(self):
        try:
            print('Ticket Scoring')
            self.logging.debug('Ticket Scoring initiated')
            # updating db to change the process state
            query = "UPDATE process_status SET scoring='Inprogress', current_state='scoring' WHERE id=" + str(self.row_id)
            result = self.connection.update(query)
            print(result)

            data = self.data_frame  # loading data frame
            data['no_problem_found'].fillna(0, inplace=True)
            unumber = data.u_number.unique()      # getting tickets from the data

            len_tkts = (data.shape[0])
            i = 0
            print('shortest path')
            self.logging.debug('finding shortest path')
            for ind,rows in data.iterrows():
                i += 1
                print(i/len_tkts * 100, end='\r')
                if rows['compliance_scenario_num'] == 0 and rows['email_type'].startswith('s') == False:
                    data.loc[ind,'shortest_path'] = self.shortestpath(rows)
                elif rows['comscore'] == 0:
                    data.loc[ind,'shortest_path'] = self.shortestpath_subsequent(rows)
            self.logging.debug('Shortest path done')
            #smilarity check of responses
            i = 0
            print('similarity check')
            self.logging.debug('similarity check initiated')
            for ticket_index in range(len(unumber)):
                i += 1
                print(i/len_tkts * 100, end='\r')
                comp_df = data.groupby('u_number').get_group(unumber[ticket_index])
                for ind,rows in comp_df.iterrows():
                    try:
                        if rows['email_type'].startswith('s') and rows['si_prog'] and comp_df.loc[ind,'si_prog']:
                            similarity = self.similarity_embeddings(self.preprocessing_obj.clean_text(str(rows['Customer Update'])),
                                self.preprocessing_obj.clean_text(str(comp_df.loc[ind+1,'Customer Update'])))
                            if similarity > 0.99: #threshold
                                if (str(rows['si_etr_time']) == str(comp_df.loc[ind+1,'si_etr_time'])) and (str(rows['si_time_update']) == str(comp_df.loc[ind+1,'si_time_update'])):
                                    if rows['si_svc_rstr'] == 0:
                                        data.loc[ind+1,'si_prog'] = 0
                                        data.loc[ind+1,'repeated_response'] = 'repeated'
                                    else:
                                        data.loc[ind+1,'repeated_response'] = 'repeated srv_rstr is 1'
                                else:
                                    data.loc[ind+1,'repeated_response'] = 'repeated timestamp difference'
                            else:
                                data.loc[ind+1,'repeated_response'] = np.nan
                    except KeyError:
                        pass
                    except Exception as e:
                        print(e,'cosine_similarity')
                        self.logging.debug(e)
                        self.logging.debug('exception in similarity check')
            self.logging.debug('similarity check successful')

            # email concatination for subsequent to reduce number of non compliant emails
            # appending new row ex(s1+s2)
            # discarded s1 and s2 are concatinated with 'd'string in email type
            print('response concatination')
            self.logging.debug('response concatination successful')

            for ticket_index in range(len(unumber)):
                email_merging_df = data.groupby('u_number').get_group(unumber[ticket_index])
                flag = []
                for email_index, email_row in email_merging_df.iterrows():
                    if email_row['email_type'].startswith('s'):
                        try:    
                            if str(email_row['email_type']) not in flag:
                                if email_row['comscore'] == 0 and email_row['time_compliance'] == 1 and \
                                    email_merging_df.loc[email_index+1, 'comscore'] == 0 and \
                                        email_merging_df.loc[email_index+1, 'time_compliance'] == 1:
                                    cols = {
                                            'si_prog': 0, 'si_svc_rstr': 0, 'si_no_prob': 0, 'si_etr': 0,
                                            'si_logs': 0,'si_resp_time': 0, 'si_cd': 0
                                    }
                                    for col in cols.items():
                                        if int(email_row[col[0]]) == 1 or \
                                                int(email_merging_df.loc[email_index+1, col[0]]) == 1:
                                            cols[col[0]] = 1
                                        else:
                                            cols[col[0]] = 0
                                    
                                    '''data_dic = {
                                        'si_prog': int(cols['si_prog']),
                                        'si_svc_rstr': int(cols['si_svc_rstr']),
                                        'si_no_prob': int(cols['si_no_prob']),
                                        'si_etr': int(cols['si_etr']),
                                        'si_logs': int(cols['si_logs']),
                                        'si_resp_time': int(cols['si_resp_time']),
                                        'si_cd': int(cols['si_cd'])
                                    }'''
                                    data_dic = self.data_dic_method(int(cols['si_prog']),int(cols['si_svc_rstr']),int(cols['si_no_prob']),
                                                                    int(cols['si_etr']),int(cols['si_logs']),int(cols['si_resp_time']),int(cols['si_cd']))
                                    
                                    com_score,resp_rating = self.scoring_concatinated(data_dic)

                                    if int(com_score) == 0:
                                        try:
                                            if email_row['comscore'] == 0 and email_row['time_compliance'] == 1 and \
                                                email_merging_df.loc[email_index+1, 'comscore'] == 0 and \
                                                email_merging_df.loc[email_index+1, 'time_compliance'] == 1 and\
                                                email_merging_df.loc[email_index+2, 'comscore'] == 0 and \
                                                email_merging_df.loc[email_index+2, 'time_compliance'] == 1:
                                                cols = {
                                                    'si_prog': 0, 'si_svc_rstr': 0, 'si_no_prob': 0, 'si_etr': 0,
                                                    'si_logs': 0,'si_resp_time': 0, 'si_cd': 0
                                                }
                                                for col in cols.items():
                                                    if int(email_row[col[0]]) == 1 or \
                                                        int(email_merging_df.loc[email_index+1, col[0]]) == 1 or \
                                                        int(email_merging_df.loc[email_index+2, col[0]]) == 1:

                                                        cols[col[0]] = 1
                                                    else:
                                                        cols[col[0]] = 0
                                                '''data_dic ={
                                                    'si_prog': int(cols['si_prog']),
                                                    'si_svc_rstr': int(cols['si_svc_rstr']),
                                                    'si_no_prob': int(cols['si_no_prob']),
                                                    'si_etr': int(cols['si_etr']),
                                                    'si_logs': int(cols['si_logs']),
                                                    'si_resp_time': int(cols['si_resp_time']),
                                                    'si_cd': int(cols['si_cd'])
                                                    }'''
                                                data_dic = self.data_dic_method(int(cols['si_prog']),int(cols['si_svc_rstr']),int(cols['si_no_prob']),
                                                                                int(cols['si_etr']),int(cols['si_logs']),int(cols['si_resp_time']),int(cols['si_cd']))
                                                
                                                com_score,resp_rating = self.scoring_concatinated(data_dic)

                                                email_type = [str(email_row['email_type'])+'+'
                                                       + str(data.loc[email_index+1, 'email_type'])+'+'
                                                       + str(data.loc[email_index+2, 'email_type'])]

                                                flag.append(str(data.loc[email_index+1, 'email_type']))
                                                flag.append(str(data.loc[email_index+2, 'email_type']))

                                                dict1 = {
                                                'u_number': [data.loc[email_index+2, 'u_number']],
                                                'Customer Update': [data.loc[email_index + 2, 'Customer Update']],
                                                'Closure notes': [data.loc[email_index + 2, 'Closure notes']],
                                                'RFO Specification': [data.loc[email_index + 2, 'RFO Specification']],
                                                'Fault Segment': [data.loc[email_index + 2, 'Fault Segment']],
                                                'u_ticket_created_time':[data.loc[email_index+2, 'u_ticket_created_time']],
                                                'sys_created_on': [data.loc[email_index+2, 'sys_created_on']],
                                                'email_type': email_type,
                                                'time_compliance': [data.loc[email_index+2, 'time_compliance']],
                                                'comscore': com_score,
                                                'resp_rating': resp_rating,
                                                'E_number': [data.loc[email_index+2, 'E_number']],
                                                'si_prog': int(cols['si_prog']),
                                                'si_svc_rstr': int(cols['si_svc_rstr']),
                                                'si_no_prob': int(cols['si_no_prob']),
                                                'si_etr': int(cols['si_etr']),
                                                'si_logs': int(cols['si_logs']),
                                                'si_resp_time': int(cols['si_resp_time']),
                                                'si_cd': int(cols['si_cd']),
                                                'si_etr_time': [data.loc[email_index+2, 'si_etr_time']],
                                                'si_time_update': [data.loc[email_index+2, 'si_time_update']],
                                                'pwc_count': [data.loc[email_index+2, 'pwc_count']],
                                                'no_problem_found': [data.loc[email_index+2, 'no_problem_found']],
                                                'sys_created_by': [data.loc[email_index+2, 'sys_created_by']],
                                                'repeated_response': [data.loc[email_index+2, 'repeated_response']],
                                                'shortest_path': [data.loc[email_index+2, 'shortest_path']],
                                                'sla_diff': [data.loc[email_index+2, 'sla_diff']],
                                                'u_current_assignment_group': [data.loc[email_index+2, 'u_current_assignment_group']],
                                                    'rfo_closure': [data.loc[email_index + 2, 'rfo_closure']]
                                                }

                                                data.loc[email_index, 'email_type'] = 'c'+str(data.loc[email_index, 'email_type'])
                                                data.loc[email_index+1, 'email_type'] = 'c'+str(data.loc[email_index+1,
                                                                                                        'email_type'])
                                                data.loc[email_index+2, 'email_type'] = 'c'+str(data.loc[email_index+2,
                                                                                                        'email_type'])                                                        
                                                df2 = pd.DataFrame(dict1)
                                                data = data.append(df2, ignore_index=True, sort=False)
                                        except Exception as e:
                                            print(e,email_merging_df.loc[email_index,'u_number'])
                                            continue
                                    else:
                                        flag.append(str(data.loc[email_index+1, 'email_type']))
                                        email_type = [str(email_row['email_type'])+'+'
                                                        + str(data.loc[email_index+1, 'email_type'])]
                                        dict1 = {
                                            'u_number': [data.loc[email_index+1, 'u_number']],
                                            'Customer Update': [data.loc[email_index + 1, 'Customer Update']],
                                            'Closure notes': [data.loc[email_index + 1, 'Closure notes']],
                                            'RFO Specification': [data.loc[email_index + 1, 'RFO Specification']],
                                            'Fault Segment': [data.loc[email_index + 1, 'Fault Segment']],
                                            'u_ticket_created_time':[data.loc[email_index+1, 'u_ticket_created_time']],
                                            'sys_created_on': [data.loc[email_index+1, 'sys_created_on']],
                                            'sys_created_by': [data.loc[email_index + 1, 'sys_created_by']],
                                            'email_type': email_type,
                                            'time_compliance': [data.loc[email_index+1, 'time_compliance']],
                                            'comscore': com_score,
                                            'resp_rating': resp_rating,
                                            'E_number': [data.loc[email_index+1, 'E_number']],
                                            'si_prog': int(cols['si_prog']),
                                            'si_svc_rstr': int(cols['si_svc_rstr']),
                                            'si_no_prob': int(cols['si_no_prob']),
                                            'si_etr': int(cols['si_etr']),
                                            'si_logs': int(cols['si_logs']),
                                            'si_resp_time': int(cols['si_resp_time']),
                                            'si_cd': int(cols['si_cd']),
                                            'si_etr_time': [data.loc[email_index+1, 'si_etr_time']],
                                            'si_time_update': [data.loc[email_index+1, 'si_time_update']],
                                            'pwc_count': [data.loc[email_index+1, 'pwc_count']],
                                            'no_problem_found': [data.loc[email_index+1, 'no_problem_found']],
                                            'repeated_response': [data.loc[email_index+1, 'repeated_response']],
                                            'shortest_path': [data.loc[email_index+1, 'shortest_path']],
                                            'sla_diff': [data.loc[email_index+1, 'sla_diff']],
                                            'u_current_assignment_group': [data.loc[email_index+1, 'u_current_assignment_group']],
                                            'rfo_closure': [data.loc[email_index + 1, 'rfo_closure']]
                                        }
                                        data.loc[email_index, 'email_type'] = 'c'+str(data.loc[email_index, 'email_type'])
                                        data.loc[email_index+1, 'email_type'] = 'c'+str(data.loc[email_index+1,
                                                                                                'email_type'])
                                        df2 = pd.DataFrame(dict1)
                                        data = data.append(df2, ignore_index=True, sort=False)
                            
                                elif email_row['comscore'] == 0 and email_row['time_compliance'] == 1 and \
                                        email_merging_df.loc[email_index+1, 'comscore'] > 0 and \
                                        email_merging_df.loc[email_index+1, 'time_compliance'] == 1:

                                    flag.append(str(data.loc[email_index+1, 'email_type']))
                                    
                                    dict1 = {
                                        'u_number': [data.loc[email_index+1, 'u_number']],
                                        'Customer Update': [data.loc[email_index + 1, 'Customer Update']],
                                        'Closure notes': [data.loc[email_index + 1, 'Closure notes']],
                                        'RFO Specification': [data.loc[email_index + 1, 'RFO Specification']],
                                        'Fault Segment': [data.loc[email_index + 1, 'Fault Segment']],
                                        'u_ticket_created_time': [data.loc[email_index+1, 'u_ticket_created_time']],
                                        'sys_created_on': [data.loc[email_index+1, 'sys_created_on']],
                                        'sys_created_by': [data.loc[email_index + 1, 'sys_created_by']],
                                        'email_type': [str(email_row['email_type']) + '+' + str(data.loc[email_index+1, 'email_type'])],
                                        'time_compliance': [data.loc[email_index+1, 'time_compliance']],
                                        'comscore': [data.loc[email_index+1, 'comscore']],
                                        'resp_rating': [data.loc[email_index + 1, 'resp_rating']],
                                        'E_number': [data.loc[email_index+1, 'E_number']],
                                        'si_prog': [data.loc[email_index+1, 'si_prog']],
                                        'si_svc_rstr': [data.loc[email_index+1, 'si_svc_rstr']],
                                        'si_no_prob': [data.loc[email_index+1, 'si_no_prob']],
                                        'si_etr': [data.loc[email_index+1, 'si_etr']],
                                        'si_logs': [data.loc[email_index+1, 'si_logs']],
                                        'si_resp_time': [data.loc[email_index+1, 'si_resp_time']],
                                        'si_cd': [data.loc[email_index+1, 'si_cd']],
                                        'si_etr_time': [data.loc[email_index+1, 'si_etr_time']],
                                        'si_time_update': [data.loc[email_index+1, 'si_time_update']],
                                        'pwc_count': [data.loc[email_index + 1, 'pwc_count']],
                                        'no_problem_found': [data.loc[email_index + 1, 'no_problem_found']],
                                        'repeated_response': [data.loc[email_index + 1, 'repeated_response']],
                                        'shortest_path': [data.loc[email_index + 1, 'shortest_path']],
                                        'sla_diff': [data.loc[email_index + 1, 'sla_diff']],
                                        'u_current_assignment_group': [data.loc[email_index + 1, 'u_current_assignment_group']],
                                        'rfo_closure': [data.loc[email_index + 1, 'rfo_closure']]
                                    }
                                                
                                    data.loc[email_index, 'email_type'] = 'c' + str(data.loc[email_index, 'email_type'])
                                    data.loc[email_index+1, 'email_type'] = 'c' + \
                                                                            str(data.loc[email_index+1, 'email_type'])
                                    df2 = pd.DataFrame(dict1)
                                    data = data.append(df2, ignore_index=True, sort=False)

                        except KeyError:
                            pass
                        except Exception as e:
                            print(e, 'Email Concat')
                            self.logging.debug('Error on line {}'.format(sys.exc_info()[-1].tb_lineno),type(e).__name__, e)
            print('response concatination finished')
            # added new rows at resetting the index
            data = data.sort_values(by=['u_number', 'sys_created_on']).reset_index(drop=True)
            data_rfo = data.groupby('u_number').tail(1)
            rfo = self.rfo.run(data_rfo)

            # for ticket scoring we dont consider the discarded emails
            data1 = data[~data['email_type'].str.startswith('d')]
            data1 = data1[~data1['email_type'].str.startswith('c')]
            for i in range(len(unumber)):
                count = 0     # to check number of emails which are time compliant
                df = data1.groupby('u_number').get_group(unumber[i])
                #rfo realated data from rfo file
                rfo_temp = rfo.groupby('u_number').get_group(unumber[i])
                rfo_comp = int(rfo_temp.iloc[0]['RFO_comp'])
                r1_comp = int(rfo_temp.iloc[0]['R1_comp'])
                r2_comp = int(rfo_temp.iloc[0]['R2_comp'])
                rfo_time = 1  #pending yet to get proper data till then it is treated as compliant
                rfo_count = rfo_comp + r1_comp + r2_comp + rfo_time
                data_size = df.shape[0]
                quality = df[df['time_compliance'] > 0].shape[0]
                for ind, rows in df.iterrows():
                    if rows['email_type'] == 'i':
                        if rows['compliance_scenario_num'] > 0:
                            count += 1
                    elif rows['email_type'].startswith('s'):
                        if rows['comscore'] > 0:
                            count += 1
                # ticket score= number of quality compliant and time compliant emails/overall opportunities
                #tkt_score = (count+quality)/(data_size*2)
                overall = data_size * 2
                tkt_score = (count + quality + rfo_count) / (overall + 4)
                for ind, rows in df.iterrows():
                    if tkt_score < 0.8:
                        data.loc[ind, 'ticket_score'] = 'poor'
                        data.loc[ind, 'ticket_absolute_score'] = int(tkt_score*100)
                    elif tkt_score == 1:
                        data.loc[ind, 'ticket_score'] = 'perfect'
                        data.loc[ind, 'ticket_absolute_score'] = int(tkt_score*100)
                    else:
                        data.loc[ind, 'ticket_score'] = 'good'
                        data.loc[ind, 'ticket_absolute_score'] = int(tkt_score*100)

            query = "UPDATE process_status SET scoring='Success' WHERE id="+str(self.row_id)
            self.connection.update(query)
            print('Scoring Done') 

            return data,rfo
        except Exception as e:
            self.logging.debug(e)
            self.logging.debug('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print(e, 'Ticket Scoring')
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            query = "UPDATE process_status SET scoring='Error' WHERE id=" + str(self.row_id)
            self.connection.update(query)

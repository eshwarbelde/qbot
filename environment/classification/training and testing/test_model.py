from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import pandas as pd

import matplotlib.pyplot as plt # Helps in plotting any graph

from environment.classification.modules import utils
from environment.classification.modules.preprocessing import Preprocessing


class TestModel(object):

    def __init__(self,data):
        self.file_path = data
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.models = {}
        self.dtm_models = {}

    @staticmethod
    def train_model(X, y):
        # Intialize classifier
        model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        # Fit training data using the Bernoulli NB to train the model
        model.fit(X, y)
        return model

    def predict_values(self, X_test, predict_variable):
        # Predict the target variable values of the test dataset on trained model.
        return self.models[predict_variable].predict(X_test)

    def are_columns_exist(self, df_columns, validate_columns):
        for v_col in validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def start_training(self, df, predict_variables):
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['customer_update_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.

        # # one hot encoding
        # onehotencoder = OneHotEncoder()
        # products = pd.DataFrame(onehotencoder.fit_transform(df[['u_product']]).toarray(), columns=onehotencoder.get_feature_names())

        # Distribute the data into train and test datasets where 80% is used for training and 20% for testing.
        # X = pd.concat([df_email_word_matrix, products], axis=1)
        X = df_email_word_matrix
        for predict_variable in predict_variables:
            print(predict_variable)
            self.dtm_models[predict_variable] = count_vectorizer
            y = df[predict_variable].astype(int)
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, stratify=y, random_state=25)
            model = self.train_model(X_train, y_train)
            self.models[predict_variable] = model
            y_pred = self.predict_values(X_test, predict_variable)
            y_pred = pd.DataFrame(y_pred, index=y_test.index, columns=[predict_variable])

            if predict_variable == 'i_etr':
                for row_index, row in y_pred.iterrows():
                    val = row[predict_variable]
                    if val == 1:
                        extracted_time = utils.extract_time(df.loc[row_index, 'Customer Update'])
                        print('i_etr_time: ', extracted_time)
            if predict_variable == 'i_resp_time':
                for row_index, row in y_pred.iterrows():
                    val = row[predict_variable]
                    if val == 1:
                        extracted_time = utils.extract_time(df.loc[row_index, 'Customer Update'])
                        print('i_time_update: ', extracted_time)

            # Drop rows if all values of the predicted variables are zeros.
            keep_indexes = df.loc[~(df[predict_variables] == 0).all(axis=1)].index
            y_test = y_test[y_test.index.isin(list(keep_indexes))]
            y_pred = y_pred[y_pred.index.isin(list(keep_indexes))]
            # Get accuracy score
            accuracy = round(accuracy_score(y_true=y_test, y_pred=y_pred), 2)
            print('Accuracy: ', accuracy)
            # Get confusion matrix
            binary = confusion_matrix(y_true=y_test, y_pred=y_pred)
            # plot confusion matrix
            plot_confusion_matrix(conf_mat=binary)
            plt.savefig('confusion_matrix_{}.png'.format(predict_variable))

    def execute(self):
        # Training on all columns except i_link stat_sim
        validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_link_stat_chk',
                                 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        predict_variables = ['i_link_stat_chk', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']
        ignore_cols = ['i_link stat_sim']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        drop_rows_for_cols = ['Customer Update', 'i_link_stat_chk', 'i_logs',
                              'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        remove_na_rows_by_col = 'i_link_stat_chk'
        df = self.file_path[select_cols]
        df = self.preprocessing_obj.filter_data_by_email_method_2(df, drop_rows_for_cols, remove_na_rows_by_col)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

        # Training for i_link stat_sim
        validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_link stat_sim']
        predict_variables = ['i_link stat_sim']
        ignore_cols = ['i_link_stat_chk', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        drop_rows_for_cols = ['Customer Update', 'i_link stat_sim']
        remove_na_rows_by_col = 'i_link stat_sim'
        df = self.file_path[select_cols]
        df = self.preprocessing_obj.filter_data_by_email_method_2(df, drop_rows_for_cols, remove_na_rows_by_col)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

if __name__=='__main__':
    data_file = '../data/Inital Data Verification_23 Apr.xlsx.xlsx'
    sheet_names = ['GVPN', 'ILL', 'NDE - Hub', 'IPL - IPLCCC', 'NPL Intracity', 'NDE', 'MVOIP']
    data = utils.merge_dataframe(data_file, sheet_names)
    # Shuffle the rows
    data = data.sample(frac=1).reset_index(drop=True)
    TestModel(data).execute()
import pandas as pd
pd.set_option('mode.chained_assignment', None)
# from environment.classification.utils import save_pickle_file, merge_dataframe
# from environment.classification.preprocessing import Preprocessing
from sklearn.ensemble import RandomForestClassifier
from utils import save_pickle_file, merge_dataframe
from preprocessing import Preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score, confusion_matrix ,classification_report,f1_score
from sklearn.model_selection import train_test_split

class Training(object):
    def __init__(self, file_path_data,file_path_data1=None):
        """
        Initialize the variables.
        """
        self.file_path = file_path_data
        self.file_path1 = file_path_data1
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.models = {}
        self.dtm_models = {}
        self.model_path = '../models/email_classifier_model_v7.pkl'
        self.dtm_model_path = '../models/document_term_matrix_model_v7.pkl'

    @staticmethod
    def train_model(X, y ,predict_variable):
        """
        Train the classification model.
        :param X: Independent variables/features
        :param y: dependent variable/feature
        :return: Returns the model object after fitting on the training data.
        """
        if predict_variable == 'i_svc_rstr':
            #model = SVC(gamma='scale',kernel = 'poly' ,degree = 6,random_state=1,class_weight='balanced')
            model= LogisticRegression(tol=0.0002,max_iter=2000,class_weight = 'balanced', penalty = 'l2' , random_state=0,solver='liblinear')
            #model = SGDClassifier(max_iter = 500, penalty='l1',random_state=0)
            #model = RandomForestClassifier(n_estimators=150, class_weight='balanced', random_state=25)
        elif predict_variable == 'i_poa':
            #model = SGDClassifier(max_iter=500, penalty='l1', random_state=0)
            #model = SVC(gamma='scale',degree = 4,kernel = 'poly' ,random_state=1,class_weight='balanced')
            # model = LogisticRegression(tol=0.0002, max_iter=2000,class_weight = 'balanced' ,  penalty='l2', random_state=0,
            #                            solver='liblinear') #l1_ratio=0,
            model = RandomForestClassifier(n_estimators=200, class_weight='balanced', random_state=25)

        elif predict_variable == 'i_resp_time':
            #model = SVC(gamma='scale',kernel = 'poly' ,degree = 6,random_state=1,class_weight='balanced')
            model= LogisticRegression(tol=0.0002,max_iter=2000,class_weight = 'balanced', penalty = 'l2' , random_state=0,solver='liblinear')
            #model = SGDClassifier(max_iter = 500, penalty='l1',random_state=0)
            #model = RandomForestClassifier(n_estimators=150, class_weight='balanced', random_state=25)
        elif predict_variable == 'i_etr':
            #model = SVC(gamma='scale',kernel = 'poly' ,degree = 6,random_state=1,class_weight='balanced')
            model= LogisticRegression(tol=0.0002,max_iter=2000,class_weight = 'balanced', penalty = 'l2' , random_state=0,solver='liblinear')
            #model = SGDClassifier(max_iter = 500, penalty='l1',random_state=0)
            #model = RandomForestClassifier(n_estimators=150, class_weight='balanced', random_state=25)
        # elif predict_variable == 'i_link_stat_chk':
        #     model = LogisticRegression(tol=0.001, max_iter=2500, class_weight='balanced', penalty='l2', random_state=0,
        #                                solver='liblinear')
        elif predict_variable == 'i_cust_dep':
            #model = SVC(gamma='scale',kernel = 'poly' ,degree = 6,random_state=1,class_weight='balanced')
            model= LogisticRegression(tol=0.005,max_iter=2000,class_weight = 'balanced', penalty = 'l2' , random_state=0,solver='liblinear')
        else:
            model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        # Fit training data using the Bernoulli NB to train the model
        model.fit(X, y)
        return model

    def predict_values(self, X_test, predict_variable):
        # Predict the target variable values of the test dataset on trained model.
        return self.models[predict_variable].predict(X_test)

    def are_columns_exist(self, df_columns, validate_columns):
        """
        Check all columns are exist or not
        :param df_columns: List of columns in the data
        :return:
        """
        for v_col in validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def start_training(self, df, predict_variables):
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(x))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['customer_update_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.
        X = df_email_word_matrix

        for predict_variable in predict_variables:
            print(predict_variable)
            self.dtm_models[predict_variable] = count_vectorizer
            y = df[predict_variable]  # .astype(int)
            X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y,test_size=0.20,  random_state=25)
            X_train_temp = X_train.copy()
            model = self.train_model(X_train, y_train, predict_variable)
            self.models[predict_variable] = model
            keep_indexes = df.loc[~(df[predict_variables] == 0).all(axis=1)].index
            y_pred = self.predict_values(X_train, predict_variable)
            y_pred = pd.DataFrame(y_pred, index=y_train.index, columns=[predict_variable + '_predicted'])
            y_train = y_train[y_train.index.isin(list(keep_indexes))]
            y_pred = y_pred[y_pred.index.isin(list(keep_indexes))]
            temp = df.loc[y_pred.index][['Customer Update', predict_variable]]
            y_pred1 = pd.merge(temp, y_pred, left_index=True, right_index=True)
            y_pred1.to_excel('../output/training_set_' + predict_variable + '.xlsx')
            # Get accuracy score
            accuracy = round(f1_score(y_true=y_train, y_pred=y_pred, average='weighted'), 2)
            print('Accuracy_train: ', accuracy)
            target_name = ['0', '1']
            print(classification_report(y_train, y_pred, target_names=target_name,labels=self.models[predict_variable].classes_,zero_division=0))

            y_pred = self.predict_values(X_test, predict_variable)
            y_pred = pd.DataFrame(y_pred, index=y_test.index, columns=[predict_variable + '_predicted'])
            keep_indexes = df.loc[~(df[predict_variables] == 0).all(axis=1)].index
            y_test = y_test[y_test.index.isin(list(keep_indexes))]
            y_pred = y_pred[y_pred.index.isin(list(keep_indexes))]
            temp = df.loc[y_pred.index][['Customer Update', predict_variable]]
            y_pred1 = pd.merge(temp, y_pred, left_index=True, right_index=True)
            y_pred1.to_excel('../output/test_set_' + predict_variable + '.xlsx')
            # Get accuracy score
            accuracy = round(f1_score(y_true=y_test, y_pred=y_pred, average='weighted'), 2)
            print('Accuracy_test: ', accuracy)
            target_name = ['0', '1']
            print(classification_report(y_test, y_pred, target_names=target_name,labels=self.models[predict_variable].classes_,zero_division=0))

    def execute(self):
        """
        List of operations over the training data and build the model and store in the models directory.
        :return:
        """
        # Training on all columns except i_link stat_sim
        validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_link_stat_chk',
                                 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        predict_variables = ['i_link_stat_chk', 'i_logs', 'i_poa', 'i_svc_rstr']
        ignore_cols = ['i_link stat_sim']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        drop_rows_for_cols = ['Customer Update', 'i_link_stat_chk', 'i_logs',
                              'i_poa', 'i_svc_rstr']
        remove_na_rows_by_col = 'i_link_stat_chk'
        df = self.file_path[select_cols]
        df.to_excel('../output/data_used_for_training_and_testing.xlsx')
        df = self.preprocessing_obj.filter_data_by_email_method_2(df, drop_rows_for_cols, remove_na_rows_by_col)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

        #training for etr and resp_time
        predict_variables = ['i_etr', 'i_resp_time', 'i_cust_dep']
        ignore_cols = ['i_link stat_sim']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        drop_rows_for_cols = ['Customer Update',  'i_etr', 'i_resp_time', 'i_cust_dep']
        remove_na_rows_by_col = 'i_link_stat_chk'
        df = self.file_path[select_cols]
        #df.to_excel('../output/data_used_for_training_and_testing.xlsx')
        #df = self.preprocessing_obj.filter_data_by_email_method_2(df, drop_rows_for_cols, remove_na_rows_by_col)
        print(df.shape)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

        # Training for i_link stat_sim
        validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_link stat_sim']
        predict_variables = ['i_link stat_sim']
        ignore_cols = ['i_link_stat_chk', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        drop_rows_for_cols = ['Customer Update', 'i_link stat_sim']
        remove_na_rows_by_col = 'i_link stat_sim'
        df = self.file_path[select_cols]
        df = self.preprocessing_obj.filter_data_by_email_method_2(df, drop_rows_for_cols, remove_na_rows_by_col)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

        # Save all the models
        save_pickle_file(self.dtm_model_path, self.dtm_models)
        save_pickle_file(self.model_path, self.models)

if __name__=='__main__':
    #data_file = 'environment/dashboard/data/Data Verification update.xlsx'
    data_file = '../data/Inital Data Verification_19 May.xlsx' #Inital Data Verification_23 Apr
    sheet_names = ['GVPN', 'ILL', 'NDE - Hub', 'IPL - IPLCCC', 'NPL Intracity', 'NDE', 'MVOIP','new','subsequent'] #,'newdata'
    data = merge_dataframe(data_file, sheet_names)
    # Shuffle the rows
    data = data.sample(frac=1).reset_index(drop=True)

    print(data.shape)
    print(data['i_poa'].value_counts())
    print(data['i_svc_rstr'].value_counts())
    Training(data).execute()
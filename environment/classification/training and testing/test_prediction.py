import pandas as pd # Operation over CSV or excel
from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import accuracy_score, confusion_matrix
from environment.dashboard.dbo.connection import MySQLConnection
from environment.classification.filter_data import DataFilter
from environment.classification.miscellaneous.handle_excel_data import gpvn
from environment.classification.modules.preprocessing import Preprocessing # Custom class for preprocessing
from environment.classification.modules.utils import load_pickle_file, save_excel, extract_time
from cerberus.validator import Validator
import matplotlib.pyplot as plt


class EmailClassification:
    def __init__(self, file_path=None, flag_save_in_csv=None, flag_save_in_db=None):
        """
        Initialize neccessary variables
        """
        self.file_path = file_path
        self.flag_save_in_csv = flag_save_in_csv
        self.flag_save_in_db = flag_save_in_db
        self.mongodb_connection = MySQLConnection()
        self.validate_columns = ['Customer Update', 'u_number', 'sys_created_on']
        self.predict_variables = ['i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']  # Target variables.
        self.drop_columns_for_subsequent = ['i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa',
                                            'i_svc_rstr', 'i_etr', 'i_resp_time', 'i_time_update', 'i_etr_time']
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.initial_df = pd.DataFrame()
        self.subsequent_df = pd.DataFrame()
        self.models = {}
        self.predict_variables_values = {}
        self.model_path = 'environment/dashboard/models/email_classifier_model_v3.pkl'
        self.dtm_model_path = 'environment/dashboard/models/document_term_matrix_model_v3.pkl'
        self.output_csv_path = 'environment/dashboard/output/gvpn_results.xlsx'
        self.schema_validator = Validator()
        print('Initialization has been done.')

    def predict_values(self, X_test, predict_variable):
        """
        Predict the target variable values of the test dataset on the trained model.
        """
        return self.models[predict_variable].predict(X_test)

    def are_columns_exist(self, df_columns):
        """
        Check all columns are exist or not
        :param df_columns: List of columns in the data
        :return:
        """
        for v_col in self.validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def run(self):
        """
        List of operations over the test data and predict values on the loaded trained model.
        :return:
        """
        df = self.preprocessing_obj.filter_drop_rows_by_emails(self.file_path)
        df.fillna('', inplace=True)
        # self.are_columns_exist(df.columns)
        print('Validation on the columns has been done.')
        # Text preprocessing, stop word removal and stemming over the words of the text.
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))
        print('Text cleansing on the email contents has been done.')
        emails = list(df['customer_update_cleaned'])
        # Generate document term vector space.
        count_vectorizer = load_pickle_file(self.dtm_model_path)
        dtm = self.preprocessing_obj.transform_document_term_vector(count_vectorizer, emails)
        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names(), index=df.index
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.
        self.models = load_pickle_file(self.model_path)
        X = df_email_word_matrix

        # Predicting results
        res = {}
        for predict_variable in self.predict_variables:
            print(predict_variable)
            if predict_variable not in df.columns:
                continue
            if predict_variable not in res:
                res[predict_variable] = []
            y_pred = self.predict_values(X, predict_variable)
            y_pred = pd.DataFrame(y_pred, index=df.index, columns=[predict_variable])
            # Get accuracy score
            accuracy = round(accuracy_score(y_true=df[predict_variable], y_pred=y_pred), 2)
            print('Accuracy: ', accuracy)
            # Get confusion matrix
            binary = confusion_matrix(y_true=df[predict_variable], y_pred=y_pred)
            # plot confusion matrix
            plot_confusion_matrix(conf_mat=binary)
            plt.rcParams["figure.figsize"] = (2, 2)
            plt.savefig('{}.png'.format(predict_variable))

            res[predict_variable].extend(list(y_pred[predict_variable].values))

            if predict_variable == 'i_etr':
                if 'i_etr_time' not in res:
                    res['i_etr_time'] = []
                if 'i_time_update' not in res:
                    res['i_time_update'] = []
                for row_index, row in y_pred.iterrows():
                    val = row[predict_variable]
                    if val == 1:
                        extracted_time = extract_time(str(df.loc[row_index, 'Customer Update']))
                        print('i_etr_time: ', extracted_time)
                        res['i_etr_time'].append(extracted_time)
                    else:
                        res['i_etr_time'].append('')
            if predict_variable == 'i_resp_time':
                for row_index, row in y_pred.iterrows():
                    val = row[predict_variable]
                    if val == 1:
                        extracted_time = extract_time(str(df.loc[row_index, 'Customer Update']))
                        print('i_time_update: ', extracted_time)
                        res['i_time_update'].append(extracted_time)
                    else:
                        res['i_time_update'].append('')
        # Concat input + results
        # df.drop(columns='customer_update_cleaned', axis=1, inplace=True)
        df_res = pd.DataFrame(res)
        df_final = pd.concat([df, df_res], axis=1)
        save_excel(self.output_csv_path, df_final) if self.flag_save_in_csv else None
        print('Finish prediciting...')

if __name__=='__main__':
    data_file = 'environment/dashboard/data/Data Verification.xlsx'
    data = DataFilter(data_file).filtering(sheet_name='ILL')
    data = gpvn(data, value_starts='CSR')
    EmailClassification(
        file_path=data,
        flag_save_in_csv=0,
        flag_save_in_db=0
    ).run()
import pandas as pd

# from environment.classification.utils import save_pickle_file, merge_dataframe
# from environment.classification.preprocessing import Preprocessing
from sklearn.ensemble import RandomForestClassifier
from utils import save_pickle_file, merge_dataframe
from preprocessing import Preprocessing


class Training(object):
    def __init__(self, file_path_data):
        """
        Initialize the variables.
        """
        self.file_path = file_path_data
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.models = {}
        self.dtm_models = {}
        self.model_path = 'models/email_classifier_model_v5.pkl'
        self.dtm_model_path = 'models/document_term_matrix_model_v5.pkl'

    @staticmethod
    def train_model(X, y):
        """
        Train the classification model.
        :param X: Independent variables/features
        :param y: dependent variable/feature
        :return: Returns the model object after fitting on the training data.
        """
        model = RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=25)
        # Fit training data using the Bernoulli NB to train the model
        model.fit(X, y)
        return model

    def are_columns_exist(self, df_columns, validate_columns):
        """
        Check all columns are exist or not
        :param df_columns: List of columns in the data
        :return:
        """
        for v_col in validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def start_training(self, df, predict_variables):
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(x))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['customer_update_cleaned'])
        # Generate document term vector space.
        count_vectorizer, dtm = self.preprocessing_obj.fit_document_term_vector(emails)

        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names()
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.

        for predict_variable in predict_variables:
            print(predict_variable)
            self.dtm_models[predict_variable] = count_vectorizer
            model = self.train_model(df_email_word_matrix, df[predict_variable].astype(int))
            self.models[predict_variable] = model

    def execute(self):
        """
        List of operations over the training data and build the model and store in the models directory.
        :return:
        """
        # Training on all columns except i_link stat_sim
        validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_link_stat_chk',
                                 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        predict_variables = ['i_link_stat_chk', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']
        ignore_cols = ['i_link stat_sim']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        drop_rows_for_cols = ['Customer Update', 'i_link_stat_chk', 'i_logs',
                              'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        remove_na_rows_by_col = 'i_link_stat_chk'
        df = self.file_path[select_cols]
        df = self.preprocessing_obj.filter_data_by_email_method_2(df, drop_rows_for_cols, remove_na_rows_by_col)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

        # Training for i_link stat_sim
        validate_columns = ['Customer Update', 'u_number', 'sys_created_on', 'i_link stat_sim']
        predict_variables = ['i_link stat_sim']
        ignore_cols = ['i_link_stat_chk', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']
        select_cols = list(set(self.file_path.columns) - set(ignore_cols))
        drop_rows_for_cols = ['Customer Update', 'i_link stat_sim']
        remove_na_rows_by_col = 'i_link stat_sim'
        df = self.file_path[select_cols]
        df = self.preprocessing_obj.filter_data_by_email_method_2(df, drop_rows_for_cols, remove_na_rows_by_col)
        self.are_columns_exist(df.columns, validate_columns)
        self.start_training(df, predict_variables)

        # Save all the models
        save_pickle_file(self.dtm_model_path, self.dtm_models)
        save_pickle_file(self.model_path, self.models)

if __name__=='__main__':
    #data_file = 'environment/dashboard/data/Data Verification update.xlsx'
    data_file = 'data/Inital Data Verification_23 Apr.xlsx'
    sheet_names = ['GVPN', 'ILL', 'NDE - Hub', 'IPL - IPLCCC', 'NPL Intracity', 'NDE', 'MVOIP']
    data = merge_dataframe(data_file, sheet_names)
    # Shuffle the rows
    data = data.sample(frac=1).reset_index(drop=True)
    # data = data_filter(data_file).filtering(sheet_name='Initial Training data')
    # data_file2 = 'environment/dashboard/data/Data Verification.xlsx'
    # data2 = data_filter(data_file2).filtering(sheet_name='GVPN')
    # data2 = gpvn(data2, value_starts='CSR')
    # print(data1.columns, data2.columns)
    # data = pd.concat([data1, data2], axis=0)
    # print(data1.shape, data2.shape, data.shape)
    # print(data.columns)
    Training(data).execute()
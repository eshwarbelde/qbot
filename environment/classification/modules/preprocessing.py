import re # Libaray for regular expression
import pandas as pd # Operation over CSV or excel
import unidecode # Unicode handler
# NLTK library for NLP techniques
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
# Sklearn library for word frequency counting
from sklearn.feature_extraction.text import CountVectorizer
from nltk.tokenize import RegexpTokenizer



class Preprocessing:
    def __init__(self):
        """
        Constructor is used to initialize NLTK stemmer and stop words list
        """
        self.pstemmer = PorterStemmer()
        stop_words = list(set(stopwords.words('english')))
        stop_words = stop_words + ["dear", "regard"]  # data specific
        stop_words = list(set(stop_words))

        ignore_stop_words = ["isn't", "aren't", "wasn't", "weren't", "hasn't",
                             "haven't", "hadn't", "doesn't", "don't", "didn't",
                             "won't", "wouldn't", "shan't", "shouldn't", "can't",
                             "cannot", "couldn't", "mustn't", "but", "no", "nor", "not", "too", "very", 'up', 'by',
                             'through', 'will',
                             "was", "before", "down", 'did', 'further', 'above', 'than', 'here', 'on', 'again',
                             'because', 'below']

        self.stop_words = [x for x in stop_words if x not in ignore_stop_words] # Final list of stop words

    # @staticmethod
    # def clean_text(text):
    #     """
    #     Class method is used for text cleansing.
    #     :param text: Sentence or text corpus
    #     :return: Preprocessed text
    #     """
    #     text = str(text)
    #     text = re.sub('<.*?>', '', text) # for removing HTML tags
    #     text = re.sub('[^[A-Za-z0-9]/\' ]', ' ', text) # keep only alpha numeric
    #     text = unidecode.unidecode(text) # Convert latin to ASCII characters (American Standard Code for Information Interchange)
    #     text = text.lower() # convert to lower case characters
    #     text = re.sub(r'[^\w\s]+', '', text) # removing punctuation marks
    #     text = re.sub(r'[\d]+', '', text) # removing numbers
    #     text = re.sub(r'^\\s+|\\s+$', '', text) # remove leading and trailing white space
    #     text = text.strip() # removing white space
    #     return text

    @staticmethod
    def clean_text(text):
        text = text.lower()
        text = re.sub(r'from:.*\n', r' ', text)
        text = re.sub(r'to:.*\n', r' ', text)
        text = re.sub(r'sent:.*\n', r' ', text)
        text = re.sub(r're:.*\n', r' ', text)
        text = re.sub(r'cc:.*\n', r' ', text)
        text = re.sub(r'reply:.*\n', r' ', text)
        text = re.sub('<[a-z1-9]+@[a-z]+.[a-z]+>', '', text)
        text = re.sub(r'______.*', r'', text)
        text = re.sub(r'we hope this response has sufficiently.*\n', r'', text)
        text = re.sub(r'service support posted on.*\n', r'', text)
        text = re.sub(r'case task.*\n', r' ', text)
        text = re.sub(r'State for case task.\n', r' ', text)
        text = re.sub(r'work notes added.*\n', r' ', text)
        text = text.replace('\n', ' ').replace('\r', '')
        text = text.strip()
        # logs removing
        text = re.sub(r'Logs ::.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'Logs:-.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'Switch logs:-.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'SM Logs:.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'PE LOGS.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'PFB logs:.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'Logs:.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'VCG-Interval statistics.*|VCG-Interval statistic.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'Ethernet-Interval statistics.*|ethernet-interval statistic.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'CE logs.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'SS NR:.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'Base Logs:.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'logs:.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'HBS logs -.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'SM NR:.*', r' ', text, re.IGNORECASE)
        text = re.sub(r'BS LOGS:.*', r' ', text, re.IGNORECASE)
        text = re.sub(
            r"thanks and regard's.*|thanks & regards.*|thanks regard.*\n|warm regards.*|best regards.*|with regards.*|regards.*",
            r' ', text, re.IGNORECASE)
        text = re.sub(r'[0-9]+', '', text)
        text = text.strip()  # removing white space
        text = re.sub(r'[^\w]', ' ', text)
        text = re.sub(r'\s+', r' ', text)
        text = re.sub(r'-', r' ', text)

        return text

    def get_stem_words(self,  word):
        """
        It is used to apply stemmer over the word and convert a word into base word.
        :param word: Any word
        :return: Stemmed word
        """
        return self.pstemmer.stem(word) # stem to root word

    def remove_stop_words_and_apply_stemmer(self, text):
        """
        It is used to remove stop words and calls stemmer method.
        :param text: text or sentence
        :return: text without stopword and contain base word
        """
        return ' '.join([self.get_stem_words(x) for x in text.split() if x not in self.stop_words])

    @staticmethod
    def fit_document_term_vector(corpus):
        """
        It is used to build word frequency over the corpus.
        :param corpus: List of List of sentences.
        :return: Document word vector space.
        """
        count_vectorizer = CountVectorizer(min_df=5) # Initialize word frequency with param min_df = 5 which is used to consider only those word whose occurence is more than 5 in the whole corpus.
        dtm = count_vectorizer.fit_transform(corpus) # Fit corpus to train word frequency model
        print('Shape of document term matrix: {} & number of features: {}'.format(
            dtm.shape, len(count_vectorizer.get_feature_names())))
        return count_vectorizer, dtm

    @staticmethod
    def transform_document_term_vector(count_vectorizer, corpus):
        """
        Build document term vector space
        :param count_vectorizer: object of count vectorizer
        :param corpus: list of sentences.
        :return: Returns word frequency of each document.
        """
        print('Transforming the words of each document to vectors.')
        dtm = count_vectorizer.transform(corpus)
        return dtm

    @staticmethod
    def filter_drop_rows_by_emails(file_path):
        """
        This function is used to drop all na rows for column Customer Update.
        First 2 rows are considered as initial emails and rest are subsequent emails.
        :param file_path: Raw file path
        :return: Return intial emails and subsequent emails data in tuple format
        """
        # print('Reading the file: {}'.format(file_path))
        # df = pd.read_excel(file_path)
        df = file_path
        # Add serial number
        df['serial_no'] = range(1, len(df)+1)
        df.dropna(subset=['Customer Update'], inplace=True)
        df.sort_values(by=['u_number', 'sys_created_on'], inplace=True)
        return df

    @staticmethod
    def filter_data_by_email_method_1(file_path):
        """
        This function is used to drop all na rows for column Customer Update.
        First 2 rows are considered as initial emails and rest are subsequent emails.
        :param file_path: Raw file path
        :return: Return intial emails and subsequent emails data in tuple format
        """
        print('Reading the file: {}'.format(file_path))
        df = pd.read_excel(file_path)
        df.dropna(subset=['Customer Update'], inplace=True)
        df.sort_values(by=['u_number', 'sys_created_on'], inplace=True)
        intial_df = df.groupby('u_number').apply(lambda group: group.iloc[0:2, 1:]).reset_index().drop(
            columns=['level_1'])
        subs_df = df.groupby('u_number').apply(lambda group: group.iloc[2:, 1:]).reset_index().drop(
            columns=['level_1'])
        return intial_df, subs_df

    @staticmethod
    def filter_data_by_email_method_2(file_path, drop_rows_for_cols, remove_na_rows_by_col):
        """
        This function is used to drop all na rows for columns ['Customer Update',
        'i_ack', 'i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
        'i_etr', 'i_na', 'i_gram']
        All rows are considered as intial emails.
        :param file_path: Raw file path
        :return: Return intials emails data.
        """
        print('Reading the file: {}'.format(file_path))
        df = file_path
        df = df[df[remove_na_rows_by_col].isin([1, 0])]
        # drop_rows_for_cols = ['Customer Update', 'i_link_stat_chk', 'i_link stat_sim', 'i_logs',
        #                       'i_cust_dep', 'i_poa', 'i_svc_rstr', 'i_etr']
        df.dropna(subset=drop_rows_for_cols, inplace=True)
        df.sort_values(by=['u_number', 'sys_created_on'], inplace=True)
        return df

    @staticmethod
    def clean_text_subseuqent(text):
        """
        Class method is used for text cleansing.
        :param text: Sentence or text corpus
        :return: Preprocessed text
        """
        text = str(text)
        text = text.lower() # convert to lower case characters
        text = text.strip() # removing white space
        text = re.sub(r'caution:.*\n|\xa0', r' ',text)
        text = re.sub(r'step by step.*\n', r' ',text)
        text = re.sub(r'from:.*\n|sent:.*\n|to:.*\n|cc:.*\n|subject:.*\n', r' ',text)
        text = re.sub(r'reply from.*\n', r' ',text)
        text = re.sub(r'[\d]{2,3}\.[\d]{2,3}\.[\d]{2,3}\.[\d]{2,3}', r'ipaddress',text)
        text = re.sub(r'csp\d*', r'csptkt',text)
        text = re.sub(r'csr\d*', r'csrtkt',text)
        text = re.sub(r'https:.*.com', r'webaddress',text)
        text = re.sub(r'[a-z]*@[a-z]*.com', r'emailadd',text)
        text = re.sub(r'[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}|[0-9]{1,2}:[0-9]{1,2}:[0-9]{2}', r'date',text)
        text = re.sub(r'[\d]{2,4}-...-[\d]{2,4}|[\d]{2,4}-....-[\d]{2,4}', r'date',text)
        text = re.sub(r'=====.*\n', r'',text) 
        text = re.sub(r'[^\w]', r' ',text)
        text = re.sub(r'\s\s*', r' ',text)
        text = re.sub(r'logs.*', r'logstatus',text)
        text = re.sub(r'checked at.*|pe_ce.*|interval statistics.*', r'',text)
        text = re.sub('<.*?>', '', text) # for removing HTML tags
        text = re.sub('[^[A-Za-z0-9]/\' ]', ' ', text) # keep only alpha numeric
        text = unidecode.unidecode(text) # Convert latin to ASCII characters (American Standard Code for Information Interchange)
        text = re.sub(r'[^\w\s]+', '', text) # removing punctuation marks
        text = re.sub(r'[\d]+', '', text) # removing numbers
        text = re.sub(r'^\\s+|\\s+$', '', text) # remove leading and trailing white space'''

        return text

    @staticmethod
    def clean_text_timing(text):
        text = str(text)
        text = text.lower() # convert to lower case characters
        text = text.strip() # removing white space
        text = re.sub(r'caution:.*\n|\xa0', r' ',text)
        text = re.sub(r'from:.*\n|sent:.*\n|to:.*\n|cc:.*\n|subject:.*\n', r' ',text)
        text = re.sub(r'step by step.*\n', r' ',text)
        text = re.sub(r'reply from.*\n', r' ',text)
        text = re.sub(r'[\d]{2,3}\.[\d]{2,3}\.[\d]{2,3}\.[\d]{2,3}', r'ipaddress',text)
        text = re.sub(r'csp\d*', r'csptkt',text)
        text = re.sub(r'csr\d*', r'csrtkt',text)
        text = re.sub(r'https:.*.com', r'webaddress',text)
        text = re.sub(r'[a-z]*@[a-z]*.com', r'emailadd',text)
        text = re.sub(r'[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}|[0-9]{1,2}:[0-9]{1,2}:[0-9]{2}', r'date',text)
        text = re.sub(r'[\d]{2,4}-...-[\d]{2,4}|[\d]{2,4}-....-[\d]{2,4}', r'date',text)
        text = re.sub(r'=====.*\n', r'',text) # if type(text)== str else text
        #text = re.sub(r'[^\w]', r' ',text)
        text = re.sub(r'\s\s*', r' ',text)
        text = re.sub(r'logs.*', r'logstatus',text)
        text = re.sub(r'checked at.*|pe_ce.*|interval statistics.*', r'',text)
        text = re.sub('<.*?>', '', text) # for removing HTML tags
        text = re.sub('[^[A-Za-z0-9]/\' ]', ' ', text) # keep only alpha numeric
        text = unidecode.unidecode(text) # Convert latin to ASCII characters (American Standard Code for Information Interchange)
        #text = re.sub(r'[^\w\s]+', '', text) # removing punctuation marks
        text = re.sub(r'^\\s+|\\s+$', '', text) # remove leading and trailing white space'''

        return text

    def normalize_text(self,notes):
        for i,j in zip(notes.index,range(notes.shape[0])):
            note = str(notes[i]).lower()
            regx_tokenizer = RegexpTokenizer('\S+')
            tokens = regx_tokenizer.tokenize(note)
            lemmatizer = WordNetLemmatizer()
            note=[lemmatizer.lemmatize(word)for word in tokens ] #if not word in set(stopwords.words('english'))
            note = ' '.join(note)
            yield note


    def corpus(self,data):
        corpus1=[]
        for doc in self.normalize_text(data):
            corpus1.append(doc)
        return corpus1







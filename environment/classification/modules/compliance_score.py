import numpy as np
import pandas as pd
from environment.dashboard.dbo.connection import MySQLConnection


class ComplianceScore:
    def __init__(self):
        self.connection = MySQLConnection()
        self.only_owner_groups = self.connection.get_all_owner_groups()
        self.owner_groups_on_segment = self.connection.get_all_owner_groups_on_segmanets()
        self.default_sla_time = 30 # Default value for initial.

    def get_records_based_on_sla_window(self, df_of_same_ticket_num):
        def _get_sla_time_as_per_segment(ticket_num):
            identifer_of_ticket_num = ticket_num[3:5]
            for each_owner_group in self.owner_groups_on_segment:
                if each_owner_group['service_desk_identifier'] == identifer_of_ticket_num:
                    return each_owner_group['initial_sla']
            return self.default_sla_time

        def _get_sla_time(row_x):
            current_assignment_group = row_x['u_current_assignment_group'].lower()
            ticket_num = row_x['u_number']
            for each_owner_group in self.only_owner_groups:
                if each_owner_group['name'].lower() == current_assignment_group:
                    if type(each_owner_group['initial_sla']) is int:
                        print(each_owner_group['initial_sla'])
                        return each_owner_group['initial_sla']
                    elif type(each_owner_group['initial_sla']) is str and each_owner_group['initial_sla'] == 'as per customer segment':
                        return _get_sla_time_as_per_segment(ticket_num)
            return self.default_sla_time
        df_of_same_ticket_num = df_of_same_ticket_num.sort_values(by=['sys_created_on'])
        time_diff = df_of_same_ticket_num['sys_created_on'] - df_of_same_ticket_num['u_ticket_created_time']
        time_diff_in_min = round(time_diff/np.timedelta64(1,'m'), 2)
        df_sla_time = df_of_same_ticket_num.apply(_get_sla_time, axis=1)
        df_of_same_ticket_num['in_sla_window'] = time_diff_in_min <= df_sla_time
        df_of_same_ticket_num['sla_time'] = time_diff_in_min
        df_of_same_ticket_num['sla_time'] = df_of_same_ticket_num['sla_time'].astype('str') + ' min'
        df_within_sla_window = df_of_same_ticket_num[df_of_same_ticket_num['in_sla_window'] == True]
        df_first_3_resp_within_sla_window = df_within_sla_window[:3] # Only first 3 responses will be considered into SLA windows
        df_rest_resp_within_sla_window = df_within_sla_window[3:] # Rest of the responses will be considered outside SLA windows
        df_within_sla_window = df_first_3_resp_within_sla_window
        df_within_sla_window['time_compliance'] = 1
        df_outside_sla_window = df_of_same_ticket_num[df_of_same_ticket_num['in_sla_window'] == False]
        df_outside_sla_window = pd.concat([df_rest_resp_within_sla_window, df_outside_sla_window], axis=0)
        df_outside_sla_window['time_compliance'] = 0
        return df_within_sla_window, df_outside_sla_window

    def check_scenarios(self, data, columns, ticket_type):
        if ticket_type == 'CSR' or ticket_type is None:
            return self.check_scenarios_for_csr(data, columns)
        elif ticket_type == 'CSP':
            return self.check_scenarios_for_csp(data, columns)
        else:
            raise Exception('Invalid data type. Requires. CSR or CSP')

    @staticmethod
    def check_scenarios_for_csr(data, columns):
        values = [int(data[x]) for x in columns]
        if data['i_link_stat_chk'] and data['i_poa'] and data['i_resp_time']:
            return 1, sum(values),str('Perfect')
        elif data['i_link_stat_chk'] and data['i_link stat_sim'] and data['i_poa'] and data['i_etr']:
            return 2, sum(values),str('Perfect')
        elif data['i_link_stat_chk'] and data['i_cust_dep'] and data['i_svc_rstr'] == 0:
            return 3, sum(values),str('Perfect')
        elif data['i_link_stat_chk'] and data['i_link stat_sim']==0 and data['i_poa'] and data['i_cust_dep'] and data['i_svc_rstr'] == 0:
            return 4, sum(values),str('Perfect')
        elif data['i_link_stat_chk'] and data['i_svc_rstr']:
            return 5, sum(values),str('Perfect')
        else:
            return 0, sum(values),str('Needs Improvement')

    @staticmethod
    def check_scenarios_for_csp(data, columns):
        values = [int(data[x]) for x in columns]
        # sum(values)
        if data['i_link_stat_chk'] and data['i_poa'] and data['i_resp_time']:
            return 6, sum(values),str('Perfect')
        elif data['i_link_stat_chk'] and data['i_poa'] and data['i_etr']:
            return 7, sum(values),str('Perfect')
        elif data['i_link_stat_chk']  and data['i_cust_dep'] and data['i_svc_rstr'] == 0 and data['i_etr'] == 0 and data['i_resp_time'] == 0:
            return 8, sum(values),str('Perfect')
        elif data['i_link_stat_chk'] and data['i_logs'] and data['i_svc_rstr'] and data['i_etr'] == 0 and data['i_resp_time'] == 0:
            return 9, sum(values),str('Perfect')
        elif data['i_link_stat_chk'] and data['i_svc_rstr']:
            return 10, sum(values),str('Perfect')
        else:
            return 0, sum(values),str('Needs Improvement')


if __name__=='__main__':
    ComplianceScore()
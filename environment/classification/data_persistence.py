import pandas as pd
import sys
from .log import get_logger
from datetime import datetime
from environment.dashboard.dbo.connection import MySQLConnection


class DataPersistence:
    def __init__(self, data_frame=None, timing_file_name=None, flag_save_in_db=None,
                 row_id=None, mode=None, time_for_file_path=None, timing_file=None,rfo=None):
        self.logging = get_logger('Data Persistence')
        self.rfo_dataframe = rfo
        self.data_frame = data_frame
        self.timing_file = timing_file
        self.connection = MySQLConnection()
        self.cursor = self.connection.cursor
        self.flag_save_in_db = flag_save_in_db
        self.timing_file_name = timing_file_name
        self.row_id = row_id
        self.unique_records = []
        self.already_processed_tickets = []
        self.mode = mode
        self.output_csv_path = 'environment/classification/output/ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.filename_to_download='ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.columnsTitles = ['username', 'mode', 'process_start_time', 'data_file_name', 'timing_file_name',
                              'file_upload', 'total_tickets', 'total_responses', 'total_null', 'Product',
                              'no_of_tickets_products_removed', 'no_of_responses_products_removed',
                              'Update_Type1', 'no_of_tickets_internal_removed', 'no_of_responses_internal_removed',
                              'Resolver group', 'no_of_tickets_resolvergrp_removed',
                              'no_of_responses_resolvergrp_removed', '1_Resp_no_resolver',
                              'no_of_tickets_1resolvergrpNA_removed', 'no_of_responses_1resolvergrpNA_removed',
                              'Impact', 'no_of_tickets_impact_removed', 'no_of_responses_impact_removed',
                              'Ticket_State', 'no_of_tickets_pwc_susp_removed', 'no_of_responses_pwc_susp_removed',
                              'Unique_dates_in_file', 'Ticket Duplication', 'no_of_tickets_duplicate_removed',
                              'no_of_responses_duplicate_removed', 'filtering','no_tickets_into_machine',
                              'no_emails_into_machine', 'prediction', 'no_emails_discarded', 'no_emails_initial',
                              'no_emails_subsequent', 'scoring', 'process_end_time','unique_dates']

    def already_exists(self, ticket):
        is_record_exist = self.connection.is_ticket_exist(ticket)
        if is_record_exist:
            self.already_processed_tickets.append(ticket)
        else:
            self.unique_records.append(ticket)

    def run(self):
        try:
            self.logging.debug('Data Persistance initiated')
            query = "UPDATE process_status SET persistance='Inprogress', current_state='persistance' WHERE id="+str(self.row_id)
            self.connection.update(query)

            if self.mode:
                print('Persistence in Test Mode')
                self.logging.debug('Persistance in test mode')
                data = self.data_frame
                data['no_problem_found'].fillna(0, inplace=True)
                data.to_excel(self.output_csv_path, index=False)
                df = pd.read_sql('SELECT * FROM process_status where id='+str(self.row_id), con=self.connection.con)
                df.drop(columns=['final_status', 'filename_to_download', 'current_state', 'persistance'], axis=1, inplace=True)
                df = df.reindex(columns=self.columnsTitles)
                df = df.T
                writer = pd.ExcelWriter(self.output_csv_path, engine='xlsxwriter')

                # Write each dataframe to a different worksheet.
                data.to_excel(writer, sheet_name='Data', index=False)
                df.to_excel(writer, sheet_name='Process Status')

                # Close the Pandas Excel writer and output the Excel file.
                writer.save()
                query = "UPDATE process_status SET persistance='Success', filename_to_download='" \
                        + self.filename_to_download+"', process_end_time='"+str(datetime.now()) \
                        + "' WHERE id=" + str(self.row_id)
                self.connection.update(query)

                return self.filename_to_download
            else:
                self.logging.debug('Data Persistance in run mode')
                print('writing to DB Run Mode')
                data = self.data_frame
                data = data[['u_number','u_ticket_created_time','sys_created_on','sys_created_by','u_current_assignment_group','repeated_response','u_customer_name',\
                             'u_product','u_impact','Customer Update','RFO Responsbile','RFO Cause','RFO Specification','Fault Segment','Resolver Group','E_number',\
                             'i_link_stat_chk','i_link stat_sim','i_logs','i_cust_dep','i_poa','i_svc_rstr','i_etr','i_resp_time','i_time_update',\
                             'i_etr_time','compliance_scenario_num','email_type','resp_rating_initial','si_prog','si_svc_rstr','si_no_prob',\
                             'si_etr','si_logs','si_resp_time','si_cd','si_etr_time','si_time_update','comscore','no_problem_found','resp_rating',\
                             'time_compliance','time_sec','time_format','sla_diff','shortest_path','ticket_score','ticket_absolute_score','u_service_identifier',\
                             'pwc_count','pwc_time']]
                timing_file = self.timing_file
                self.rfo_dataframe = self.rfo_dataframe[['u_number','RFO Specification','RFO Responsbile','RFO Cause','Fault Segment',\
                                      'What','Where','Why','Who fixed it','How fixed','RFO_comp','Reason1','R1_comp',\
                                      'Reason2','R2_comp','RFO_Scenario','rfo_time_compilance','rfo_time_diff','missing_parameters']]
                unumber = data.u_number.unique()
                temp_int=0
                if self.flag_save_in_db:
                    if len(unumber) > 0:
                        query_list_tickets = []
                        query_list_sla = []
                        query_list_email_content_discarded = []
                        query_list_email_content_initial = []
                        query_list_email_content_subsequent = []
                        query_list_initial = []
                        query_list_discarded = []
                        query_list_subsequent = []
                        last_inserted_ticket_ids = []
                        record_number = 0
                        for i in range(len(unumber)):
                            temp_int+=1
                            print(temp_int/len(unumber) * 100, end='\r')
                            df = data.groupby('u_number').get_group(unumber[i])
                            rfo_df=self.rfo_dataframe.groupby('u_number').get_group(unumber[i])
                            #df[['resp_rating','shortest_path','repeated_response']] = df[['resp_rating','shortest_path','repeated_response']].fillna(value='None')
                            df_sla = timing_file.groupby('Number').get_group(unumber[i])
                            if self.flag_save_in_db:
                                query = (df.iloc[0]['u_number'], str(df.iloc[0]['u_ticket_created_time']),
                                                 df.iloc[0]['sys_created_by'], df.iloc[0]['u_current_assignment_group'],
                                                 df.iloc[0]['u_product'], str(df_sla.iloc[-1]['State Of Ticket(New)']),
                                                 str(df.iloc[0]['u_impact']), str(df.iloc[0]['u_customer_name']),
                                                 df.iloc[0]['u_service_identifier'],str( df_sla.iloc[-1]['State Start Time']),
                                                 str(df.iloc[-1]['ticket_score']), df.iloc[-1]['ticket_absolute_score'],
                                                 df.iloc[-1]['no_problem_found'], df.iloc[-1]['pwc_count'],
                                                 df.iloc[0]['pwc_time'],
                                                 rfo_df.iloc[0]['RFO Specification'],str(rfo_df.iloc[0]['RFO Responsbile']),
                                                 str(rfo_df.iloc[0]['RFO Cause']),str(rfo_df.iloc[0]['Fault Segment']),
                                                 int(rfo_df.iloc[0]['What']), int(rfo_df.iloc[0]['Where']),
                                                 int(rfo_df.iloc[0]['Why']), int(rfo_df.iloc[0]['Who fixed it']),
                                                 int(rfo_df.iloc[0]['How fixed']), int(rfo_df.iloc[0]['RFO_comp']),
                                                 str(rfo_df.iloc[0]['Reason1']), str(rfo_df.iloc[0]['R1_comp']),
                                                 str(rfo_df.iloc[0]['Reason2']), str(rfo_df.iloc[0]['R2_comp']),
                                                 int(rfo_df.iloc[0]['RFO_Scenario']),
                                                 int(rfo_df.iloc[0]['rfo_time_compilance']),
                                                 int(rfo_df.iloc[0]['rfo_time_diff']),
                                                 str(rfo_df.iloc[0]['missing_parameters']))
                                query_list_tickets.append(query)

                                for row_index, row_of_grped in df_sla.iterrows():
                                    query = (row_of_grped['Number'], row_of_grped['State Of Ticket(New)'],row_of_grped['State Start Time'],row_of_grped['Current Assignment Group'])

                                    query_list_sla.append(query)


                                for row_index, row_of_grped in df.iterrows():

                                    record_number += 1
                                    if row_of_grped['email_type'].startswith('d'):
                                        query = (row_of_grped['u_number'], row_of_grped['sys_created_by'],
                                                row_of_grped['E_number'], row_of_grped['sys_created_on'],
                                                row_of_grped['i_link_stat_chk'], row_of_grped['i_logs'],
                                                row_of_grped['i_cust_dep'], row_of_grped['i_poa'],
                                                row_of_grped['i_svc_rstr'], row_of_grped['i_etr'],
                                                row_of_grped['i_etr_time'], row_of_grped['i_resp_time'],
                                                row_of_grped['i_time_update'], row_of_grped['time_compliance'],
                                                row_of_grped['compliance_scenario_num'], row_of_grped['email_type'],
                                                row_of_grped['resp_rating_initial'], row_of_grped['shortest_path'])
                                        query_list_discarded.append(query)

                                        query = (str(row_of_grped['u_number']), str(row_of_grped['Customer Update']),str(row_of_grped['email_type']), int(row_of_grped['E_number']))
                                        query_list_email_content_discarded.append(query)

                                    elif row_of_grped['email_type'] == 'i':
                                        query = (str(row_of_grped['u_number']), str(row_of_grped['sys_created_by']),
                                                        row_of_grped['sys_created_on'], int(row_of_grped['i_link_stat_chk']),
                                                        int(row_of_grped['i_logs']), int(row_of_grped['i_cust_dep']),
                                                        int(row_of_grped['i_poa']), row_of_grped['i_svc_rstr'],
                                                        int(row_of_grped['i_etr']), str(row_of_grped['i_etr_time']),
                                                        int(row_of_grped['i_resp_time']), str(row_of_grped['i_time_update']),
                                                        int(row_of_grped['time_compliance']),
                                                        int(row_of_grped['compliance_scenario_num']),int(row_of_grped['E_number']),
                                                        str(row_of_grped['email_type']),str(row_of_grped['resp_rating_initial']),
                                                        str(row_of_grped['shortest_path']),int(row_of_grped['sla_diff']))
                                        query_list_initial.append(query)
                                        query = (str(row_of_grped['u_number']), str(row_of_grped['Customer Update']),
                                                 str(row_of_grped['email_type']), int(row_of_grped['E_number']))
                                        query_list_email_content_initial.append(query)



                                    elif row_of_grped['email_type'].startswith('s'):
                                        query = (str(row_of_grped['u_number']), str(row_of_grped['sys_created_by']),
                                                        row_of_grped['sys_created_on'], int(row_of_grped['si_prog']),
                                                        int(row_of_grped['si_svc_rstr']), int(row_of_grped['si_no_prob']),
                                                        int(row_of_grped['si_etr']), str(row_of_grped['si_etr_time']),
                                                        int(row_of_grped['si_logs']), row_of_grped['si_resp_time'],
                                                        str(row_of_grped['si_time_update']), int(row_of_grped['si_cd']),
                                                        int(row_of_grped['time_compliance']), row_of_grped['comscore'],
                                                        int(row_of_grped['E_number']), str(row_of_grped['email_type']),
                                                        str(row_of_grped['resp_rating']),str(row_of_grped['repeated_response']),
                                                        str(row_of_grped['shortest_path']),int(row_of_grped['sla_diff']))
                                        query_list_subsequent.append(query)
                                        # last_inserted_ticket_id = self.connection.insert(query)
                                        # last_inserted_ticket_ids.append(last_inserted_ticket_id)

                                        query = (str(row_of_grped['u_number']), str(row_of_grped['Customer Update']),
                                                 str(row_of_grped['email_type']), int(row_of_grped['E_number']))
                                        query_list_email_content_subsequent.append(query)



                            else:
                                None
                        current_time = datetime.now()
                        tickets_query = "INSERT INTO tickets (ticket_number,ticket_booking_time,agent_name,owner_group,product,ticket_final_status,severity,customer_name,service_id,ticket_close_time,ticket_overall_score,ticket_absolute_score,no_problem_found,pwc_count,pwc_time,rfo_specification,RFO_resp,RFO_cause,RFO_FS,What,Where_found,Why,Who,How,RFO_comp,Reason1,R1_comp,Reason2,R2_comp,Resp_rating_scenario,RFO_timecomp,RFO_diff,missing_parameters) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                        self.cursor.executemany(tickets_query, query_list_tickets)


                        sla_query = "INSERT INTO ticket_history (ticket_number, ticket_state, state_start_time, owner_group) values (%s,%s,%s,%s)"
                        self.cursor.executemany(sla_query, query_list_sla)

                        discarded_query = "INSERT INTO discarded_responses (ticket_number, agent_name, email_number, d_time_stamp, d_link_stat_chk,d_logs,d_cust_dep, d_poa, d_svc_rstr, d_etr, d_etr_time, d_resp_time, d_time_update, d_time_compliance, resp_score, email_type, resp_rating, shortest_path) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                        self.cursor.executemany(discarded_query, query_list_discarded)

                        email_query = "INSERT INTO email_content (ticket_number, response_text, email_type, email_number) values (%s,%s,%s,%s)"
                        self.cursor.executemany(email_query, query_list_email_content_discarded)

                        initial_query = "INSERT INTO initial_responses (ticket_number, agent_name, i_time_stamp, i_link_stat_chk, i_logs, i_cust_dep, i_poa, i_svc_rstr, i_etr, i_etr_time, i_resp_time, i_time_update, i_time_compliance, resp_score, email_number, email_type, resp_rating, shortest_path, i_sla_time) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                        self.cursor.executemany(initial_query, query_list_initial)

                        email_query = "INSERT INTO email_content (ticket_number, response_text, email_type, email_number) values (%s,%s,%s,%s)"
                        self.cursor.executemany(email_query, query_list_email_content_initial)

                        subsequent_query = "INSERT INTO subsequent_responses (ticket_number, agent_name, si_timestamp, si_prog, si_svc_rstr, si_no_prob, si_etr, si_etr_time, si_logs, si_resp_time, si_time_update, si_cd, si_time_compliance, resp_score, email_number, email_type, resp_rating, repeated_response, shortest_path, si_sla_time) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                        self.cursor.executemany(subsequent_query, query_list_subsequent)

                        email_query = "INSERT INTO email_content (ticket_number, response_text, email_type, email_number) values (%s,%s,%s,%s)"
                        self.cursor.executemany(email_query, query_list_email_content_subsequent)

                        closing_time = datetime.now()
                        duration = closing_time - current_time
                        print('time in seconds:', duration.seconds)
                        
                        print('Data stored to database!!!')
                        self.logging.debug('Data stored in database !!!')
                    else:
                        print('Tickets Already in DataBase')
                        self.logging.debug('Tickets Already Exists')
                        print(len(self.already_processed_tickets))
                if len(self.already_processed_tickets) >= 0:
                    processed_records = pd.DataFrame(self.already_processed_tickets, columns=['Ticket Numbers'])
                    processed_records.to_excel(self.output_csv_path, index=False)

                    query = "UPDATE process_status set process_end_time='"+str(datetime.now())+"' WHERE id="+str(self.row_id)
                    self.connection.update(query)

                    df = pd.read_sql('SELECT * FROM process_status where id=' + str(self.row_id), con=self.connection.con)

                    df.drop(columns=['final_status', 'filename_to_download', 'current_state', 'persistance'], axis=1, inplace=True)
                    df = df.reindex(columns=self.columnsTitles)
                    df = df.T
                    writer = pd.ExcelWriter(self.output_csv_path, engine='xlsxwriter')

                    # Write each dataframe to a different worksheet.
                    processed_records.to_excel(writer, sheet_name='processed_tickets', index=False)
                    df.to_excel(writer, sheet_name='Process Status')

                    # Close the Pandas Excel writer and output the Excel file.
                    writer.save()
                    query = "UPDATE process_status SET persistance='Success', filename_to_download='"\
                            + self.filename_to_download+"' WHERE id="+str(self.row_id)
                    self.connection.update(query)
                    return self.filename_to_download
        except Exception as e:
            self.logging.debug(e)
            self.logging.debug('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print('Exception Occured', e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            query = "UPDATE process_status SET persistance='Error' WHERE id=" + str(self.row_id)
            self.connection.update(query)


if __name__ == '__main__':
    DataPersistence().run()
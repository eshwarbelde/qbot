import logging
from logging.handlers import RotatingFileHandler


def get_logger(name):
    log_format = '%(asctime)s:%(levelname)s:%(message)s'
    logging.basicConfig(
            handlers=[RotatingFileHandler('./my_log.log', maxBytes=10000000, backupCount=5)],
            level=logging.DEBUG,
            format=log_format,
            datefmt='%Y-%m-%dT%H:%M:%S',
            filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(log_format))
    logging.getLogger(name).addHandler(console)
    return logging.getLogger(name)

from django.http import JsonResponse
from datetime import datetime

from environment.cache.qbot_cache import CacheRefresher
from environment.dashboard.dbo.connection import MySQLConnection
import os
import threading
from .filter_data import DataFilter
from .initial_prediction import EmailClassification
from .subsequent_prediction import Predictions
from .process_timing import Timing
from .ticket_score import TicketScoring
from environment.classification.data_persistence import DataPersistence
from environment.validation.validate_file import ValidateFile
from .log import get_logger

logging = get_logger('upload')
logging.debug('script initiated')

connection = MySQLConnection()


def upload(request):
    print('File Upload Initiated')
    logging.debug('file upload initiated')
    if request.method == 'POST':
        data_file = request.FILES['dataFile']
        timing_file = request.FILES['timingFile']
        capture_mode = request.POST.get('captureMode', None)
        username = request.session['user_name']
        logging.debug(username)
        logging.debug(data_file.name)
        logging.debug(timing_file.name)
        print(data_file.name)
        print(timing_file.name)
        with open('environment/classification/input/'+data_file.name, 'wb+') as destination:
            for chunk in data_file.chunks():
                destination.write(chunk)
        with open('environment/classification/input/'+timing_file.name, 'wb+') as destination:
            for chunk in timing_file.chunks():
                destination.write(chunk)

        query = "INSERT INTO process_status(username, mode, process_start_time, data_file_name, timing_file_name," \
                + " file_upload, current_state) values('"+username+"', '"+capture_mode+"', '"+str(datetime.now()) \
                + "', '" + data_file.name + "', '" + timing_file.name + "','Inprogress', 'FileUpload')"
        row_id = connection.insert(query)
        print(row_id)
        logging.debug(row_id)
        data_file_flag, timing_file_flag = ValidateFile(data_file, timing_file).run()
        if data_file_flag and timing_file_flag:
            query = "UPDATE process_status SET file_upload='Success' WHERE id="+str(row_id)
            result = connection.update(query)
            print(result)
            t1 = threading.Thread(target=process, args=(capture_mode, data_file.name, timing_file.name,
                                                        username, row_id))
            t1.daemon = True
            t1.start()
            flags_status = {
                 'status': True,
                 'data_file_flag': data_file_flag,
                 'timing_file_flag': timing_file_flag,
                 'row_id': row_id
            }
            return JsonResponse(flags_status)


def process(capture_mode, data_file_name, timing_file_name, username, row_id):
    print("Filtering process started:", username)
    logging.debug("Filtering process started:{}".format(username))
    if capture_mode == 'Test':
        query = "UPDATE process_status SET mode='Test' WHERE id=" + str(row_id)
        result = connection.update(query)
        print(result)
        data, timing_dataframe = DataFilter(
            data_file_name, username, row_id,
            timing_file=timing_file_name
        ).filtering()
        print(data.shape)
        time_for_file_path = str(datetime.now().strftime("%d-%m-%Y %Hh%Mm%Ss"))
        subsequent_df = EmailClassification(
            file_path=data,
            timing_file=timing_file_name,
            time_for_file_path=time_for_file_path + '_' + username,
            flag_save_in_file=True,
            row_id=row_id
        ).run()
        data_frame_subsequent = Predictions(
            data_frame=subsequent_df,
            flag_save_in_file=True,
            time_for_file_path=time_for_file_path + '_' + username,
            row_id=row_id
        ).run()
        # timing_file_name
        timing_data = Timing(
            data_file=data_frame_subsequent,
            timing_file=timing_dataframe,
            time_for_file_path=time_for_file_path+'_'+username,
            row_id=row_id
        ).run()
        data,rfo_data_frame = TicketScoring(data_frame=timing_data, row_id=row_id).run()
        filename_to_download = DataPersistence(
            data_frame=data, timing_file_name=timing_file_name, flag_save_in_db=1,
            row_id=row_id, mode=1, time_for_file_path=time_for_file_path+'_'+username,
            timing_file=timing_dataframe
        ).run()

        query = "UPDATE process_status SET final_status='Success', filename_to_download='"+str(filename_to_download)\
                + "', end_process='Success' WHERE id=" + str(row_id)
        connection.update(query)
    else:
        query = "UPDATE process_status SET mode='Run' WHERE id=" + str(row_id)
        connection.update(query)
        data, timing_dataframe = DataFilter(
            file_name=data_file_name,
            username=username,
            row_id=row_id,
            timing_file=timing_file_name,
            mode=1
        ).filtering()
        if data.shape[0]>0:
            time_for_file_path = str(datetime.now().strftime("%d-%m-%Y %Hh%Mm%Ss"))
            subsequent_df = EmailClassification(
                file_path=data,
                timing_file=timing_file_name,
                time_for_file_path=time_for_file_path + '_' + username,
                flag_save_in_file=True,
                row_id=row_id  # To enable db storage = 1 OR To disable db storage = None
            ).run()
            data_frame_subsequent = Predictions(
                data_frame=subsequent_df,
                flag_save_in_file=True,
                time_for_file_path=time_for_file_path + '_' + username,
                row_id=row_id
            ).run()
            timing_data = Timing(data_file=data_frame_subsequent, timing_file=timing_dataframe,
                                 time_for_file_path=time_for_file_path + '_' + username, row_id=row_id, ).run()
            data,rfo_data_frame = TicketScoring(data_frame=timing_data, row_id=row_id).run()
            filename_to_download = DataPersistence(data_frame=data, timing_file_name=timing_file_name,
                                                   flag_save_in_db=1, row_id=row_id, mode=None,
                                                   time_for_file_path=time_for_file_path + '_' + username,
                                                   timing_file=timing_dataframe,rfo=rfo_data_frame).run()

            query = "UPDATE process_status SET final_status='Success', filename_to_download='" + filename_to_download \
                    + "', end_process='Success' WHERE id=" + str(row_id)
            connection.update(query)
            cache = CacheRefresher()
            cache.refresh_cache()
            logging.debug("Processing done and cache swapped successfully")
            print("Processing done and cache swapped successfully")
        else:
            query = "UPDATE process_status SET final_status='Success', end_process='Success' WHERE id=" + str(row_id)
            connection.update(query)
            print('No new tickets')

        if os.path.isfile('environment/classification/input/' + data_file_name):
            os.remove('environment/classification/input/' + data_file_name)
        if os.path.isfile('environment/classification/input/' + timing_file_name):
            os.remove('environment/classification/input/' + timing_file_name)

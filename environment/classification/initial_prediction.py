import pandas as pd
pd.set_option('mode.chained_assignment', None)
import copy
from environment.classification.modules.compliance_score import ComplianceScore
from environment.classification.filter_data import DataFilter
from environment.classification.modules.preprocessing import Preprocessing # Custom class for preprocessing
from environment.classification.modules.utils import load_pickle_file, save_excel, extract_time
from environment.dashboard.dbo.connection import MySQLConnection
from .log import get_logger
import sys

class EmailClassification:
    def __init__(self, file_path=None, timing_file = None, time_for_file_path=None, flag_save_in_file=False, row_id=None):
        """
        Initialize neccessary variables
        """
        self.logging = get_logger('initial predictions')
        self.file_path = file_path
        self.flag_save_in_file = flag_save_in_file
        self.connection = MySQLConnection()
        self.preserve_columns_order = []
        self.validate_columns = ['Customer Update', 'u_number', 'sys_created_on']
        self.predict_variables = ['i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                               'i_etr', 'i_resp_time']  # Target variables.
        self.predict_time_variables = ['i_time_update', 'i_etr_time']
        self.drop_columns_for_df = ['serial_no', 'customer_update_cleaned', 'compliance_sum_score']
        self.drop_columns_for_initial = ['serial_no', 'customer_update_cleaned', 'email_type', 'compliance_sum_score']
        self.drop_columns_for_subsequent = ['serial_no', 'customer_update_cleaned',  'i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa',
                                            'i_svc_rstr', 'i_etr', 'i_resp_time', 'i_time_update', 'i_etr_time', 'compliance_scenario_num', 'compliance_sum_score']
        self.drop_columns_for_discard = ['serial_no', 'customer_update_cleaned', 'email_type', 'compliance_sum_score']
        self.preprocessing_obj = Preprocessing()  # Initialize the object of the preprocessing class
        self.initial_df = pd.DataFrame()
        self.subsequent_df = pd.DataFrame()
        self.discard_df = pd.DataFrame()
        self.models = {}
        self.predict_variables_values = {}
        self.model_path = 'environment/classification/models/email_classifier_model_v7.pkl'
        self.dtm_model_path = 'environment/classification/models/document_term_matrix_model_v7.pkl'
        self.output_csv_path = 'environment/classification/output/ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.initial_conversation_csv_path = 'environment/classification/output/initial_conversation_ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.subsequent_conversation_csv_path = 'environment/classification/output/subsequent_conversation_ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.discard_conversation_csv_path = 'environment/classification/output/discard_conversation_ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.timing_file_name=timing_file
        self.case_state_transition_data=pd.read_excel('environment/classification/input/'+self.timing_file_name)
        #self.schema_validator = Validator()
        self.row_id = row_id
        print('Initialization has been done.')

    def predict_values(self, X_test, predict_variable):
        """
        Predict the target variable values of the test dataset on the trained model.
        """
        return self.models[predict_variable].predict(X_test)

    def are_columns_exist(self, df_columns):
        """
        Check all columns are exist or not
        :param df_columns: List of columns in the data
        :return:
        """
        for v_col in self.validate_columns:
            if v_col not in df_columns:
                raise KeyError('{} column is not exist in the data'.format(v_col))

    def mark_flags(self, df, grped_by_tickets, dict_dtm_email_word_matrix, compliance_score_obj):
        compliance_sum_scores = []
        compliance_scenario_nums = []
        for row_index, row_of_grped in grped_by_tickets.iterrows():
            initial_emails_record_data = {}

            # Check ticket type
            ticket_number = df.loc[row_index, 'u_number']
            ticket_type = 'CSR' if ticket_number.startswith('CSR') else 'CSP' \
                if ticket_number.startswith('CSP') else None
            if ticket_type == 'CSR':
                self.predict_variables = ['i_link_stat_chk', 'i_link stat_sim', 'i_logs', 'i_cust_dep', 'i_poa',
                                          'i_svc_rstr',
                                          'i_etr', 'i_resp_time']
            elif ticket_type == 'CSP':
                self.predict_variables = ['i_link_stat_chk', 'i_logs', 'i_cust_dep', 'i_poa', 'i_svc_rstr',
                                          'i_etr', 'i_resp_time']

            for predict_variable in self.predict_variables:
                df_email_word_matrix = dict_dtm_email_word_matrix[predict_variable]
                df_each_email_word_matrix = df_email_word_matrix.loc[[row_index]]

                y_pred = self.predict_values(df_each_email_word_matrix, predict_variable)
                if predict_variable not in self.predict_variables_values:
                    self.predict_variables_values[predict_variable] = []
                predicted_value = int(y_pred[0])
                self.predict_variables_values[predict_variable].append(predicted_value)
                initial_emails_record_data[predict_variable] = predicted_value
                df.loc[row_index, predict_variable] = predicted_value
                grped_by_tickets.loc[row_index, predict_variable] = predicted_value

                if predict_variable == 'i_etr':
                    if predicted_value == 1:
                        extracted_time = extract_time(df.loc[row_index, 'Customer Update'])
                        df.loc[row_index, 'i_etr_time'] = str(extracted_time)
                        grped_by_tickets.loc[row_index, 'i_etr_time'] = str(extracted_time)
                    else:
                        df.loc[row_index, 'i_etr_time'] = ''
                        grped_by_tickets.loc[row_index, 'i_etr_time'] = ''

                if predict_variable == 'i_resp_time':
                    if predicted_value == 1:
                        extracted_time = extract_time(df.loc[row_index, 'Customer Update'])
                        df.loc[row_index, 'i_time_update'] = str(extracted_time)
                        grped_by_tickets.loc[row_index, 'i_time_update'] = str(extracted_time)
                    else:
                        df.loc[row_index, 'i_time_update'] = ''
                        grped_by_tickets.loc[row_index, 'i_time_update'] = ''

            compliance_scenario_num, compliance_sum_score,resp_rating_initial = compliance_score_obj.check_scenarios(
                initial_emails_record_data, self.predict_variables[:], ticket_type)
            df.loc[row_index, 'compliance_scenario_num'] = compliance_scenario_num
            df.loc[row_index, 'compliance_sum_score'] = compliance_sum_score
            df.loc[row_index, 'resp_rating_initial'] = resp_rating_initial
            grped_by_tickets.loc[row_index, 'compliance_scenario_num'] = compliance_scenario_num
            grped_by_tickets.loc[row_index, 'compliance_sum_score'] = compliance_sum_score
            grped_by_tickets.loc[row_index, 'resp_rating_initial'] = resp_rating_initial
            compliance_sum_scores.append(compliance_sum_score)
            compliance_scenario_nums.append(compliance_scenario_num)
        return compliance_sum_scores, compliance_scenario_nums

    def replace_created_time_with_case_state_transition_time_for_csp(self, ticket_num, grp_ticket_num):
        grp_cst_ticket_num = self.case_state_transition_data[
            (self.case_state_transition_data['Number'] == ticket_num) &
            (self.case_state_transition_data['State Of Ticket(New)'].str.lower() == 'work in progress')
        ]
        if not grp_cst_ticket_num.empty:
            state_start_time = list(grp_cst_ticket_num['State Start Time'])[0]
            return state_start_time
        return None

    def run(self):
        """
        List of operations over the test data and predict values on the loaded trained model.
        :return:
        """
        try:
            self.logging.debug("initial predictions initiated")
            query = "UPDATE process_status SET prediction='Inprogress', current_state='Prediction' WHERE id="+str(self.row_id)
            self.connection.update(query)
            add_new_columns = self.predict_variables + self.predict_time_variables + ['compliance_scenario_num', 'compliance_sum_score', 'email_type',
                                                        'customer_update_cleaned','resp_rating_initial']
            for col in add_new_columns:
                if col in self.file_path.columns:
                    self.file_path.drop(col, axis=1, inplace=True)
            df = self.preprocessing_obj.filter_drop_rows_by_emails(self.file_path)
            # Drop predict columns if exist in the dataframe
            self.preserve_columns_order = list(df.columns) + add_new_columns
            df.fillna('', inplace=True)
            df = pd.concat([df, pd.DataFrame(columns=add_new_columns)], axis=1)
            self.are_columns_exist(df.columns)
            print('Validation on the columns has been done.')
            self.logging.debug('Validation on the columns has been done')
            # Text preprocessing, stop word removal and stemming over the words of the text.
            df['customer_update_cleaned'] = df['Customer Update'].apply(
                lambda x: self.preprocessing_obj.clean_text(str(x)))
            df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
                lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))
            print('Text cleansing on the email contents has been done.')
            emails = list(df['customer_update_cleaned'])
            # Generate document term vector space.
            count_vectorizers = load_pickle_file(self.dtm_model_path)
            dict_dtm_email_word_matrix = {}
            for predict_variable in self.predict_variables:
                count_vectorizer = count_vectorizers[predict_variable]
                dtm = self.preprocessing_obj.transform_document_term_vector(count_vectorizer, emails)
                df_email_word_matrix = pd.DataFrame(
                    dtm.toarray(), columns=count_vectorizer.get_feature_names(), index=df.index
                )  # Use vocabulary list as columns and row values are frequency of each word in the document.
                df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.
                dict_dtm_email_word_matrix[predict_variable] = df_email_word_matrix
            self.models = load_pickle_file(self.model_path)
            print('Predicting the values for the emails: ')
            self.logging.debug("predicting the values for emails")
            grouped_by_tickets = df.groupby(by=['u_number'])
            compliance_score_obj = ComplianceScore()
            i, len_grp_tickets = 0, len(grouped_by_tickets)
            for ticket_num, grp_ticket_num in grouped_by_tickets:
                if ticket_num.startswith('CSP'):
                    state_start_time = self.replace_created_time_with_case_state_transition_time_for_csp(
                        ticket_num, grp_ticket_num)
                    if state_start_time is not None:
                        indexes = grp_ticket_num.index
                        grp_ticket_num.loc[indexes, 'u_ticket_created_time'] = state_start_time
                        df.loc[indexes, 'u_ticket_created_time'] = state_start_time
                last_index_of_grp_ticket_num = grp_ticket_num.index[-1]
                i += 1
                print(i/len_grp_tickets * 100, end='\r')
                # flag_sla_all_in_out, grped_by_tickets = compliance_score_obj.get_records_based_on_sla_window(grp_ticket_num)
                df_within_sla_window, df_outside_sla_window = compliance_score_obj.get_records_based_on_sla_window(grp_ticket_num)
                grped_by_tickets = copy.deepcopy(df_within_sla_window)
                compliance_sum_scores, compliance_scenario_nums = self.mark_flags(df, grped_by_tickets, dict_dtm_email_word_matrix, compliance_score_obj) # mark flags

                subsequent_inc = 0
                discard_inc = 0
                if compliance_sum_scores:
                    has_match_compliance_scenario = any(compliance_scenario_nums)
                    max_compliance_sum_score = max(compliance_sum_scores)
                    flag_discard_email = True
                    flag_initial_email = True
                    for row_index, row_of_grped in grped_by_tickets.iterrows():
                        condition_for_discard = (
                            flag_discard_email and has_match_compliance_scenario != bool(row_of_grped['compliance_scenario_num'])
                        ) if has_match_compliance_scenario else (
                            flag_discard_email and int(row_of_grped['compliance_sum_score']) < max_compliance_sum_score
                        )
                        condition_for_intial = (
                            flag_initial_email and has_match_compliance_scenario == bool(row_of_grped['compliance_scenario_num'])
                        ) if has_match_compliance_scenario else (
                            flag_initial_email and row_of_grped['compliance_sum_score'] == max_compliance_sum_score
                        )
                        if condition_for_discard:
                            discard_inc += 1
                            email_type = 'd' + str(discard_inc)  # d<index> for discard
                            df.loc[row_index, 'email_type'] = email_type
                            self.discard_df = self.discard_df.append(df.loc[row_index], ignore_index=True)

                        elif condition_for_intial:
                            flag_discard_email = False
                            flag_initial_email = False
                            email_type = 'i' # i for initial
                            df.loc[row_index, 'email_type'] = email_type
                            self.initial_df = self.initial_df.append(df.loc[row_index], ignore_index=True)

                        else:
                            subsequent_inc += 1
                            email_type = 's' + str(subsequent_inc)  # s<index> for subsequent
                            df.loc[row_index, 'email_type'] = email_type
                            self.subsequent_df = self.subsequent_df.append(df.loc[row_index], ignore_index=True)


                grped_by_tickets = df_outside_sla_window
                if df_within_sla_window.empty:
                    mark_initial = True
                    outside_sla_initial_grped_by_tickets = copy.deepcopy(grped_by_tickets.head(1))
                    self.mark_flags(df, outside_sla_initial_grped_by_tickets, dict_dtm_email_word_matrix, compliance_score_obj)  # mark flags
                    grped_by_tickets.loc[outside_sla_initial_grped_by_tickets.index,
                                        list(outside_sla_initial_grped_by_tickets.columns)] = outside_sla_initial_grped_by_tickets
                else:
                    mark_initial = False
                for row_index, row_of_grped in grped_by_tickets.iterrows():

                    if mark_initial:
                        mark_initial = False
                        email_type = 'i' # i for initial
                        df.loc[row_index, 'email_type'] = email_type
                        self.initial_df = self.initial_df.append(df.loc[row_index], ignore_index=True)

                    else:
                        subsequent_inc += 1
                        email_type = 's' + str(subsequent_inc)  # s<index> for subsequent
                        df.loc[row_index, 'email_type'] = email_type
                        self.subsequent_df = self.subsequent_df.append(df.loc[row_index], ignore_index=True)

            # For whole df(contents)
            df = df[self.preserve_columns_order]
            df.sort_values(by='serial_no', inplace=True)
            df.drop(columns=self.drop_columns_for_df, axis=1, inplace=True)
            # save_excel(self.output_csv_path, df) if self.flag_save_in_file else None

            # For initial conversation df
            self.initial_df = self.initial_df[self.preserve_columns_order]
            self.initial_df.sort_values(by='serial_no', inplace=True)
            self.initial_df.drop(columns=self.drop_columns_for_initial, axis=1, inplace=True)
            # save_excel(self.initial_conversation_csv_path, self.initial_df) if self.flag_save_in_file else None

            # For subsequent conversation df
            self.subsequent_df = self.subsequent_df[self.preserve_columns_order]
            self.subsequent_df.sort_values(by='serial_no', inplace=True)
            self.subsequent_df.drop(columns=self.drop_columns_for_subsequent, axis=1, inplace=True)
            # save_excel(self.subsequent_conversation_csv_path, self.subsequent_df) if self.flag_save_in_file else None

            # For discard conversation df
            if list(self.discard_df.columns):
                self.discard_df = self.discard_df[self.preserve_columns_order]
                self.discard_df.sort_values(by='serial_no', inplace=True)
                self.discard_df.drop(columns=self.drop_columns_for_discard, axis=1, inplace=True)
            save_excel(self.discard_conversation_csv_path, self.discard_df) if self.flag_save_in_file else None

            #if customer dependency is mentioned there sholud be a plan of action ,to increase the model accuracy we are
            #turning the 'i_poa'.
            df.loc[df['i_cust_dep'] == 1, 'i_poa'] = int(1)
            # for index,rows in df.iterrows():
            #     if rows['i_cust_dep'] == 1:
            #         df.loc[index,'i_poa'] = 1

            print('Finish prediciting...')
            self.logging.debug('initial predictions successful')
            no_of_discarded=self.discard_df.shape[0]
            no_of_subsequent=self.subsequent_df.shape[0]
            no_of_initial=self.initial_df.shape[0]

            query = "UPDATE process_status SET no_emails_discarded="+ str(no_of_discarded) \
                    + ", no_emails_initial=" + str(no_of_initial) \
                    + ", no_emails_subsequent="+str(no_of_subsequent)+" WHERE id="+str(self.row_id)
            print(query)
            self.connection.update(query)
            return df
        except Exception as e:
            print(e)
            self.logging.debug('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            self.logging.debug(e)
            query = "UPDATE process_status SET prediction='Error' WHERE id="+str(self.row_id)
            self.connection.update(query)

if __name__=='__main__':
    data_file = 'environment/classification/data/ManOnNet_2019-09-09(input file).xlsx'
    data_file_name = '/'.join(data_file.split('.')[:-1])
    data = DataFilter(data_file).filtering()
    time_for_file_path = str(data.sys_created_on.iloc[-1]).split(' ')[0]
    EmailClassification(
        file_path=data,
        time_for_file_path = time_for_file_path,
        flag_save_in_file=1,
        flag_save_in_db=1
    ).run()

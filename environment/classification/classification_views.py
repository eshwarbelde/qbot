from django.template import loader
from django.http import HttpResponse
from django.shortcuts import redirect


class ClassificationView:
    @staticmethod
    def file_upload(request):
        if 'user_name' in request.session and request.session['user_name'] != '' and \
                request.session['user_role'] == 'ADMIN':
            template = loader.get_template('file_upload.html')
            return HttpResponse(template.render())
        else:
            template = loader.get_template('login.html')  # getting our template
            response = redirect('/login/')
            return response

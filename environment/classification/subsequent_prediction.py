import sys
import pandas as pd
pd.set_option('mode.chained_assignment', None)
from environment.classification.modules.preprocessing import Preprocessing
from environment.classification.modules.utils import load_pickle_file, extract_time
from environment.dashboard.dbo.connection import MySQLConnection
from environment.classification.ticket_score import TicketScoring
from .log import get_logger

class Predictions:
    def __init__(self, data_frame=None, flag_save_in_file=None, time_for_file_path=None, row_id=None):
        self.logging = get_logger('subsequent predictions')
        self.data_frame = data_frame
        self.preprocessing_obj = Preprocessing()
        self.ticket_score_obj = TicketScoring()
        self.connection = MySQLConnection()
        #self.schema_validator = Validator()
        self.flag_save_in_file = flag_save_in_file
        self.row_id = row_id
        self.models = {}
        self.dtm_models = {}
        self.model_path = 'environment/classification/models/email_classifier_model_v7.pkl'
        self.dtm_model_path = 'environment/classification/models/document_term_matrix_model_v7.pkl'
        self.model_path_sub = 'environment/classification/models/sub_email_classifier_model_v7.pkl'
        self.dtm_model_path_sub = 'environment/classification/models/sub_document_term_matrix_model_v7.pkl'

        self.subsequent_prediction_csv_path = 'environment/classification/output/subsequent_prediction_ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.output_csv_path = 'environment/classification/output/ManOnNet_{0}.xlsx'.format(time_for_file_path)
        self.result = pd.DataFrame(columns=['si_etr_time', 'si_time_update'])
        self.columns = ['si_prog', 'si_svc_rstr', 'si_no_prob', 'si_etr', 'si_logs', 'si_resp_time', 'si_cd',
                        'si_etr_time', 'si_time_update', 'comscore', 'no_problem_found','resp_rating']
        self.predict_variables = ['si_prog', 'si_svc_rstr', 'si_no_prob', 'si_etr', 'si_logs', 'si_resp_time', 'si_cd']
        self.initial_models = ['si_svc_rstr', 'si_etr', 'si_logs', 'si_resp_time', 'si_cd']

    def predict_values(self,X_test,predict_variable):
        # Predict the target variable values of the test dataset on trained model.
        return self.models[predict_variable].predict(X_test)

    def start_predicting(self, df, predict_variable):
        df['customer_update_cleaned'] = df['Customer Update'].apply(
            lambda x: self.preprocessing_obj.clean_text(str(x)))
        df['customer_update_cleaned'] = df['customer_update_cleaned'].apply(
            lambda x: self.preprocessing_obj.remove_stop_words_and_apply_stemmer(x))

        emails = list(df['customer_update_cleaned'])
        dict_dtm_email_word_matrix = {}
        count_vectorizer = self.dtm_models[predict_variable]
        dtm = self.preprocessing_obj.transform_document_term_vector(count_vectorizer, emails)
        df_email_word_matrix = pd.DataFrame(
            dtm.toarray(), columns=count_vectorizer.get_feature_names(), index=df.index
        )  # Use vocabulary list as columns and row values are frequency of each word in the document.
        df_email_word_matrix[df_email_word_matrix >= 1] = 1  # Convert word frequency value to 1 whose frequency value is greater or equals to 1.
        dict_dtm_email_word_matrix[predict_variable] = df_email_word_matrix
        X = df_email_word_matrix
        y_pred = self.predict_values(X, predict_variable)
        return y_pred

    def run(self):
        try:
            self.logging.debug('subsequent predictions initiated')
            print('subsequent predictions')
            df = self.data_frame
            self.logging.debug(df.columns)
            df['Customer Update'].fillna('empty', inplace=True)
            self.result = df.copy()
            self.result = self.result[self.result['email_type'].str.startswith('s')]
            self.logging.debug('loading pickle files')
            self.models = load_pickle_file(self.model_path_sub)
            models_temp1 = load_pickle_file(self.model_path)
            models_temp2 = self.models.update(models_temp1)
            self.dtm_models = load_pickle_file(self.dtm_model_path_sub)
            dtm_models_temp1 = load_pickle_file(self.dtm_model_path)
            dtm_models_temp2 = self.dtm_models.update(dtm_models_temp1)

            self.models['si_logs'] = self.models.pop('i_logs')
            self.dtm_models['si_logs'] = self.dtm_models.pop('i_logs')
            self.models['si_cd'] = self.models.pop('i_cust_dep')
            self.dtm_models['si_cd'] = self.dtm_models.pop('i_cust_dep')
            self.models['si_svc_rstr'] = self.models.pop('i_svc_rstr')
            self.dtm_models['si_svc_rstr'] = self.dtm_models.pop('i_svc_rstr')
            self.models['si_resp_time'] = self.models.pop('i_resp_time')
            self.dtm_models['si_resp_time'] = self.dtm_models.pop('i_resp_time')
            self.models['si_etr'] = self.models.pop('i_etr')
            self.dtm_models['si_etr'] = self.dtm_models.pop('i_etr')
            print('models initiated')
            self.logging.debug('pickle files loaded sucessfully')
            for predict_variable in self.predict_variables:
                self.result[predict_variable] = self.start_predicting(self.result, predict_variable)

            self.logging.debug(self.result.head())
            len_tkts = (self.result.shape[0])
            i = 0
            # email scoring adding it to comscore column for subsequent emails
            for idx, rows in self.result.iterrows():
                i += 1
                print(i/len_tkts * 100, end='\r')
                if rows['email_type'].startswith('s'):
                    self.result.loc[idx,'comscore'],self.result.loc[idx, 'resp_rating'] = self.ticket_score_obj.scoring_concatinated(rows)

            i = 0
            for idx,rows in self.result.iterrows():
                i += 1
                print(i/len_tkts * 100, end='\r')  
                if rows['si_etr'] == 1 or rows['si_resp_time'] == 1:
                    time = extract_time(self.preprocessing_obj.clean_text_timing(str(rows['Customer Update'])))
                    if time is None:
                        self.result.loc[idx, 'si_etr_time'] = 0
                        self.result.loc[idx, 'si_time_update'] = 0
                    else:
                        if self.result.loc[idx, 'si_etr'] == 1:
                            self.result.loc[idx, 'si_etr_time'] = time
                            self.result.loc[idx, 'si_time_update'] = 0
                        elif self.result.loc[idx, 'si_resp_time'] == 1:
                            self.result.loc[idx, 'si_etr_time'] = 0
                            self.result.loc[idx, 'si_time_update'] = time
                        else:
                            self.result.loc[idx, 'si_etr_time'] = 0
                            self.result.loc[idx, 'si_time_update'] = 0
                else:
                    self.result.loc[idx, 'si_etr_time'] = 0
                    self.result.loc[idx, 'si_time_update'] = 0
            print('predictions done!!!')
            self.logging.debug('subsequent predictions done successfully')
            # checking 'no problem found' for a ticket
            unumber = list(self.result.u_number.unique())
            for i in range(len(unumber)):
                df_temp = self.result.groupby('u_number').get_group(unumber[i])
                flag_temp = False
                for index_temp, rows_temp in df_temp.iterrows():
                    if flag_temp:
                        break
                    elif rows_temp['si_no_prob'] == 1:
                        flag_temp = True
                        for index_1, rows_1 in df_temp.iterrows():
                            self.result.loc[index_1, 'no_problem_found'] = 1
            if self.flag_save_in_file:
                result1 = self.result[self.columns]
                self.result = pd.concat([df, result1], axis=1, sort=False)
                self.result.to_excel(self.output_csv_path, index=False)
            return self.result
        
        except Exception as e:
            print(e)
            self.logging.debug(e)
            self.logging.debug('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
            query = "UPDATE process_status set prediction='Error' WHERE id="+str(self.row_id)
            self.connection.update(query)
